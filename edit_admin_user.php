<html>
<head><link rel="stylesheet" href="include/admin.css"></head>
<body>
<?php
include "include/header.php";
ini_set('display_errors',1);
$display_message = '';

if(isset($_SESSION["username"])){
	$username = $_SESSION["username"];
	if($_SESSION["permissions_user_mgmt"] != "Y"){
		//The logged in user doesn't have permission to add users
		$display_message = "You do not have permission to view this page.  Please contact a system administrator if you believe this is incorrect.";
	}else if(isset($_GET["username"])){
		//The user id is set so get the user
		$user_results = mm_get_admin_user($_GET["username"]);
		$stored_username = $user_results["username"];
		$stored_display_name = $user_results["display_name"];
		$stored_password = $user_results["password"];
		$stored_user_id = $user_results["user_id"];
		$stored_salt_string = $user_results["salt_string"];
		$stored_active_ind = $user_results["active_ind"];
		$stored_permissions_user_mgmt = $user_results["permissions_user_mgmt"];
		$stored_permissions_ach_mgmt = $user_results["permissions_ach_mgmt"];
		$stored_permissions_manual_decisioning = $user_results["permissions_manual_decisioning"];
		$stored_locked_flag = $user_results["locked_flag"];
	}else if($_SERVER["REQUEST_METHOD"] == "POST"){
		//There has been a request to update the user info
		$stored_username = $_POST["stored_username"];
		$stored_display_name = $_POST["stored_display_name"];
		$stored_user_id = $_POST["stored_user_id"];
		$stored_active_ind = $_POST["stored_active_ind"];
		$stored_permissions_user_mgmt = $_POST["stored_permissions_user_mgmt"];
		$stored_permissions_ach_mgmt = $_POST["stored_permissions_ach_mgmt"];
		$stored_permissions_manual_decisioning = $_POST["stored_permissions_manual_decisioning"];
		$stored_locked_flag = $_POST["stored_locked_flag"];
		$new_username = $_POST["new_username"];
		$new_display_name = $_POST["new_display_name"];
		$new_password = $_POST["new_password"];
		$new_active_ind = isset($_POST["new_active_ind"]) ? "1" : "0";
		$new_permissions_user_mgmt = isset($_POST["new_permissions_user_mgmt"]) ? "Y" : "N";
		$new_permissions_ach_mgmt = isset($_POST["new_permissions_ach_mgmt"]) ? "Y": "N";
		$new_permissions_manual_decisioning = isset($_POST["new_permissions_manual_decisioning"]) ? "Y" : "N";
		$new_locked_flag = isset($_POST["new_locked_flag"]) ? "Y" : "N";
		

		//Check to see what fields changed
		if($stored_active_ind != $new_active_ind){
			mm_update_database_value("mm_admin_user", 'active_ind',$new_active_ind , 'i', 'user_id', $stored_user_id, $username);	
			$display_message .= "Active Indicator Updated\n";
		}
		
		if($stored_locked_flag != $new_locked_flag){
		    mm_update_database_value('mm_admin_user', 'locked_flag', $new_locked_flag, 's', 'user_id', $stored_user_id, $username);
		    if($new_locked_flag == 'N'){
		        //update the number of unsuccessful attempts also
		        mm_update_database_value('mm_admin_user', 'num_unsuccessful_logins', 0,'i', 'user_id', $stored_user_id, $username);
		    }
		    $display_message .= "The Locked Status has been Updated";
		}
	
		if($stored_permissions_user_mgmt != $new_permissions_user_mgmt){
			mm_update_database_value("mm_admin_user", 'permissions_user_mgmt',$new_permissions_user_mgmt , 's', 'user_id', $stored_user_id, $username);	
			$display_message .= "User Management Permissions Updated\n";
		}

		if($stored_permissions_ach_mgmt != $new_permissions_ach_mgmt){
			mm_update_database_value("mm_admin_user", 'permissions_ach_mgmt',$new_permissions_ach_mgmt , 's', 'user_id', $stored_user_id, $username);	
			$display_message .= "ACH Management Permissions Updated\n";
		}

		if($new_password != ''){
			$password_validation = mm_validate_admin_password($new_password, $new_password); //Just passing the password twice to the since it's not collected twice
			if($password_validation["return_value"] == 0){
				//The password is fine so create an encrypted version and store it in the DB
				$user_details = mm_get_admin_user($stored_username);
				$salt_string = $user_details["salt_string"];
				$new_password_encrypted = mm_encrypt_password($new_password, $salt_string);	
				mm_update_database_value("mm_admin_user", 'password',$new_password_encrypted , 's', 'user_id', $stored_user_id, $username);	
			$display_message .= "Password Updated\n";
			}else{
				//The new password isn't acceptable
				$password_validation_message = $password_validation["return_message"];
				$display_message .= "Password Wasn't Able To Be Updated. $password_validation_message $\n";
			}
		}

		if($new_permissions_manual_decisioning != $stored_permissions_manual_decisioning){
			mm_update_database_value("mm_admin_user", 'permissions_manual_decisioning',$new_permissions_manual_decisioning , 's', 'user_id', $stored_user_id, $username);	
			$display_message .= "Manual Decisioning Permissions Updated\n";
		}
		if($stored_display_name != $new_display_name){
			mm_update_database_value("mm_admin_user", 'display_name',$new_display_name , 's', 'user_id', $stored_user_id, $username);	
			$display_message .= "Display Name Updated\n";
		}
		if($stored_username != $new_username){
			$username_results = mm_validate_admin_username($new_username);
			$username_return_value = $username_results["return_value"];
			$username_return_message = $username_results["return_message"];
			if($username_return_value == 0){
				mm_update_database_value("mm_admin_user", 'username',$new_username , 's', 'user_id', $stored_user_id, $username);	
			$display_message .= "Username  Updated\n";
			}else{
				//The new username isn't valid
				$display_message .= "Unable to update the username.  $username_return_message\n";
			}
		}

	}else{
		//The user has permission but hasn't specified a user and doesn't 
		$display_message = "You must specify a user to edit.  Click <a href=\"manage_admin_users.php\">here</a> to select a user.";
	}
?>

<h2>Manage Admin Users</H2>
<?php if($display_message != ''){echo "<p style=\"color:red\">$display_message</p>";}
	if($_SESSION["permissions_user_mgmt"] == "Y"){ 
		//User has permission to view this page so get the list of users and display a table
		if(isset($_GET["username"])){
			//No user ID is set so set the display message
			echo "<form method = \"POST\" action=\"edit_admin_user.php\">";	
			//output the stored variables for comparison
			echo "<input type=\"hidden\" name=\"stored_username\" value=\"$stored_username\"/>";
			echo "<input type=\"hidden\" name=\"stored_locked_flag\" value = \"$stored_locked_flag\">";
			echo "<input type=\"hidden\" name=\"stored_user_id\" value=\"$stored_user_id\"/>";
			echo "<input type=\"hidden\" name=\"stored_display_name\" value=\"$stored_display_name\"/>";
			echo "<input type=\"hidden\" name=\"stored_active_ind\" value=\"$stored_active_ind\"/>";
			echo "<input type=\"hidden\" name=\"stored_permissions_user_mgmt\" value=\"$stored_permissions_user_mgmt\"/>";
			echo "<input type=\"hidden\" name=\"stored_permissions_ach_mgmt\" value=\"$stored_permissions_ach_mgmt\"/>";
			echo "<input type=\"hidden\" name=\"stored_permissions_manual_decisioning\" value=\"$stored_permissions_manual_decisioning\"/>";
			echo "<label>Username: </label><input type=\"text\" name=\"new_username\" value=\"$stored_username\"/><br>";
			echo "<label>Display Name: </label><input type=\"text\" name=\"new_display_name\" value=\"$stored_display_name\"/><br>";
			echo "<label>New Password (Not Required): </label><input type=\"text\" name=\"new_password\" /><br>";
			$active_display_value = $stored_active_ind == '1' ? "checked" : "";
			echo "<label>Active/Inactive?: </label><input type=\"checkbox\" name=\"new_active_ind\" value=\"Y\" $active_display_value style=\"zoom:1.5\"/><br>";
			$locked_flag_display_value = $stored_locked_flag == 'Y' ? "checked" : "";
			echo "<label>Lock Account: </label><input type=\"checkbox\" name=\"new_locked_flag\" value=\"Y\" $locked_flag_display_value style=\"zoom:1.5\"/><br>";
			$active_display_value = $stored_permissions_user_mgmt == 'Y' ? "checked" : "";
			echo "<label>Allow User Management?: </label><input type=\"checkbox\" name=\"new_permissions_user_mgmt\" value=\"Y\" $active_display_value style=\"zoom:1.5\"/><br>";
			$active_display_value = $stored_permissions_ach_mgmt == 'Y' ? "checked" : "";
			echo "<label>Allow ACH Management?: </label><input type=\"checkbox\" name=\"new_permissions_ach_mgmt\" value=\"Y\" $active_display_value style=\"zoom:1.5\"/><br>";
			$active_display_value = $stored_permissions_manual_decisioning == 'Y' ? "checked" : "";
			echo "<label>Allow Manual Application Decisioning?: </label><input type=\"checkbox\" name=\"new_permissions_manual_decisioning\" value=\"Y\" $active_display_value style=\"zoom:1.5\"/><br>";

			echo "<input type=\"submit\" value=\"Edit User\"/>";
		
		}
	}
?>


<?php
}else{
include "include/login.php";
}
?>


</body>
</html>
