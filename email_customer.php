<html>
<head>
    <link rel="stylesheet" href="include/admin.css">
</head>
<body>
<?php

include "include/header.php";
//require_once "include/mm_middleware.php";
require_once "include/mm_system_config.php";
$display_message = '';

if (isset($_SESSION["username"])) {
    $username = $_SESSION["username"];
    $email_to = isset($_GET["email_address"]) ? $_GET["email_address"] : '';
    $application_nbr = isset($_GET["application_nbr"]) ? $_GET["application_nbr"] : 0;
    
    
    $conn = mm_get_db_connection();
    $sql_string = "Select * from mm_application where application_nbr = ?";
    if (!$stmt = $conn->prepare($sql_string)) {
        mm_log_error('mm_get_application_details', "$conn->error", $conn->errno);
    }
    $stmt->bind_param('i', $application_nbr);
     if (!$stmt->execute()) {
          
        } else {
            $rows = $stmt->get_result();
            $num_rows = $rows->num_rows;
            if ($num_rows > 0) {
             $app_data = $rows->fetch_assoc();
                
            }
        }

        if (is_resource($conn)) {
            $conn->close();
        }
        
	//$app_data = $application_details["app_data"];
	//print_r($app_data);
	$app_state =$app_data["state"];
    if(	$app_state == 'IL'){
	    $supportemail = VERIFICATION_IL_SUPPORT_EMAIL;
	}else{
	    $supportemail = VERIFICATION_SUPPORT_EMAIL;
	}
	$customer_name = $app_data["first_name"]." ".$app_data["last_name"];		

    if (isset($_POST["email_to"])) {
        //Comment here to validate the form data
        $application_nbr = isset($_POST["application_nbr"]) ? $_POST["application_nbr"] : 0;
        //The customer submitted the form so send the email
        $email_data["to"] = $_POST["email_to"];
        $email_data["from"] = $_POST["email_from"];
        $email_data["subject"] = $_POST["email_subject"];
        $email_data["from_name"] = SITE_NAME . " Support Team";
        $email_data["subject"] = $_POST["email_subject"];

        $email_data["txt_body"] = $_POST["email_body"];
        $email_data["html_body"] = "<html><head></head><body>" . $_POST["email_body"] . "</html>";
        $email_data["html_body"] = str_replace("\n", "<br>", $email_data["html_body"]);
        $email_data["html_body"] = str_replace("&#10", "<br>", $email_data["html_body"]);
        $tmp_dt = new DateTime();
        $scheduled_dt = $tmp_dt->format("Y-m-d H:i:s");
        $email_data["scheduled_dt"] = $scheduled_dt;
        $email_message = json_encode($email_data);
        $email_results = mm_schedule_email_message($email_message);
        if ($email_results["return_value"] != 0 || $email_results["email_message_nbr"] == 0) {
            //The email scheduling failed so log an error
            $display_message = "There was a problem sending the email.  Please try again.";
        } else {
            $display_message = "The message was sent.";
        }
    }

    ?>

    <script>
        function fill_email_template() {
            var template = document.getElementById("email_template_id").value;
            var subject = document.getElementById("email_subject_id");
            var textbody = document.getElementById("email_body_id");

            textbody.innerHTML = "";

            if (template === "Verifications Document Request") {
                subject.value = "Additional Information Is Needed To Complete Your Application";
                textbody.innerHTML = "Dear <?php echo $customer_name;?>, \nThank you for your recent loan application with <?php echo SITE_NAME?>.  We are in need of some verification documents in order to fund your loan.&#10&#10Please send a copy of your ID and your two most recent Bank Statements to <?php echo $supportemail;?>. &#10&#10For any questions you may respond directly to this email or call our customer support team at <?php echo SUPPORT_PHONE?>.";
            } else if (template === "Verifications Complete") {
                subject.value = "Application Verifications Complete";
                textbody.innerHTML = "Congratulations, your loan has been approved.  Please log into your account at <?php echo SITE_URI?> to sign your loan documents.&#10&#10For any questions you may respond directly to this email or call our customer support team at <?php echo SUPPORT_PHONE?>.";
            } else if (template === "Payoff Email") {
                subject.value = "Thank You For Your Business";
                textbody.innerHTML = "Congratulations, your loan with <?php echo SITE_NAME?> has been paid in full.  If you ever need a loan again in the future, we hope you will consider us first!";
            } else if (template === "Soft Collections") {
                subject.value = "Action Needed On Your Account";
                textbody.innerHTML = "Your recent payment to us was returned by your bank.  Please contact us at your earliest convenience to re-schedule your payment.  You can respond directly to this email or call our support team at <?php echo SUPPORT_PHONE?> between 8:00 am and 5:00 pm CST.&#10&#10Thank you for your prompt response to this important matter";
            } else if (template === "Escalated Collections") {
                textbody.innerHTML = "Your account is now past due ten (10) days and a late fee may be assessed.  To prevent additional interest from accruing, please contact us at your earliest convenience to schedule a payment on your account.  You can respond directly to this email or call our support team at <?php echo SUPPORT_PHONE?> between 8:00 am and 5:00 pm CST.";
                subject.value = "Urgent - Action Needed On Your Account";
            } else if (template === "Recovery Collections") {

            } else if (template === "Do Nothing") {
                subject.value = "Congratulations, You're Approved!";
                textbody.innerHTML = "Congratulations, your loan has been approved.  Please log into your account at www.<?php echo SITE_URI?> to finish your application.";
            }

            if (template === "Verifications Document Request") {
                textbody.innerHTML = textbody.innerHTML + '&#10&#10<?php echo SITE_NAME?> Support Team&#10Phone: <?php echo SUPPORT_PHONE?>&#10Email: <?php echo  $supportemail;?>&#10&#10Privileged and Confidential.  This e-mail, and any attachments thereto, is intended only for use by the addressee(s) named herein and may contain privileged and/or confidential information.  If you have received this e-mail in error, please notify me immediately by return e-mail and delete this e-mail.  You are hereby notified that any dissemination, distribution or copying of this e-mail and/or any attachments thereto, is strictly prohibited.';  
            }else{
                textbody.innerHTML = textbody.innerHTML + '&#10&#10<?php echo SITE_NAME?> Support Team&#10Phone: <?php echo SUPPORT_PHONE?>&#10Email: <?php echo SUPPORT_EMAIL?>&#10&#10Privileged and Confidential.  This e-mail, and any attachments thereto, is intended only for use by the addressee(s) named herein and may contain privileged and/or confidential information.  If you have received this e-mail in error, please notify me immediately by return e-mail and delete this e-mail.  You are hereby notified that any dissemination, distribution or copying of this e-mail and/or any attachments thereto, is strictly prohibited.';
            }
        }

    </script>

    <h2>Email Customer</H2>
  

<?php
if ($display_message != '') {
    echo "<font color=\"red\"><b>$display_message</b></font><br><br>";
}
if ($email_to != ''){

?>
    <form method="Post" action="">

        <input type=hidden name=application_nbr value="<?php echo "$application_nbr";?>"></input>
        <label>To: <?php echo "$email_to"; ?></label><input type="hidden" name="email_to" value="<?php echo "$email_to"; ?>"></input><br>
        <label>From:</label><select name="email_from">
            <option><?php echo SUPPORT_EMAIL?></option>
        </select><br>
        <label>Template:</label><select name="email_template" id="email_template_id" onchange="fill_email_template()">
            <option></option>
            <option>Verifications Document Request</option>
            <option>Verifications Complete</option>
            <option>Payoff Email</option>
            <option>Soft Collections</option>
            <option>Escalated Collections</option>
            <option>Recovery Collections</option>
            <option>Do Nothing</option>
        </Select>
        <br>
        <label>Subject:</label><input type="text" name="email_subject" id="email_subject_id" size="50"></input><br>
        <label>Email Body:</label><br>
        <textarea name="email_body" id="email_body_id" rows="15" cols="200">
	&#10&#10<?php echo SITE_NAME?> Support Team&#10Phone: <?php echo SUPPORT_PHONE?>&#10Email: <?php echo SUPPORT_EMAIL?>&#10&#10Privileged and Confidential.  This e-mail, and any attachments thereto, is intended only for use by the addressee(s) named herein and may contain privileged and/or confidential information.  If you have received this e-mail in error, please notify me immediately by return e-mail and delete this e-mail.  You are hereby notified that any dissemination, distribution or copying of this e-mail and/or any attachments thereto, is strictly prohibited.
</textarea><br>

        <input type="Submit" value="Send Email"><br><br>
        <a href="application_details.php?application_nbr=<?php echo "$application_nbr";?>">Return To Application Summary</a>
    </form>
    <?php
} else {
    echo "You must first select a customer to email.  Click <a href=\"application_details.php\">here</a> to find an account<br>";
}
    ?>

    <?php
} else {
    include "include/login.php";
}
?>


</body>
</html>
