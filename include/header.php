<?php
if (!session_id())
{
    session_start();
}

require_once("admin_functions.php");
/*
function login_user($username, $password){
    if($username == "Jason" && $password == "FFC2017!"){
        $_SESSION["username"] = "Jason";

    }else if($username == "Rex" && $password == "FFC2017!"){
        $_SESSION["username"] = "Rex";

    }else if($username == "Debbie" && $password == "FFC2017!"){
        $_SESSION["username"] = "Debbie";
    }else if($username == "jesparza" && $password == "FFC2017!"){
        $_SESSION["username"] = "jesparza";
    }
}
*/

if (isset($_POST["username"]))
{
    $login_results        = mm_admin_login_user($_POST["username"], $_POST["password"]);
    $login_return_value   = $login_results["return_value"];
    $login_return_message = $login_results["return_message"];
    
    if ($login_return_value == 0)
    {
        $active_ind         = $login_results["active_ind"];
        $password_reset_ind = $login_results["password_reset_ind"];
        $locked_flag        = $login_results["locked_flag"];
        
        if ($active_ind == 0)
        {
            //The user exists but isn't active so display a message
            $display_message = "This account is no longer active.  Please contact your system administrator.";
        }
        elseif ($locked_flag == 'Y')
        {
              $display_message = "This account has been locked.  Please contact your system administrator.";
        }
        elseif ($password_reset_ind == 1)
        {
            //The password reset indicator is set so customer needs to reset their password
        }
        else
        {
            //The user login was successful
            $_SESSION["username"]                       = $login_results["username"];
            $_SESSION["display_name"]                   = $login_results["display_name"];
            $_SESSION["permissions_user_mgmt"]          = $login_results["permissions_user_mgmt"];
            $_SESSION["permissions_ach_mgmt"]           = $login_results["permissions_ach_mgmt"];
            $_SESSION["permissions_manual_decisioning"] = $login_results["permissions_manual_decisioning"];
        }
    }
    else
    {
        //    The user login is incorrect so display a message
        $display_message = $login_results["return_message"];
    }
}
elseif (isset($_GET["action"]))
{
    if ($_GET["action"] = "logout")
    {
            session_unset();
    }
}
?>
<!-- Header -->
<a href="index.php"><img src="./aspen-logo.png" alt="Aspen Financial Direct" style="width: 500px;height: 50px; border: 0;" /></a>
<!-- Header -->
<!--<H1><a href="index.php">HOME</a></H1>-->
