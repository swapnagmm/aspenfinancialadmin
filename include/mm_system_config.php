<?php

function baseUrl(){
    return sprintf("%s://%s",
        isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
        $_SERVER['SERVER_NAME']
    );
}

function mm_get_db_credentials(){
    $return_array = array();
    $return_array["db_server_name"] = "localhost";
    $return_array["db_user_name"] = "mmdtcusr_devadmin";
    $return_array["db_password"] = "Direct2Con!18";
    $return_array["db_name"] = "mmdtcusr_dev";

    return $return_array;
}

function mm_get_lp_credentials(){
    $return_array = array();
    $return_array["token"] = "Bearer 9d44ea12d24e145abb78dac37b7f5d82d539c5dd";
    $return_array["tenant_id"] = "5200816";

    return $return_array;
}

function mm_get_lp_environment_portfolio(){
    //For test return the value 4 and for production return the value 5
    return "4";
}

function mm_get_pci_credentials(){
    $return_array = array();
    $return_array["username"] = "support@moneymartdirect.com";
    $return_array["password"] = "Direct2Con!18";

    return $return_array;
}

function mm_get_ftp_details(){
    $return_array = array();
    $return_array["server"] = "moneymartdirect.com";
    $return_array["password"] = "gk3EQ3yJWaN7";
    $return_array["username"]  = "mm_web_documents_dev@moneymartdirect.com";
    $return_array["port"] = "21";

    return $return_array;
}

function mm_get_pdo_connection(){
    $credentials = mm_get_db_credentials();
    $db_server_name = $credentials["db_server_name"];
    $db_user_name = $credentials["db_user_name"];
    $db_password = $credentials["db_password"];
    $db_name = $credentials["db_name"];        $conn = new PDO("mysql:host=$db_server_name;dbname=$db_name", $db_user_name, $db_password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    return $conn;
}

function mm_get_db_connection(){
    $credentials = mm_get_db_credentials();
    $db_server_name = $credentials["db_server_name"];
    $db_user_name = $credentials["db_user_name"];
    $db_password = $credentials["db_password"];
    $db_name = $credentials["db_name"];
    $conn = new mysqli($db_server_name, $db_user_name, $db_password, $db_name);

    return $conn;
}

function get_lms_connection_details(){
}

function mm_get_log_path(){
    $log_path = "/home/mmdtcusr/dev.moneymartdirect.com/mm_logs/";
	return $log_path;
}

function mm_get_document_path(){
    $log_path = "/home/mmdtcusr/dev.moneymartdirect.com/mm_logs/";
	return $log_path;
}

function get_origination_system_connection_details(){
	$return_array = array();
	$header_array = array();
	//$return_array["url"] = "http://kleverlend-dev-app.azurewebsites.net/api/";
	$return_array["url"] = "http://moneymart-dev-app.azurewebsites.net/api/";
	$return_array["UserName"] = "vivacetest1";
	$return_array["Password"] = "Password123";
	$return_array["ApplicationSource"] = 'TestSource';
	$return_array["AuthToken"] = '123ABCDEF';
    $return_array["PortfolioID"] = '1';
	$header_array = array("Content-type: application/json", "key: Authorization", "Authorization: Basic dGVzdDp0ZXN0", "Description: \"\"");
	$return_array["header_array"] = $header_array;

	return $return_array;
}

function mm_get_ach_server_details(){
    $return_array = array();
    $return_array["server"] = "transfer.greenbank.com";
    $return_array["username"] = "ASPEN_LTD";
    $return_array["password"] = "ZcF77fGr";
    $return_array["port"] = "992";

    return $return_array;
}

function mm_get_ach_path(){
    return "/home/mmdtcusr/dev_documents/ach_documents/outbound_files/";
}

function mm_get_document_storage_path(){
    return "/home/mmdtcusr/dev_documents/customer_documents/";
}

function mm_create_cache() {
    $cache = new Memcached();
    $cache->addServer('127.0.0.1', 11211);
    return $cache;
}

function mm_set_cache_value($key, $value, $expiration=60) {
    error_log("mark: setting cache key: $key");
    mm_create_cache()->set($key, $value, $expiration);
}

function mm_get_cache_value($key) {
    error_log("mark: getting cache key: $key");
    return mm_create_cache()->get($key);
}


function error_log_vardump(string $text, $obj) {
    ob_start();
    var_dump($obj);
    error_log($text.ob_get_clean());
}

function error_log_dump_associative_array($pretext, $inarray) {
    error_log($pretext);
    if (is_array($inarray)) {
        foreach ($inarray as $key => $value) {
            if (is_array($value)){
                foreach ($value as $key2 => $value2) {
                    error_log("'$key2' => '$value2'");
                }
            }
            else {
                error_log("'$key' => '$value'");
            }
        }
    }
    else
        error_log_vardump("Not an array passed into error_log_dump_associative_array.", $inarray);
}

function array_group_by(array $array, $key)
	{
		if (!is_string($key) && !is_int($key) && !is_float($key) && !is_callable($key) ) {
			trigger_error('array_group_by(): The key should be a string, an integer, or a callback', E_USER_ERROR);
			return null;
		}
		$func = (!is_string($key) && is_callable($key) ? $key : null);
		$_key = $key;
		// Load the new array, splitting by the target key
		$grouped = [];
		foreach ($array as $value) {
			$key = null;
			if (is_callable($func)) {
				$key = call_user_func($func, $value);
			} elseif (is_object($value) && isset($value->{$_key})) {
				$key = $value->{$_key};
			} elseif (isset($value[$_key])) {
				$key = $value[$_key];
			}
			if ($key === null) {
				continue;
			}
			$grouped[$key][] = $value;
		}
		// Recursively build a nested grouping if more parameters are supplied
		// Each grouped array value is grouped according to the next sequential key
		if (func_num_args() > 2) {
			$args = func_get_args();
			foreach ($grouped as $key => $value) {
				$params = array_merge([ $value ], array_slice($args, 2, func_num_args()));
				$grouped[$key] = call_user_func_array('array_group_by', $params);
			}
		}
		return $grouped;
	}

// these should match database values in mm_lender table
define('SITE_NAME', 'Aspen Financial Direct');
define('SITE_URI', 'dev.moneymartdirect.com');
define('SITE_URI_PREFIX', 'dev.');
define('SITE_HOME_PATH', 'dev.moneymartdirect.com');
define('SUPPORT_PHONE', '(877) 293-2987');
define('SUPPORT_FAX', '(888) 252-2298');
define('SUPPORT_EMAIL', 'support@aspenfinancialdirect.com');
define('SUPPORT_ADDRESS', 'PO Box 882533');
define('SUPPORT_CITY', 'Dallas');
define('SUPPORT_STATE', 'TX');
define('SUPPORT_ZIP', '75380');
define('PHYSICAL_ADDRESS', '74 E. Swedesford Road, Suite 150');
define('PHYSICAL_CITY', 'Malvern');
define('PHYSICAL_STATE', 'PA');
define('PHYSICAL_ZIP', '19355');
define('APPROVED_APRS', array(116.13, 132.73, 213.27, 266.34, 96.73 ));
define('FADIR', 'fa_dev_ftp');
define('VERIFICATION_SUPPORT_EMAIL', 'verify@aspenfinancialdirect.com');
define('VERIFICATION_IL_SUPPORT_EMAIL', 'VerifyIL@aspenfinancialdirect.com');
