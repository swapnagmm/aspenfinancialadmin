<?php
   
    //require_once "mm_system_config.php";
    require_once "mm_middleware.php";
	 error_reporting(E_ALL);
	ini_set('display_errors', 1);
	ini_set("include_path", '/home/mmdtcusr/php:' . ini_get("include_path") );
	ini_set("include_path", '/home/mmdtcusr/php/phpseclib1:' . ini_get("include_path") );

use Simnang\LoanPro\LoanProSDK as LPSDK,
\Simnang\LoanPro\Constants\LOAN as LOAN,
\Simnang\LoanPro\Constants\CUSTOMERS as CUSTOMERS,
\Simnang\LoanPro\Constants\LOAN_SETUP as LOAN_SETUP,
\Simnang\LoanPro\Constants\LOAN_SETTINGS as LOAN_SETTINGS,
\Simnang\LoanPro\Constants\PORTFOLIO as PORTFOLIO,
\Simnang\LoanPro\Constants\PAYMENT_ACCOUNT as PAYMENT_ACCOUNT,
\Simnang\LoanPro\Constants\CHECKING_ACCOUNT as CHECKING_ACCOUNT,
\Simnang\Loanpro\Constants\LOAN_SETUP\LOAN_SETUP_LCLASS__C as LOAN_SETUP_LCLASS,
\Simnang\Loanpro\Constants\LOAN_SETUP\LOAN_SETUP_LTYPE__C as LOAN_SETUP_LTYPE,
\Simnang\Loanpro\Constants\ADDRESS as ADDRESS,
\Simnang\Loanpro\Constants\STATUS_ARCHIVE as STATUS_ARCHIVE,
\Simnang\Loanpro\Constants\PHONES as PHONES,
\Simnang\Loanpro\Constants\PHONES\PHONES_TYPE__C as PHONES_TYPE_C,
\Simnang\Loanpro\Constants\ADDRESS\ADDRESS_STATE__C as STATE,
\Simnang\Loanpro\Constants\BASE_ENTITY as BASE_ENTITY,
\Simnang\LoanPro\Constants\LOAN_TRANSACTIONS as LOAN_TRANSACTIONS;

				
//************************************************************************************************
//Function mm_format_nacha_detail() - This function formats a file header record into mm_ach_file_nacha
//************************************************************************************************/
Function mm_format_nacha_detail($file_nbr, $file_name, $effective_dt, $authorization_type){
	
	$return_array = array();
	$bError = "0";
    	$err_ststus = "0";
	$err_status_msg = "0";
	$return_message = "0";
	$return_value = 0;
	$return_err_status = 0;
	$return_err_message = 0;
				
	/* sql statements used during the nacha formatting functions use the
	  the character ^ instead of space because php functions used later will 
	  strip spaces. the ^ will be replaced with spaces on sql insert */
	
    $sql_string = " SELECT		d.detail_nbr as detail_nbr,
    							lpad(substr(rtrim(d.detail_nbr),1,15),15,'0') as detail_nbr_fmt,
								rpad(substr(rtrim(d.trans_id),1,15),15,'^') as trans_id,
								d.trans_id as trans_id_key,
								rpad(substr(rtrim(d.customer_name),1,22),22,'^') as customer_name,			
								lpad(round(d.amount*100),10,'0') as amount,
								rpad(substr(rtrim(d.routing_nbr),1,9),9,'^') as routing_nbr,
								rpad(substr(rtrim(d.bank_acct_nbr),1,17),17,'^') as bank_acct_nbr,
								CASE concat(d.trans_type,d.bank_acct_type)
										WHEN 'CCHK' THEN '22'
										WHEN 'DCHK' THEN '27'
										WHEN 'CSAV' THEN '32'
										WHEN 'DSAV' THEN '37'
										ELSE ''
								END as detail_rec_code
						FROM mm_ach_detail d 
						WHERE d.file_nbr = '$file_nbr'
								AND d.effective_dt = '$effective_dt'
								AND d.authorization_type = '$authorization_type'
						ORDER BY d.detail_nbr";
	try{
	
	    $conn = mm_get_pdo_connection();	
		$results = $conn->query($sql_string);
        $rows = $results->fetchAll();
		$num_rows = count($rows);
	
		$file_total_detail_recs = 0;

		if($num_rows > 0){
			foreach($rows as $row){
				$detail_nbr  = $row["detail_nbr"];
				$detail_nbr_fmt  = $row["detail_nbr_fmt"];
				$trans_id  = $row["trans_id"];
				$trans_id_key  = $row["trans_id_key"];
				$customer_name = $row["customer_name"];
				$amount = $row["amount"];
				$routing_nbr = $row["routing_nbr"];
				$bank_acct_nbr = $row["bank_acct_nbr"];
				$detail_rec_code = $row["detail_rec_code"];
					
				/* check if any values used when inserting nacha record are null or empty string */
				if($detail_nbr == null OR $detail_nbr == "" OR
					$file_nbr == null OR $file_nbr == "" OR
					$file_name == null OR $file_name == "" OR
					$detail_nbr_fmt == null OR $detail_nbr_fmt == "" OR
					$authorization_type == null OR $authorization_type == "" OR
					$trans_id == null OR $trans_id == "" OR
					$trans_id_key == null OR $trans_id_key == "" OR
					$customer_name == null OR $customer_name == "" OR
					$routing_nbr == null OR $routing_nbr == "" OR
					$bank_acct_nbr == null OR $bank_acct_nbr == "" OR
					$detail_rec_code == null OR $detail_rec_code == "" OR
					$amount == null OR $amount == "" ) { 
						$bError = "1";
					    $err_status = "ERROR";
					    $err_status_msg = "FORMAT: Null values found in detail record";
					   }
									
				/* see if this transaction record has already been inserted */
				
				$sql_string = "SELECT * FROM mm_ach_file_nacha  
							   WHERE trans_id = '$trans_id_key'";
				
				$detailresults = $conn->query($sql_string);
				$detailrecs = $detailresults->fetchAll();
				$num_detail_recs = count($detailrecs);
		        
			    if($num_detail_recs > 0) { 
						$bError = "1";
					    $err_status = "ERROR";
					    $err_status_msg = "FORMAT: DETAIL: Transaction ID already processed:-" . $trans_id_key;
			     }
				
				/* insert nacha rec if no errors*/
									
				if($bError == "0") { 
					$load_status = "PROCESSING";
					$load_status_msg = "Detail";
					$file_total_detail_recs = $file_total_detail_recs + 1;
																	
					$nacha_text = substr(('6' . $detail_rec_code . $routing_nbr . $bank_acct_nbr . 
											$amount . $trans_id . $customer_name .  "  0" .
											$detail_nbr_fmt),0,94);
					
					$sql_string = "INSERT INTO mm_ach_file_nacha
							(nacha_file_name, nacha_text, load_status, load_status_msg, load_ref_nbr, ach_file_nbr, trans_id)
							VALUES	('$file_name', rpad(replace('".addslashes($nacha_text)."','^',' '),94, ' '), 
									 '$load_status', '$load_status_msg', '$detail_nbr', '$file_nbr', '$trans_id_key')";
									 
				   $results = $conn->prepare($sql_string);
				   $results->execute();
					}
		     }
		  }else {
		  	  $bError = "1";
			  $err_status = "ERROR";
			  $err_status_msg = "FORMAT: DETAIL: No detail recs found";
			}
	}catch(PDOException $e){

		$return_array["return_value"] = 1;
		$return_array["return_message"] = "$e";
		$return_array["return_err_message"] = "sql error";
		$return_array["return_err_status"] = "ERROR";
		$return_array["file_total_detail_recs"] = 0;
		return $return_array;
	}

	$conn = null;
	   				
	if($bError == "0") {
		$return_array["return_value"] = 0;	
		$return_array["return_message"] = $return_message;
		$return_array["return_err_message"] = 0;	
		$return_array["return_err_status"] = 0;	
		$return_array["file_total_detail_recs"] = $file_total_detail_recs;
	}
	else {
		$return_array["return_value"] = 1;	
		$return_array["return_message"] = $return_message;
		$return_array["return_err_message"] = $err_status_msg;
		$return_array["return_err_status"] = $err_status;
		$return_array["file_total_detail_recs"] = 0;
	}
	
	return $return_array;	
}


//************************************************************************************************
//Function mm_format_nacha_batch() - This function formats a file header record into mm_ach_file_nacha
//************************************************************************************************/
Function mm_format_nacha_batch($file_nbr){
	
	$return_array = array();
	$bError = "0";
    $err_status = "0";
	$err_status_msg = "0";
	$return_message = "0";
	$return_value = 0;
	$return_err_status = 0;
	$return_err_message = 0;
				
				
	/* sql statements used during the nacha formatting functions use the                         
	  the character ^ instead of space because php functions used later will 
	  strip spaces. the ^ will be replaced with spaces on sql insert */
	
    $sql_string = " SELECT f.file_name as file_name,
    					lpad(substr(rtrim(e.bank_routing_nbr),1,8),8,'^') as bank_routing_nbr,
						rpad(substr(rtrim(e.company_id),1,10),10,'^') as company_id,
						rpad(substr(rtrim(e.company_name),1,16),16,'^') as company_name,
						rpad(substr(rtrim(authorization_type),1,3),3,'^') as authorization_type,
						rpad('',20,'^') as discretionary_data,
						d.effective_dt as effective_dt,
						rpad(date_format(d.effective_dt, '%y%m%d'),6,'0') as effective_dt_fmt,
						rpad('ePay',10,'^') as company_entry_description,
						sum(if(d.trans_type='D',1,0)) as nbr_debits,
						sum(if(d.trans_type='C',1,0)) as nbr_credits,
						count(*) as nbr_of_recs,
						lpad(count(*),6,'0') as nbr_of_recs_fmt,
						substr(lpad(sum(substr(routing_nbr,1,8)),10,'0'),1,10) as entry_hash_fmt,
						lpad(sum(if(d.trans_type='D',round(amount*100),0)),12,'0') as total_debits_fmt,
						lpad(sum(if(d.trans_type='C',round(amount*100),0)),12,'0') as total_credits_fmt,
						sum(substr(routing_nbr,1,8)) as entry_hash,
						sum(if(d.trans_type='D',round(amount*100),0)) as total_debits,
						sum(if(d.trans_type='C',round(amount*100),0)) as total_credits,
						rpad('',19,'^') as msg_authentication, 
						rpad('',6,'^') as reserved
				FROM mm_ach_file f
				JOIN mm_ach_detail d on f.file_nbr = d.file_nbr 
				LEFT JOIN mm_ach_endpoint e on f.endpoint_nbr = e.endpoint_nbr
				WHERE f.file_nbr = $file_nbr
				GROUP BY f.file_name, authorization_type, e.endpoint_nbr, bank_name, bank_routing_nbr,
				         company_id, company_name, effective_dt
				ORDER BY authorization_type,effective_dt";

	try{
	
	    $conn = mm_get_pdo_connection();	
		$results = $conn->query($sql_string);
        $rows = $results->fetchAll();
		$num_rows = count($rows);
	
		$file_total_batch_count = 0;
		$file_total_entry_hash = 0;
		$file_total_debits = 0;
		$file_total_credits = 0;

		if($num_rows > 0){
			foreach($rows as $row){
				$file_name  = $row["file_name"];
				$bank_routing_nbr  = $row["bank_routing_nbr"];
				$company_id  = $row["company_id"];
				$company_name = $row["company_name"];
				$authorization_type = $row["authorization_type"];
				$discretionary_data = $row["discretionary_data"];
				$effective_dt = $row["effective_dt"];
				$effective_dt_fmt = $row["effective_dt_fmt"];
				$company_entry_description = $row["company_entry_description"];
				$nbr_debits = $row["nbr_debits"];
				$nbr_credits = $row["nbr_credits"];
				$nbr_of_recs = $row["nbr_of_recs"];
				$nbr_of_recs_fmt = $row["nbr_of_recs_fmt"];
				$entry_hash_fmt = $row["entry_hash_fmt"];
				$total_debits_fmt = $row["total_debits_fmt"];
				$total_credits_fmt = $row["total_credits_fmt"];
				$entry_hash = $row["entry_hash"];
				$total_debits = $row["total_debits"];
				$total_credits = $row["total_credits"];
				$msg_authentication = $row["msg_authentication"];
				$reserved = $row["reserved"];

				if($nbr_debits > 0 and $nbr_credits > 0) {
							$service_code = '200';
				}else {
					if($nbr_debits == 0 and $nbr_credits > 0) {
							$service_code = '220';
					}else {
						if($nbr_debits > 0 and $nbr_credits == 0) {
							$service_code = '225';
						}else {$bError = "1";
							$err_status = "ERROR";
							$err_status_msg = "FORMAT: BATCH: Nbr of Debits and Credits = 0";
						}
					}
				}
					
				/* check if any values used when inserting nacha record are null or empty string */
				if($file_name == null OR $file_name == "" OR
					$file_nbr == null OR $file_nbr == "" OR
					$bank_routing_nbr == null OR $bank_routing_nbr == "" OR
					$company_id == null OR $company_id == "" OR
					$effective_dt == null OR $effective_dt == "" OR
					$company_name == null OR $company_name == "") { 
						$bError = "1";
					    $err_status = "ERROR";
					    $err_status_msg = "FORMAT: Null values found in batch header record";
					   }
									                                                       
				/* insert nacha rec if no errors*/
									
				if($bError == "0") { 
					$load_status = "PROCESSING";
					$load_status_msg = "Batch Header";
					$file_total_batch_count = $file_total_batch_count + 1;
					$file_total_entry_hash = $file_total_entry_hash + $entry_hash;
					$file_total_debits = $file_total_debits + $total_debits;
					$file_total_credits = $file_total_credits + $total_credits;
					$batch_count_len = strlen($file_total_batch_count);
					$batch_count_fill_len = 7-$batch_count_len;
					if($batch_count_fill_len > 0){
						$batch_count_fill = str_repeat('0',$batch_count_fill_len);
						$batch_count_fmt =  $batch_count_fill . $file_total_batch_count;
					}else {
						$batch_count_fmt =  $file_total_batch_count;
					}
					
					$nacha_text = substr(('5' . $service_code . $company_name . $discretionary_data . 
  										$company_id  . $authorization_type  . $company_entry_description . 
  										$effective_dt_fmt . $effective_dt_fmt  . "   1"  . $bank_routing_nbr . 
										$batch_count_fmt),0,94);
					
					$sql_string = "INSERT INTO mm_ach_file_nacha
							(nacha_file_name, nacha_text, load_status, load_status_msg, load_ref_nbr, ach_file_nbr)
							VALUES	('$file_name', rpad(replace('".addslashes($nacha_text)."','^',' '),94, ' '), 
									 '$load_status', '$load_status_msg', '$file_total_batch_count', '$file_nbr')";
									 
				   $results = $conn->prepare($sql_string);
				   $results->execute();
				 				   
				   // ******  call function to build detail **************
				   $detail_recs = mm_format_nacha_detail($file_nbr, $file_name, $effective_dt, $authorization_type);
				   // ***************************************************
				   
				   $return_value = $detail_recs["return_value"];
				   $return_message = $detail_recs["return_message"];
				   $return_err_status = $detail_recs["return_err_status"];
				   $return_err_message = $detail_recs["return_err_message"];
				   $file_total_detail_recs = $detail_recs["file_total_detail_recs"];
				   
				   if($return_value == 0){

		            	$nacha_text = substr(("8" . $service_code . $nbr_of_recs_fmt . $entry_hash_fmt . 
											$total_debits_fmt . $total_credits_fmt . 
											$company_id . $msg_authentication . $reserved . 
											$bank_routing_nbr . $batch_count_fmt),0,94);

						/* check if any values used when inserting nacha record are null or empty string */
						if($file_name == null OR $file_name == "" OR
							$file_nbr == null OR $file_nbr == "" OR
							$nacha_text == null OR $nacha_text == "" ) { 
								$bError = "1";
								$err_status = "ERROR";
								$err_status_msg = "FORMAT: Null values found in batch control record";
							}
					
							if($bError == "0") {
								$load_status = "PROCESSING";
								$load_status_msg = "Batch Control";
				   	   
								$sql_string = "INSERT INTO mm_ach_file_nacha
											(nacha_file_name, nacha_text, load_status, load_status_msg, load_ref_nbr, ach_file_nbr)
											VALUES	
											('$file_name', rpad(replace('".addslashes($nacha_text)."','^',' '),94, ' '), '$load_status', '$load_status_msg','$file_total_batch_count','$file_nbr')";
								$results = $conn->prepare($sql_string);
								$results->execute();				   
							}
				 	  }else {
							$bError = "1";
							$err_status = $return_err_status;
							$err_status_msg = $return_err_message;
					  }
	 		    }
		     }
		  }else {
		  	  $bError = "1";
			  $err_status = "ERROR";
			  $err_status_msg = "FORMAT: BATCH: No detail recs found";
			}
	}catch(PDOException $e){

		$return_array["return_value"] = 1;
		$return_array["return_message"] = "$e";
		$return_array["return_err_message"] = "sql error";
		$return_array["return_err_status"] = "ERROR";
		$return_array["file_total_batch_count"] = 0;
		$return_array["file_total_entry_hash"] = 0;
		$return_array["file_total_debits"] = 0;
		$return_array["file_total_credits"] = 0;
		$return_array["file_total_detail_recs"] = 0;
		return $return_array;
	}

	$conn = null;
	   				
	if($bError == "0") {
		$return_array["return_value"] = 0;	
		$return_array["return_message"] = $return_message;
		$return_array["return_err_message"] = 0;	
		$return_array["return_err_status"] = 0;	
		$return_array["file_total_batch_count"] = $file_total_batch_count;
		$return_array["file_total_entry_hash"] = $file_total_entry_hash;
		$return_array["file_total_debits"] = $file_total_debits;
		$return_array["file_total_credits"] = $file_total_credits;
		$return_array["file_total_detail_recs"] = $file_total_detail_recs;
	}
	else {
		$return_array["return_value"] = 1;	
		$return_array["return_message"] = $return_message;
		$return_array["return_err_message"] = $err_status_msg;
		$return_array["return_err_status"] = $err_status;
		$return_array["file_total_batch_count"] = 0;
		$return_array["file_total_entry_hash"] = 0;
		$return_array["file_total_debits"] = 0;
		$return_array["file_total_credits"] = 0;
		$return_array["file_total_detail_recs"] = 0;
	}
	
	return $return_array;	
}
           
				
//************************************************************************************************
//Function mm_format_nacha_file() - This function formats a file header record into mm_ach_file_nacha
//************************************************************************************************/
Function mm_format_nacha_file($file_nbr){
	
	$return_array = array();
	$bError = "0";
	$err_status = "0";
	$err_status_msg = "0";
	$return_message = "0";
    $return_err_message = "";
    $return_err_status = "";
    $return_value = "0";

		/* sql statements used during the nacha formatting functions use the
	  the character ^ instead of space because php functions used later will 
	  strip spaces. the ^ will be replaced with spaces on sql insert */
	
    $sql_string = "  SELECT f.status as file_status,
        				lpad(f.file_nbr,8,'0') as file_ref_nbr,
        				rpad(concat(date_format(f.file_dt, '%y%m%d'),
        				date_format(f.file_tm, '%H%I')),10,'0') as file_dt,
		            	f.file_name,
		            	rpad(substr(rtrim(e.bank_name),1,23),23,'^') as bank_name, 
		            	lpad(substr(rtrim(e.bank_routing_nbr),1,10),10,'^') as bank_routing_nbr,
		            	lpad(substr(rtrim(e.company_id),1,10),10,'^') as company_id,
		            	rpad(substr(rtrim(e.company_name),1,23),23,'^') as company_name
					FROM mm_ach_file f
					LEFT JOIN mm_ach_endpoint e on f.endpoint_nbr = e.endpoint_nbr
					WHERE f.file_nbr = $file_nbr";

	try{
		$conn = mm_get_pdo_connection();	
		$results = $conn->query($sql_string);
        $rows = $results->fetchAll();
		$num_rows = count($rows);

		if($num_rows = 1){
			foreach($rows as $row){
				$file_status = $row["file_status"];
				$file_dt  = $row["file_dt"];
				$file_ref_nbr  = $row["file_ref_nbr"];
				$file_name = $row["file_name"];
				$bank_name = $row["bank_name"];
				$bank_routing_nbr = $row["bank_routing_nbr"];
				$company_id = $row["company_id"];
				$company_name = $row["company_name"];
				$file_block_filler = 0;
				
				/* check if this file has the correct status */
				if($file_status != "READY TO FORMAT") { 
						$bError = "1";
					    $err_status = "ERROR";
					    $err_status_msg = "FORMAT: File status <> READY TO FORMAT";
             	}
				
             	/* delete any recs for this file where status is not FORMATTED */
             	
				$sql_string = "DELETE from mm_ach_file_nacha where ach_file_nbr = '$file_nbr'
									AND load_status = 'PROCESSING'";
				$results = $conn->prepare($sql_string);
				$results->execute();
             	
				/* check if this file has already been processed */
				$sql_string = "SELECT * FROM mm_ach_file_nacha                                 
							   WHERE ach_file_nbr = $file_nbr OR nacha_file_name = '$file_name'";
				$fileresults = $conn->query($sql_string);
				$files = $fileresults->fetchAll();
				$num_files = count($files);
		        
			    if($num_files > 0) { 
						$bError = "1";
					    $err_status = "ERROR";
					    $err_status_msg = "FORMAT: File already processed:-" . $file_nbr . "-" . $file_name;
			     }
        
			    /* set values to build nacha record to insert */
					
				$nacha_text = substr(("101" . $bank_routing_nbr . $company_id . $file_dt . "A094101" . $bank_name . 
                            $company_name . $file_ref_nbr),0,94);
                    
				/* check if any values used when inserting nacha record are null or empty string */
				if($file_name == null OR $file_name == "" OR
					$file_nbr == null OR $file_nbr == "" OR
					$bank_routing_nbr == null OR $bank_routing_nbr == "" OR
					$company_id == null OR $company_id == "" OR
					$file_dt == null OR $file_dt == "" OR
					$bank_name == null OR $bank_name == "" OR
					$company_name == null OR $company_name == "" OR
					$file_ref_nbr == null OR $file_ref_nbr == "" OR
					$nacha_text == null OR $nacha_text == "" ) { 
						$bError = "1";
					    $err_status = "ERROR";
					    $err_status_msg = "FORMAT: Null values found in file header record";
					}
				
				/* insert nacha rec if no errors*/
									
				if($bError == "0") { 

					$load_status = "PROCESSING";
					$load_status_msg = "File Header";
					
					$sql_string = "INSERT INTO mm_ach_file_nacha
							(nacha_file_name, nacha_text, load_status, load_status_msg, load_ref_nbr, ach_file_nbr)
							VALUES	('$file_name', rpad(replace('".addslashes($nacha_text)."','^',' '),94, ' '), 
									 '$load_status', '$load_status_msg', '$file_nbr', '$file_nbr')";
				   $results = $conn->prepare($sql_string);
				   $results->execute();
				   
				   // ******  call function to build batch **************
				   $batch_recs = mm_format_nacha_batch($file_nbr);
				   // ***************************************************
				   
				   $return_value = $batch_recs["return_value"];
				   $return_message = $batch_recs["return_message"];
				   $return_err_status = $batch_recs["return_err_status"];
				   $return_err_message = $batch_recs["return_err_message"];
				   $file_total_batch_count = $batch_recs["file_total_batch_count"];
				   $file_total_entry_hash = $batch_recs["file_total_entry_hash"];
				   $file_total_debits = $batch_recs["file_total_debits"];
				   $file_total_credits = $batch_recs["file_total_credits"];
				   $file_total_detail_recs = $batch_recs["file_total_detail_recs"];
				   
				   if($return_value == 0){
				     
				       // select how many recs and blocks there are for this file into $file_rec_count and format as well
				   	   // as well as formatting other fields passed by batch
			   
				   	   $sql_string = "SELECT count(*)+1 as file_total_nbr_of_recs,
				   					lpad((count(*)+1) ,8,'0') as file_total_nbr_of_recs_fmt,
				   	   				lpad('$file_total_batch_count',6,'0') as file_total_batch_count_fmt,
									lpad(ceil((count(*)+1) / 10)  ,6,'0') as file_block_count_fmt,
									lpad('$file_total_detail_recs',8,'0') as file_total_detail_recs_fmt,
									lpad('$file_total_entry_hash',10,'0') as file_total_entry_hash_fmt,
									lpad('$file_total_debits',12,'0') as file_total_debits_fmt,
									lpad('$file_total_credits',12,'0') as file_total_credits_fmt
				   			   FROM mm_ach_file_nacha  
							   WHERE ach_file_nbr = $file_nbr
							   GROUP BY ach_file_nbr";
							   
					   $fileresults = $conn->query($sql_string);
					   $recs = $fileresults->fetchAll();
						foreach($recs as $rec){
							$file_total_nbr_of_recs = $rec["file_total_nbr_of_recs"];
							$file_total_nbr_of_recs_fmt = $rec["file_total_nbr_of_recs_fmt"];
							$file_total_batch_count_fmt = $rec["file_total_batch_count_fmt"];
							$file_block_count_fmt = $rec["file_block_count_fmt"];
							$file_total_detail_recs_fmt = $rec["file_total_detail_recs_fmt"];
							$file_total_entry_hash_fmt = $rec["file_total_entry_hash_fmt"];
							$file_total_debits_fmt = $rec["file_total_debits_fmt"];
							$file_total_credits_fmt = $rec["file_total_credits_fmt"];
						}
					    
						$nacha_text = ("9" . $file_total_batch_count_fmt . $file_block_count_fmt . $file_total_detail_recs_fmt . 
								 $file_total_entry_hash_fmt . $file_total_debits_fmt . $file_total_credits_fmt );

				   
						/* check if any values used when inserting nacha record are null or empty string */
						if($file_name == null OR $file_name == "" OR
							$file_nbr == null OR $file_nbr == "" OR
							$file_total_batch_count_fmt == null OR $file_total_batch_count_fmt == "" OR
							$file_block_count_fmt == null OR $file_block_count_fmt == "" OR
							$file_total_detail_recs_fmt == null OR $file_total_detail_recs_fmt == "" OR
							$file_total_entry_hash_fmt == null OR $file_total_entry_hash_fmt == "" OR
							$file_total_debits_fmt == null OR $file_total_debits_fmt == "" OR
							$file_total_credits_fmt == null OR $file_total_credits_fmt == "" OR
							$nacha_text == null OR $nacha_text == "" ) { 
								$bError = "1";
								$err_status = "ERROR";
								$err_status_msg = "FORMAT: Null values found in file control record";
							}
					
							if($bError == "0") {
								$load_status = "PROCESSING";
								$load_status_msg = "File Control";
				   	   
								$sql_string = "INSERT INTO mm_ach_file_nacha
											(nacha_file_name, nacha_text, load_status, load_status_msg, load_ref_nbr, ach_file_nbr)
											VALUES	
											('$file_name', rpad(replace('".addslashes($nacha_text)."','^',' '),94, ' '), '$load_status', '$load_status_msg','$file_nbr','$file_nbr')";
								$results = $conn->prepare($sql_string);
								$results->execute();				   
							}
				   
							$file_rec_count_last_digit = substr($file_total_nbr_of_recs_fmt,7,1);
				    
							$file_block_filler = 10 - $file_rec_count_last_digit;
		   
							/* check if any values used when inserting nacha record are null or empty string */
							if($file_name == null OR $file_name == "" OR
								$file_nbr == null OR $file_nbr == "" OR
								$file_block_filler == null OR $file_block_filler == "" ) { 
									$bError = "1";
									$err_status = "ERROR";
									$err_status_msg = "FORMAT: Null values found in file filler record";
								}
					
							WHILE ($file_block_filler > 0 AND $bError == "0") {
						
								if($bError == "0") {
	
									$load_status = "PROCESSING";
									$load_status_msg = "File Control Fill";
				   
									$sql_string = "	INSERT INTO mm_ach_file_nacha
											(nacha_file_name, nacha_text, load_status, load_status_msg, load_ref_nbr, ach_file_nbr)
											VALUES	
											('$file_name', rpad('',94, '9'), '$load_status', '$load_status_msg','$file_nbr','$file_nbr')";
									$results = $conn->prepare($sql_string);
									$results->execute();
								}
						
								$file_block_filler = $file_block_filler -1 ;

							}
					}else {
							$bError = "1";
							$err_status = $return_err_status;
							$err_status_msg = $return_err_message;
					}
				   } else {
						$bError = "1";
						// other values should be set in file error handling
				     }
				  }
		      }
		    if ($bError == "0") {
		    		$sql_string = "UPDATE mm_ach_file_nacha set load_status = 'FORMATTED' where ach_file_nbr = '$file_nbr'"; 
		    		$results = $conn->prepare($sql_string);
		    		$results->execute();
						
		    		$sql_string = "UPDATE mm_ach_file set status = 'FORMATTED', status_msg = '' where file_nbr = '$file_nbr'"; 
		    		$results = $conn->prepare($sql_string);
		    		$results->execute();
		    } else {
		    		$sql_string = "UPDATE mm_ach_file set status_msg = '$err_status_msg' where file_nbr = '$file_nbr'"; 
		    		$results = $conn->prepare($sql_string);
		    		$results->execute();
		    }
		    
		   
	}catch(PDOException $e){
		$return_array["return_value"] = 1;
		$return_array["return_message"] = "$e for Query $sql_string";
		$return_array["return_err_message"] = 0;
		$return_array["return_err_status"] = 0;
		return $return_array;
	}

	$conn = null;
			   
	echo("***FORMATTING NACHA FILE***: " . $file_nbr . "<BR>");
	echo(" return value: " . $return_value . "<BR>");
	echo(" return message: " . $return_message . "<BR>");
	echo(" err_message: " . $err_status_msg . "<BR>");
	echo(" err_status: " . $err_status  . "<BR>");
		
	if($bError == "0") {
		$return_array["return_value"] = 0;	
		$return_array["return_message"] = $return_message;	
		$return_array["return_err_message"] = 0;
		$return_array["return_err_status"] = 0;	
	}else {
		$return_array["return_value"] = 1;
		$return_array["return_message"] = $return_message;
		$return_array["return_err_message"] = $err_status_msg;
		$return_array["return_err_status"] = $err_status;	
	}
		
	return $return_array;	
}

/*****************************************************************************************************
Function mm_ach_nacha_ftp_wrapper()
*****************************************************************************************************/
function mm_ach_nacha_ftp_wrapper($filename = ''){
	$return_value = 0;
	$return_message = '';
	$return_array = array();
	$files_to_ftp = array();
	$num_files = 0;
	$num_errors = 0;
	$error_array = array();
	$num_successful = 0;
	$successful_files_array = array();


	if($filename == ''){
		//There was no filename provided so get the list of files needing to be FTP'd from the database
		$sql_string = "select file_nbr, file_name from mm_ach_file where status = 'READY TO FTP'";
		try {
		        $conn = mm_get_pdo_connection();
                	$results = $conn->query($sql_string);
        		$rows = $results->fetchAll();
                	$num_rows = count($rows);
                if($num_rows == 0){
			$return_message = "NO FILES TO ACH";
		}else{
                        foreach($rows as $row){
				$files_to_ftp[$num_files]["file_name"] = $row["file_name"];
				$files_to_ftp[$num_files]["file_nbr"] = $row["file_nbr"];
				$num_files += 1;
                        }
		}
        	}catch(PDOException $e){
                	$error_array[$num_errors] = "Database Error: $e";
                	$num_errors +=1;
        	}
		$conn = null;
	}else{
		//There was a specific file named so attempt to ftp just that file.
		$files_to_ftp[0] = $filename;
		$num_files = 1;
	}

	if($num_files > 0){
		//There is at least 1 file to FTP
		
		//Get the FTP credentials
		 $ach_server_details = mm_get_ach_server_details();
        	$ftp_server = $ach_server_details["server"];
        	$ftp_username = $ach_server_details["username"];
        	$ftp_password = $ach_server_details["password"];
        	$ftp_port = $ach_server_details["port"];

		//Open the FTP connection
		$num_ftp_attempts = 0;
		$successful_ftp_connection = 0;
		while($num_ftp_attempts < 5 && $successful_ftp_connection == 0){
			//Try to connect to the FTP server up to 5 times sleeping 15 seconds between each attempt
			if(!$sftp = new Net_SFTP($ftp_server, $ftp_port)){
				$num_ftp_attempts +=1;
				echo "Failed connecting on attempt number $num_ftp_attempts<br>"; 
				sleep(5);
			}else{
				if(!$sftp->login($ftp_username, $ftp_password)){
					$num_ftp_attempts+=1;
					echo "Failed logging in to ftp server on attempt number $num_ftp_attempts<br>";
					sleep(5);
				}else{
					$successful_ftp_connection = 1;
				}
			}
	
		}

		if($successful_ftp_connection !=1){
			//There was no successful connection to the FTP server so set the appropriate variables
			$return_value = 1;
			$return_message = "Failed connecting to the FTP server $ftp_server on port $port with username $ftp_username";
		}else{
			//The FTP server connection was successful so loop through the files to FTP and attempt to put them on the server
			//Get the location for the ACH files
			$ach_file_path = mm_get_ach_path();

			//Loop through the ACH file path
			foreach($files_to_ftp as $file){
				$filename = $file["file_name"];
				$file_nbr = $file["file_nbr"];
				$filepath = $ach_file_path . $filename;
				$remote_filepath = "Uploads/$filename";
				if(!file_exists($filepath)){
					$error_array[$num_errors] = "The file $filepath doesn't exists";
					$num_errors +=1;
				}else{
					//The file exists so try to FTP the file 5 times	
						$ftp_attempts = 0;
						$ftp_successful = 0;
					while($ftp_attempts < 5 && $ftp_successful == 0){
						if(!$result = $sftp->put($remote_filepath, $filepath, NET_SFTP_LOCAL_FILE)){
							//The FTP wasn't successful so sleep and try again later
							$ftp_attempts +=1;
							sleep(5);
						}else{
							//The FTP was successful to break the loop
							$ftp_successful = 1;
						}
					}
					if($ftp_successful !=1){
						//The File couldn't be FTP'd successfully so log a message
						$error_array[$num_errors] = "Failed to FTP file $filename";
						$num_errors += 1;
					}else{
						//The file was successfully uploaded so update the database and move the file to the archive directory
						mm_update_database_value('mm_ach_file', 'status', 'FTP COMPLETE', 's', 'file_nbr', $file_nbr);
				
						//The file was successfully uploaded so move it to the archive directory
						$archive_filepath = str_replace("outbound_files", "archive", $filepath);
						if(!rename($filepath, $archive_filepath)){
							//Failed to move the file to the archive directory
							$error_array[$num_errors] = "Failed to move the FTP file, $filename, to the archive directory $archive_filepath";
							$num_errors +=1;
						}else{
							//The file was successfully moved to the achive directory.  There is nothing more to do
							$successful_files_array[$num_successful] = $filename;
							$num_successful +=1;
						}
					}
				}
			}
		}
     	}

	if($num_errors >0){
		$return_value = 1;
	}

	$return_array["return_value"] = $return_value;
	$return_array["return_message"] = $return_message;
	$return_array["num_successful"] = $num_successful;
	$return_array["num_files"] = $num_files;
	$return_array["successful_files_array"] = $successful_files_array;
	$return_array["num_errors"] = $num_errors;
	$return_array["error_array"] = $error_array;

	return $return_array;


}

//************************************************************************************************
//Function mm_build_nacha_recs_wrapper() - This function builds nacha recs from mm_ach_file/_detail tables and calls process to put them in mm_ach_nacha_file
//************************************************************************************************/
Function mm_ach_build_nacha_recs_wrapper(){
	
	$return_array = array();
	$return_value = 0;
	$return_message = 0;
	$return_err_status = 0;
	$return_err_message = 0;
	$num_file_to_process = 0;
	$num_files_successfully_processed = 0;
	$num_files_with_errors = 0;
	$num_errors = 0;
	$error_array = array();
	
    $sql_string = "SELECT file_nbr FROM mm_ach_file
    				WHERE status = 'READY TO FORMAT' order by file_nbr, seq_nbr";
	
	try{
		$conn = mm_get_pdo_connection();	
		$results = $conn->query($sql_string);
        $rows = $results->fetchAll();
		$num_rows = count($rows);

		if($num_rows > 0){
			foreach($rows as $row){
				$file_nbr = $row["file_nbr"];
				$format_recs = mm_format_nacha_file($file_nbr);
				if($format_recs["return_value"] !=0){
					$error_status = $format_recs["return_err_status"];
					$error_message = $format_recs["return_err_message"];
					$error_array[$num_errors] = "Error Building File $file_nbr.  The error message is: $error_message and the error status is $err_status.";
					$num_errors +=1;
				}else{
					$num_files_successfully_processed +=1;
				}
				
			}
	    }else {   
	   	   		$return_value = 0;
		        	$return_message = '';
				$return_err_status = 'WARNING';
				$return_err_message = 'No files to process';
			}

	}catch(PDOException $e){
		$error_array[$num_errors] = "Database Error: $e";
		$num_errors +=1;
	}

	$conn = null;
	if($num_errors >1){	
		$return_array["return_value"] = 1;
	}else{
		$return_array["return_value"] = 0;
	}
	$return_array["return_message"] = $return_message;
	$return_array["num_errors"] = $num_errors;
	$return_array["num_files_processed"] = $num_files_successfully_processed;
	$return_array["error_array"] = $error_array;
	$return_array["return_err_message"] = $return_err_message;
	$return_array["return_err_status"] = $return_err_status;
		
	return $return_array;	
}
									

//************************************************************************************************
//Function mm_ach_create_nacha_file_wrapper() - This function determines what nacha files need to be created and calls a function to create them
//************************************************************************************************/
Function mm_ach_create_nacha_file_wrapper(){
	     
	$return_array = array();
	$bError = "0";
	$err_status = "0";
	$err_status_msg = "0";
	$return_value = "0";
	$return_message = "0";
    $return_err_message = "0";
    $return_err_status = "";
    $ach_file_nbr = 0;
		
	$sql_string = "select nacha_file_name, ach_file_nbr
				   from mm_ach_file_nacha
				   where load_status = 'FORMATTED'
				   GROUP BY nacha_file_name";	
	try{
		$conn = mm_get_pdo_connection();	
		$results = $conn->query($sql_string);
        $rows = $results->fetchAll();
		$num_rows = count($rows);

		if($num_rows > 0){
			foreach($rows as $row){
				$nacha_file_name = $row["nacha_file_name"];
				$ach_file_nbr = $row["ach_file_nbr"];
				
				$file_results = mm_ach_create_nacha_file($nacha_file_name);
								  
				$return_value = $file_results["return_value"];
				$return_message = $file_results["return_message"];
				$return_err_status = $file_results["return_err_status"];
				$return_err_message = $file_results["return_err_message"];
				
				if($return_value == 0){
					//There wasn't an error so update the ach_file and nacha_file record status
					$sql_string = "UPDATE mm_ach_file_nacha set load_status = 'READY TO FTP' where ach_file_nbr = '$ach_file_nbr'"; 
		    		$results = $conn->prepare($sql_string);
		    		$results->execute();
						
		    		$sql_string = "UPDATE mm_ach_file set status = 'READY TO FTP', status_msg = '' where file_nbr = '$ach_file_nbr'"; 
		    		$results = $conn->prepare($sql_string);
		    		$results->execute();
		    		
				}else{
					//There was a problem creating the file
							$bError = "1";
							$err_status = $return_err_status;
							$err_status_msg = $return_err_message;
				}
		      }
		    }else 
				{$return_message = "No ACH files to write";
				} 
		    	
	}catch(PDOException $e){
		$return_array["return_value"] = 1;
		$return_array["return_message"] = "$e";
		$return_array["return_err_message"] = 0;
		$return_array["return_err_status"] = 0;
		return $return_array;
	}

	$conn = null;
			   		
	    echo("***CREATING NACHA FILE***: " . $ach_file_nbr . "<BR>");
		echo(" return value: " . $return_value);
		echo(" return message: " . $return_message . "<BR>");
		echo(" err_message: " . $err_status_msg . "<BR>");
		echo(" err_status: " . $err_status  . "<BR>");
		
		
	if($bError == "0") {
		$return_array["return_value"] = 0;	
		$return_array["return_message"] = $return_message;	
		$return_array["return_err_message"] = 0;
		$return_array["return_err_status"] = 0;	
	}else {
		$return_array["return_value"] = 1;
		$return_array["return_message"] = $return_message;
		$return_array["return_err_message"] = $err_status_msg;
		$return_array["return_err_status"] = $err_status;	
	}	
		
	return $return_array;	
}

//************************************************************************************************
//Function mm_ach_create_nacha_file($p_filename) - This function creates the nacha file and puts them on server
//************************************************************************************************/
Function mm_ach_create_nacha_file($file_name){
    
    $document_path = mm_get_ach_path();
    $filename = $document_path . $file_name;

    $fd = fopen($filename, "w") or die("Can't open file $filename");
 
   	$return_array = array();
   	$bError = "0";
	$err_status = "0";
	$err_status_msg = "0";
	$return_message = "0";
    $return_err_message = "0";
    	
	$sql_string = "select nacha_text from mm_ach_file_nacha 
	    				where nacha_file_name = '$file_name'
	    				order by nacha_rec_nbr ";	
	try{
		$conn = mm_get_pdo_connection();	
		$results = $conn->query($sql_string);
        $rows = $results->fetchAll();
		$num_rows = count($rows);

		if($num_rows > 0){
				foreach($rows as $row){
				$nacha_text = $row["nacha_text"]."\r\n" ;
				fwrite($fd, $nacha_text); 
		   }
		}

	}catch(PDOException $e){
		$return_array["return_value"] = 1;
		$return_array["return_message"] = "$e";
		$return_array["return_err_message"] = 0;
		$return_array["return_err_status"] = 0;
		fclose($fd); 
		return $return_array;
	}

	$conn = null;

	$return_array["return_value"] = 0;	
	$return_array["return_message"] = 0;	
	$return_array["return_err_message"] = 0;
	$return_array["return_err_status"] = 0;
		
    fclose($fd); 
    
	return $return_array;	

}	


/***********************************************************************************************************
Function mm_get_lp_customer_details($email_address)
***********************************************************************************************************/
function mm_get_lp_customer_details($email_address){

        $return_array = array();
        $customer_array = array(array());
        $expand_array = array();
        $payment_accts_array = array(array());

        $lp_login_details = mm_get_lp_credentials();
        $account_token = $lp_login_details["token"];
        $auto_pal_id = $lp_login_details["tenant_id"];
        $filter_string = CUSTOMERS::EMAIL . " == '$email_address'";
        $expand_array_string = CUSTOMERS::PAYMENT_ACCOUNTS . "/" . PAYMENT_ACCOUNT::CHECKING_ACCOUNT;
        //echo "$filter_string\n";
        //echo "$expand_array_string\n";
        $expand_array[0] = $expand_array_string;

        try{
                $i = 0;
         $sdk = LPSDK::GetInstance();
                $customers = $sdk->GetCustomers_RAW($expand_array, $sdk->CreateNoPagingParam(), $sdk->CreateFilterParams_Logic($filter_string));
                $num_customers = sizeof($customers);
                $customer_nbr = 0;
                if($num_customers >0){
                        foreach($customers as $customer){
                                $customer_string = json_encode($customer);
                                //echo "$customer_string\n";
                                $customer_array[$customer_nbr]["first_name"] = $customer->get("firstName");
                                $customer_array[$customer_nbr]["last_name"] = $customer->get("lastName");
                                $customer_array[$customer_nbr]["id"] = $customer->get("id");
                                $customer_array[$customer_nbr]["birth_date"] = $customer->get("birthDate");
                                $customer_array[$customer_nbr]["is_primary"] = $customer->get("isPrimary");
                                $customer_array[$customer_nbr]["is_secondary"] = $customer->get("isSecondary");
                                $customer_array[$customer_nbr]["email_address"] = $customer->get("email");
                                $customer_array[$customer_nbr]["active"] = $customer->get("active");
                                $payment_accts_array = array(array());
                                $payment_accts = $customer->get(CUSTOMERS::PAYMENT_ACCOUNTS);
                                $num_payment_accts = sizeof($payment_accts);
                                $customer_array[$customer_nbr]["num_accts"] = $num_payment_accts;
                                if($num_payment_accts ==0){
                                        //There are no payment accounts
                                }else{
                                        //There is at least one payment account so loop through the accounts and collect the data
                                        $payment_acct_nbr = 0;
                                        foreach($payment_accts as $payment_acct){
                                                $payment_accts_array[$payment_acct_nbr]["title"] = $payment_acct->get("title");
                                                $payment_accts_array[$payment_acct_nbr]["type"] = $payment_acct->get("type");
                                                $payment_accts_array[$payment_acct_nbr]["is_primary"] = $payment_acct->get("isPrimary");
                                                if($payment_accts_array[$payment_acct_nbr]["type"] == "paymentAccount.type.checking" ){
                                                        $checking_acct = $payment_acct->get("CheckingAccount");
                                                        $payment_accts_array[$payment_acct_nbr]["acct_nbr"] = $checking_acct->get("accountNumber");
                                                        $payment_accts_array[$payment_acct_nbr]["routing_nbr"] = $checking_acct->get("routingNumber");
                                                        $payment_accts_array[$payment_acct_nbr]["token"] = $checking_acct->get("token");
                                                }
                                                $payment_acct_nbr += 1;
                                        }
                                }
                                $customer_array[$customer_nbr]["payment_accts"] = $payment_accts_array;
                                $customer_nbr += 1;
                        }

                }else{
                        //There are no matches on email address
                }

                        $return_array["return_value"] = 0;
                        $return_array["return_message"] = '';
                        $return_array["num_customers"] = $num_customers;
                        $return_array["customer_array"] = $customer_array;

                /*foreach($customers as $customer){
                        $customer_string = json_encode($customer);
                        //echo "$customer_string\n\n";
                        $i +=1;
                }*/
        }catch(Exception $e){
                //echo "Failed: $e\n";
                        $return_array["return_value"] = 1;
                        $return_array["return_message"] = '$e';
                        $return_array["num_customers"] = $num_customers;
                        $return_array["customer_array"] = $customer_array;
                        //Log an error
                        mm_log_error('mm_get_lp_customer_details', "Failed Getting Customer data for $email address: Error returned is $e");

                        return $return_array;
  }

                        return $return_array;
}



/*******************************************************************************
Function mm_get_lp_loans_ready_to_fund()
*******************************************************************************/
function mm_get_lp_loans_ready_to_fund(){
        $num_warnings = 0;
        $num_errors = 0;
        $num_credits = 0;
        $total_credits = 0;
        $warning_array = array();
        $error_array = array();
        $query_array = array();
        $match_array = array();
        $match_array = array();
        $bool_array = array();
        $must_array = array(array());
        $fields_array = array();
        $data_array = array();
        $loan_data = array(array());

        $environment = mm_get_lp_environment_portfolio();
        $lp_login_details = mm_get_lp_credentials();
        $account_token = $lp_login_details["token"];
        $auto_pal_id = $lp_login_details["tenant_id"];
        $url = "https://loanpro.simnang.com/api/public/api/1/Loans/Autopal.Search()?\$top=500000&\$orderby=id";
        //echo "$url\n";
        $header_array = array("Content-Type: application/json", 'Accept: application/json', "Authorization: $account_token", "Autopal-Instance-ID: $auto_pal_id");
        $temp = json_encode($header_array);
        //echo "header array: $temp\n";

        //$fields_array = ["id", "loanStatusText", "loanSubStatusText", "contractDate", "loanAmount", "primaryCustomerName", "primaryCustomerEmail", "customers.ssn", "active", "displayId", "primaryCustomerId"];

        $match_array["loanSubStatusId"] = 2;
        $must_array[0]["match"] = $match_array;

        $match_array = array();
        $match_array["subPortfolios.portfolio__6"] = "40";
        $must_array[1]["match"] = $match_array;

        $bool_array["must"] = $must_array;
        $query_array["bool"] = $bool_array;

        //commented out due to change in elastic search standard
        //$data_array["_source"] = $fields_array;
        $data_array["query"] = $query_array;

        $json_message = json_encode($data_array);
        echo "$json_message\n";

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HTTPHEADER,$header_array );
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $json_message);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $json_response = curl_exec($curl);

        echo "$json_response\n";

        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        if ( $status != 201 && $status != 200 ) {
                //There was an error in the message sending  Add error handling
                $return_value = 1;
                $return_message = "Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl) . " $json_message";
        }else{
  $response_array = json_decode($json_response, true);
                $d_array = $response_array["d"];
                $results_array = $d_array["results"];
                $i = 0;
                foreach($results_array as $record){
                        //$email = $record["primaryCustomerEmail"][0];
                        //echo "$email\n";
                        $record_string = json_encode($record);
                        //echo "RECORD $i: $record_string\n\n";
                        $loan_data[$i]["loan_system_id"] = $record["id"];
                        $loan_data[$i]["display_id"] = $record["displayId"];
                        $loan_data[$i]["loan_amt"] = $record["loanAmount"];
                        $loan_data[$i]["contract_dt"] = $record["contractDate"];
                        $loan_data[$i]["active"] = $record["active"];
                        $loan_data[$i]["customer_name"] = $record["primaryCustomerName"];
                        $loan_data[$i]["customer_email"] = $record["primaryCustomerEmail"];
                        /*//For echoing and testing
                        $loan_system_id = $record["id"];
                        $display_id = $record["displayId"];
                        $loan_amt = $record["loanAmount"];
                        $contract_dt = $record["contractDate"];
                        $active = $record["active"];
                        $customer_name = $record["primaryCustomerName"];
                        $customer_email = $record["primaryCustomerEmail"];
                        */
                        $i +=1;
                }
                $return_value = 0;
                $return_message = '';
        }
        $return_array["loan_data"] = $loan_data;
        $return_array["num_loans"] = $i;
        $return_array["return_value"]  = $return_value;
        $return_array["return_message"] = $return_message;

        return $return_array;
}


/****************************************************************************************
Function mm_process_ach_funding($tran_dt='')
****************************************************************************************/

function mm_process_ach_funding($tran_dt=''){
                //Default Variables
                $return_array = array();
                $return_value = 0;
                $num_warnings = 0;
                $num_errors = 0;
                $num_successful = 0;
                $return_message = '';
                $return_err_message = '';
                $return_err_status = '';
                $ach_file_name = '';
                $ach_file_nbr = '';
                $return_error_array = array();
                $return_warning_array = array();
                $ach_detail_array = array(array());
                $loan_counter = 0;


        if($tran_dt == ''){
                //There was no date provided so use today's date
                $create_dt = new DateTime();
        }else{
                //The call contained a value for tran date so use it instead of today's date
                $create_dt = new DateTime($tran_dt);

        }
                $create_dt_yyyymmdd = $create_dt->format('Ymd');
                $create_dt_mmddyyyy = $create_dt->format('m-d-Y');
                $create_dt_yyyy_mm_dd = $create_dt->format('Y-m-d');
                //echo "$create_dt_yyyymmdd $create_dt_mmddyyyy\n";

        //Create a new ACH file record - The record id is used late for inserting detail records
        $ach_file_results = mm_create_ach_file_record('C', $create_dt_yyyymmdd);
        if($ach_file_results["return_value"] != 0){
                $return_array["return_value"] = $ach_file_results["return_value"];
                $return_array["return_message"] = $ach_file_results["return_message"];
                return $return_array;
        }else{
                $ach_file_nbr = $ach_file_results["ach_file_nbr"];
                $ach_file_name = $ach_file_results["ach_file_name"];
        }

        $ready_to_fund_arr = mm_get_lp_loans_ready_to_fund();
        $ready_to_fund_return_value  = $ready_to_fund_arr["return_value"];
        $ready_to_fund_return_message  = $ready_to_fund_arr["return_message"];
        if($ready_to_fund_return_value !=0){
                //There was a problem getting the loans so log an error message and abort the process
		$return_value = 1;
		$return_message = "There was a problem getting the loans from Loan Pro. Error Message is: $return_message";
        }else{
                //There was no problem getting the loans so continue with building the file
                $loan_data = $ready_to_fund_arr["loan_data"];
                $num_loans = $ready_to_fund_arr["num_loans"];
		if($num_loans == 0){
			//There are no loans to process so update the file status in mm_ach_file to "NO ACH RECORDS"
			mm_update_database_value('mm_ach_file', 'status', 'NO ACH RECORDS', 's','file_nbr', $ach_file_nbr);
		}else{
                	foreach($loan_data as $loan){	
                		$customer_email = $loan["customer_email"];
                        	$loan_error_ind = 0;
                        	$loan_warning_ind =0;
                        	$active = $loan["active"];
                        	$loan_amt = $loan["loan_amt"];
                        	$display_id = $loan["display_id"];
                        	$contract_dt = $loan["contract_dt"];
                        	$customer_name = $loan["customer_name"];
                        	$loan_system_id = $loan["loan_system_id"];
                        	$inactive_loan_ind = 0;
                        	$missing_customer_ind = 0;
                        	$missing_checking_acct_ind = 0;
                        	$customer_retrieval_error_ind = 0;
				$duplicate_primary_checking_acct_ind = 0;
                        	$past_effective_dt_ind = 0;
                        	$acct_nbr = 0;
                        	$routing_nbr = 0;
                        	$token = '';

                        	//Check for warnings
                        	if($contract_dt < $create_dt_yyyy_mm_dd){
                                	$past_effective_dt_ind = 1;
					$loan_warning_ind = 1;
					$return_warning_array[$num_warnings] = "$display_id has a contract date in the past: $contract_dt";
					$num_warnings += 1;
                        	}


                        	if($active != 1){
                                	//The active flag isn't set to 1 so log an error
                                	$inactive_loan_ind = 1;
                                	$loan_error_ind = 1;
					$return_error_array[$num_errors] = "Loan $display_id is inactive";
					$num_errors += 1;
                        	}else if($customer_email == ''){
                                	//The customer email address isn't populated so log an error
                                	$missing_customer_ind = 1;
                                	$loan_error_ind = 1;
					$return_error_array[$num_errors] = "No customer found on loan $display_id";
					$num_errors +=1;
                        	}else{
                                	$customer_details_arr = mm_get_lp_customer_details($customer_email);
                                	$customer_detail_return_value = $customer_details_arr["return_value"];
                                	$customer_detail_return_message = $customer_details_arr["return_message"];
                                	if($customer_detail_return_value != 0){
                                        	//There was a problem getting the customer so log an error
                                        	$customer_retrieval_error_ind = 1;
                                        	$loan_error_ind = 1;
						$return_error_array[$num_errors] = "Unable to get LoanPro Customer Detail for $display_id and email: $customer_email_";
						$num_errors+=1;
                                	}else{
                                        	//There was no problem getting the customer so continue building the ACH record
                                        	$num_customers = $customer_details_arr["num_customers"];
                                        	$customer_array = $customer_details_arr["customer_array"];
                                        	$primary_customer_ind = 0;
                                        	$primary_checking_acct_ind = 0;
                                        	$duplicate_primary_customer_ind = 0;
                                        	foreach($customer_array as $customer){
                                                	//Loop through the customers looking for the primary customer
                                                	$is_primary = isset($customer["is_primary"]) ? $customer["is_primary"] : 0;
                                                	if($is_primary || $num_customers == 1){
                                                        	if($primary_customer_ind == 1){
                                                                	//There's a problem because there are two primary customers attached to the loan so produce an error
                                                                	$duplicate_primary_customer_ind = 1;
                                                                	$loan_error_ind = 1;
									$return_error_array[$num_errors] = "Duplicate Primary Customers Found on $display_id";
									$num_errors +=1;
                                                        	}else{
                                                                	//This is the first primary customer seen on the account
                                                                	$primary_customer_ind = 1;
                                                                	$payment_accts = $customer["payment_accts"];
                                                                	$num_accts = $customer["num_accts"];
                                                                	$primary_checking_acct_ind = 0;
                                                                	$acct_nbr = 0;
                                                                	$routing_nbr = 0;
                                                                	$token = '';
                                                                	if($num_accts == 0){
                                                                        	$missing_checking_acct_ind = 1;
                                                                        	$loan_error_ind = 1;
										$return_error_array[$num_errors] = "No checking acount found for loan $display_id";
										$num_errors+=1;
                                                                	}else{
										$dupliate_primary_checking_acct_ind = 0;
                                                                        	foreach($payment_accts as $acct){
                                                                                	$acct_string = json_encode($acct);
                                                                                	$type = $acct["type"];
                                                                                	$acct_is_primary = $acct["is_primary"];
                                                                                	//echo "Payment Account: $type $is_primary\n";
                                                                                	if($acct["type"] == 'paymentAccount.type.checking' && $acct["is_primary"]){
                                                                                        	//The account is a primary checking account
                                                                                        	if($primary_checking_acct_ind == 1){
                                                                                                	//Capture an error because there is more than 1 primary account tied to this customer
                                                                                                	$duplicate_primary_checking_acct_ind = 1;
                                                                                                	$loan_error_ind = 1;
													$return_error_array[$num_errors] = "Duplicate Primary Checking Accounts Found for loan $display_id";
													$num_errors+=1;
 												}else{
                                                                                                	//This is the first primary checking account
                                                                                                	$acct_nbr = $acct["acct_nbr"];
                                                                                                	$routing_nbr = $acct["routing_nbr"];
                                                                                                	$token = $acct["token"];
                                                                                                	$primary_checking_acct_ind = 1;
                                                                                        	}
	
                                                                                	}

                                                                        	}
                                                                	}
                                                        	}
                                                	}
                                        	}

                                	}
                        	}

                        $ach_detail_array[$loan_counter]["loan_nbr"] = $display_id;
                        $ach_detail_array[$loan_counter]["amount"] = $loan_amt;
                        $ach_detail_array[$loan_counter]["payment_display_nbr"] = 0;
                        $ach_detail_array[$loan_counter]["trans_id"] = $display_id;
                        $ach_detail_array[$loan_counter]["trans_type"] = 'C';
                        $ach_detail_array[$loan_counter]["trans_subtype"] = 'F';
                        $ach_detail_array[$loan_counter]["trans_sign"] = -1;
                        $ach_detail_array[$loan_counter]["customer_name"] = $customer_name;
                        $ach_detail_array[$loan_counter]["ach_file_nbr"] = $ach_file_nbr;
                        $ach_detail_array[$loan_counter]["effective_dt"] = $contract_dt;
                        $ach_detail_array[$loan_counter]["acct_nbr"] = $acct_nbr;
                        $ach_detail_array[$loan_counter]["routing_nbr"] = $routing_nbr;
			if($acct_nbr == "2066474" && $routing_nbr == "065302303"){
				$ach_detail_array[$loan_counter]["bank_acct_type"] == "SAV";	
			}else{
                        	$ach_detail_array[$loan_counter]["bank_acct_type"] = "CHK";
			}
                        $ach_detail_array[$loan_counter]["authorization_type"] = "PPD";
                        $ach_detail_array[$loan_counter]["warning_ind"] = $loan_warning_ind;
                        $ach_detail_array[$loan_counter]["system_loan_id"] = $loan_system_id;
                        $ach_detail_array[$loan_counter]["warning_ind"] = $loan_warning_ind;
                        $ach_detail_array[$loan_counter]["payment_system_id"] = 0;
                        $ach_detail_array[$loan_counter]["error_ind"] = $loan_error_ind;

                        $ach_detail_result_arr = mm_create_ach_detail_record($ach_detail_array[$loan_counter]);
                        $ach_detail_return_value = $ach_detail_result_arr["return_value"];
                        if($ach_detail_return_value !=0){
                                $ach_detail_return_message = $ach_detail_result_arr["return_message"];
				$return_error_array[$num_errors] = "Failed inserting the ach detail record for $display_id.  Error is: $ach_detail_return_message";
				$num_errors +=1;
                                if($loan_error_ind == 0){
                                        //Failed entering the record and there no previous errors so try to load it to the ach detail array
					$loan_error_ind = 1;
                                        $ach_detail_array[$loan_counter]["error_ind"] = 1;
                                        $ach_detail_result_arr = mm_create_ach_detail_record($ach_detail_array[$loan_counter]);
                                        $ach_detail_return_value = $ach_detail_result_arr["return_value"];
                                        if($ach_detail_return_value == 1){
                                                //Tried inserting an error record but failed
						$return_error_array[$num_errors] = "Failed inserting a record into the ach_detail_error table for loan $display_id";
						$num_errors +=1;
                                        }else{
						//Successfully inserted the record in the detail error.  Add a validation record with a value of 1 for tracking
						$function_results = mm_create_ach_detail_validation_xref_record($display_id, $ach_file_nbr, 1);
						if($function_results["return_value"] != 0){
							//failed inserting the validation xref record
							$return_error_array[$num_errors] = "Failed inserting a detail validation xref error for loan $display_id.";
							$num_errors +=1;
						}
					}
                                }else{
                                        //Failed adding a record to the detail error table
					$return_error_array[$num_errors] = "Failed inseting a record into the ach detail error table for loan $display_id";
					$num_errors +=1;
                                }
                        }

			if($ach_detail_return_value == 0 && $loan_error_ind == 0){
				//The record was sucessfully added
				$num_successful += 1;

				//Update the loan status in loan pro
				$loan_update_results = mm_activate_lp_loan($loan_system_id);
				if($loan_update_results["return_value"] !=0){
					//Failed to update the loan
					$loan_update_results_return_message = $loan_update_results["return_message"];
					$return_error_array[$num_errors] = "Failed Updating the loan $display_id to active.  Error message is: $loan_update_results_return_message";
					$num_errors +=1;
				}
			}

                        if($ach_detail_return_value == 0 && ($loan_error_ind == 1 || $loan_warning_ind == 1)){
                                //A detail record was successfully added to the database and there are errors or warnings present so add them
                                //Check to see if the record failed inserting because it's a duplicate

                                // Check to see if the warning is because the effective date is in the past
                                        if($past_effective_dt_ind == 1){
                                                $function_results = mm_create_ach_detail_validation_xref_record($display_id, $ach_file_nbr, 2);
                                                $function_return_value = $function_results["return_value"];
                                                $function_return_message = $function_results["return_message"];
						if($function_return_value != 0){
							$return_error_array[$num_errors] = "Failed adding a validation xref error for loan $display_id.  The error is due to a contract date in the past.  The return error is $function_return_message";
							$num_errors+=1;
						}
                                        }

                                //Check to see if the error is because of no payment profile
                                        if($missing_checking_acct_ind == 1){
                                                $function_results = mm_create_ach_detail_validation_xref_record($display_id, $ach_file_nbr, 3);
                                                $function_return_value = $function_results["return_value"];
                                                $function_return_message = $function_results["return_message"];
						if($function_return_value != 0){
							$return_error_array[$num_errors] = "Failed adding a validation xref error for loan $display_id.  The error is due to a contract date in the past.  The return error is $function_return_message";
							$num_errors+=1;
						}

                                        }
                                //Check to see if the issue is becasue the loan isn't active
                                        if($inactive_loan_ind == 1){
                                                $function_results = mm_create_ach_detail_validation_xref_record($display_id, $ach_file_nbr, 4);
                                                $function_return_value = $function_results["return_value"];

                                                $function_return_message = $function_results["return_message"];
						if($function_return_value != 0){
							$return_error_array[$num_errors] = "Failed adding a validation xref error for loan $display_id.  The error is due to a contract date in the past.  The return error is $function_return_message";
							$num_errors+=1;
						}
                                        }
                               //Check to see if the error is because there are multiple primary payment profiles
                                        if($duplicate_primary_checking_acct_ind == 1){
                                                $function_results = mm_create_ach_detail_validation_xref_record($display_id, $ach_file_nbr, 5);
                                                $function_return_value = $function_results["return_value"];
                                                $function_return_message = $function_results["return_message"];
						if($function_return_value != 0){
							$return_error_array[$num_errors] = "Failed adding a validation xref error for loan $display_id.  The error is due to a contract date in the past.  The return error is $function_return_message";
							$num_errors+=1;
						}

                                        }
                                // Check to see if the error is because there is no customer tied to the loan
                                        if($missing_customer_ind == 1 || $customer_retrieval_error_ind == 1){
                                                $function_results = mm_create_ach_detail_validation_xref_record($display_id, $ach_file_nbr, 6);
                                                $function_return_value = $function_results["return_value"];
                                                $function_return_message = $function_results["return_message"];
						if($function_return_value != 0){
							$return_error_array[$num_errors] = "Failed adding a validation xref error for loan $display_id.  The error is due to a contract date in the past.  The return error is $function_return_message";
							$num_errors+=1;
						}
                                        }
                                //Check to see if the error is because there are two customers tied to the loan
                                        if($duplicate_primary_customer_ind == 1){
                                                $function_results = mm_create_ach_detail_validation_xref_record($display_id, $ach_file_nbr, 7);
                                                $function_return_value = $function_results["return_value"];
                                                $function_return_message = $function_results["return_message"];
						if($function_return_value != 0){
							$return_error_array[$num_errors] = "Failed adding a validation xref error for loan $display_id.  The error is due to a contract date in the past.  The return error is $function_return_message";
							$num_errors+=1;
						}

                                        }
                                //Check to see if the loan has an invalid status


                                //Check to see if the payment exceeds payoff threshold

                                //Chedk to see if the payment exceeds payment but within a tolerable threshold
                        }

                        //Update processing summary tracking arrays

                        $loan_counter +=1;

                        //echo "$loan_counter $loan_error_ind $display_id $num_accts $customer_email $inactive_loan_ind $missing_customer_ind $customer_retrieval_error_ind $duplicate_primary_customer_ind $duplicate_primary_checking_acct_ind $primary_checking_acct_ind $routing_nbr $acct_nbr\n";
                        $loan_error_ind = 0;
                        $loan_warning_ind = 0;
                }
	}

                //Check to see if the number of successful records added is > 0.  If it is then udpate the file status accordingly
		if($num_successful > 0){
			mm_update_database_value('mm_ach_file', 'status', 'READY TO FORMAT', 's', 'file_nbr', $ach_file_nbr);
		}else{
			mm_update_database_value('mm_ach_file', 'status', 'NO ACH RECORDS', 's', 'file_nbr', $ach_file_nbr);
		}

        }
/*

	$ach_nacha_build_results = mm_ach_build_nacha_recs_wrapper();
	if($ach_nacha_build_results["return_value"] != 0){
		//There was a problem building the nacha records
		$return_value = 1;
		$return_err_status = $ach_nacha_build_results["return_err_status"];
		$return_err_message = $ach_nacha_build_results["return_err_message"];
	}else{
		//There wasn't a problem building the nacha records so build the nacha files(s)
		$ach_nacha_file_build_results = mm_ach_create_nacha_file_wrapper();
		if($ach_nacha_file_build_results["return_value"] != 0){
			//Insert error handling in case the file(s) couldn't be built
			$return_value = 1;
			$return_err_status = $ach_nacha_build_results["return_err_status"];
			$return_err_message = $ach_nacha_build_results["return_err_message"];
		}else{
			//The file(s) were able to be build so place them in the archive directory, FTP them and then remove them from the temporary location?
			
		}
	}
		
		echo("***FINAL STATUSES***: ");
		echo(" return value: " . $return_value);
		echo(" return message: " . $return_message);
		echo(" err_message: " . $return_err_message);
		echo(" err_status: " . $return_err_status );
*/
        $return_array["return_value"] = $return_value;
        $return_array["return_message"] = $return_message;
        $return_array["num_funding_records"] = $num_successful;
        $return_array["num_errors"] = $num_errors;
        $return_array["num_warnings"] = $num_warnings;
        $return_array["error_array"] = $return_error_array;
        $return_array["warning_array"] = $return_warning_array;
        $return_array["ach_file_nbr"] = $ach_file_nbr;
        $return_array["ach_file_name"] = $ach_file_name;
        /*$return_array["return_err_message"] = $return_err_message;
        $return_array["return_err_status"] = $return_err_status;*/

        return $return_array;

}


/****************************************************************************************
Function mm_create_ach_detail_validation_xref_record($tans_id, $file_nbr, $validation_nbr)
****************************************************************************************/
function mm_create_ach_detail_validation_xref_record($trans_id, $file_nbr, $validation_nbr){
        $return_value = 0;
        $return_message = '';
        $ach_detail_validation_xref_nbr = 0;
        $return_array = array();
        $temp_dt = new DateTime();
        $create_dt = $temp_dt->format('Y-m-d H:i:s');

         try{
                $conn = mm_get_pdo_connection();
                $sql_string = "insert into mm_ach_detail_validation_xref(trans_id, file_nbr, validation_nbr, create_dt) values (?,?,?,?)";
                $bind_params = [$trans_id, $file_nbr, $validation_nbr, $create_dt];
                $stmt = $conn->prepare($sql_string);
                $stmt->execute($bind_params);
                $ach_detail_validation_xref_nbr = $conn->lastInsertId();
        }catch(PDOException $e){
                $return_array["return_value"] = 1;
                $return_array["return_message"] = "$e";
                $return_array["ach_detail_validation_xref_nbr"] = $ach_detail_validation_xref_nbr;
                return $return_array;
        }
        $return_array["return_value"] = $return_value;
        $return_array["return_message"] = $return_message;
        $return_array["ach_detail_validation_xref_nbr"] = $ach_detail_validation_xref_nbr;
        return $return_array;
}


/*****************************************************************************************
Function mm_create_ach_detail_record($data_array)
*****************************************************************************************/
function mm_create_ach_detail_record($data_array){
        $return_value = 0;
        $return_message = '';
        $return_array = array();
        $ach_detail_nbr = 0;

        $temp_dt = new DateTime();
        $create_dt = $temp_dt->format('Y-m-d H:i:s');
        $update_dt = $create_dt;

        //Initialze all of the necessary variables
        $file_nbr = $data_array["ach_file_nbr"];
        $payment_display_nbr = $data_array["payment_display_nbr"];
        $trans_id = $data_array["trans_id"];
        $loan_nbr = $data_array["loan_nbr"];
        $trans_type = $data_array["trans_type"];
        $trans_subtype = $data_array["trans_subtype"];
        $trans_sign = $data_array["trans_sign"];
        $effective_dt = $data_array["effective_dt"];
        $customer_name = $data_array["customer_name"];
        $amount = $data_array["amount"];
        $routing_nbr = $data_array["routing_nbr"];
        $bank_acct_nbr = $data_array["acct_nbr"];
        $authorization_type = $data_array["authorization_type"];
        $lms_update_status = "NONE";
        $system_loan_id = $data_array["system_loan_id"];
        $warning_ind = $data_array["warning_ind"];
        $payment_system_id = $data_array["payment_system_id"];
        $bank_acct_type = $data_array["bank_acct_type"];
        $error_ind = $data_array["error_ind"];
        if($error_ind == 1){
                $table_name = 'mm_ach_detail_error';
        }else{
                $table_name = 'mm_ach_detail';
        }


        try{
                $conn = mm_get_pdo_connection();
                $sql_string = "insert into $table_name(file_nbr, loan_nbr, payment_display_nbr, trans_id, trans_type, trans_subtype, trans_sign, effective_dt, customer_name, amount, routing_nbr, bank_acct_nbr, bank_acct_type, authorization_type, lms_update_status, system_loan_id, warning_ind, payment_system_id, create_dt, update_dt) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                $bind_params = [$file_nbr, $loan_nbr, $payment_display_nbr, $trans_id, $trans_type, $trans_subtype, $trans_sign, $effective_dt, $customer_name, $amount, $routing_nbr, $bank_acct_nbr, $bank_acct_type, $authorization_type, $lms_update_status, $system_loan_id, $warning_ind, $payment_system_id, $create_dt, $update_dt];
                $stmt = $conn->prepare($sql_string);
                $stmt->execute($bind_params);
                $ach_detail_nbr = $conn->lastInsertId();
        }catch(PDOException $e){
                $return_array["return_value"] = 1;
                $return_array["return_message"] = "$e";
                $return_array["ach_detail_nbr"] = $ach_detail_nbr;
                return $return_array;
        }

        $return_array["return_value"] = $return_value;
        $return_array["return_message"] = $return_message;
        return $return_array;

}

/**********************************************************************************
Function mm_get_ach_endpoint($file_type)
**********************************************************************************/

function mm_get_ach_endpoint($file_type){
        $return_value = 0;
        $return_message = '';
        $return_array = '';
        $results_array = array();
        //1.  Select all application where the create date is more than 10 days ago
        if($file_type == 'C'){ //C stands for credits
                $endpoint_name = 'MMDEP';
        }else if($file_type == 'D'){ //D stands for debits
                $endpoint_name = 'MMPAY';
        }else{
                $endpoint_name = 'UNKWN';
        }


        try{
                $conn = mm_get_pdo_connection();
                $sql_string = "select * from mm_ach_endpoint where endpoint_name = ?";
                $bind_params = [$endpoint_name];
                $stmt = $conn->prepare($sql_string);
                $stmt->execute($bind_params);
                $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
                $row_count = $stmt->rowCount();
                if($row_count == 0){
                        //This is an error so set variables apprpriately
                        $return_value = 1;
                        $return_message = 'Failed to get an endpoint record from the database';
                }else if($row_count == 1){
                        foreach($results as $row){
                                foreach($row as $key=>$value){
                                        $results_array[$key] = $value;
                                }
                        }
                }else{
                        $return_value = 1;
                        $return_message = "Unexpectedly received more than 1 record from the ACH end points table";
                }

        }catch(PDOException $e){
                $return_array["return_value"] = 1;
                $return_array["return_message"] = "$e";
                return $return_array;
        }

        $conn = null;

        $return_array["return_value"] = $return_value;
        $return_array["return_message"] = $return_message;
        $return_array["results_array"] = $results_array;
        return $return_array;
}


/****************************************************************************************************
Function mm_get_ach_next_sequence_nbr($endpoint_nbr, $file_dt)
****************************************************************************************************/

function mm_get_ach_next_sequence_nbr($endpoint_nbr, $file_dt){
        $return_value = 0;
        $return_array = array();
        $return_message = '';
        $sequence_nbr = 0;

        try{
                $conn = mm_get_pdo_connection();
                $sql_string = "select max(seq_nbr) as seq_nbr from mm_ach_file where endpoint_nbr = ? and file_dt = ?";
                $bind_params = [$endpoint_nbr, $file_dt];
                $stmt = $conn->prepare($sql_string);
                $stmt->execute($bind_params);
                $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
                $row_count = $stmt->rowCount();
                if($row_count == 0){
                        //This is an error so set variables apprpriately
                        $return_value = 0;
                        $return_message = 'Failed to get an endpoint record from the database';
                        $sequence_nbr = 1;
                }else if($row_count == 1){
                        foreach($results as $row){
                                $sequence_nbr = $row["seq_nbr"] + 1;
                        }
                }else{
                        $return_value = 1;
                        $return_message = "Unexpectedly received more than 1 record from the ACH end points table";

                }

        }catch(PDOException $e){
                $return_array["return_value"] = 1;
                $return_array["return_message"] = "$e";
                $return_array["sequence_nbr"] = $sequence_nbr;
                return $return_array;
        }



        $return_array["return_value"] = $return_value;
        $return_array["return_message"] = $return_message;
        $return_array["sequence_nbr"] = $sequence_nbr;
        return $return_array;


}

/**********************************************************************************************
Function mm_create_ach_file_record($file_type, $file_dt)
**********************************************************************************************/

function mm_create_ach_file_record($file_type, $file_dt=''){
        $return_value = 0;
        $return_message = 0;
        $ach_file_nbr = 0;
	$curr_date = new DateTime();
	$tm_string = $curr_date->format("H:i:s");
        if($file_dt == ''){
                $temp_dt = new DateTime();
        }else{
                $temp_dt = new DateTime($file_dt);
        }
                $file_dt = $temp_dt->format('Y-m-d');
                //$file_tm = $temp_dt->format('H:i:s');
                $temp_dt = new DateTime();
                $create_dt = $temp_dt->format('Y-m-d H:i:s');
                $update_dt = $create_dt;

        $endpoint_arr = mm_get_ach_endpoint($file_type);
        if($endpoint_arr["return_value"] != 0){
                //Add Error Handling
                $return_value = 1;
                $return_string = $endpoint_arr["return_message"];
        }else{
                //There was an endpoint returned
                $endpoint_details = $endpoint_arr["results_array"];
                $endpoint_name = $endpoint_details["endpoint_name"];
                $endpoint_nbr = $endpoint_details["endpoint_nbr"];
                $file_name = $endpoint_details["file_name"];
                $sequence_results = mm_get_ach_next_sequence_nbr($endpoint_nbr, $file_dt);
                if($sequence_results["return_value"] != 0){
                        $return_value = 1;
                        $return_mesage = $sequence_results["return_message"];
                }else{
                        //We now have enough information to create an ACH file record
                        $sequence_nbr = $sequence_results["sequence_nbr"];
                        $file_name = $file_name . $temp_dt->format("Ymd") . "_" . $sequence_nbr . ".txt";
                          try{
                                $conn = mm_get_pdo_connection();
                                $sql_string = "insert into mm_ach_file(file_dt, file_tm, seq_nbr, endpoint_nbr, status, file_name, create_dt, update_dt) values(?,?,?,?,?,?,?,?)";
                                $bind_params = [$file_dt, $tm_string, $sequence_nbr, $endpoint_nbr, "PROCESSING", $file_name,  $create_dt, $update_dt];
                                $stmt = $conn->prepare($sql_string);
                                $stmt->execute($bind_params);
                                $ach_file_nbr = $conn->lastInsertId();

                        }catch(PDOException $e){
                                $return_array["return_value"] = 1;
                                $return_array["return_message"] = "$e";
                                $return_array["ach_file_nbr"] = 0;
                                return $return_array;
                        }

                }
        }

        $return_array["return_value"] = $return_value;
        $return_array["return_message"] = $return_message;
        $return_array["ach_file_nbr"] = $ach_file_nbr;
        $return_array["ach_file_name"] = $file_name;
        return $return_array;
}


/*********************************************************************************
Function mm_process_ach_payments()
*********************************************************************************/
function mm_process_ach_payments($apply_from_dt = '', $apply_to_dt = '', $log_from_dt='', $log_to_dt=''){
        $return_array = array();
        $return_value = 0;
        $return_message = 0;
	$num_payments = 0;
	$num_successful = 0;
	$num_errors = 0;
	$num_warnings = 0;
	$ach_file_nbr = 0;
	$ach_file_name = '';
	$return_error_array = array();
	$return_warning_array = array();


        //Set the dates
        if($apply_from_dt == ''){
                //There is no apply from date so set it to tomorrow's date.
                $temp_dt = new DateTime();
                $temp_dt = $temp_dt->modify("+1 Day");
                $apply_from_dt = $temp_dt->format('Y-m-d');
                $apply_to_dt  = $apply_from_dt;
        }


        //Create an ACH File Record
        $ach_record_array = mm_create_ach_file_record('D');
        if($ach_record_array["return_value"] != 0){
                //There was an error creating the ach file record - ADD ERROR HANDLING
                $ach_file_nbr = 0;
		$return_value = 1;
		$ach_file_return_message = $ach_record_array["return_message"];
		$return_message = "Failed creating an ach file with error $ach_file_return_message";
		$return_error_array[$num_errors] = "Failed creating an ach file with error $ach_file_return_message";
		$num_errors +=1;
        }else{
                //The record must have been successfully createdA
                $ach_file_nbr = $ach_record_array["ach_file_nbr"];
		$ach_file_name = $ach_record_array["ach_file_name"];
        }
        //Get the payment detail report
        //Need to add logic about how to set the dates for the report
        $payment_detail_array = mm_get_lp_payment_report($apply_from_dt, $apply_to_dt, $log_from_dt, $log_to_dt);
        $payment_detail_return_value = $payment_detail_array["return_value"];
        if($payment_detail_return_value != 0){
                //There was an error getting the payment report - ADD ERROR HANDLING
		$return_value = 1;
		$payment_detail_return_message = $payment_detail_array["return_message"];
		$return_message = "Failed getting the payment report with error message: $payment_detail_return_message";
		$return_error_array[$num_errors] = "Failed getting the payment report with error message: $payment_detail_return_message";
		$num_errors +=1;
        }else{
                $num_payments = $payment_detail_array["num_payments"];
                if($num_payments == 0){
                        //There were no payments so update the ach file status with NO ACH RECORDS
                        mm_update_database_value('mm_ach_file', 'status', 'NO ACH RECORDS', 's', 'file_nbr', $ach_file_nbr);
                }else{
                        //There are payments to process so create detail records for each
                        $payment_details = $payment_detail_array["payment_details"];
			$payment_error_ind = 0;
			$payment_warning_ind = 0;
                        foreach($payment_details as $payment){
                                $ach_detail = array();
                                $ach_detail["ach_file_nbr"] = $ach_file_nbr;
                                $trans_id = $payment["display_id"] . str_pad($payment["tran_display_id"],6, "0", STR_PAD_LEFT);
                                $ach_detail["trans_id"] = $trans_id;
                                $ach_detail["loan_nbr"] = $payment["display_id"];
                                $ach_detail["amount"] = $payment["tran_amt"];
                                $ach_detail["trans_type"] = 'D';
                                $ach_detail["trans_subtype"] = 'P';
                                $ach_detail["trans_sign"] = 1;
                                $ach_detail["customer_name"] = $payment["customer_name"];
                                $ach_detail["effective_dt"] = $payment["tran_apply_dt"];
                                $ach_detail["acct_nbr"] = $payment["acct_nbr"];
                                $ach_detail["routing_nbr"] = $payment["routing_nbr"];
                                $ach_detail["bank_acct_type"] = "CHK";
                                $ach_detail["authorization_type"] = "PPD";
 				$ach_detail["warning_ind"] = 0;
                                $ach_detail["payment_system_id"] = $payment["tran_id"];
                                $ach_detail["payment_display_nbr"] = $payment["tran_display_id"];
                                $ach_detail["system_loan_id"] = $payment["loan_id"];
                                $ach_detail["error_ind"] = 0;

                                 $ach_detail_result_arr = mm_create_ach_detail_record($ach_detail);
                                $ach_detail_return_value = $ach_detail_result_arr["return_value"];
                                if($ach_detail_return_value !=0){
                                        $ach_detail_return_message = $ach_detail_result_arr["return_message"];
					$return_error_array[$num_errors] = "Failed inserting an ach detail record for $trans_id.  Error is: $ach_detail_return_message";
					$num_errors +=1;
                                        $payment_error_ind = $ach_detail["error_ind"];
                                        if($payment_error_ind == 0){
                                                //Failed entering the record and there no previous errors so try to load it to the ach detail array
                                                $ach_detail["error_ind"] = 1;
                                                $ach_detail_result_arr = mm_create_ach_detail_record($ach_detail);
                                                $ach_detail_return_value = $ach_detail_result_arr["return_value"];
                                                if($ach_detail_return_value != 0){
                                                        //Tried inserting an error record but failed
							$ach_detail_return_message = $ach_detail_result_arr["return_message"];
							$return_error_array[$num_errors] = "Failed inserting a detail error record for transacton $trans_id.  Error is: $ach_detail_return_message";  
							$num_errors +=1;
                                                }else{
                                                        //Successfully inserted the record in the detail error.  Add a validation record with a value of 1 for tracking
                                                        $function_results = mm_create_ach_detail_validation_xref_record($ach_detail["trans_id"], $ach_file_nbr, 1);
							$function_return_value = $function_results["return_value"];
							$function_return_messge = $function_results["return_value"];
							if($function_return_value !=0){
								$return_error_array[$num_errors] = "Failed inserting a validation xref record for $trans_id and error type 1";
								$num_errors +=1;
							}
                                                }
                                        }else{
                                                //Failed adding a record to the detail error table
                                                        $function_results = mm_create_ach_detail_validation_xref_record($ach_detail["trans_id"], $ach_file_nbr, 1);
							$function_return_value = $function_results["return_value"];
							$function_return_messge = $function_results["return_value"];
							if($function_return_value !=0){
								$return_error_array[$num_errors] = "Failed inserting a validation xref record for $trans_id and error type 1";
								$num_errors +=1;
							}
                                        }
                                }
				if($ach_detail["error_ind"] == 0 ){
					$num_successful +=1;
				}
				$payment_error_ind = 0;
				$payment_warning_ind = 0;
                        }
                }
        }
        //If there were successful records inserted successfully then update the status of the ACH file to READY TO FORMAT
        	/*
		// DK BEGIN ADD TEMP BLOCK HERE
		
		$sql_string = "SELECT * FROM mm_ach_detail
							   WHERE file_nbr = '$ach_file_nbr'";
        $conn = mm_get_pdo_connection();					
		$detailresults = $conn->query($sql_string);
		$detailrecs = $detailresults->fetchAll();
		$num_successful = count($detailrecs);

       		*/ 
      //Check to see if the number of successful records added is > 0.  If it is then udpate the file status accordingly
		if($num_successful > 0){
			mm_update_database_value('mm_ach_file', 'status', 'READY TO FORMAT', 's', 'file_nbr', $ach_file_nbr);
		}else{
			mm_update_database_value('mm_ach_file', 'status', 'NO ACH RECORDS', 's', 'file_nbr', $ach_file_nbr);
		}
		
		// DK END TEMP BLOCK HERE
		
		
        $return_array["return_value"] = $return_value;
        $return_array["return_message"] = $return_message;
	$return_array["num_payments"] = $num_payments;
	$return_array["num_successful"] = $num_successful;
	$return_array["num_errors"] = $num_errors;
	$return_array["num_warnings"] = $num_warnings;
	$return_array["error_array"] = $return_error_array;
	$return_array["warning_array"] = $return_warning_array;
        return $return_array;
}



/*******************************************************************************************
Function mm_get_lp_payment_report($apply_from_dt='', $apply_to_dt='', $log_from_dt='', $log_to_dt=''){
*******************************************************************************************/
function mm_get_lp_payment_report($apply_from_dt='', $apply_to_dt='', $log_from_dt='', $log_to_dt=''){
        $return_array = array();
        $payment_details = array();
        $return_value = 0;
        $return_message = '';
        $num_payments = 0;
        $lp_credentials = mm_get_lp_credentials();
        //$lp_environment = mm_get_lp_environment_portfolio();
        $account_token = $lp_credentials["token"];
        $auto_pal_id = $lp_credentials["tenant_id"];
        $url = "https://loanpro.simnang.com/api/public/api/1/Autopal.PaymentReport?\$top=500000";
        $header_array = array("Content-Type: application/json", 'Accept: application/json', "Authorization: $account_token", "Autopal-Instance-ID: $auto_pal_id");

        //Build Dates
        if($apply_from_dt == ''){
                $temp_dt = new DateTime();
                $temp_dt = $temp_dt->modify("+1 Day");
                $apply_from_dt = $temp_dt->format('Y-m-d');
        }
        if($apply_to_dt == ''){
                $apply_to_dt = $apply_from_dt;
        }
        $apply_from_dt = $apply_from_dt . "T00:00:00";
        $apply_to_dt = $apply_to_dt . "T00:00:00";
        if($log_from_dt != ''){
                $log_from_dt = $log_from_dt . "T00:00:00";
        }
        if($log_to_dt != ''){
                $log_to_dt = $log_to_dt . "T23:23:59";
        }

        //Build the nested array necessary for the body of the request
        $request_array = array();
        $query1_array = array();
        $query_2_array = array();
        $bool_array = array();
        $must_array = array();
        $report_options_array = array();
        $temp_array = array();
        $temp_array2 = array();
        $temp_array3 = array();

        //Build the must array
        //$temp_array["match"]["loanStatusId"] = "2";
        //$temp_array2["match"]["portfolios"] = "$lp_environment";
        $temp_array2["match"]["portfolios"] = "6";
	$temp_array3["match"]["subPortfolios.portfolio__6"] = "40";
        //$must_array[0] = $temp_array;
        $must_array[0] = $temp_array2;
        $must_array[1] = $temp_array3;

        //Setup the bool array
        $bool_array["must"] = $must_array;

        //Setup the report options array
        $report_options["method"] = "Bank Account";
        $report_options["type"] = "all";
        $report_options["status"] = "new";
        $report_options["period"] = "other";
        $report_options["dateFrom"] = "$apply_from_dt";
        $report_options["dateTo"] = "$apply_to_dt";
        $report_options["changedPeriod"] = "other";
        if($log_from_dt != ''){
                $report_options["changedDateFrom"] = "$log_from_dt";
                $report_options["changedDateTo"] = "$log_to_dt";
        }
        $report_options["chargeOff"] = "no";
        //Setup the query array
        $query_2_array["bool"] = $bool_array;

        //Setup the query
        $query_1_array["query"] = $query_2_array;

        //Setup request array
        $request_array["query"] = $query_1_array;
        $request_array["reportOptions"] = $report_options;


        $json_request = json_encode($request_array);
        echo "$json_request\n";



        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HTTPHEADER,$header_array );
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $json_request);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $json_response = curl_exec($curl);

        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        if ( $status != 201 && $status != 200 ) {
                //There was an error in the message sending  Add error handling
                $i = 0;
                $return_value = 1;
                $return_message = "Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl);
        }else{
                echo "$json_response\n";
                $response_array = json_decode($json_response, true);
                $d_array = $response_array["d"];
		$i = 0;
		if(!isset($d_array["summary"])){
			//There are no results
			$total_hits = 0;
		}else{
                	$summary_data = $d_array["summary"];
                	$total_hits = $d_array["totalHits"];
                	$report_data = $d_array["reportData"];
                	foreach($report_data as $payment){
                        	//get the ID and output it
                        	$payment_id = $payment["tranId"];
                        	$loan_id = $payment["loanId"];
                        	$tran_display_id = $payment["tranDisplayId"];
                        	$tran_amt = $payment["tranAmount"];
                        	$display_id = $payment["displayId"];
                        	$customer_name = $payment["primaryCustomerName"];
                        	$payment_details_arr = mm_get_lp_payment_details($payment_id);
                        	//Add error handling regarding return value
                        	$payment_details_return_value = $payment_details_arr["return_value"];
                        	$payment_details_return_message = $payment_details_arr["return_message"];
                        	$acct_nbr = $payment_details_arr["acct_nbr"];
                        	$routing_nbr = $payment_details_arr["routing_nbr"];
                        	$tran_apply_dt = $payment["tranApplyDate"];
	
                        	//Set the return array values
                        	$payment_details[$i]["tran_id"] = $payment_id;
                        	$payment_details[$i]["loan_id"] = $loan_id;
                        	$payment_details[$i]["tran_display_id"] = $tran_display_id;
                        	$payment_details[$i]["tran_amt"] = $tran_amt;
                        	$payment_details[$i]["display_id"] = $display_id;
                        	$payment_details[$i]["acct_nbr"] = $acct_nbr;
                        	$payment_details[$i]["routing_nbr"] = $routing_nbr;
                        	$payment_details[$i]["customer_name"] = $customer_name;
                        	$payment_details[$i]["tran_apply_dt"] = $tran_apply_dt;
	
                        	$i+=1;
   			}
                	$return_value = 0;
                	$return_message = '';
        	}
	}

        $return_array["return_value"] = $return_value;
        $return_array["return_message"] = $return_message;
        $return_array["payment_details"] = $payment_details;
        $return_array["num_payments"] = $i;
        return $return_array;


}

/**********************************************************************************************
Function mm_get_lp_payment_details($payment_nbr)
**********************************************************************************************/

function mm_get_lp_payment_details($payment_nbr){
        $return_array = array();
        $return_value = 0;
        $acct_nbr = 0;
        $routing_nbr = 0;
        $return_message = '';


         $lp_credentials = mm_get_lp_credentials();
        $account_token = $lp_credentials["token"];
        $auto_pal_id = $lp_credentials["tenant_id"];
        $url = "https://loanpro.simnang.com/api/public/api/1/odata.svc/Payments(id=$payment_nbr)?\$expand=PaymentAccount/CheckingAccount&\$select=active,info,PaymentAccount/CheckingAccount/accountNumber,PaymentAccount/CheckingAccount/routingNumber,PaymentAccount/isPrimary,PaymentAccount/type,PaymentAccount/active";
        $header_array = array("Content-Type: application/json", 'Accept: application/json', "Authorization: $account_token", "Autopal-Instance-ID: $auto_pal_id");
        $request_array = array();
        $json_request = json_encode($request_array);

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HTTPHEADER,$header_array );
        curl_setopt($curl, CURLOPT_POST, false);
        curl_setopt($curl, CURLOPT_HTTPGET, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $json_response = curl_exec($curl);

        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        if ( $status != 201 && $status != 200 ) {
                //There was an error in the message sending  Add error handling
                $return_value = 1;
                $return_message = "Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl);
        }else{
                $response_array = json_decode($json_response, true);
                $d_array = $response_array["d"];
                $payment_acct_array = $d_array["PaymentAccount"];
                $checking_acct_array = $payment_acct_array["CheckingAccount"];
                $acct_nbr = $checking_acct_array["accountNumber"];
                $routing_nbr = $checking_acct_array["routingNumber"];

                $return_value = 0;
                $return_message = '';
        }

        $return_array["return_value"] = $return_value;
        $return_array["return_message"] = $return_message;
        $return_array["acct_nbr"] = $acct_nbr;
        $return_array["routing_nbr"] = $routing_nbr;
        return $return_array;
}



/******************************************************************************************************
Function mm_activate_lp_loan($loan_id)
******************************************************************************************************/
function mm_activate_lp_loan($loan_id=''){
        $return_value = 0;
        $return_message = '';
        $return_array = array();
        $request_array = array();
        $loan_settings = array();
        $lp_login_details = mm_get_lp_credentials();
        $account_token = $lp_login_details["token"];
        $auto_pal_id = $lp_login_details["tenant_id"];

	//First get the loan settings id for the loan

                $url = "https://loanpro.simnang.com/api/public/api/1/odata.svc/Loans(id=$loan_id)?\$select=settingsId";
                $header_array = array("Content-Type: application/json", 'Accept: application/json', "Authorization: $account_token", "Autopal-Instance-ID: $auto_pal_id");

                $json_request = json_encode($request_array);
                $curl = curl_init($url);
                curl_setopt($curl, CURLOPT_HTTPHEADER,$header_array );
                curl_setopt($curl, CURLOPT_POST, false);
                curl_setopt($curl, CURLOPT_HTTPGET, true);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

                $json_response = curl_exec($curl);

                $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

                if ( $status != 201 && $status != 200 ) {
                        //There was an error in the message sending  Add error handling
                        $return_value = 1;
                        $return_message = "Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl);
                }else{
                        $response_array = json_decode($json_response, true);
			if(!isset($response_array["d"])){
				if(isset($response_array["error"])){
					$error_array = $response_array["error"];
					$message_array = $error_array["message"];	
					$error_message = $message_array["value"];	
					$return_value =1;
					$return_message = "Unable to get Loan Settings ID with error message: $error_message";
				}else{
					$return_value = 1;
					$return_message = "Unable to determine error reason: $json_response\n";
				}
				
			}else{
                        	$d_array = $response_array["d"];
                        	$settings_id = $d_array["settingsId"];

				//Now that we have the settings id, update the loan settings appropriately.	
                        	$loan_settings["loanStatusId"] = "2";
                        	$loan_settings["loanSubStatusId"] = "9";
                        	$loan_settings["autopayEnabled"] = "1";
                        	$loan_settings["__id"] = $settings_id;
                        	$loan_settings["__update"] = true;
                        	$request_array["LoanSettings"] = $loan_settings;
	
                        	$json_request = json_encode($request_array);
                        	$url = "https://loanpro.simnang.com/api/public/api/1/odata.svc/Loans($loan_id)";
                        	$curl = curl_init($url);
                        	curl_setopt($curl, CURLOPT_HTTPHEADER,$header_array );
                        	curl_setopt($curl, CURLOPT_POST, true);
                         	curl_setopt($curl, CURLOPT_POSTFIELDS, $json_request);
                        	curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
                        	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

                        	$json_response = curl_exec($curl);
                        	$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	
                        	if ( $status != 201 && $status != 200 ) {
                                	//There was an error in the message sending  Add error handling
                                	$return_value = 1;
                                	$return_message = "Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl);
                        	}else{
					//NEED TO ADD SOME ERROR HANDLING HERE
					//Set the application sub status to 2 which means loan has been activated in loan pro
					//mm_update_database_value("mm_application", 'application_sub_status', 2, 'i', 'application_nbr', $application_nbr);
					$results_array = json_decode($json_response);
					$return_value = 0;
					$return_message = "";
					
					
                        	}
                }
	}
        	$return_array["return_value"] = $return_value;
        	$return_array["return_message"] = $return_message;

        	return $return_array;
}




function mm_lp_create_autopays($display_id = '', $loan_id = ''){
	$return_array = array();
	$return_value = 0;
	$return_message = '';
	$num_errors = 0;
	$counter = 0;
	$error_array = array();
	$lp_credentials = mm_get_lp_credentials();
        $account_token = $lp_credentials["token"];
        $auto_pal_id = $lp_credentials["tenant_id"];
        $header_array = array("Content-Type: application/json", 'Accept: application/json', "Authorization: $account_token", "Autopal-Instance-ID: $auto_pal_id");

        $request_array = array();
	$application_nbr = substr($display_id, 1)*1;
        $application_array = mm_get_application_details($application_nbr);
        if($application_array["return_value"] !=0){
                //There was an error getting the application data
                echo "Failed Getting the app data for $application_nbr\n";
        }else{
                $app_data = $application_array["app_data"];
                $loan_agreement_nbr = $app_data["loan_agreement_nbr"];
                $payment_schedule_array = mm_get_payment_schedule($loan_agreement_nbr);
                if($payment_schedule_array["return_value"] !=0){
                        //There was an error gettingt the payment schedule
                        echo "<br>Failed getting the payment schedule for loan agreement $loan_agreement_nbr<br>" ;
                }else{
                        $payment_schedule = $payment_schedule_array["payment_schedule"];
                        $url = "https://loanpro.simnang.com/api/public/api/1/odata.svc/Loans($loan_id)?\$expand=Customers/PaymentAccounts&\$select=Customers/PaymentAccounts/id,Customers/PaymentAccounts/isPrimary,Customers/PaymentAccounts/active,Customers/PaymentAccounts/type";

                        $curl = curl_init($url);
                        curl_setopt($curl, CURLOPT_HTTPHEADER,$header_array );
                        curl_setopt($curl, CURLOPT_POST, false);
                        curl_setopt($curl, CURLOPT_HTTPGET, true);
                        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

                        $json_response = curl_exec($curl);

                        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		
			
                        if ( $status != 201 && $status != 200 ) {
                                //There was an error in the message sending  Add error handling
                                $return_value = 1;
                                $return_message = "Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl);
                        }else{
                                $response_array = json_decode($json_response, true);
                                $primary_acct_url = isset($response_array["d"]["Customers"]["results"][0]["PaymentAccounts"]["results"][0]["__metadata"]["uri"]) ? $response_array["d"]["Customers"]["results"][0]["PaymentAccounts"]["results"][0]["__metadata"]["uri"] : "";
                                $primary_acct_id = isset($response_array["d"]["Customers"]["results"][0]["PaymentAccounts"]["results"][0]["id"]) ? $response_array["d"]["Customers"]["results"][0]["PaymentAccounts"]["results"][0]["id"] : "";
                                $primary_acct_type = isset($response_array["d"]["Customers"]["results"][0]["PaymentAccounts"]["results"][0]["type"]) ? $response_array["d"]["Customers"]["results"][0]["PaymentAccounts"]["results"][0]["type"] : "";
                                if($primary_acct_url == ''){
                                        //There was a problem getting a primary acct url so throw an error;
                                        $return_value = 1;
                                        $return_message = "No payment account returned for $display_id.  The return message is $json_response";
                                }else{
                                        //There was a payment account found so continue
					                    $num_payments = sizeof($payment_schedule);
					                    $payment = $payment_schedule[0];
                                        $payment_nbr = $payment["payment_nbr"];
                                        $pay_frequency = $app_data["pay_frequency_1"];
                                        $payment_dt = $payment["payment_dt"];
                                        $payment_amt = $payment["payment_amt"];
                                        $temp_dt = new DateTime($payment_dt);
                                        $temp_dt2 = new DateTime($payment_dt);
                                        $temp_dt2 = $temp_dt2->modify("-1 Day");
                                        $curr_dttime = new DateTime();
                                        $apply_dttime = $temp_dt;
                                        $apply_dt = $apply_dttime->format("Y-m-d");
                                        $process_dt = $temp_dt2->format("Y-m-d");
                                        $process_dttime = $temp_dt2;

                					 if($pay_frequency == "Bi-Weekly"){
                                           $pay_freq_txt = 'biWeekly';
                                          $payment_day = $apply_dttime->format('D');
                                     }else if($pay_frequency == "Paid Monthly"){
                                             $pay_freq_txt = 'Monthly';
                                            $payment_day = $apply_dttime->format('j');
                                     }else if($pay_frequency == "Paid Twice Monthly"){
                                            $pay_freq_txt = 'semiMonthly';
                                            $payment_day = $apply_dttime->format('j');
                                     }else if($pay_frequency == "Paid Weekly"){
                                            $pay_freq_txt = 'biWeekly';
                                            $payment_day = $apply_dttime->format('D');
                                    }else{
                                           $pay_freq_txt = 'error';
                                     }
                                                    //echo "Processing payment for $payment_dt and apply date of $apply_dt with payment day $payment_day and process_dt is $process_dt<br>";
                                         $temp_array["name"] = "Scheduled: $payment_nbr: $pay_freq_txt: $payment_day";
                                         $temp_array["type"] = "autopay.type.recurringMatch";
                                         $temp_array["paymentExtraTowards"] = "payment.extra.tx.classic";
                                         $temp_array["amountType"] = "autopay.amountType.static";
                                         $temp_array["amount"] = $payment_amt;
                                         $temp_array["paymentType"] = 8;
                                         $temp_array["chargeServiceFee"] = "0";
                                         $temp_array["processCurrent"] = 1;
                                         $temp_array["retryDays"] = 0;
                                         $temp_array["processTime"] = 16;
                                         $temp_array["postPaymentUpdate"] = 1;
                                         $temp_array["applyDate"] = "$apply_dt" ;
                                         $temp_array["processDate"] = "$process_dt";
                                         $temp_array["methodType"] = "autopay.methodType.echeck";
                                         $temp_array["recurring_frequency"] = "autopay.recurringFrequency." . $pay_freq_txt;
                                         $temp_array["recurringDateOption"] = "autopay.recurringDate.processDate";
                                         $temp_array["schedulingType"] = "auto.schedulingType.calendarDay";
                                         $temp_array["processDateCondition"] = "calendarDays";
                                         $temp_array["payoffAdjustment"] = 1;
                                         $temp_array["chargeOffRecovery"] = 0;
                                         $temp_array["paymentMethodAuthType"] = "payment.echeckauth.PPD";
                                         $temp_array["paymentMothodAccountType"] = "bankacct.type.checking";
                                         $temp_array["processZeroOrNegativeBalance"] = 0;


                                                        //prepare the payment account array
                                        $payment_acct_array["__metadata"]["type"] = "Entity.PaymentAccount";
                                        $payment_acct_array["__metadata"]["uri"] = $primary_acct_url;
                                        $payment_acct_array["id"] = $primary_acct_id;
                                        $payment_acct_array["type"] = $primary_acct_type;
                                        $temp_array["PrimaryPaymentMethod"] = $payment_acct_array;
                                        $temp_array["recurringPeriods"] = $num_payments;
                                        $temp_array["lastDayOfMonthEnabled"] = 0;
                                        $temp_array["mcProcessor"] = "{\"bankAccount\":{\"id\":\"1\",\"key\":\"nacha\",\"name\":\"ASPENFINPAY\",\"default\":\"1\"}}";
                                        $temp_array["baProcessor"] = 3;
                                        $temp_array["processDateTime"] = $process_dt . " 21:00:00"; //4PM CST but this is UTC
                                        $temp_array["PaymentType"]["__metadata"]["type"] = "Entity.CustomPaymentType";
                                        $temp_array["PaymentType"]["__metadata"]["uri"] = "/api/1/odata/svc/CustomerPaymentTypes(id=)";

                                        //Set the results array
                                        //$results_array[0] = $temp_array;

                                        //Set the Autopays Array
                                        $autopays_array["results"][0] = $temp_array;
                                        $counter+=1;
                                        //Set the request_array
                                        $request_array["Autopays"] = $autopays_array;

                                        $json_request = json_encode($request_array);
                                        //      echo "$json_request<br><br>";

                                        if($counter >0){
                                            $url = "https://loanpro.simnang.com/api/public/api/1/odata.svc/Loans($loan_id)";
                                            $curl = curl_init($url);
                                            curl_setopt($curl, CURLOPT_HTTPHEADER,$header_array );
                                            curl_setopt($curl, CURLOPT_POST, true);
                                            curl_setopt($curl, CURLOPT_POSTFIELDS, $json_request);
                                            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
                                            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                                            $json_response = curl_exec($curl);
                    						$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

                                            if ( $status != 201 && $status != 200 ) {
                                                    //There was an error in the message sending  Add error handling
                                                    $return_value = 1;
                                                    $error_array[$num_errors] = "Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl);
                                                    $num_errors +=1;
                                            }else{
                                                    //NEED TO ADD SOME ERROR HANDLING HERE
                                                    //$num_payments_added +=1;
                                            }
                                        }


                                }
                        }
                }

        }


        $return_array["return_value"] = $return_value;
        $return_array["num_errors"] = $num_errors;
        $return_array["num_successful"] = $counter;
        $return_array["error_array"] = $error_array;
        $return_array["return_message"] = $return_message;
        return $return_array;

}

?>
