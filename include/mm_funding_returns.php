<?php
//require_once 'mm_system_config.php';
require_once 'mm_middleware.php';

class mm_funding_returns
{
    public function mm_start_fundingreturn_process($display_id)
    {
        error_log("Begin funding return process for: $display_id");
        //Get loans loanpro id from loan_agreement table
        $lp_loan_id = $this->mm_fetch_lp_loan_id($display_id);
        $lp_loansettings = $this->lp_get_loansettings($lp_loan_id);
        $lp_stats = $this->lp_get_admin_stats($lp_loan_id);
        $lp_loansettings_id = $lp_loansettings['id'];
        $lp_loansubstatus = $lp_loansettings['LoanSubStatus']['id'];

        if ($lp_stats['principalBalance'] <= 0) {
            return "Error: loan has balance of $0.00 ID: $display_id";
        }
        if ($lp_loansubstatus == 17) {
            return "Error: loan is already in funding return ID: $display_id";
        }

        //log payment to loanpro
        $logPaymentResult = $this->mm_lp_log_payment($lp_loan_id, $display_id);
        error_log("End logging payment in LP for: $display_id");
        if ($logPaymentResult != 201 && $logPaymentResult != 200) {
            return "Error logging payment for ID: $display_id";
        }

        //cancel autopay for loan
        $cancelAutoPayResponse = $this->mm_lp_cancel_autopay($lp_loan_id);
        error_log("End cancel autopay LP for: $lp_loan_id");
        if ($cancelAutoPayResponse != 201 && $cancelAutoPayResponse != 200) {
            return "Error canceling autopay for ID: $display_id";
        }

        //Change loan settings to set loan status to paid off and sub status to funding return
        $loanStatusChangesResponse = $this->mm_lp_change_loanstatuses($lp_loan_id, $lp_loansettings_id);
        error_log("End change loan statuses in LP for: $lp_loan_id");
        if ($loanStatusChangesResponse != 201 && $loanStatusChangesResponse != 200) {
            return "Error changing loan statuses for ID: $display_id";
        }

        //Insert record to mm_pendingfundingreturn_queue table to later be picked up by job ___ and archive loan in Loanpro
        $insertResult = $this->mm_insert_pendingfundingreturn($lp_loan_id, $lp_loansettings_id);
        if($insertResult != true) {
            return "Error inserting record to mm_pendingfundingreturn_queue table for ID: $display_id";
        }

        //Set payments profile to inactive
        $paymentProfileResponse = $this->mm_lp_setpaymentprofile_Inactive($lp_loan_id);
        error_log("End change payment profile in LP for: $lp_loan_id");
        if ($paymentProfileResponse != 201 && $paymentProfileResponse != 200) {
            return "Error setting payment profile to inactive for ID: $display_id";
        }

        //Suspend interest for account from funding date
        $suspendInterestResponse = $this->mm_lp_suspend_interest($lp_loan_id, $display_id);
        error_log("End suspend interest in LP for: $lp_loan_id");
        if ($suspendInterestResponse != 201 && $suspendInterestResponse != 200) {
            return "Error suspending interest for ID: $display_id";
        }
        // Commenting out at Surekhas request. We no longer want to expire the applications
//    $expireAppResponse = mm_update_expire_application($display_id);
//    if($expireAppResponse !== true)
//    {
//        return $expireAppResponse;
//    }

        error_log("End funding return process for: $display_id");
        return true;
    }

    function mm_lp_log_payment($lp_loan_id, $display_id)
    {
        error_log("Begin logging payment in LP for: $display_id");
        //fetch origination amount from mm_loan_agreement table
        $paymentAmount = $this->mm_fetch_principal_amt($display_id);
        //prepare API body
        $apiBody = $this->prepare_fundingreturn_payment_apibody($paymentAmount);
        //Make API call to put Funding Return payment in Loan Pro

        return $this->mm_lp_put_loan_call($lp_loan_id, $apiBody);
    }

    function mm_fetch_lp_loan_id($display_id)
    {
        $conn = mm_get_pdo_connection();
        $stmt = $conn->prepare('SELECT lp_loan_id FROM mm_loan_agreement WHERE loan_nbr = ? ORDER BY create_dt DESC');
        $stmt->execute([$display_id]);
        $lp_loan_id = $stmt->fetchAll();

        return $lp_loan_id[0]['lp_loan_id'];
    }

    function mm_update_expire_application($display_id)
    {
        try {
            error_log("Begin expire application for: $display_id");
            //fetch latest application number of loan
            $conn = mm_get_pdo_connection();
            $stmt = $conn->prepare('SELECT application_nbr FROM mm_loan_agreement WHERE loan_nbr = ? ORDER BY create_dt DESC');
            $stmt->execute([$display_id]);
            $appnbr = $stmt->fetchAll();
            $appnbr = $appnbr[0]['application_nbr'];

            //Update application status to 11 for "EXPIRED"
            $stmt = $conn->prepare('UPDATE mm_application SET application_status = 10 WHERE application_nbr = ?');
            $stmt->execute([$appnbr]);

            error_log("End expire application for: $display_id");
        } catch (PDOException $e) {
            return "Error expiring application for ID: $display_id";
        }
        return true;
    }

    function mm_fetch_principal_amt($display_id)
    {
        $conn = mm_get_pdo_connection();
        $stmt = $conn->prepare('SELECT principal_amt FROM mm_loan_agreement WHERE loan_nbr = ? ORDER BY create_dt DESC');
        $stmt->execute([$display_id]);
        $paymentAmount = $stmt->fetchAll();

        return $paymentAmount[0]['principal_amt'];
    }

    function mm_insert_pendingfundingreturn($lp_loan_id, $lp_loanSetting_id)
    {
        $sql_string = 'INSERT INTO mm_pendingfundingreturn_queue SET lp_loan_id = ?, lp_loanSetting_id = ?, set_date = ?, flag = false';
        try {
            $date = new DateTime("now");
            $conn = mm_get_pdo_connection();
            $stmt = $conn->prepare($sql_string);
            $stmt->execute([$lp_loan_id, $lp_loanSetting_id, $date->format("Y-m-d")]);
        } catch (PDOException $e){
            error_log("error in sql: $sql_string\n");
            error_log("error in sql: $e\n");
            return false;
        }
        return true;
    }

    function mm_fetch_funding_date($display_id)
    {
        $conn = mm_get_pdo_connection();
        $stmt = $conn->prepare('SELECT effective_dt FROM mm_loan_agreement WHERE loan_nbr = ? ORDER BY create_dt DESC');
        $stmt->execute([$display_id]);
        $fundingDate = $stmt->fetchAll();

        return $fundingDate[0]['effective_dt'];
    }

    function prepare_fundingreturn_payment_apibody($paymentAmount)
    {
        $date = date('Y-m-d');
        $payments = array('Payments' => array(
            'results' => array(0 => array(
                'selectedProcessor' => '0',
                'paymentMethodId' => 13,
                'amount' => $paymentAmount,
                'date' => $date,
                'info' => "$date Funding Return",
                'paymentTypeId' => 2,
                'active' => 1,
                'resetPastDue' => 0,
                'payOffPayment' => true,
                'extra' => 'payment.extra.tx.principal',
                '__logOnly' => true,
                'payoffFlag' => 1
            ))
        ));
        return json_encode($payments);
    }

    function mm_lp_put_loan_call($lp_loan_id, $apiBody)
    {
        $lp_credentials = mm_get_lp_credentials();
        $account_token = $lp_credentials["token"];
        $auto_pal_id = $lp_credentials["tenant_id"];
        $header_array = array("Content-Type: application/json", 'Accept: application/json', "Authorization: $account_token", "Autopal-Instance-ID: $auto_pal_id");
        $url = "https://loanpro.simnang.com/api/public/api/1/odata.svc/Loans($lp_loan_id)";
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header_array);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $apiBody);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        curl_exec($curl);
        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        return $status;
    }

    function mm_lp_put_customer_call($lp_customer_id, $apiBody)
    {
        $lp_credentials = mm_get_lp_credentials();
        $account_token = $lp_credentials["token"];
        $auto_pal_id = $lp_credentials["tenant_id"];
        $header_array = array("Content-Type: application/json", 'Accept: application/json', "Authorization: $account_token", "Autopal-Instance-ID: $auto_pal_id");
        $url = "https://loanpro.simnang.com/api/public/api/1/odata.svc/Customers($lp_customer_id)";
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header_array);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $apiBody);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        curl_exec($curl);

        return $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    }

    function mm_lp_cancel_autopay($lp_loan_id)
    {
        error_log("Begin cancel autopay LP for: $lp_loan_id");
        $autoPayId = $this->lp_get_autopayid($lp_loan_id);
        $apiBody = $this->prepare_cancel_autopay_body($autoPayId);
        return $this->mm_lp_put_loan_call($lp_loan_id, $apiBody);
    }

    function lp_get_autopayid($lp_loan_id)
    {
        $lp_credentials = mm_get_lp_credentials();
        $account_token = $lp_credentials["token"];
        $auto_pal_id = $lp_credentials["tenant_id"];
        $header_array = array("Content-Type: application/json", 'Accept: application/json', "Authorization: $account_token", "Autopal-Instance-ID: $auto_pal_id");
        $url = "https://loanpro.simnang.com/api/public/api/1/odata.svc/Loans($lp_loan_id)?\$expand=Autopays";
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header_array);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPGET, true);
        $json_response = curl_exec($curl);
        $lp_loan = json_decode($json_response, 1);
        $d = $lp_loan["d"];
        $autoPayId = $d['Autopays']['results'][0]['id'];
        return $autoPayId;
    }

    function prepare_cancel_autopay_body($autoPayId)
    {
        $autoPay = array('Autopays' => array(
            'results' => array(
                0 => array(
                    'id' => $autoPayId,
                    'status' => 'autopay.status.cancelled',
                    '__id' => $autoPayId,
                    '__update' => true
                )
            )
        ));

        return json_encode($autoPay);
    }

    function mm_lp_change_loanstatuses($lp_loan_id, $lp_loanSetting_id)
    {
        error_log("Begin change loan statuses in LP for: $lp_loan_id");
        $apiBody = $this->prepare_fundingreturn_changeloansettings_body($lp_loanSetting_id);
        $returnVal = $this->mm_lp_put_loan_call($lp_loan_id, $apiBody);

        return $returnVal;
    }

    function lp_get_admin_stats($lp_loan_id)
    {
        $lp_credentials = mm_get_lp_credentials();
        $account_token = $lp_credentials["token"];
        $auto_pal_id = $lp_credentials["tenant_id"];
        $header_array = array("Content-Type: application/json", 'Accept: application/json', "Authorization: $account_token", "Autopal-Instance-ID: $auto_pal_id");
        $url = "https://loanpro.simnang.com/api/public/api/1/Loans($lp_loan_id)/Autopal.GetAdminStats(active)";
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header_array);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPGET, true);
        $json_response = curl_exec($curl);
        $lp_stats = json_decode($json_response, 1);
        $lp_stats = $lp_stats['d'];
        return $lp_stats['active'];
    }

    function lp_get_loansettings($lp_loan_id)
    {
        $lp_credentials = mm_get_lp_credentials();
        $account_token = $lp_credentials["token"];
        $auto_pal_id = $lp_credentials["tenant_id"];
        $header_array = array("Content-Type: application/json", 'Accept: application/json', "Authorization: $account_token", "Autopal-Instance-ID: $auto_pal_id");
        $url = "https://loanpro.simnang.com/api/public/api/1/odata.svc/Loans($lp_loan_id)?\$expand=LoanSettings,LoanSettings/LoanSubStatus";
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header_array);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPGET, true);
        $json_response = curl_exec($curl);
        $lp_loan = json_decode($json_response, 1);
        $d = $lp_loan["d"];
        $loanSettings = $d['LoanSettings'];
        return $loanSettings;
    }

    function prepare_fundingreturn_changeloansettings_body($lp_loanSetting_id)
    {
        $loanSettingsBody = array('LoanSettings' => array(

            'loanStatusId' => '3',
            'loanSubStatusId' => '49',
            '__id' => $lp_loanSetting_id,
            '__update' => true
        ));

        return json_encode($loanSettingsBody);
    }

    function mm_lp_setpaymentprofile_Inactive($lp_loan_id)
    {
        $customerDetails = $this->lp_get_customerdetails_by_loanid($lp_loan_id);
        $apiBody = $this->prepare_paymentaccountchange_body($customerDetails);
        return $this->mm_lp_put_customer_call($customerDetails['customerId'], $apiBody);
    }

    function lp_get_customerdetails_by_loanid($lp_loan_id)
    {
        $lp_credentials = mm_get_lp_credentials();
        $account_token = $lp_credentials["token"];
        $auto_pal_id = $lp_credentials["tenant_id"];
        $header_array = array("Content-Type: application/json", 'Accept: application/json', "Authorization: $account_token", "Autopal-Instance-ID: $auto_pal_id");
        $url = "https://loanpro.simnang.com/api/public/api/1/odata.svc/Loans($lp_loan_id)?\$expand=Customers,Customers/PaymentAccounts";
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header_array);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPGET, true);
        $json_response = curl_exec($curl);
        $lp_loan = json_decode($json_response, 1);
        $d = $lp_loan["d"];
        $returnArr = array();
        $returnArr['paymentAccountId'] = $d['Customers']['results'][0]['PaymentAccounts']['results'][0]['id'];
        $returnArr['customerId'] = $d['Customers']['results'][0]['id'];

        return $returnArr;
    }

    function prepare_paymentaccountchange_body($customerDetails)
    {
        $paymentAccountsBody = array(
            'PaymentAccounts' => array(
                'results' => array(
                    0 => array(
                        'active' => 0,
                        '__id' => $customerDetails['paymentAccountId'],
                        '__update' => true
                    )
                )
            )
        );

        return json_encode($paymentAccountsBody);
    }

    function mm_lp_suspend_interest($lp_loan_id, $display_id)
    {
        $funding_date = $this->mm_fetch_funding_date($display_id);
        $apiBody = $this->prepare_suspend_interest_apibody($funding_date);
        return $this->mm_lp_put_loan_call($lp_loan_id, $apiBody);
    }

    function prepare_suspend_interest_apibody($funding_date)
    {
        $data = array(
            "StopInterestDates" => array(
                "results" => array(
                    0 => array(
                        "type" => 'loan.stopInterestType.suspend',
                        "date" => $funding_date
                    )
                )
            )
        );

        return json_encode($data);
    }
}