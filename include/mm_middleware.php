<?php
date_default_timezone_set('America/Chicago');
require "mm_system_config.php";
require __DIR__ . "/vendor/autoload.php";
require "pdf.php";
error_reporting(E_ALL);
ini_set('display_errors', 0);
ini_set("include_path", '/home/mmdtcusr/php:' . ini_get("include_path"));
ini_set("include_path", '/home/mmdtcusr/php/phpseclib1:' . ini_get("include_path"));
include "phpseclib1/Net/SFTP.php";

use Simnang\LoanPro\LoanProSDK as LPSDK;
use \Simnang\Loanpro\Constants\ADDRESS as ADDRESS;
use \Simnang\Loanpro\Constants\BASE_ENTITY as BASE_ENTITY;
use \Simnang\LoanPro\Constants\CUSTOMERS as CUSTOMERS;
use \Simnang\LoanPro\Constants\LOAN as LOAN;
use \Simnang\LoanPro\Constants\LOAN_SETTINGS as LOAN_SETTINGS;
use \Simnang\LoanPro\Constants\LOAN_SETUP as LOAN_SETUP;
use \Simnang\LoanPro\Constants\LOAN_TRANSACTIONS as LOAN_TRANSACTIONS;
use \Simnang\Loanpro\Constants\PHONES as PHONES;
use \Simnang\Loanpro\Constants\PHONES\PHONES_TYPE__C as PHONES_TYPE_C;
use \Simnang\Loanpro\Constants\STATUS_ARCHIVE as STATUS_ARCHIVE;

/************************************************************************************************
 * Function mm_create_lp_customer($customer_data) - This function takes an array of data and will attempt to create a customer in LoanPro.
 ************************************************************************************************/
function mm_create_lp_customer($customer_data)
{
    error_log("flow: mm_create_lp_customer: \n");
    //NEED TO HANDLE THAT THE CUSTOMER EXISTS AND ADD SOME ERROR HANDLING
    $first_name = $customer_data["first_name"];
    $account_nbr = $customer_data["account_nbr"];
    $last_name = $customer_data["last_name"];
    $email_address = $customer_data["email_address"];
    $dob = new DateTime($customer_data["dob"]);
    $birth_date = $dob->format('Y-m-d');
    $ssn = $customer_data["ssn"];
    //$ssn_formatted = substr($customer_data["ssn"], 0, 3) . "-" . substr($customer_data["ssn"], 3, 2) . "-" . substr($customer_data["ssn"], -4);
    $street_address_1 = $customer_data["street_address"];
    $street_address_2 = $customer_data["appt_suite"];
    $city = $customer_data["city"];
    $state = 'geo.state.' . $customer_data["state"];
    $zip_code = $customer_data["zip_code"];
    $mobile_phone_nbr = $customer_data["mobile_phone_nbr"];
    $home_phone_nbr = $customer_data["home_phone_nbr"];
    $lp_customer_id = 0;

    $sdk = LPSDK::GetInstance();
    $customers = $sdk->GetCustomers_RAW([CUSTOMERS::PHONES, CUSTOMERS::PRIMARY_ADDRESS], $sdk->CreateNoPagingParam(), $sdk->CreateFilterParams_Logic(CUSTOMERS::EMAIL . " ='$email_address'"));
    $num_customers = sizeof($customers);

    try {
        $address = $sdk->CreateAddress($state, $zip_code)
            ->Set([ADDRESS::ADDRESS_1 => $street_address_1, ADDRESS::ADDRESS_2 => $street_address_2, ADDRESS::CITY => $city]);
    } catch (Exception $e) {
        $return_array["return_value"] = 1;
        $return_array["return_message"] = "Failed Creating Address For Customer with email $email_address.  Exception is $e";
        return $return_array;
    }
    try {
        $primary_phone = $sdk->CreatePhoneNumber($mobile_phone_nbr)->Set([PHONES::TYPE__C => PHONES_TYPE_C::CELL, PHONES::IS_PRIMARY => "1", PHONES::IS_SECONDARY => "0", "__ignoreWarnings" => true]);
        $phones_arr = array();
        $phones_arr[0] = $primary_phone;
    } catch (Exception $e) {
        $return_array["return_value"] = 1;
        $return_array["return_message"] = "Failed creating primary phone number for customer with email $email_address.  Exception is $e";
        return $return_array;
    }
    try {
        if ($home_phone_nbr != '' && $home_phone_nbr != '0000000000') {
            $secondary_phone = $sdk->CreatePhoneNumber($home_phone_nbr)->Set([PHONES::TYPE__C => PHONES_TYPE_C::HOME, PHONES::IS_SECONDARY => "1", PHONES::IS_PRIMARY => "0", "__ignoreWarnings" => true]);
            $phones_arr[1] = $secondary_phone;
        }
    } catch (Exception $e) {
        $return_array["return_value"] = 1;
        $return_array["return_message"] = "Failed creating secondary (home) phone number for customer with email $email_address.  Exception is $e";
        return $return_array;
    }
    try {
        if ($num_customers == 1) {
            $customer = $customers[0];
            $lp_customer_id = $customer->Get(BASE_ENTITY::ID);
            //There were customers in the system so need to update the information rather than add from scratch
            $address->Set(BASE_ENTITY::ID, $customer->Get(CUSTOMERS::PRIMARY_ADDRESS)->Get(BASE_ENTITY::ID));
              $customer = $customer->Set([CUSTOMERS::FIRST_NAME => $first_name, CUSTOMERS::LAST_NAME => $last_name, CUSTOMERS::EMAIL => $email_address, CUSTOMERS::PRIMARY_ADDRESS => $address, CUSTOMERS::BIRTH_DATE => $birth_date, CUSTOMERS::SSN => $ssn, "gender" => "customer.gender.unknown"]);
        } else if ($num_customers == 0) {
            //There are no matching customers in the system so just add a new one.
            $customer = $sdk->CreateCustomer($first_name, $last_name)
                ->Set([CUSTOMERS::EMAIL => $email_address, CUSTOMERS::PRIMARY_ADDRESS => $address, CUSTOMERS::MAIL_ADDRESS => $address, CUSTOMERS::BIRTH_DATE => $birth_date, CUSTOMERS::SSN => $ssn, CUSTOMERS::STATUS => "Active", CUSTOMERS::PHONES => $phones_arr, "gender" => "customer.gender.unknown"]);
            //Update the account record to store the ID of the customer on Loan Pro
        } else {
            //There must have been more than 1 customer returned which shouldn't happen.  Throw an error and return
            $return_array["return_value"] = 1;
            $return_array["return_message"] = "More than 1 customer exists with the proposed email addres: $email_address or SSN $ssn";
            return $return_array;

        }
    } catch (Exception $e) {
        $return_array["return_value"] = 1;
        $return_array["return_message"] = "Failed creating the customer with email $email_address.  Exception is: $e";
        return $return_array;
    }
    try {
        $lp_customer_id = $customer->Save()->Get('id');
        mm_update_database_value('mm_account', 'lp_customer_id', $lp_customer_id, 'i', 'account_nbr', $account_nbr);

    } catch (Exception $e) {
        //Failed, but log the error and try to add the customer by bypassing warnings.
        $customer = $customer->Set(["__ignoreWarnings" => true]);
        try {
            $lp_customer_id = $customer->Save()->Get('id');
            mm_update_database_value('mm_account', 'lp_customer_id', $lp_customer_id, 'i', 'account_nbr', $account_nbr);
            mm_log_error('mm_create_lp_customer', "Received an error adding customer but was able to successfully add after bypassing warnings.  The error received was: $e");
            $return_array["return_value"] = 0;
            $return_array["return_message"] = '';
            $return_array["lp_customer_id"] = $lp_customer_id;
        } catch (Exception $e) {
            $return_array["return_value"] = 1;
            $return_array["return_message"] = "Failed to save the customer record for email address $email_address.  Exception is $e";
        }

        return $return_array;
    }

    $return_array["return_value"] = 0;
    $return_array["return_message"] = "Customer was successfully added.";
    $return_array["lp_customer_id"] = $lp_customer_id;

    return $return_array;

}

function mm_determine_term_for_state($state, $pay_frequency, $requested_amt, $apr)
{
    error_log("flow: mm_determine_term_for_state: $state, $pay_frequency, $requested_amt, $apr\n");
    $return_array = array();
    $return_value = 0;
    $return_message = '';

    $sql_string = "select number_of_payments from mm_pricing_detail where state = ? and payment_freq = ? and loan_amt = ? and apr = ?";
    try {
        $conn = mm_get_pdo_connection();
        $stmt = $conn->prepare($sql_string);
        $params = [$state, $pay_frequency, $requested_amt, $apr];
        $stmt->execute($params);

    } catch (PDOException $e) {
        $return_array["return_value"] = 1;
        $return_array["return_message"] = "$e";
        $return_array["sql_string"] = $sql_string;
        return $return_array;
    }

    if (is_resource($conn)) {
        $conn = null;
    }

    return $return_array;
}

/****************************************************************************************************
 * Function mm_create_lp_loan($loan_data)
 ****************************************************************************************************/
function mm_create_lp_loan($loan_data, $quick_quote_ind = 0, $active_ind = 0)
{
    error_log("flow: mm_create_lp_loan\n");
    //Perform some validation on the data that needs to be set for
    $application_nbr = $loan_data["application_nbr"];
    $state = $loan_data["state"];
    $requested_deposit_type = $loan_data["requested_deposit_type"];
    $pay_frequency = $loan_data["pay_frequency"];
    $temp_dt = new DateTime($loan_data["effective_dt"]);
    $effective_dt = $temp_dt->format('Y-m-d');
    $temp_dt = new DateTime($loan_data["orig_dt"]);
    $orig_dt = $temp_dt->format('Y-m-d');
    $temp_dt = new DateTime($loan_data["first_due_dt"]);
    $first_due_dt = $temp_dt->format('Y-m-d');
    $last_day_of_month = date("Y-m-t", strtotime($first_due_dt));
    $requested_amt = $loan_data["requested_amt"];

    $apr = $loan_data["apr"];

    //Reformat Some of the data as Needed
    $display_id = str_pad($application_nbr, 8, '0', STR_PAD_LEFT);
    $display_id = "L" . $display_id;
    $loan_setup_array = array();
    $loan_setup_array["loanClass"] = "loan.class.consumer";
    $loan_setup_array["loanType"] = "loan.type.installment";
    $loan_setup_array["loanAmount"] = $requested_amt;
    $loan_setup_array["loanRate"] = $apr;
    $loan_setup_array["loanRateType"] = "loan.rateType.annually";
    //
    // call a function that takes $pay_frequency, $state, $apr to determine this, that returns an array
    //
    if ($pay_frequency == "Bi-Weekly") {
        if ($state == "MS") {
            $loan_setup_array["loanTerm"] = "26";
        } else {
            $loan_setup_array["loanTerm"] = $requested_amt <= 1500 ? "26" : "39";
        }
        $loan_setup_array["paymentFrequency"] = "loan.frequency.biWeekly";
    } else if ($pay_frequency == "Paid Monthly") {
        if ($state == "MS") {
            $loan_setup_array["loanTerm"] = "12";
        } else {
            $loan_setup_array["loanTerm"] = $requested_amt <= 1500 ? "12" : "18";
        }
        $loan_setup_array["paymentFrequency"] = "loan.frequency.monthly";
        //Check to see if the date selected is the last day of the month and if it is set the Last Day of Month Flag.  This will force a payment schedule so that all payments are on the last day of the month
        if ($first_due_dt == $last_day_of_month) {
            $loan_setup_array["dueDateOnLastDOM"] = "1";
        }

    } else if ($pay_frequency == "Paid Weekly") {
        if ($state == "MS") {
            $loan_setup_array["loanTerm"] = "26";
        } else {
            $loan_setup_array["loanTerm"] = $requested_amt <= 1500 ? "26" : "39";
        }
        $loan_setup_array["paymentFrequency"] = "loan.frequency.biWeekly";
    } else if ($pay_frequency == "Paid Twice Monthly") {
        if ($state == "MS") {
            $loan_setup_array["loanTerm"] = "24";
        } else {
            $loan_setup_array["loanTerm"] = $requested_amt <= 1500 ? "24" : "36";
        }
        $loan_setup_array["paymentFrequency"] = "loan.frequency.semiMonthly";
    }
    $loan_setup_array["contractDate"] = $effective_dt;
    //
    // todo: the following line needs to be fixed for california
    //
    $loan_setup_array["firstPaymentDate"] = $first_due_dt;
    $loan_setup_array["calcType"] = "loan.calcType.simpleInterest";
    $loan_setup_array["daysInYear"] = "loan.daysInYear.actual";
    $loan_setup_array["interestApplication"] = "loan.interestApplication.betweenTransactions";
    $loan_setup_array["begEnd"] = "loan.begend.end";
    $loan_setup_array["firstPeriodDays"] = "loan.firstPeriodDays.actual";
    $loan_setup_array["firstDayInterest"] = "loan.firstdayinterest.yes";
    $loan_setup_array["discountCalc"] = "loan.discountCalc.straightLine";
    $loan_setup_array["diyAlt"] = "loan.diyAlt.no";
    $loan_setup_array["roundDecimals"] = "5";
    $loan_setup_array["lastAsFinal"] = "loan.lastasfinal.yes";
    $loan_setup_array["curtailPercentBase"] = "loan.curtailpercentbase.loanAmount";
    $loan_setup_array["nddCalc"] = "loan.nddCalc.standard";
    $loan_setup_array["endInterest"] = "loan.endInterest.no";
    $loan_setup_array["feesPaidBy"] = "loan.feesPaidBy.date";
    $loan_setup_array["graceDays"] = $loan_data["late_fee_grace_period"];
    if ($loan_data["late_fee_calc"] == "GT") {
        $loan_setup_array["lateFeeType"] = "loan.lateFee.4";
    } else if ($loan_data["late_fee_calc"] == "LT") {
        $loan_setup_array["lateFeeType"] = "loan.lateFee.5";
    } else if ($loan_data["late_fee_calc"] == "AMT") {
        $loan_setup_array["lateFeeType"] = "loan.lateFee.2";
    } else if ($loan_data["late_fee_calc"] == "PCT") {
        $loan_setup_array["lateFeeType"] = "loan.lateFee.3";
    } else if ($loan_data["late_fee_calc"] == "STD") {
        $loan_setup_array["lateFeeType"] = "loan.lateFee.2";
    }
    $loan_setup_array["lateFeeAmount"] = $loan_data["late_fee_amt"];
    $loan_setup_array["lateFeePercent"] = $loan_data["late_fee_pct"];
    $loan_setup_array["lateFeeCalc"] = "loan.lateFeeCalc.standard";
    $loan_setup_array["lateFeePercentBase"] = "loan.latefeepercentbase.regular";
    $loan_setup_array["paymentDateApp"] = "loan.pmtdateapp.actual";

    try {
        $sdk = LPSDK::GetInstance();

        //Check to see if the loan exists and if it does then just update the data otherwise add a new loan
        $loans = $sdk->GetLoans_RAW([LOAN::LOAN_SETUP, LOAN::LOAN_SETTINGS, LOAN::TRANSACTIONS], $sdk->CreateNoPagingParam(), $sdk->CreateFilterParams_Logic(LOAN::DISP_ID . " = '$display_id'"));
        $loan_setup = $sdk->CreateLoanSetup('loan.class.consumer', 'loan.type.installment')->Set($loan_setup_array);

        $num_loans = sizeof($loans);
        if ($num_loans == 0) { //There is no loan in the system with that id so create a new one
            $loan = $sdk->CreateLoan("$display_id")->Set([LOAN::TEMPORARY_ACCT => $quick_quote_ind, LOAN::TITLE => 'Personal Loan', LOAN::ACTIVE => $active_ind, LOAN::ARCHIVED => 0, LOAN::DELETED => 0]);
            $loan_settings = $sdk->CreateLoanSettings();
            $loan = $loan->Set(LOAN::LOAN_SETTINGS, $loan_settings);
            $loan = $loan->Set([LOAN::LOAN_SETUP => $loan_setup]);
            $loan->Save();
            //After saving the loan, get the loan so that you have can get the loan id
            $loans = $sdk->GetLoans_RAW([LOAN::LOAN_SETUP, LOAN::LOAN_SETTINGS, LOAN::TRANSACTIONS], $sdk->CreateNoPagingParam(), $sdk->CreateFilterParams_Logic(LOAN::DISP_ID . " = '$display_id'"));
            $lp_loan_id = $loans[0]->Get(BASE_ENTITY::ID);

        } else { //There is a loan so just get the old loan and update the loan setup array
            $loan = $loans[0];
            $lp_loan_id = $loan->Get(BASE_ENTITY::ID);
            if (!is_null($loan->Get(LOAN::LOAN_SETUP))) { // Will set the loan setup id if there exists a loan setup entity for that loan
                $loan_setup = $loan_setup->Set(BASE_ENTITY::ID, $loan->Get(LOAN::LOAN_SETUP)->Get(BASE_ENTITY::ID));
            }
            $loan = $loan->Set([LOAN::TEMPORARY_ACCT => $quick_quote_ind, LOAN::TITLE => 'Personal Loan', LOAN::ACTIVE => 0, LOAN::ARCHIVED => 0, LOAN::DELETED => 0]);
            $loan = $loan->Set([LOAN::LOAN_SETUP => $loan_setup]);
            $loan->Save();
        }

        if ($quick_quote_ind == 0 && $active_ind == 1) {
            //Assign the state level portfolio
            if ($state == "MO") {
                $loan->AddPortfolio(1);
            } else if ($state == "UT") {
                $loan->AddPortfolio(2);
            } else if ($state == "NM") {
                $loan->AddPortfolio(9);
            } else if ($state == "MS") {
                $loan->AddPortfolio(10);
            } else if ($state == "CA") {
                $loan->AddPortfolio(8);
            //} else if ($state == "WI") {
            //    $loan->AddPortfolio(13);
            } else if ($state == "SC") {
                $loan->AddPortfolio(11);
            } else if ($state == "AL") {
                $loan->AddPortfolio(12);
            } else {
                $loan->AddPortfolio(3);
                mm_log_error('mm_create_lp_loan', "Non-Critical Added a loan for State $state and was assigned to the other portfolio");
            }

            //Assign the test/or production level portfolio - hard coded to production for now
            $lp_env = mm_get_lp_environment_portfolio();
            $loan->AddPortfolio($lp_env);

            //Assign a portfolio based on the deposit preference of the customer
            if ($requested_deposit_type == "ACH") {
                //Customer signed up for ACH Authorization
                $loan->AddPortfolio(6);
            } else {
                //Customer signed up for Manual Check
                $loan->AddPortfolio(7);
            }

            //Update the loan substatus to that of Pending Deposit
            $loan_settings = $loan->Get(LOAN::LOAN_SETTINGS);
            $loan_settings = $loan_settings->Set([LOAN_SETTINGS::LOAN_SUB_STATUS_ID, 2]);
            $loan = $loan->Set([LOAN::LOAN_SETTINGS, $loan_settings]);
            $loan->Save();
            //$loan = $loan->Activate();
        }
    } catch (Exception $e) {
        $return_array["return_value"] = 1;
        $return_array["return_message"] = "There was a problem adding or updating the loan $display_id. Exception is: $e";
        return $return_array;
    }

    $return_array["return_value"] = 0;
    $return_array["display_id"] = $display_id;
    $return_array["lp_loan_id"] = $lp_loan_id;
    $return_array["return_message"] = "The loan was added/updated successfully";

    return $return_array;

}

/*******************************************************************************************
 * Function mm_process_loan_agreement()
 * 
 * NOTE: Called by external Chron job:  process_loan_booking.php
 *******************************************************************************************/
function mm_process_loan_agreement($application_nbr)
{
    error_log("flow: mm_process_loan_agreement\n");
    $return_array = array();
    $return_message = '';
    $return_value = 0;

    $app_data_arr = mm_get_application_details($application_nbr);
    $app_data = $app_data_arr["app_data"];

    $application_nbr = $app_data["application_nbr"];
    $loan_agreement_nbr = $app_data["loan_agreement_nbr"];
    $requested_amt = $app_data["requested_amt"];
    $state = $app_data["state"];
    $requested_deposit_type = $app_data["requested_deposit_type"];
    $pay_frequency = $app_data["pay_frequency_1"];

    //NEED to add error handling.  Also should pull la details from session but need to be stored there when created in a different function
    $lender_details = mm_get_lender_details($state);
    $la_details = mm_get_loan_agreement_details($loan_agreement_nbr);
    $payment_schedule_details = mm_get_payment_schedule($loan_agreement_nbr);

    //Get the loan agreement details - assumes that this function is only called from processing on the website and the session variables will contain the necessary information
    $lp_data = array();
    $lp_data["application_nbr"] = $application_nbr;
    $lp_data["state"] = $state;
    $lp_data["requested_deposit_type"] = $requested_deposit_type;
    $lp_data["pay_frequency"] = $pay_frequency;
    $lp_data["effective_dt"] = $la_details["effective_dt"];
    $lp_data["first_due_dt"] = $la_details["first_due_dt"];
    $lp_data["orig_dt"] = $la_details["origination_dt"];
    $lp_data["requested_amt"] = $requested_amt;
    error_log_dump_associative_array("mark: dumping app_data in mm_process_loan_agreement", $app_data);
    error_log_dump_associative_array("mark: dumping la_details in mm_process_loan_agreement", $la_details);
    // todo: Mark to fix when Kleverlend passes back APR
    //$lp_data["apr"] = 132.73;
    $lp_data["apr"] = $app_data["approved_apr"];
    $lp_data["late_fee_amt"] = $lender_details["late_fee_amt"];
    $lp_data["late_fee_pct"] = $lender_details["late_fee_pct"];
    $lp_data["late_fee_calc"] = $lender_details["late_fee_calc"];
    $lp_data["returned_item_fee_amt"] = $lender_details["returned_item_fee_amt"];
    $lp_data["late_fee_grace_period"] = $lender_details["late_fee_grace_period"];

    $customer_data = array();
    $customer_data["first_name"] = $app_data["first_name"];
    $customer_data["account_nbr"] = $app_data["account_nbr"];
    $customer_data["last_name"] = $app_data["last_name"];
    $customer_data["email_address"] = $app_data["email_address"];
    $customer_data["dob"] = $app_data["dob"];
    $customer_data["ssn"] = $app_data["ssn"];
    $customer_data["street_address"] = $app_data["street_address"];
    $customer_data["appt_suite"] = isset($app_data["appt_suite"]) ? $app_data["appt_suite"] : '';
    $customer_data["city"] = $app_data["city"];
    $customer_data["state"] = $app_data["state"];
    $customer_data["zip_code"] = $app_data["zip_code"];
    $customer_data["mobile_phone_nbr"] = $app_data["mobile_phone_nbr"];
    $customer_data["home_phone_nbr"] = $app_data["home_phone_nbr"];

    //Send the relevant loan information to book the loan on the system
    $loan_results = mm_create_lp_loan($lp_data, 0, 1);
    //$display_id = str_pad($application_nbr, 8, '0', STR_PAD_LEFT);
    //$loan_results = mm_lp_activate_loan($display_id);
    $loan_return_value = $loan_results["return_value"];
    $loan_return_message = $loan_results["return_message"];

    //Generate the signed loan agreement document - ADD ERROR HANDLING
    $document_results = mm_generate_loan_agreement_pdf($application_nbr, 1);

    // Mark was here, creating these variables in outer scope as they are used throughout this routine
    $lp_customer_id = "";
    $payment_profile_return_value = "";

    //ADD ERROR HANDLING
    if ($loan_return_value != 0) {
        mm_log_error('mm_process_loan_agreement', "Unable to add the loan to loan pro for application number $application_nbr.  Error from Loan Pro is $loan_return_message");
        $return_value = 1;
        $return_message = "Unable to add the loan to loan pro for application number $application_nbr";
    } else {
        $lp_loan_id = $loan_results["lp_loan_id"];
        $lp_display_id = $loan_results["display_id"];
        $customer_results = mm_create_lp_customer($customer_data);
        $customer_return_value = $customer_results["return_value"];
        $customer_return_message = $customer_results["return_message"];
        //ADD ERROR HANDLING
        if ($customer_return_value != 0) {
            mm_log_error('mm_process_loan_agreement', "Unable to add the customer to loan pro for application number $application_nbr.  Error from Loan Pro is $customer_return_message");
            $return_value = 1;
            $return_message = "Unable to add the customer to loan pro for application $application_nbr.  Error from Loan Pro is $customer_return_message";
        } else {
            // Mark was here:  Commenting out the original code and then refactoring $lp_customer_id and $payment_profile_return_value to a more outer scope, since they are needed later in this routine
            //The customer was added successfully so create the payment profile.  This code needs to be updated to update the payment profile, not just create a new one.
            /*
            $lp_customer_id = $customer_results["lp_customer_id"];
            $payment_profile_results = mm_create_lp_payment_profile($lp_display_id);
            $payment_profile_return_value = $payment_profile_results["return_value"];
            $payment_profile_return_message = $payment_profile_results["return_message"];
             */
            $lp_customer_id = $customer_results["lp_customer_id"];
            $payment_profile_results = mm_create_lp_payment_profile($lp_display_id);
            $payment_profile_return_value = $payment_profile_results["return_value"];
            $payment_profile_return_message = $payment_profile_results["return_message"];
            if ($payment_profile_return_value != 0) {
                mm_update_database_value("mm_application", 'application_status', 9, 'i', 'application_nbr', $application_nbr);
                mm_log_error('mm_process_loan_agreement', "Unable to add the payment profile for loan $lp_display_id.  The error message is $payment_profile_return_message");
                $return_value = 1;
                $return_message = $payment_profile_return_message;
            }
        }

        //Update the application status based on the result
        if ($customer_return_value == 0 && $loan_return_value == 0 && $payment_profile_return_value == 0) {
            //Create the link between the two loans
            $link_results = mm_create_lp_customer_loan_link($lp_customer_id, $lp_loan_id);
            $link_results_return_value = $link_results["return_value"];
            $link_results_return_message = $link_results["return_message"];
            if ($link_results_return_value != 0) {
                mm_update_database_value("mm_application", 'application_status', 9, 'i', 'application_nbr', $application_nbr);
                mm_log_error('mm_process_loan_agreement', "Unable to link the customer to the loan in loan pro for application number $application_nbr.  Error from Loan Pro is $link_results_return_message.");
                $return_value = 1;
                $return_message = $link_results_return_message;
            } else {
                $return_value = 0;
                $return_message = "The loan was successfully added and linked to the customer.";
            }
        }
        //If the loan was booked successfully then go ahead and send the welcome email.
        if ($loan_return_value == 0) {
            //Update the email to approved and schedule the welcome email to be sent.
            //mm_update_database_value("mm_application", 'application_status', 7, 'i', 'application_nbr', $application_nbr);
            $email_data = array();
            $email_data["to"] = $app_data["email_address"];
            $email_data["from"] = SUPPORT_EMAIL;
            $email_data["from_name"] = SITE_NAME;
            $email_data["subject"] = "Welcome To " . SITE_NAME;
            $tmp_first_name = ucwords(strtolower($app_data["first_name"]));

            //Build Payment Schedule Strings
            $payment_schedule = $payment_schedule_details["payment_schedule"];
            $payment_schedule_string = 'Payment Schedule\n';
            $payment_schedule_string_html = '<p><b>Payment Schedule</b></p>';
            foreach ($payment_schedule as $payment) {
                $pay_dt = new DateTime($payment["payment_dt"]);
                $payment_dt = $pay_dt->format("m/d/Y");
                $payment_amt = $payment["payment_amt"];
                $payment_schedule_string .= "$payment_dt:          \$$payment_amt\n";
                $payment_schedule_string_html .= "<p>$payment_dt:<span>          </span>\$$payment_amt</p>";

            }

            $email_data["txt_body"] = "Hello $tmp_first_name,\n\nWelcome to " . SITE_NAME . ", where we make borrowing better!\n\nWe’re glad you chose us for your loan, and your funds are on the way.  Depending on your bank, the funds could be available as soon as the next business day.  We appreciate your business and value you as a customer - so please call us if there's anything we can do better, or if you have a question about your loan or account at 844.678.LOAN (5626) or email us at " . SUPPORT_EMAIL . ".\n\nBelow you'll find a complete list of scheduled payments including due dates and amounts, and if you have selected to make payments from your bank account via ACH, your payments will occur automatically.  With " . SITE_NAME . " you can always pay your loan off early with no penalties, fees or additional charges.\n\n$payment_schedule_string\nWelcome Aboard,\n\n" . SITE_NAME . "\n";
            $email_data["html_body"] = "<html><head></head><body><p>Hello $tmp_first_name,</p><p>Welcome to " . SITE_NAME . ", where we make borrowing better!</p><p>We're glad you chose us for your loan, and your funds are on the way.  Depending on your bank, the funds could be available as soon as the next business day.  We appreciate your business and value you as a customer - so please call us if there's anything we can do better, or if you have a question about your loan or account at 844.678.LOAN (5626) or email us at " . SUPPORT_EMAIL . ".</p>Below you'll find a complete list of scheduled payments including due dates and amounts, and if you have selected to make payments from your bank account via ACH, your payments will occur automatically.  With " . SITE_NAME . " you can always pay your loan off early with no penalties, fees or additional charges.</p><p>$payment_schedule_string_html</p><p>Welcome Aboard,</p><p>" . SITE_NAME . "</p></body></html>";
            $tmp_dt = new DateTime();
            $tmp_dt = $tmp_dt->modify("+1 Day");
            $scheduled_dt = $tmp_dt->format("Y-m-d H:i:s");
            $email_data["scheduled_dt"] = $scheduled_dt;
            $email_message = json_encode($email_data);
            // todo:  do we need error handling here?
            $email_results = mm_schedule_email_message($email_message);

        }
    }

    //Set the return values of the function
    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;

    //return the return array
    return $return_array;

}

/***********************************************************************************************
 * Function mm_create_lp_customer_loan_link($customer_id, $loan_id)
 ***********************************************************************************************/
function mm_create_lp_customer_loan_link($customer_id, $loan_id)
{
    error_log("flow: mm_create_lp_customer_loan_link\n");
    $return_array = array();

    try {
        $sdk = LPSDK::GetInstance();
        //Check to see if the loan exists and if it does then just update the data otherwise add a new loan
        $loan = $sdk->GetLoan($loan_id, [LOAN::LOAN_SETUP], $nopageProps = true);
        $disp_id = $loan->Get(LOAN::DISP_ID);
        if (is_null($loan)) {
            //There was no matching loan record
            $return_array["return_value"] = 1;
            $return_array["return_message"] = "There were no loans with the loan id $loan_id";
        } else {
            $customer = $sdk->GetCustomer($customer_id);
            $loan->AddCustomer($customer, \Simnang\LoanPro\Constants\CUSTOMER_ROLE::PRIMARY);
            $return_array["return_value"] = 0;
            $return_array["return_message"] = "Success";
        }

    } catch (Exception $e) {
        $return_array["return_value"] = 1;
        $return_array["return_message"] = "There was a problem linking the loan: $e";
    }

    return $return_array;

}

/***********************************************************************************************
 * Function mm_unset_application_session()
 ***********************************************************************************************/
function mm_unset_application_session()
{
    error_log("flow: mm_unset_application_session\n");
    if (isset($_SESSION)) {
        foreach ($_SESSION as $key => $value) {
            if (substr($key, 0, 4) == "app_") {
                unset($_SESSION["$key"]);
            }
        }
    }
}

/******************************************************************************************
 * Function mm_restore_session($email_address)
 ******************************************************************************************/
function mm_restore_session($email_address)
{
    error_log("flow: mm_restore_session: $email_address\n");
    //Need to add error handling
    $return_array = array();

    //If session isn't started then start it?
    if (!session_id()) {
        session_start();
    }

    $account_details = mm_get_account_details_from_email_address($email_address);
    $account_nbr = $account_details["account_nbr"];
    if ($account_nbr != 0) {
        $temp1 = mm_get_most_recent_application_details($account_nbr);
        $app_data = $temp1["app_data"];
        $num_apps = $temp1["num_apps"];
        if ($num_apps == 0) {
            //There are no applications so set default values
        } else {
            $app_status = $app_data["application_status"];

            if ($app_status == 1 || $app_status == 2 || $app_status == 3 || $app_status == 4 || $app_status == 5 || $app_status == 6) {
                //The application is either in draft, approved pending acceptance, approved pending customer docs, approved pending agent review, or approved pending customer loan doc therefore setup the appropriate session variables.
                foreach ($app_data as $key => $value) {
                    //prepend all of the application variables with the app_ prefix
                    if ($key == "return_value" || $key == "return_message" || $key == "num_apps") {
                        //Do nothing because we don't want to add these to the application array
                    } else {
                        $_SESSION["app_$key"] = $value;
                    }
                }

                if ($app_status == 2 || $app_status == 3 || $app_status == 4 || $app_status == 5 || $app_status == 6) {
                    //Get the uw_decision data
                    //Need to add error handling for these queries
                    $uw_decision_nbr = $app_data["uw_decision_nbr"];
                    $uw_decision_arr = mm_get_uw_decision_details($uw_decision_nbr);
                    // Mark was here: commenting out the following 2 lines as they are not being used
                    /*
                    $uw_decision_return_value = $uw_decision_arr["return_value"];
                    $uw_decision_return_message = $uw_decision_arr["return_message"];
                     */
                    $decision_data = $uw_decision_arr["decision_data"];
                    //$_SESSION["app_uw_decision"] = $decision_data["app_status_cd"];
                    $_SESSION["app_uw_decision"] = array_key_exists('app_status_cd', $decision_data) ? $decision_data["app_status_cd"] : null;
                }

                if ($app_status == 3 || $app_status == 4 || $app_status == 5 || $app_status == 6) {
                    //Get the Verifications Decision Data
                    //Need to add Error Handling
                    $preverif_decision_nbr = $app_data["preverif_decision_nbr"];
                    $preverif_decision_arr = mm_get_preverif_decision_details($preverif_decision_nbr);
                    // Mark was here: commenting out the following 2 lines as they are not being used
                    /*
                    $preverif_decision_return_value = $preverif_decision_arr["return_value"];
                    $preverif_decision_return_message = $preverif_decision_arr["return_message"];
                     */
                    $preverif_data = $preverif_decision_arr["preverif_data"];
                    $_SESSION["app_vf_decision"] = array_key_exists('app_status_cd', $preverif_data) ? $preverif_data["app_status_cd"] : null;
                }

                //The customer is approved pending customer loan docs so set the session variables related to the loan agreement information in the database
                if ($app_status == 6) {
                    $app_loan_agreement_nbr = $app_data["loan_agreement_nbr"];
                    $la_results = mm_get_loan_agreement_details($app_loan_agreement_nbr);
                    foreach ($la_results as $key => $value) {
                        //APPEND ALL OF THE DATA TO SESSION ARRAY EXCEPT A FEW FIELDS
                        if ($key == "loan_agreement_nbr" || $key == "application_nbr" || $key == "return_value" || $key == "return_message") {
                            //These keys are already output so ignore them
                        } else {
                            $_SESSION["app_$key"] = $value;
                        }
                    }
                }
            }
        }
    } else {
        // todo: do we need error handling here?
        //No MM Account Record exists
    }

    //Set Return Values
    $return_array["return_value"] = 0;
    $return_array["return_message"] = "Success, but no error handling added";

    return $return_array;
}

/***************************************************************************************
 * Function mm_create_loan_agreement($agreement_data) - Takes an array of data and generates the loan agreement record in mm_loan_agreement
 ***************************************************************************************/
function mm_create_loan_agreement($agreement_data)
{
    //error_log_dump_associative_array("agreement_data:", $agreement_data);
    error_log("flow: mm_restore_session\n");
    //error_log_dump_associative_array("agreement_data:", $agreement_data);
    $return_array = array();

    $temp_dt = new DateTime($agreement_data["orig_dt"]);
    $origination_dt = $temp_dt->format('Y/m/d');
    $temp_dt = new DateTime($agreement_data["effective_dt"]);
    $effective_dt = $temp_dt->format('Y/m/d');
    $apr = $agreement_data["apr"];
    $temp_dt = new DateTime($agreement_data["first_due_dt"]);
    $first_due_dt = $temp_dt->format('Y/m/d');
    $temp_dt = new DateTime($agreement_data["maturity_dt"]);
    $maturity_dt = $temp_dt->format('Y/m/d');
    $payment_freq = $agreement_data["payment_freq"];
    $number_of_payments = $agreement_data["num_payments"];
    $total_finance_charge = $agreement_data["total_finance_charge"];
    $principal_amt = $agreement_data["principal_amt"];
    $total_amt_due = $agreement_data["total_amt_due"];
    $application_nbr = $agreement_data["application_nbr"];
    $lender_nbr = $agreement_data["lender_nbr"];
    $temp_dt = new DateTime($agreement_data["create_dt"]);
    $create_dt = $temp_dt->format('Y/m/d H:i:s');
    $late_fee_pct = $agreement_data["late_fee_pct"];
    $loan_nbr = $agreement_data["loan_nbr"];
    $late_fee_amt = $agreement_data["late_fee_amt"];
    $late_fee_calc = $agreement_data["late_fee_calc"];
    $late_fee_grace_period = $agreement_data["late_fee_grace_period"];
    $returned_item_fee_amt = $agreement_data["returned_item_fee_amt"];

    $sql_string = "insert into mm_loan_agreement (origination_dt, effective_dt, apr, first_due_dt, maturity_dt, payment_freq, number_of_payments,
                    total_finance_charge, principal_amt, total_amt_due, application_nbr, lender_nbr, create_dt, late_fee_amt, late_fee_pct,
                    late_fee_calc, late_fee_grace_period, returned_item_fee_amt, loan_nbr)
                    values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    try {
        $conn = mm_get_pdo_connection();
        $stmt = $conn->prepare($sql_string);
        $parms = [$origination_dt, $effective_dt, $apr, $first_due_dt, $maturity_dt, $payment_freq, $number_of_payments, $total_finance_charge,
            $principal_amt, $total_amt_due, $application_nbr, $lender_nbr, $create_dt, $late_fee_amt, $late_fee_pct, $late_fee_calc, $late_fee_grace_period,
            $returned_item_fee_amt, $loan_nbr];
        //$stmt->bind_param('ssdsssidddiisddsids', $origination_dt, $effective_dt, $apr, $first_due_dt, $maturity_dt, $payment_freq, $number_of_payments, $total_finance_charge, $principal_amt, $total_amt_due, $application_nbr, $lender_nbr, $create_dt, $late_fee_amt, $late_fee_pct, $late_fee_calc, $late_fee_grace_period, $returned_item_fee_amt, $loan_nbr);
        $stmt->execute($parms);
        $loan_agreement_nbr = $conn->lastInsertId();

    } catch (PDOException $e) {
        $return_array["return_value"] = 1;
        $return_array["return_message"] = "$e";
        $return_array["sql_string"] = $sql_string;
        return $return_array;
    }

    if (is_resource($conn)) {
        $conn = null;
    }
    //Need to add defaulting and handle errors
    $return_array["loan_agreement_nbr"] = $loan_agreement_nbr;
    $return_array["return_value"] = 0;

    return $return_array;

}

/************************************************************************************
 * Function mm_load_payment_schedule($loan_agreement_nbr, $payment_schedule)
 ************************************************************************************/
function mm_load_payment_schedule($loan_agreement_nbr, $payment_schedule)
{
    error_log("flow: mm_load_payment_schedule\n");    
    $return_array = array();
    $return_value = 0;
    $return_message = '';
    try {
        $conn = mm_get_pdo_connection();
        $sql_query = "insert into mm_payment_schedule (payment_nbr, payment_dt, payment_amt, loan_agreement_nbr, payment_int, payment_prin) values (?,?,?,?, ?, ?)";
        $stmt = $conn->prepare($sql_query);
        foreach ($payment_schedule as $payment) {
            $payment_nbr = $payment["payment_nbr"];
            $temp_dt = new DateTime($payment["payment_dt"]);
            $payment_dt = $temp_dt->format('Y-m-d');
            $payment_amt = $payment["payment_amt"];
            $payment_int = $payment["payment_int"];
            $payment_prin = $payment["payment_prin"];
            $param_arr = [$payment_nbr, $payment_dt, $payment_amt, $loan_agreement_nbr, $payment_int, $payment_prin];
            $stmt->execute($param_arr);
        }
    } catch (PDOException $e) {
        $return_array["return_value"] = 1;
        $return_array["return_message"] = "$e";
        return $return_array;
    }

    if (is_resource($conn)) {
        $conn = null;
    }

    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    return $return_array;
}

/*************************************************************************************
 * Function mm_generate_loan_agreement($application_nbr)
 *************************************************************************************/
function mm_generate_loan_agreement($application_nbr, $timestamp = '')
{
    error_log("flow: mm_generate_loan_agreement\n");
    $return_array = array();
    $document_path = '';
    $loan_agreement_nbr = '';

    if ($timestamp == '') {
        $curr_timestamp = new DateTime();
    } else {
        $curr_timestamp = new DateTime($timestamp);
    }
    $curr_timestamp_string = $curr_timestamp->Format('m/d/Y H:i:s');

    $orig_dt = $curr_timestamp->format('m/d/Y');

    $return_data = mm_get_application_details($application_nbr);
    //Add error handling in case it fails
    $app_data = $return_data["app_data"];
    $app_state = $app_data["state"];
    $requested_deposit_type = $app_data["requested_deposit_type"];
    $lender_details = mm_get_lender_details($app_state);
    $next_pay_dt = $app_data["next_pay_dt_1"];
    $direct_deposit_ind_1 = $app_data["direct_deposit_ind_1"];
    $pay_frequency = $app_data["pay_frequency_1"];
    $requested_amt = $app_data["requested_amt"];
    if ($pay_frequency == 'Bi-Weekly') {
        $pay_freq = 'B';
    } else if ($pay_frequency == 'Paid Monthly') {
        $pay_freq = 'M';
    } else if ($pay_frequency == 'Paid Twice Monthly') {
        $day_paid_twice_monthly = $app_data["day_paid_twice_monthly_1"];
        if ($day_paid_twice_monthly == "1st and 15th") {
            $pay_freq = 'S1';
        } else if ($day_paid_twice_monthly == "15th and 30th") {
            $pay_freq = 'S2';
        }

    } else if ($pay_frequency == 'Paid Weekly') {
        $pay_freq = 'W';
    } else {
        //Default to bi-weekly and log an error
        $pay_freq = 'B';
    }

    //Insert logic here to calculate first pay date, etc
    $effective_dt = mm_calculate_contract_date($curr_timestamp_string, $requested_deposit_type);
    $date1 = new DateTime($next_pay_dt);
    $date2 = new DateTime($effective_dt);
    $date_diff = $date2->diff($date1)->format("%r%a");
    error_log("mark: state: " . print_r($app_state, true));
    if ($app_state === 'CA') {
        error_log("mark: state is california");
        $first_due_dt = mm_calculate_future_pay_dt_California($pay_freq, $next_pay_dt);
    } else if ($date_diff >= 5) {
        error_log("mark: date_diff >= 5");
        $first_due_dt = $next_pay_dt;
    } else {
        error_log("mark: date_diff < 5");
        $first_due_dt = mm_calculate_future_pay_dt($pay_freq, $next_pay_dt, 1);
    }

    //Determine if there is an adjustment needed based on the deposit method - Manual check needs an extra day added to allow time for the customer to deposit the check
    if ($direct_deposit_ind_1 == "N") {
        $temp_dt = new DateTime($first_due_dt);
        $first_due_dt = $temp_dt->Modify('+1 Day')->format('m/d/Y');

    }

    $app_data["first_due_dt"] = $first_due_dt;
    $app_data["effective_dt"] = $effective_dt;
    $app_data["orig_dt"] = $orig_dt;

    //Insert logic here to call LoanPro quick quote to get the information needed to generate the loan document
    $lp_data = array();
    $lp_data["application_nbr"] = $application_nbr;
    $lp_data["state"] = $app_state;
    $lp_data["requested_deposit_type"] = $requested_deposit_type;
    $lp_data["pay_frequency"] = $pay_frequency;
    $lp_data["effective_dt"] = $effective_dt;
    $lp_data["first_due_dt"] = $first_due_dt;
    $lp_data["orig_dt"] = $orig_dt;
    $lp_data["requested_amt"] = $requested_amt;
    // this in contract_apr
    $lp_data["apr"] = $app_data["approved_apr"];
    $lp_data["late_fee_amt"] = $lender_details["late_fee_amt"];
    $lp_data["late_fee_pct"] = $lender_details["late_fee_pct"];
    $lp_data["late_fee_calc"] = $lender_details["late_fee_calc"];
    $lp_data["returned_item_fee_amt"] = $lender_details["returned_item_fee_amt"];
    $lp_data["late_fee_grace_period"] = $lender_details["late_fee_grace_period"];

    $_SESSION["LP_DATA"] = $lp_data;
    $loan_array = mm_create_lp_loan($lp_data, 0);
    $return_value = $loan_array["return_value"];
    if ($return_value != 0) {
        $return_message = $loan_array["return_message"];
        mm_log_error('mm_generate_loan_agreement', "Failed Creating the Loan in Loan Pro: $return_message");
    } else {
        $display_id = $loan_array["display_id"];

        //Get the loan details
        $loan_details = mm_get_lp_loan_details($display_id);
        $return_value = $loan_details["return_value"];
        if ($return_value != 0) {
            $return_message = $loan_details["return_message"];
            mm_log_error('mm_generate_loan_agreement', "Failed Getting Loan Details: $return_message");
        } else {
            $agreement_data = array();
            $agreement_data["orig_dt"] = $orig_dt;
            $agreement_data["effective_dt"] = $loan_details["contract_dt"];
            // this is TILA apr
            $agreement_data["apr"] = $loan_details["apr"];
            $agreement_data["first_due_dt"] = $first_due_dt;
            $agreement_data["maturity_dt"] = $loan_details["maturity_dt"];
            $agreement_data["payment_freq"] = $pay_frequency;
            $agreement_data["num_payments"] = $loan_details["num_payments"];
            $agreement_data["total_finance_charge"] = $loan_details["total_int"];
            $agreement_data["total_amt_due"] = $loan_details["total_due"];
            $agreement_data["principal_amt"] = $loan_details["total_prin"];
            $agreement_data["late_fee_amt"] = $loan_details["late_fee_amt"];
            $agreement_data["late_fee_pct"] = $loan_details["late_fee_pct"];
            $agreement_data["late_fee_calc"] = $lender_details["late_fee_calc"]; //Not an error that it's calc vs. type
            $agreement_data["returned_item_fee_amt"] = $lender_details["returned_item_fee_amt"];
            $agreement_data["late_fee_grace_period"] = $loan_details["late_fee_grace_period"];
            $agreement_data["lender_nbr"] = $lender_details["lender_nbr"];
            $agreement_data["application_nbr"] = $application_nbr;
            $agreement_data["loan_nbr"] = $display_id;
            $agreement_data["create_dt"] = $curr_timestamp_string;

            //Create an mm_loan_agreement record
            $la_details = mm_create_loan_agreement($agreement_data);
            $return_value = $la_details["return_value"];
            if ($return_value != 0) {
                $return_message = $loan_details["return_message"];
                mm_log_error('mm_create_loan_agreement', "Failed Creating the Loan Agreement Record: $return_message");
            } else {
                $loan_agreement_nbr = $la_details["loan_agreement_nbr"];
                //Insert the payment schedule records
                $payment_sched_result = mm_load_payment_schedule($loan_agreement_nbr, $loan_details["payment_schedule"]);
                $return_value = $payment_sched_result["return_value"];
                if ($return_value != 0) {
                    //$return_message = "Failed Creating the Payment Schedule";
                    mm_log_error('mm_generate_loan_agreement', "Failed Creating the Loan Agreement Payment Schedule");
                } else {
                    //update the application record to point to this loan agreement
                    mm_update_database_value('mm_application', 'loan_agreement_nbr', $loan_agreement_nbr, 'i', 'application_nbr', $application_nbr);

                    //Generate the Loan Agreement PDF
                    $pdf_results = mm_generate_loan_agreement_pdf($application_nbr);
                    $return_value = $pdf_results["return_value"];
                    if ($return_value != 0) {
                        //$return_message = "Failed creating the loan agreement pdf";
                        mm_log_error('mm_generate_loan_agreement', "Failed Creating the Loan Agreement PDF");
                    } else {
                        $document_path = $pdf_results["document_path"];

                    }

                }
            }
        }
    }

    if ($return_value != 0) {
        mm_update_database_value('mm_application', 'application_status', 9, 'i', "application_nbr", $application_nbr);
    }
    //constuct return mesage
    $return_array["document_path"] = $document_path;
    $return_array["loan_agreement_nbr"] = $loan_agreement_nbr;
    $return_array["return_value"] = $return_value;
    //$return_array["return_message"] = $return_message;
    //send return array back

    return $return_array;
}

/**********************************************************************************************
 * Function mm_get_lp_loan_details($display_id);
 **********************************************************************************************/
function mm_get_lp_loan_details($display_id)
{
    error_log("flow: mm_get_lp_loan_details\n");
    $return_array = array();
    $return_value = 0;
    $return_message = '';
    try {
        $sdk = LPSDK::GetInstance();
        $loans = $sdk->GetLoans_RAW([LOAN::LOAN_SETUP, LOAN::TRANSACTIONS, LOAN::LOAN_SETTINGS], $sdk->CreateNoPagingParam(), $sdk->CreateFilterParams_Logic(LOAN::DISP_ID . " = '$display_id'"));

        // $loans is an iterator so you can simply use a foreach loop
        foreach ($loans as $loan) {
            $loan_setup = $loan->get(LOAN::LOAN_SETUP);
            $loan_settings = $loan->get(LOAN::LOAN_SETTINGS);
            $active_ind = $loan->get(LOAN::ACTIVE);
            $loan_setup_active_ind = $loan_setup->get(LOAN::ACTIVE);
            $apr = $loan_setup->get(LOAN_SETUP::APR);
            $id = $loan->get(BASE_ENTITY::ID);
            $loan_settings_id = $loan_settings->get(BASE_ENTITY::ID);
            $loan_setup_id = $loan_setup->get(BASE_ENTITY::ID);
            $loan_status = $loan_settings->get(LOAN_SETTINGS::LOAN_STATUS_ID);
            $loan_sub_status = $loan_settings->get(LOAN_SETTINGS::LOAN_SUB_STATUS_ID);
            $maturity_dt = DateTime::createFromFormat('U', $loan_setup->get(LOAN_SETUP::ORIG_FINAL_PAY_DATE))->format('m/d/Y');
            $total_due = $loan_setup->get(LOAN_SETUP::TIL_TOTAL_OF_PAYMENTS);
            $total_prin = $loan_setup->get(LOAN_SETUP::TIL_LOAN_AMOUNT);
            $total_int = $loan_setup->get(LOAN_SETUP::TIL_FINANCE_CHARGE);
            $num_payments = $loan_setup->get(LOAN_SETUP::LOAN_TERM);
            $contract_dt = DateTime::createFromFormat('U', $loan_setup->get(LOAN_SETUP::CONTRACT_DATE))->format('m/d/Y');
            $late_fee_amount = $loan_setup->get(LOAN_SETUP::LATE_FEE_AMT);
            $late_fee_percent = $loan_setup->get(LOAN_SETUP::LATE_FEE_PERCENT);
            $late_fee_type = $loan_setup->get(LOAN_SETUP::LATE_FEE_TYPE__C);
            $late_fee_calc = $loan_setup->get(LOAN_SETUP::LATE_FEE_CALC__C);
            $late_fee_percent_base = $loan_setup->get(LOAN_SETUP::LATE_FEE_PERC_BASE__C);
            $late_fee_grace_period = $loan_setup->get(LOAN_SETUP::GRACE_DAYS);

            $transactions = $loan->get(LOAN::TRANSACTIONS);
            // we can now iterate over the array and gather our total numbers and prep our output
            $payment_schedule = array(array());
            foreach ($transactions as $trans) {
                $type = $trans->get(LOAN_TRANSACTIONS::TYPE);
                if ($type == "scheduledPayment") {
                    $payment_nbr = $trans->get(LOAN_TRANSACTIONS::PERIOD) + 1;
                    $payment_period = $trans->get(LOAN_TRANSACTIONS::PERIOD);
                    $amt = $trans->get(LOAN_TRANSACTIONS::CHARGE_AMOUNT);
                    $prin = $trans->get(LOAN_TRANSACTIONS::CHARGE_PRINCIPAL);
                    $int = $trans->get(LOAN_TRANSACTIONS::CHARGE_INTEREST);
                    $dt = DateTime::createFromFormat('U', $trans->get(LOAN_TRANSACTIONS::DATE))->format('m/d/Y');
                    $payment_schedule[$payment_period]["payment_dt"] = $dt;
                    $payment_schedule[$payment_period]["payment_amt"] = $amt;
                    $payment_schedule[$payment_period]["payment_int"] = $int;
                    $payment_schedule[$payment_period]["payment_prin"] = $prin;
                    $payment_schedule[$payment_period]["payment_nbr"] = $payment_nbr;
                }
            }

        }

    } catch (Exception $e) {
        $return_array["return_value"] = 1;
        $return_array["return_message"] = "There was a problem getting the loan details for $display_id.  Exception is $e";
        return $return_array;
    }
    //Prepare Return Array
    $return_array = array();
    $return_array["return_value"] = isset($return_value) ? $return_value : 1;
    $return_array["return_message"] = isset($return_message) ? $return_message : 'Error: No Return Message Set';
    $return_array["total_due"] = isset($total_due) ? $total_due : 0;
    $return_array["total_prin"] = isset($total_prin) ? $total_prin : 0;
    $return_array["total_int"] = isset($total_int) ? $total_int : 0;
    $return_array["apr"] = isset($apr) ? $apr : 0;
    $return_array["num_payments"] = isset($num_payments) ? $num_payments : 0;
    $return_array["late_fee_amt"] = isset($late_fee_amount) ? $late_fee_amount : 0;
    $return_array["late_fee_calc"] = isset($late_fee_calc) ? $late_fee_calc : 0;
    $return_array["late_fee_pct"] = isset($late_fee_percent) ? $late_fee_percent : 0;
    $return_array["late_fee_type"] = isset($late_fee_type) ? $late_fee_type : 0;
    $return_array["returned_item_fee_amt"] = isset($returned_item_fee_amt) ? $returned_item_fee_amt : 0;
    $return_array["late_fee_percent_base"] = isset($late_fee_percent_base) ? $late_fee_percent_base : 0;
    $return_array["late_fee_grace_period"] = isset($late_fee_grace_period) ? $late_fee_grace_period : 0;
    $return_array["contract_dt"] = isset($contract_dt) ? $contract_dt : '';
    $return_array["maturity_dt"] = isset($maturity_dt) ? $maturity_dt : '';
    $return_array["payment_schedule"] = isset($payment_schedule) ? $payment_schedule : '';
    $return_array["loan_id"] = isset($id) ? $id : '';
    $return_array["loan_settings_id"] = isset($loan_settings_id) ? $loan_settings_id : '';
    $return_array["loan_setup_id"] = isset($loan_setup_id) ? $loan_setup_id : '';
    $return_array["loan_status"] = isset($loan_status) ? $loan_status : '';
    $return_array["loan_sub_status"] = isset($loan_sub_status) ? $loan_sub_status : '';
    $return_array["active_ind"] = isset($active_ind) ? $active_ind : '';
    $return_array["loan_setup_active_ind"] = isset($loan_setup_active_ind) ? $loan_setup_active_ind : '';

    return $return_array;

}

/*****************************************************************************************************
 * Function mm_get_db_records($table_name, $key_name, $key_value)
 *****************************************************************************************************/
function mm_get_db_records($table_name, $key_name, $key_value)
{
    error_log("flow: mm_get_db_records\n");
    $return_array = array();
    $bind_params = array();

    $bind_params[0] = $key_value;
    try {
        $sql_string = "Select * from $table_name where $key_name = ?";
        $conn = mm_get_pdo_connection();
        $stmt = $conn->prepare($sql_string);
        $stmt->execute($bind_params);
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $row_count = $stmt->rowCount();
    } catch (PDOException $e) {
        $return_array["return_value"] = 1;
        $return_array["return_message"] = "There was an issue executing the query $sql_string.  Error: $e";
        return $return_array;
    }

    $return_array["return_value"] = 0;
    $return_array["return_message"] = "The query was executed successfully";
    $return_array["results"] = $results;
    $return_array["result_count"] = $row_count;

    return $return_array;
}

/********************************************************************************************************
 * Function mm_get_loan_agreement_details($loan_agreement_nbr)
 ********************************************************************************************************/
function mm_get_loan_agreement_details($loan_agreement_nbr)
{
    error_log("flow: mm_get_loan_agreement_details\n");
    //Need to add error handling
    $return_array = array();
    $sql_string = "select * from mm_loan_agreement where loan_agreement_nbr = ?";
    $conn = mm_get_db_connection();
    $stmt = $conn->prepare($sql_string);
    $stmt->bind_param('i', $loan_agreement_nbr);
    $stmt->execute();
    $results = $stmt->get_result();
    $row = $results->fetch_assoc();

    foreach ($row as $key => $value) {
        $return_array["$key"] = $value;
    }

    $return_array["return_value"] = 0;
    return $return_array;

}

/*****************************************************************************************
 * Function mm_get_account_details($account_nbr)
 *****************************************************************************************/
function mm_get_account_details($account_nbr)
{
    error_log("flow: mm_get_account_details\n");
    $return_array = array();
    $sql_string = "select * from mm_account where account_nbr = ?";
    $conn = mm_get_db_connection();
    $stmt = $conn->prepare($sql_string);
    $stmt->bind_param('i', $account_nbr);
    $stmt->execute();
    $results = $stmt->get_result();
    $row = $results->fetch_assoc();

    foreach ($row as $key => $value) {
        $return_array["$key"] = $value;
    }

    $return_array["return_value"] = 0;
    return $return_array;

}

/****************************************************************************************
 * Function mm_get_payment_schedule($loan_agreement_nbr)
 ****************************************************************************************/
function mm_get_payment_schedule($loan_agreement_nbr)
{
    error_log("flow: mm_get_payment_schedule\n");
    //Need to add error handling
    $return_array = array();
    $payment_schedule = array(array());
    $sql_string = "select * from mm_payment_schedule where loan_agreement_nbr = ? order by payment_nbr asc";
    $conn = mm_get_db_connection();
    $stmt = $conn->prepare($sql_string);
    $stmt->bind_param('i', $loan_agreement_nbr);
    $stmt->execute();
    $results = $stmt->get_result();
    $i = 0;
    while ($row = $results->fetch_assoc()) {
        foreach ($row as $key => $value) {
            $payment_schedule[$i][$key] = $value;
        }
        $i += 1;
    }

    $return_array["payment_schedule"] = $payment_schedule;
    $return_array["return_value"] = 0;

    return $return_array;
}

/****************************************************************************************
 * Function mm_generate_loan_agreement_pdf($application_nbr, $signature_data=0)
 ****************************************************************************************/
function mm_generate_loan_agreement_pdf($application_nbr, $signature_data = 0)
{
    error_log("flow: mm_generate_loan_agreement_pdf\n");
    //Get Data Needed to Complete the Document
    $app_data_arr = mm_get_application_details($application_nbr);
    $app_data = $app_data_arr["app_data"];
    //$first_name = $app_data["first_name"];
    //$pay_frequency = $app_data["pay_frequency_1"];
    $loan_agreement_nbr = $app_data["loan_agreement_nbr"];
    $la_data = mm_get_loan_agreement_details($loan_agreement_nbr);
    $ach_authorization = $la_data["ach_authorization"];
    $ach_authorization_ack = $la_data["ach_authorization_ack"];
    $ach_agree_to_pay = $la_data["ach_agree_to_pay"];
    $signature = $la_data["signature"];
    $signature_i_agree = $la_data["signature_i_agree"];
    $signature_timestamp = $la_data["signature_timestamp"];
    $signature_ip = $la_data["signature_ip"];
    $payment_schedule_arr = mm_get_payment_schedule($loan_agreement_nbr);
    $payment_schedule = $payment_schedule_arr["payment_schedule"];
    $lender_data = mm_get_lender_details($app_data["state"]);

    //Initialize All of the variables needed
    $loan_number = $la_data["loan_nbr"];
    $account_nbr = $app_data["account_nbr"];
    $temp_dt = new DateTime($la_data["origination_dt"]);
    $orig_dt = $temp_dt->format('m/d/Y');
    $temp_dt = new DateTime($la_data["effective_dt"]);
    $contract_dt = $temp_dt->format('m/d/Y');
    //$final_due_date = $la_data["maturity_dt"];
    $bank_acct_nbr = $app_data["bank_acct_nbr"];
    $bank_acct_length = strlen($bank_acct_nbr);
    $bank_acct_display = substr($bank_acct_nbr, $bank_acct_length - 4);
    //$bank_acct_clean = str_pad($bank_acct_display, $bank_acct_length, '*', STR_PAD_LEFT);
    // This APR is from LoanPro and is exace
    $tila_apr = $la_data["apr"]."%";
    $contract_apr = $app_data["apr"];
    //$total_finance_charge = "\$" . $la_data["total_amt_due"];
    $lender_name = $lender_data["legal_name"];
    $lender_display_state = $lender_data["display_state"];

    $lender_mailing_street_address = $lender_data["mailing_street_address"];
    $lender_physical_street_address = $lender_data["physical_street_address"];
    
    $lender_mailing_city = $lender_data["mailing_city"];
    $lender_physical_city = $lender_data["physical_city"];
    
    $lender_mailing_state = $lender_data["mailing_state_cd"];
    $lender_physical_state = $lender_data["physical_state_cd"];

    $lender_mailing_zip_code = $lender_data["mailing_zip_code"];
    $lender_physical_zip_code = $lender_data["physical_zip_code"];

    $lender_phone_nbr = $lender_data["phone_nbr"];
    //$lender_fax_nbr = $lender_data["fax_nbr"];
    $lender_dba_name = $lender_data["dba_name"];
    $lender_email_address = $lender_data["email_address"];
    $customer_name = $app_data["first_name"] . " " . $app_data["last_name"];
    //$customer_ssn = $app_data["ssn"];
    //$customer_clean_ssn = '***-**-' . substr($app_data["ssn"], 5);
    $customer_street_address = $app_data["street_address"] . " " . $app_data["appt_suite"];
    $customer_zip_code = $app_data["zip_code"];
    $customer_state = $app_data["state"];
    $customer_city = $app_data["city"];
    $customer_phone_nbr = "(" . substr($app_data["mobile_phone_nbr"], 0, 3) . ") " . substr($app_data["mobile_phone_nbr"], 3, 3) . "-" . substr($app_data["mobile_phone_nbr"], 6);
    $amount_financed = "\$" . $la_data["principal_amt"];
    //$num_payments = $la_data["number_of_payments"];
    $total_finance_charge = "\$" . $la_data["total_finance_charge"];
    $total_of_all_payments = "\$" . $la_data["total_amt_due"];
    $late_fee_amt = $la_data["late_fee_amt"];
    $late_fee_amt_txt = "\$" . $la_data["late_fee_amt"];
    $late_fee_pct = $la_data["late_fee_pct"];
    $late_fee_pct_txt = $late_fee_pct . "%";
    $late_fee_calc = $la_data["late_fee_calc"];
    $late_fee_grace_period = $la_data["late_fee_grace_period"];
    $returned_item_fee_amt = "\$" . $la_data["returned_item_fee_amt"];
    $lender_late_policy = '';

    if ($late_fee_pct == 0 && $late_fee_amt == 0) {
        $lender_late_policy = '';
    } else if ($late_fee_calc == "GT") {
        $lender_late_policy = "If a payment is late by at least $late_fee_grace_period days, you will be charged the greater of $late_fee_pct_txt of the payment amount or $late_fee_amt_txt.";
    } else if ($late_fee_calc == "LT") {
        $lender_late_policy = "If a payment is late by at least $late_fee_grace_period days, you will be charged the lesser of $late_fee_pct_txt of the payment amount or $late_fee_amt_txt.";

    } else if ($late_fee_calc == "AMT") {
        $lender_late_policy = "If a payment is late by at least $late_fee_grace_period days, you will be charged a late fee of $late_fee_amt_txt.";

    } else if ($late_fee_calc == "PCT") {
        $lender_late_policy = "If a payment is late by at least $late_fee_grace_period days, you will be charged a late fee of $late_fee_pct_txt of the payment amount.";

    }
    if ($la_data["returned_item_fee_amt"] > 0) {
        $lender_returned_item_policy = "Returned Item Fee: If an instrument is returned for any reason by any financial institution, you agree to pay a fee of $returned_item_fee_amt.";
    } else {
        $lender_returned_item_policy = '';
    }

    //Build the PDF document
    $pdf = new PDF('P', 'mm', 'Letter');
    $pdf->SetMargins(4, 4, 4);
    //$page_height = $pdf->GetPageHeight();
    $pdf->AddPage();

    // top 2 lines
    $pdf->setFont('Arial', 'B', 12);
    $pdf->Cell(0, 10, "THIS AGREEMENT SHALL NOT CONSTITUTE A \"NEGOTIABLE INSTRUMENT\"", 0, 0, 'C');
    $pdf->Ln(5);
    $pdf->setFont('Arial', '', 12);
    if ($customer_state == 'MS') {
        $pdf->Cell(0, 10, "Consumer Installment Loan Agreement and Federal Truth-in-Lending Disclosures", 0, 0, 'C');
    } else {
        $pdf->Cell(0, 10, "Credit Availability Act Loan Agreement and Federal Truth-in-Lending Disclosures", 0, 0, 'C');
    }
    $pdf->Ln(10);

    // third line
    $tmp_y = $pdf->GetY();
    $pdf->Cell(0, 10, "Loan Number: $loan_number", 0, 0, 'C');
    $pdf->Ln(10);

    // two columns:  get the y value for the next column and set the left margin
    $tmp_y = $pdf->GetY();
    $pdf->setFont('Arial', 'B', 12);
    $pdf->Cell(0, 10, "BORROWER:", 0, 0, 'L');
    $pdf->Ln(6);
    $pdf->setFont('Arial', '', 12);
    $pdf->Cell(0, 10, "$customer_name", 0, 0, 'L');
    $pdf->Ln(5);
    $pdf->Cell(0, 10, "$customer_street_address", 0, 0, 'L');
    $pdf->Ln(5);
    $pdf->Cell(0, 10, "$customer_city, $customer_state $customer_zip_code", 0, 0, 'L');
    $pdf->Ln(5);
    $pdf->Cell(0, 10, "$customer_phone_nbr", 0, 0, 'L');

    $pdf->SetLeftMargin(100);
    $pdf->SetY($tmp_y);
    $pdf->setFont('Arial', 'B', 12);
    $pdf->Cell(0, 10, "LENDER NAME & LOCATION:", 0, 0, 'L');
    $pdf->Ln(6);
    $pdf->setFont('Arial', '', 12);

    $pdf->Cell(0, 10, "$lender_name" . " d/b/a $lender_dba_name", 0, 0, 'L');
    $pdf->Ln(5);
    $pdf->Cell(0, 10, "$lender_physical_street_address", 0, 0, 'L');
    $pdf->Ln(5);
    $pdf->Cell(0, 10, "$lender_physical_city, $lender_physical_state $lender_physical_zip_code", 0, 0, 'L');
    $pdf->Ln(5);
    $pdf->Cell(0, 10, "$lender_phone_nbr", 0, 0, 'L');
    if ($customer_state == 'CA') {
        // Lender License Number
        $pdf->Ln(5);
        $pdf->Cell(0, 10, "<<Lic Num>>", 0, 0, 'L');
    }
    $pdf->Ln(12);

    // dates
    $y_temp = $pdf->GetY();
    $pdf->Setx(0);
    $pdf->SetLeftMargin(4);
    $pdf->Cell(0, 4, "Contract Date: $contract_dt", 0, 2, 'L');
    $pdf->SetY($y_temp, false);
    $pdf->SetLeftMargin(100);
    $pdf->Cell(0, 4, "Disbursement Date: $contract_dt", 0, 2, 'L');
    $pdf->Setx(0);
    $pdf->SetLeftMargin(4);
    $pdf->Cell(0, 4, "Effective Date: $contract_dt", 0, 2, 'L');
    $pdf->Ln(2);
    if ($customer_state == 'AL') {
        $pdf->MultiCell(0, 4, "THE TERMS AND CONDITIONS OF THIS AGREEMENT ARE IMPORTANT. PLEASE READ THE ENTIRE AGREEMENT CAREFULLY.
     In this Consumer Installment Loan Agreement (this \"Agreement\") and in the Federal Truth-in-Lending Disclosures (the \"Disclosures\"), \"you\" and \"your\" refer to the Borrower identified above. \"We\", \"us\", \"our\", and \"Lender\" refer to $lender_name d/b/a $lender_dba_name or any assignee or subsequent holder of this Agreement. \"Loan\" means the consumer installment loan made by Lender to Borrower under this Agreement and Disclosures. This Agreement contains an arbitration provision. Unless you act promptly to reject the arbitration provision, it will have a substantial effect on your rights in the event of a dispute.", 0, 'L');
    } else if ($customer_state == 'CA') {
        $pdf->setFont('Arial', '', 10);
        $pdf->MultiCell(0, 4, "THE TERMS AND CONDITIONS OF THIS AGREEMENT ARE IMPORTANT. PLEASE READ THE ENTIRE AGREEMENT CAREFULLY.
     In this Consumer Installment Loan Agreement (this \"Agreement\") and in the Federal Truth-in-Lending Disclosures (the \"Disclosures\"), \"you\" and \"your\" refer to the Borrower identified above. \"We\", \"us\", \"our\", and \"Lender\" refer to $lender_name d/b/a $lender_dba_name or any assignee or subsequent holder of this Agreement. \"Loan\" means the consumer installment loan made by Lender to Borrower under this Agreement and Disclosures. This Agreement contains an arbitration provision. Unless you act promptly to reject the arbitration provision, it will have a substantial effect on your rights in the event of a dispute. This loan is made pursuant to the California Finance Lenders Law, Division 9 (commencing with Section 22000) of the Financial Code.", 0, 'L');
    } else if ($customer_state == 'MS') {
        $pdf->MultiCell(0, 4, "In this Credit Availability Act Loan Agreement (this \"Agreement\") and in the Federal Truth-in-Lending Disclosures (the \"Disclosures\"), \"you\" and \"your\" refer to the Borrower identified above. \"We\", \"us\", \"our\", and \"Lender\" refer to $lender_name d/b/a $lender_dba_name or any assignee or subsequent holder of this Agreement. \"Loan\" means the Credit Availability Act loan made by Lender to Borrower under this Agreement and Disclosures. This Agreement contains an arbitration provision. Unless you act promptly to reject the arbitration provision, it will have a substantial effect on your rights in the event of a dispute.", 0, 'L');
    } else {
        $pdf->MultiCell(0, 4, "THE TERMS AND CONDITIONS OF THIS AGREEMENT ARE IMPORTANT.  PLEASE READ THE ENTIRE AGREEMENT CAREFULLY.\nIn this Consumer Installment Loan Agreement (this \"Agreement\") and in the Federal Truth-in-Lending Disclosures (the \"Disclosures\"), \"you\" and \"your\" refer to the Borrower identified above.  \"We\", \"us\", \"our\", and \"Lender\" refer to $lender_name d/b/a $lender_dba_name or any assignee or subsequent holder of this Agreement.  \"Loan\" means the consumer installment loan made by Lender to Borrower under this Agreement and Disclosures.  This Agreement contains an arbitration provision.  Unless you act promptly to reject the arbitration provision, it will have a substantial effect on your rights in the event of a dispute.", 0, 'L');
    }
    if ($customer_state != 'CA') {
        $pdf->Ln(4);
    }

    $pdf->setFont('Arial', 'BU', 12);
    if ($customer_state == 'CA') {
        $pdf->MultiCell(0, 5, "FOR INFORMATION CONTACT THE DEPARTMENT OF BUSINESS OVERSIGHT, STATE OF CALIFORNIA.  FEDERAL TRUTH-IN-LENDING DISCLOSURES", 0, 'C');
        //$pdf->Ln(1); // was 15
    } else {
        $pdf->Cell(0, 10, "FEDERAL TRUTH-IN-LENDING DISCLOSURES", 0, 0, 'C');
        $pdf->Ln(15); // was 15
    }
    //$pdf->setFont('Arial', 'BU', 12);
    //$pdf->Cell(0, 10, "FEDERAL TRUTH-IN-LENDING DISCLOSURES", 0, 0, 'C');
    //$pdf->Ln(15); // was 15

    $page_height = $pdf->getPageHeight();

    $tila_x = $pdf->GetX();

    $tila_y = $pdf->GetY();

    // large/bold line width
    $pdf->SetLineWidth(1);
    //$pdf->Rect($tila_x, $tila_y, 208, $page_height - $tila_y - 10);
    $pdf->Rect($tila_x, $tila_y, 208, $page_height - $tila_y - 4);

    $tila_box_y = $pdf->GetY();
    $pdf->SetFillColor(240);
    $pdf->Rect(4, $tila_box_y, 50, 46, 'DF');
    $pdf->Rect(54, $tila_box_y, 55, 46, 'DF');
    $tila_box_x = $pdf->GetX() + 30;
    $pdf->SetX($tila_box_x);
    $pdf->setFont('Arial', 'B', 14);
    $y_temp = $pdf->GetY() + 4;
    $pdf->SetY($y_temp);
    $pdf->MultiCell(45, 4, "ANNUAL PERCENTAGE RATE", 0, 'C');
    $pdf->SetY($y_temp);
    $pdf->SetX(60); // orig: 55
    $pdf->MultiCell(45, 4, "FINANCE CHARGE", 0, 'C');
    $pdf->SetY($y_temp);
    $pdf->SetX(110); // orig 100
    $pdf->setFont('Arial', 'B', 12);
    $pdf->MultiCell(45, 4, "Amount Financed", 0, 'C');
    $pdf->SetY($y_temp);
    $pdf->SetX(160); // orig 150
    $pdf->MultiCell(45, 4, "Total of Payments", 0, 'C');

    $y_temp += 4;
    $pdf->SetY($y_temp);
    $pdf->SetX(150);
    $pdf->setFont("Arial", '', 12);
    $pdf->Ln(10);
    $y_temp = $pdf->GetY();
    $pdf->MultiCell(45, 4, "The cost of your credit as a yearly rate.", 0, 'C');
    $pdf->SetY($y_temp);
    $pdf->SetX(60);
    $pdf->MultiCell(45, 4, "The dollar amount the credit will cost you.", 0, 'C');
    $pdf->SetY($y_temp);
    $pdf->SetX(110);
    $pdf->setFont("Arial", '', 12);
    $pdf->MultiCell(45, 4, "The amount of credit provided to you or on your behalf.", 0, 'C');
    $pdf->SetY($y_temp);
    $pdf->SetX(160);
    $pdf->MultiCell(45, 4, "The amount you will have paid after making all payments as scheduled.", 0, 'C');

    $pdf->Ln(2);
    $y_temp = $pdf->GetY();
    $pdf->setFont("Arial", 'B', 12);
    $pdf->MultiCell(45, 4, "$tila_apr", 0, 'C');

    $pdf->SetXY(60, $y_temp);
    $pdf->MultiCell(45, 4, "$total_finance_charge", 0, 'C');

    $pdf->SetXY(110, $y_temp);
    $pdf->setFont("Arial", '', 12);
    $pdf->MultiCell(45, 4, "$amount_financed", 0, 'C');
    $pdf->SetXY(160, $y_temp);
    $pdf->MultiCell(45, 4, "$total_of_all_payments", 0, 'C');
    $pdf->Ln(10);

    $tila_box_y2 = $pdf->getY();
    $pdf->SetY($tila_box_y);
    //$pdf->Rect($tila_box_x - 2, $tila_box_y, 190, $tila_box_y2 - $tila_box_y, 'D');
    //$pdf->SetFillColor(250);
    // $pdf->Rect(4, $tila_box_y, 50, $tila_box_y2 - $tila_box_y-4, 'DF');
    //$pdf->Rect($tila_box_x - 2, $tila_box_y, 47, $tila_box_y2 - $tila_box_y, 'D');
    //$pdf->Rect(54, $tila_box_y, 55, $tila_box_y2 - $tila_box_y-4, 'DF');
    $pdf->SetLineWidth(.2);
    $pdf->SetFillColor(0, 0, 0);
    //$pdf->Rect(55, $tila_box_y, 45, $tila_box_y2 - $tila_box_y, 'D');
    $pdf->Rect(109, $tila_box_y, 48, $tila_box_y2 - $tila_box_y - 4, 'D');
    //$pdf->Rect(100, $tila_box_y, 45, $tila_box_y2 - $tila_box_y, 'D');
    $pdf->Rect(157, $tila_box_y, 55, $tila_box_y2 - $tila_box_y - 4, 'D');

    //End Tila Box Creation
    $pdf->SetY($tila_box_y2 - 2);
    $pdf->setFont('Arial', 'B', 12);
    $pdf->Cell(40, 4, "Payment Schedule:", 0, 0, 'L');
    $pdf->setFont('Arial', '', 12);
    $pdf->Cell(50, 4, "Your Payment Schedule will be:", 0, 0, 'L');
    $pdf->Ln(6);

    $pdf->SetX(6);
    // print table headers
    $pdf->setFont('Arial', 'B', 10);
    $pdf->Cell(40, 4, "Number of Payments", 1, 0, 'C');
    $pdf->Cell(50, 4, "Amount of Payments", 1, 0, 'C');
    $pdf->Cell(114, 4, "When Payments are Due", 1, 0, 'C');
    $pdf->Ln();

    $grouped_by_payment_amt = array_group_by($payment_schedule, "payment_amt");
    $num_payments_arr = array();
    $payment_amounts = array();
    $dates_arr = array();
    $dates_arr_string = array();
    $dates_string = '';

    foreach ($grouped_by_payment_amt as $key => $item) {
        array_push($num_payments_arr, $key);
        if (is_array($item)) {
            $tmp_arr = array();
            foreach ($item as $key1 => $value1) {
                $tmp1 = $value1['payment_dt'];
                $tmp2 = $value1['payment_amt'];
                array_push($tmp_arr, $tmp1);
                $dates_string .= $tmp1 . ", ";
            }
            $dates_string = rtrim(trim($dates_string), ",");
            array_push($dates_arr_string, $dates_string);
            $dates_string = '';
            array_push($dates_arr, $tmp_arr);
            array_push($payment_amounts, $tmp2);
        }
    }

    $left_margin_NumPayments = 6;
    $left_margin_PaymentAmt = 10;
    $left_margin_PaymentDates = 20;

    $pdf->SetX($left_margin_NumPayments);

    // first line
    $arr = $dates_arr[0];

    $pdf->Cell(40, 50, count($arr), 0, 0, 'C');
    $pdf->Cell(50, 50, "$" . $payment_amounts[0], 0, 0, 'C');

    $x = $pdf->GetX();
    $y = $pdf->GetY();
    $pdf->MultiCell(114, 5, $dates_arr_string[0]);
    $x = $pdf->GetX();
    $y = $pdf->GetY();

    // rectangles for the middle rows
    $pdf->rect(6, 181, 40, 55, "D");
    $pdf->rect(46, 181, 50, 55, "D");
    $pdf->rect(96, 181, 114, 55, "D");

    //if (count($grouped_by_payment_amt > 1)) {
        $pdf->setXY(6, 236);
        $arr = $dates_arr[1];
        $pdf->Cell(40, 5, count($arr), 0, 0, 'C');
        $pdf->Cell(50, 5, "$" . $payment_amounts[1], 0, 0, 'C');
        $pdf->Cell(114, 5, $dates_arr_string[1], 0, 0, 'C');
    //}

    // rectangles for the bottom row
    $pdf->rect(6, 236, 40, 4, "D");
    $pdf->rect(46, 236, 50, 4, "D");
    $pdf->rect(96, 236, 114, 4, "D");

    $pdf->SetAutoPageBreak(false);
    $pdf->SetXY(4, 248);
    $pdf->setFont('Arial', 'B', 12);
    $pdf->Cell(20, 4, "Security:", 0, 0, 'L');
    $pdf->setFont('Arial', '', 10);
    $pdf->MultiCell(0, 4, "If selected as your repayment method, then you are giving a security interest in the Payment Authorization.", 0, 'L');

    $pdf->setFont('Arial', 'B', 12);
    $pdf->Cell(26, 4, "Prepayment:", 0, 0, 'L');
    $pdf->setFont('Arial', '', 11);
    $pdf->MultiCell(0, 4, "If you prepay in full or in part, you will not have to pay a penalty.", 0, 'L');

    $pdf->setFont('Arial', 'B', 12);
    $pdf->Cell(26, 4, "Late Charge:", 0, 0, 'L');
    $pdf->setFont('Arial', '', 11);
    if ($customer_state == 'AL') {
        $pdf->MultiCell(0, 4, "If any scheduled payment is not paid in full within 15 days after its due date, you will be charged a late fee of 5% of the payment or $15, whichever is greater, not to exceed $100.", 0, 'L');
    } else if ($customer_state == 'CA') {
        $pdf->MultiCell(0, 4, "If any scheduled payment is not paid in full within 10 days after its due date, you will be charged a late fee of $10.", 0, 'L');
    } else if ($customer_state == 'MS') {
        $pdf->MultiCell(0, 4, "If a payment is more than 10 business days late, you will be charged the lesser of $5.00 or 5% of the installment due.", 0, 'L');
    } else if ($customer_state == 'NM') {
        $pdf->MultiCell(0, 4, "If a payment is more than 10 days late, you will be charged 5% of the installment due, up to a maximum of $10.", 0, 'L');
    } else if ($customer_state == 'UT') {
        $pdf->MultiCell(0, 4, "If a payment is more than 10 days late, you will be charged a late fee of $30 .", 0, 'L');
    } else {
        $pdf->MultiCell(0, 4, "If a payment is more than 15 days late, you will be charged 5% of the installment due.", 0, 'L');
    }
    $pdf->MultiCell(0, 4, "See the terms of this Agreement for any additional information about nonpayment, default, any required repayment in full before the scheduled date, and prepayment refunds and penalties.", 0, 'L');

    $pdf->AddPage();
    $pdf->SetAutoPageBreak(true);

    $pdf->setFont('Arial', 'B', 12);
    $pdf->Cell(0, 10, "Itemization of Amount Financed", 0, 0, 'L');
    $pdf->Ln(6);
    $pdf->setFont('Arial', '', 12);
    $pdf->Cell(120, 10, "Amount given to you directly:", 0, 0, 'L');
    $pdf->Cell(0, 10, "$amount_financed", 0, 0, 'L');
    $pdf->Ln(6);
    $pdf->Cell(120, 10, "(Plus) Amount paid on your Account:", 0, 0, 'L');
    $pdf->Cell(0, 10, "\$0.00 (Plus)", 0, 0, 'L');
    $pdf->Ln(6);
    $pdf->Cell(120, 10, "Amount paid to other on your behalf to ______________:", 0, 0, 'L');
    $pdf->Cell(0, 10, "\$0.00", 0, 0, 'L');
    $pdf->Ln(6);
    $pdf->Cell(120, 10, "(Equals) Principal amount of your loan:", 0, 0, 'L');
    $pdf->Cell(0, 10, "$amount_financed", 0, 0, 'L');
    $pdf->Ln(6);
    $pdf->setFont('Arial', 'B', 12);
    $pdf->Cell(120, 10, "(Equals) Amount Financed:", 0, 0, 'L');
    $pdf->Cell(0, 10, "$amount_financed", 0, 0, 'L');
    $pdf->Ln(6);
    $pdf->Cell(0, 10, "_______________________________________________");
    $pdf->Ln(10);
    $pdf->MultiCell(0, 4, "HIGH COST CREDIT DISCLOSURE: A " . strtoupper($lender_dba_name) . " LOAN IS AN EXPENSIVE FORM OF CREDIT. IT IS DESIGNED TO HELP CUSTOMERS MEET THEIR SHORT-TERM BORROWING NEEDS. THIS SERVICE IS NOT INTENDED TO PROVIDE A SOLUTION FOR LONGER-TERM CREDIT OR OTHER FINANCIAL NEEDS. ALTERNATIVE FORMS OF CREDIT MAY BE LESS EXPENSIVE AND MORE SUITABLE FOR YOUR FINANCIAL NEEDS.", 0, 'L');
    $pdf->Ln(10);

    //$pdf->setFont('Arial', 'B', 12);
    //$pdf->Cell(14, 4, "PROMISE TO PAY:");
    //$pdf->Ln(4);
    $pdf->setFont('Arial', '', 12);
    if ($customer_state == 'MS') {
        $pdf->MultiCell(0, 4, "PROMISE TO PAY: You promise to pay to the order of Lender the principal sum of $amount_financed plus a handling fee calculated at a rate of $contract_apr per year (\"Contract Rate\"). You agree to make payments on the dates and in the amounts shown in the Payment Schedule above, or as may be later modified (\"Payment Due Dates\"). You also promise to pay the Lender all other charges provided for under this Agreement.", 0, 'L');
    } else {
        $pdf->MultiCell(0, 4, "PROMISE TO PAY: You promise to pay to the order of Lender the principal sum of $amount_financed plus interest at a rate of $contract_apr per year.  You agree to make payments on the dates and in the amounts shown in the Payment Schedule above. You also promise to pay the Lender all other charges provided for under this Agreement.", 0, 'L');
    }
    $pdf->Ln(4);
    if ($customer_state == 'MS') {
        $pdf->MultiCell(0, 4, "HANDLING FEE: The handling fee will accrue daily on the unpaid principal balance of this Loan, beginning on the Effective Date. The handling fee will accrue until you pay the unpaid principal balance in full. However, after maturity or acceleration, any unpaid principal shall continue to accrue handling fees until repaid in full at the Contract Rate OR at the maximum rate allowed by law, whichever is less. The handling fee is not interest for the purposes of Mississippi law. If a Loan is refinanced with a new loan from us, you agree that we may continue to charge the handling fee on any refinanced amount of a Loan (shown as the \"Amount paid on your Account\" in the Itemization of Amount Financed above) until the effective date of the new loan. We calculate the handling fee based on a 365-day year (unless otherwise required by state law).  In calculating your payments, we have assumed you will make each payment on the day and in the amount due set forth in the Payment Scheduled. If any payment is received after the Payment Due Date, you must pay any additional handling fee that accrues after the Payment Due Date.  If any payment is made before a Payment Due Date, the handling fee due on the scheduled payment will be reduced and you will owe a lesser handling fee. If any Payment Due Date falls on a non-banking business day, then you agree to pay on the next banking business day, and we will credit such payment as if it was received on the Payment Due Date.  The amount of any decrease or increase in the handling fee due will affect the amount of your final payment.  If the amount of any payment is not enough to pay the handling fee due, the unpaid handling fee will be paid from your next payment(s), if any, and will not be added to the principal balance. Time is of the essence. The handling fee will continue to accrue on past due amounts in accordance with this Agreement and as permitted by applicable state law. The Contract Rate and other charges under this Agreement will never exceed the highest rate or charge allowed by applicable state law for this Loan. If the amount collected is found to have exceeded the highest rate or charge allowed, Lender will refund an amount necessary to comply with the law.", 0, 'L');
    } else {
        $pdf->MultiCell(0, 4, "INTEREST: Interest will accrue daily on the unpaid principal balance of this Loan, beginning on the Effective Date. Interest will accrue until you pay the unpaid principal balance in full. However, after maturity or acceleration, any unpaid principal shall continue to accrue interest until repaid in full at the Contract Rate OR at the maximum rate allowed by law, whichever is less.  If a Loan is refinanced with a new loan from us, you agree that we may continue to charge interest on any refinanced amount of a Loan (shown as the \"Amount paid on your Account\" in the Itemization of Amount Financed above) until the effective date of the new loan.  We calculate interest based on a 365-day year (unless otherwise required by state law).  In calculating your payments, we have assumed you will make each payment on the day and in the amount due set forth in the Payment Scheduled. If any payment is received after the Payment Due Date, you must pay any additional interest that accrues after the Payment Due Date.  If any payment is made before a Payment Due Date, the interest due on the scheduled payment will be reduced and you will owe less interest. If any Payment Due Date falls on a non-banking business day, then you agree to pay on the next banking business day, and we will credit such payment as if it was received on the Payment Due Date.  The amount of any decrease or increase in interest due will affect the amount of your final payment.  If the amount of any payment is not enough to pay the interest due, the unpaid interest will be paid from your next payment(s), if any, and will not be added to the principal balance. Time is of the essence. Interest will continue to accrue on past due amounts in accordance with this Agreement and as permitted by applicable state law. The interest rate and other charges under this Agreement will never exceed the highest rate or charge allowed by applicable state law for this Loan. If the amount collected is found to have exceeded the highest rate or charge allowed, Lender will refund an amount necessary to comply with law.", 0, 'L');
    }
    $pdf->Ln(4);
    if ($customer_state == 'NM') {
        $pdf->MultiCell(0, 4, "COMPUTATION OF INTEREST CHARGES UNDER THE SMALL LOAN ACT:  The simple interest method shall be used for loans made under the New Mexico Small Loan Act of 1955.  Interest charges shall not be paid, deducted, or received in advance.  Interest charges shall not be compounded.  However, if part or all of the consideration for a loan contract is the unpaid principal balance of a prior loan, then the principal amount payable under the loan contract may include any unpaid charges that have accrued within sixty days on the prior loan.  Such charges shall be computed on the basis of the number of days actually elapsed.", 0, 'L');
        $pdf->Ln(4);
    }
    if ($customer_state == 'MS') {
        $pdf->MultiCell(0, 4, "PAYMENTS: Lender will apply your payments in the following order: (1) any accrued but unpaid handling fee, (2) principal amounts outstanding, then (3) unpaid fees. If you have chosen the Payment Authorization option, each payment (including any accrued but unpaid handling fee and any fees, if applicable) will be debited from your Bank Account on each Payment Due Date and in accordance with the Payment Authorization. Any late payments may extend the term of your loan. See the Payment Authorization below for further information. If you choose to receive your Loan proceeds via check and to repay all amounts due pursuant to this Agreement via check, please mail each payment payable to $lender_dba_name ATTN: Payment Processing $lender_mailing_street_address, $lender_mailing_city, $lender_mailing_state $lender_mailing_zip_code, in time for Lender to receive the payment by 4:00PM Central Time on the applicable Payment Due Date.", 0, 'L');
    } else {
        $pdf->MultiCell(0, 4, "PAYMENTS: Lender will apply your payments in the following order: (1) accrued but unpaid interest, (2) principal amounts outstanding, then (3) unpaid fees. If you have chosen the Payment Authorization option, each payment (including any accrued but unpaid interest and any fees, if applicable) will be debited from your Bank Account on each Payment Due Date and in accordance with the Payment Authorization. Any late payments may extend the term of your loan. See the Payment Authorization below for further information. If you choose to receive your Loan proceeds via check and to repay all amounts due pursuant to this Agreement via check, please mail each payment payable to $lender_dba_name ATTN: Payment Processing $lender_mailing_street_address, $lender_mailing_city, $lender_mailing_state $lender_mailing_zip_code, in time for Lender to receive the payment by 4:00PM Central Time on the applicable Payment Due Date.", 0, 'L');
    }
    $pdf->Ln(4);
    if ($customer_state == 'MS') {
        $pdf->MultiCell(0, 4, "PREPAYMENT: You may prepay in whole or in part at any time without penalty. If you prepay in part, you must still make each later payment according to the Payment Schedule until this Loan is paid in full.  Any principal amounts you prepay will not continue to accrue the handling fee.", 0, 'L');
    } else {
        $pdf->MultiCell(0, 4, "PREPAYMENT: You may prepay in whole or in part at any time without penalty. If you prepay in part, you must still make each later payment according to the Payment Schedule until this Loan is paid in full.  Any principal amounts you prepay will not continue to accrue interest.", 0, 'L');
    }
    $pdf->Ln(4);
    $pdf->MultiCell(0, 4, "RIGHT OF RESCISSION: You may rescind or cancel this Loan if you do so on or before 5:00 p.m., Central Time on the third (3rd) business day after the Contract Date (the \"Rescission Deadline\"). To cancel, call Lender at $lender_phone_nbr to tell us you want to rescind or cancel this Loan and provide us with written notice of rescission as directed by our customer service representative.  The Jury Trial Waiver and Arbitration Clause set forth below survives any rescission.", 0, 'L');
    $pdf->Ln(4);
    $pdf->SetLeftMargin(10);
    if ($customer_state == 'MS') {
        $pdf->MultiCell(0, 4, "(a)	If you have provided a Payment Authorization: If we timely receive your written notice of rescission on or before the Rescission Deadline but before the Loan proceeds have been credited to your Bank Account, we will not deposit your Loan proceeds to your Bank Account and both ours and your obligations under this agreement will be rescinded. If we timely receive your written notice of rescission on or before the Rescission Deadline but after the Loan proceeds have been credited to your Bank Account, we will debit your Bank Account for the principal amount owed under this Agreement, in accordance with the Payment Authorization. If we receive payment of the principal amount via the debit, ours and your obligations under this Agreement will be rescinded. If we do not receive payment of the principal amount via the debit, then the Agreement will remain in full force and effect until all amounts under this Agreement are repaid in full, including any handling fee.", 0, 'L');
    } else {
        $pdf->MultiCell(0, 4, "(a)	If you have provided a Payment Authorization: If we timely receive your written notice of rescission on or before the Rescission Deadline but before the Loan proceeds have been credited to your Bank Account, we will not deposit your Loan proceeds to your Bank Account and both ours and your obligations under this agreement will be rescinded. If we timely receive your written notice of rescission on or before the Rescission Deadline but after the Loan proceeds have been credited to your Bank Account, we will debit your Bank Account for the principal amount owed under this Agreement, in accordance with the Payment Authorization. If we receive payment of the principal amount via the debit, ours and your obligations under this Agreement will be rescinded. If we do not receive payment of the principal amount via the debit, then the Agreement will remain in full force and effect until all amounts under this Agreement are repaid in full, including any interest.", 0, 'L');
    }
    $pdf->Ln(4);
    if ($customer_state == 'MS') {
        $pdf->MultiCell(0, 4, "(b)	If you have elected to receive your Loan proceeds via check delivered by mail: If we timely receive your written notice of rescission on or before the Rescission Deadline, and (a) if we have not mailed the check representing the Loan proceeds to you or (b) if you have not cashed the check representing the Loan proceeds, then we will cancel the check and both ours and your obligations under this Agreement will be rescinded. If you have cashed the check representing the Loan proceeds, you must return the full amount of cash you received to us by the Rescission Deadline. If we do not receive the full amount by the Rescission Deadline, then the Agreement will remain in full force and effect until all amounts owed under this Agreement are repaid in full, including any handling fee.", 0, 'L');
    } else {
        $pdf->MultiCell(0, 4, "(b)	If you have elected to receive your Loan proceeds via check delivered by mail: If we timely receive your written notice of rescission on or before the Rescission Deadline, and (a) if we have not mailed the check representing the Loan proceeds to you or (b) if you have not cashed the check representing the Loan proceeds, then we will cancel the check and both ours and your obligations under this Agreement will be rescinded. If you have cashed the check representing the Loan proceeds, you must return the full amount of cash you received to us by the Rescission Deadline. If we do not receive the full amount by the Rescission Deadline, then the Agreement will remain in full force and effect until all amounts owed under this Agreement are repaid in full, including any interest.", 0, 'L');
    }
    $pdf->Ln(4);
    $pdf->SetX(0);
    $pdf->SetLeftMargin(4);
    if ($customer_state == 'AL') {
        $pdf->MultiCell(0, 4, "LATE CHARGE: If any scheduled payment is not paid in full within 15 days after its due date, you will be charged a late fee of 5% of the payment or $15, whichever is greater, not to exceed $100. Only one late charge shall be charged on any one late installment, regardless of the period during which the installment remains unpaid.", 0, 'L');
    } else if ($customer_state == 'CA') {
        $pdf->MultiCell(0, 4, "LATE CHARGE: If any scheduled payment is not paid in full within 10 after its due date, you will be charged a late fee of $10.", 0, 'L');
    } else if ($customer_state == 'MS') {
        $pdf->MultiCell(0, 4, "LATE CHARGE: If a payment is more than 10 business days late, you will be charged the lesser of $5.00 or 5% of the installment due.", 0, 'L');
    } else if ($customer_state == 'NM') {
        $pdf->MultiCell(0, 4, "LATE CHARGE: If a payment is more than 10 days late, you will be charged 5% of the installment due, up to a maximum of $10.", 0, 'L');
    } else if ($customer_state == 'UT') {
        $pdf->MultiCell(0, 4, "LATE CHARGE: If a payment is more than 10 days late, you will be charged a late fee of $30 .", 0, 'L');
    } else {
        $pdf->MultiCell(0, 4, "LATE CHARGE: If a payment is more than 15 days late, you will be charged 5% of the installment due.", 0, 'L');
    }
    $pdf->Ln(4);
    $pdf->MultiCell(0, 4, "ELECTRONIC CHECK RE-PRESENTMENT POLICY: In the event a check is returned unpaid for insufficient or uncollected funds, we may re-present the check electronically.  In the ordinary course of business, the check will not be provided to you with your bank statement, but a copy can be retrieved by contacting your financial institution.", 0, 'L');
    $pdf->Ln(4);
    $pdf->MultiCell(0, 4, "CHECK CONVERSION NOTIFICATION: When you provide a check as payment, you agree we can either use the information from your check to make a one-time electronic withdrawal from your Bank Account or to process the payment as a check transaction. When we use information from your check to make a withdrawal from your Bank Account, funds may be withdrawn from your Bank Account as soon as the same day we receive your payment and you will not receive your check back from your financial institution.  For questions please call our customer service number, $lender_phone_nbr.", 0, 'L');
    $pdf->Ln(4);
    $pdf->MultiCell(0, 4, "BORROWER BANK CHARGES: Your financial institution may charge a fee if your Bank Account becomes overdrawn or if a payment is attempted from your Bank Account that would cause it to become overdrawn. You will not hold us or our agents, representatives, successors or assigns responsible for any such fee you must pay.", 0, 'L');
    $pdf->Ln(4);
    $pdf->MultiCell(0, 4, "SECURITY: We have disclosed to you that our interest in the payment method indicated in the Federal Truth-in-Lending Disclosures above is a security interest for Truth-in-Lending purposes only because federal and applicable state law do not clearly address the issue. However, the federal Truth-in-Lending disclosures are not intended to create a security interest under applicable state law.", 0, 'L');
    $pdf->Ln(4);
    $pdf->MultiCell(0, 4, "REFINANCE POLICY: Subject to applicable state law and our credit policies, we will determine, in our sole discretion, whether your Loan may be refinanced.", 0, 'L');
    $pdf->Ln(4);
    if ($customer_state == 'UT') {
        $pdf->MultiCell(0, 4, "CREDIT REPORTING: You agree that Lender may make inquiries concerning your credit history and standing. As required by Utah law, you are hereby notified that a negative credit report reflecting on your credit record may be submitted to a credit reporting agency if you fail to fulfill the terms of your credit obligations.", 0, 'L');
        $pdf->Ln(4);
    } else {
        $pdf->MultiCell(0, 4, "CREDIT REPORTING: You agree that Lender may make inquiries concerning your credit history and standing, and may report information concerning your performance under this Agreement to credit reporting agencies. Late payments, missed payments or other defaults on your Loan may be reflected in your credit report.", 0, 'L');
        $pdf->Ln(4);
    }
    $pdf->MultiCell(0, 4, "CHANGE OF PRIMARY RESIDENCE: You agree to notify Lender of any change in your primary residence as soon as possible, but no later than five (5) days after any change. You agree that the address provided on this Agreement will govern this Agreement until you have met all obligation under this Agreement and that any subsequent change in your address will not affect the terms or enforceability of this Agreement.", 0, 'L');
    $pdf->Ln(4);
    $pdf->MultiCell(0, 4, "CORRESPONDENCE WITH LENDER: General correspondence with Lender concerning this Loan, this Agreement or your relationship with Lender must be sent to Lender at the following address: $lender_name, $lender_mailing_street_address, $lender_mailing_city, $lender_mailing_state $lender_mailing_zip_code. Communications related to the bankruptcy of the Borrower must be sent to Lender at the following address: $lender_name ATTN: Bankruptcy Handling, $lender_mailing_street_address, $lender_mailing_city, $lender_mailing_state $lender_mailing_zip_code.", 0, 'L');
    $pdf->Ln(4);
    $pdf->MultiCell(0, 4, "FORCE MAJEURE: Unavoidable delays because of inadvertent processing errors and/or \"acts of God\" may extend the time for the deposit of Loan proceeds and the processing of your payments.", 0, 'L');
    $pdf->Ln(4);
    if ($customer_state == 'MS') {
        $pdf->MultiCell(0, 4, "TRANSFER OF RIGHTS/HYPOTHECATION: This instrument is non-negotiable in form but may be pledged as collateral security. If so pledged, any payment made to the Lender, either of principal or of any handling fee, upon the debt evidenced by this obligation, will be considered and construed as a payment on this instrument, the same as though it were still in the possession and under the control of the Lender named herein; and the pledgee holding this instrument as collateral security hereby makes said Lender its agent to accept and receive payments hereon, either of principal or of any handling fee.", 0, 'L');
    } else {
        $pdf->MultiCell(0, 4, "TRANSFER OF RIGHTS/HYPOTHECATION: This instrument is non-negotiable in form but may be pledged as collateral security. If so pledged, any payment made to the Lender, either of principal or of interest, upon the debt evidenced by this obligation, will be considered and construed as a payment on this instrument, the same as though it were still in the possession and under the control of the Lender named herein; and the pledgee holding this instrument as collateral security hereby makes said Lender its agent to accept and receive payments hereon, either of principal or of interest.", 0, 'L');
    }
    $pdf->Ln(4);
    $pdf->MultiCell(0, 4, "SUCCESSORS AND ASSIGNS: This Agreement is binding upon your heirs and personal representatives in probate upon anyone to whom you assign your assets or who succeeds you in any other way. You agree that Lender may assign or transfer this Agreement and any of lender's rights hereunder at any time without prior notice to you, except as required by applicable law. You may not assign this Agreement without the prior, written consent of Lender.", 0, 'L');
    $pdf->Ln(4);
    $pdf->MultiCell(0, 4, "SERVICING COMMUNICATIONS AND COMMUNICATIONS AFTER DEFAULT: You authorize Lender and its authorized representatives to contact you according to your consent provided in your application or according to your account preferences, as modified by you after submitting your application. This may include (i) calling you during reasonable hours at any of the phone numbers listed on your most recent application (ii) contacting you by text message or other wireless communication method on the mobile phone number listed on your application, (iii) leaving a message with a person or a voice mail service, and (iv) contacting you using autodialers or pre-recorded message, including calls to your mobile phone.", 0, 'L');
    $pdf->Ln(4);
    $pdf->setFont('Arial', 'B', 12);
    $pdf->MultiCell(0, 4, "AUTOMATIC PAYMENT AUTHORIZATION:", 0, 'L');
    $pdf->Ln(4);
    $pdf->MultiCell(0, 4, "(Applies only if you select the electronic funding option or authorize recurring debit card payments)", 0, 'L');
    $pdf->Ln(4);
    $pdf->setFont('Arial', '', 12);
    $pdf->MultiCell(0, 4, "This Automatic Payment Authorization (\"Payment Authorization) is a part of and related to this Agreement. You voluntarily authorize us, and our successors, affiliates, agents, representatives, employees, and assigns, to initiate automatic credit and debit entries to your Bank Account in accordance with this Agreement. For purposes of this Payment Authorization, \"Bank Account\" also includes any other bank account that you provide to Lender, from which you intend to permit recurring payments.  You agree that we will initiate a credit entry to your Bank Account for the amount that we are to give to you directly, as indicated in the Itemization of Amount Financed, on or about the Disbursement Date. You also authorize us to credit or debit your Bank Account to correct any errors related to this Loan.", 0, 'L');
    $pdf->Ln(4);
    if ($customer_state == 'MS') {
        $pdf->MultiCell(0, 4, "You agree that we will initiate a debit entry to debit your Bank Account on or after each Payment Due Date in the payment amount described in the Payment Schedule (including as modified) plus any late fees and/or accrued but unpaid handling fees, up to and including the full amount you owe. You acknowledge that you will receive a notice at least 10 days before a payment is debited from your Bank Account if the payment we are going to debit from your Bank Account falls outside this range. You have the right to receive notice of all varying amounts but have agreed to only receive notice if a debit will fall outside this range.", 0, 'L');
    } else {
        $pdf->MultiCell(0, 4, "You agree that we will initiate a debit entry to debit your Bank Account on or after each Payment Due Date in the payment amount described in the Payment Schedule (including as modified) plus any late fees and/or accrued but unpaid interest, up to and including the full amount you owe. You acknowledge that you will receive a notice at least 10 days before a payment is debited from your Bank Account if the payment we are going to debit from your Bank Account falls outside this range. You have the right to receive notice of all varying amounts but have agreed to only receive notice if a debit will fall outside this range.", 0, 'L');
    }
    $pdf->Ln(4);
    $pdf->MultiCell(0, 4, "For each scheduled payment, whenever a debit entry to your Bank Account is returned to us for any reason, we may initiate a debit entry to your Bank Account up to two additional times after our first attempt for each scheduled payment amount, to the extent permitted by law.  If your payment is due on a non-business day, it will be processed on the next business day and credited as if made on the Payment Date.  If you still have an outstanding balance after the final Payment Date, then you authorize us to issue debits in the same (or lesser) amount as the final Payment Date debit on the same recurring intervals as set forth in the Payment Schedule (including as modified) until the balance is paid in full. ", 0, 'L');
    $pdf->Ln(4);
    if ($customer_state == 'MS') {
        $pdf->MultiCell(0, 4, "You agree that this Payment Authorization is for repayment of a Credit Availability Act loan and that payments shall recur at substantially regular intervals as set forth in this Agreement. This Payment Authorization is to remain in full force and effect for the transaction until you pay your Loan, including any handling fee, in full. You may only revoke this Payment Authorization by contacting us directly at $lender_phone_nbr or via mail at $lender_mailing_street_address $lender_mailing_city, $lender_mailing_state $lender_mailing_zip_code.  If you revoke your Payment Authorization, you agree to make payments to us as set forth in the \"Payments\" section above by another acceptable payment method. In no event will any revocation of this Payment Authorization be effective with respect to entries processed by us prior to us receiving such revocation.", 0, 'L');
    } else {
        $pdf->MultiCell(0, 4, "You agree that this Payment Authorization is for repayment of a consumer installment loan and that payments shall recur at substantially regular intervals as set forth in this Agreement. This Payment Authorization is to remain in full force and effect for the transaction until you pay your Loan, including any interest, in full. You may only revoke this Payment Authorization by contacting us directly at $lender_phone_nbr or via mail at $lender_mailing_street_address $lender_mailing_city, $lender_mailing_state $lender_mailing_zip_code.  If you revoke your Payment Authorization, you agree to make payments to us as set forth in the \"Payments\" section above by another acceptable payment method. In no event will any revocation of this Payment Authorization be effective with respect to entries processed by us prior to us receiving such revocation.", 0, 'L');
    }
    $pdf->Ln(4);
    $pdf->MultiCell(0, 4, "Your bank may charge you a fee in connection with our credit and/or debit entries. Contact your financial institution for more information specific to your Bank Account.", 0, 'L');
    $pdf->Ln(4);
    $pdf->MultiCell(0, 4, "Missing or Incorrect Information. If there is any missing or incorrect information in or with your loan application regarding your bank, bank routing number, or account number, then you authorize us to verify and correct such information.", 0, 'L');
    $pdf->Ln(4);
    $pdf->MultiCell(0, 4, "This Payment Authorization is a payment mechanism only and does not give us collection rights greater than those otherwise contained in this Agreement.", 0, 'L');
    $pdf->Ln(4);
    if ($customer_state == 'AL' || $customer_state == 'DE' || $customer_state == 'MS' || $customer_state == 'NM' || $customer_state == 'UT') {
        $pdf->MultiCell(0, 4, "EVENTS OF DEFAULT: The following constitute events of default under this Agreement: (a) failure to pay as scheduled; and (b) our prospect of payment or performance is significantly impaired.", 0, 'L');
    } else if ($customer_state == 'CA') {
        $pdf->MultiCell(0, 4, "LENDER'S RIGHTS IN THE EVENT OF DEFAULT: Upon the occurrence of any event of default, Lender may at its option, do any one or more of the following: (a) exercise all rights, powers and remedies given by law; (b) subject to any limitations imposed by applicable state law, declare the entire unpaid outstanding balance due, but any failure of Lender to exercise such option will not constitute a waiver of Lender’s option in the event of any subsequent default; (c) recover from you all costs and disbursements in connection with any suit to collect a loan to which the Lender becomes entitled by law; and/or (d) assign any and all of Lender's interest in and to this Agreement to a third party, thereby vesting in such a third party all rights, powers and privileges of Lender hereunder. Any delay by Lender in exercising any or all of these rights shall not constitute a waiver of such rights.", 0, 'L');
    } else {
        $pdf->MultiCell(0, 4, "EVENTS OF DEFAULT: The following constitute events of default under this Agreement: (a) failure to pay as scheduled; and (b) our prospect of payment or performance is significantly impaired.  If you default, then you have a right to cure such default in accordance with Missouri law.", 0, 'L');
    }
    $pdf->Ln(4);
    if ($customer_state == 'AL' || $customer_state == 'CA') {
        $pdf->MultiCell(0, 4, "LENDER'S RIGHTS IN THE EVENT OF DEFAULT: Upon the occurrence of any event of default, Lender may at its option, do any one or more of the following: (a) exercise all rights, powers and remedies given by law; (b) subject to any limitations imposed by applicable state law, declare the entire unpaid outstanding balance due, but any failure of Lender to exercise such option will not constitute a waiver of Lender’s option in the event of any subsequent default; (c) recover from you reasonable attorney fees incurred by Lender as a result of the suit or activity and to which the Lender becomes entitled by law, not exceeding 15% of the unpaid debt after default; and/or (d) assign any and all of Lender's interest in and to this Agreement to a third party, thereby vesting in such a third party all rights, powers and privileges of Lender hereunder. Any delay by Lender in exercising any or all of these rights shall not constitute a waiver of such rights.", 0, 'L');
    } else if ($customer_state == 'DE') {
        $pdf->MultiCell(0, 4, "LENDER'S RIGHTS IN THE EVENT OF DEFAULT: Upon the occurrence of any event of default, Lender may at its option, do any one or more of the following: (a) exercise all rights, powers and remedies given by law; (b) subject to any limitations imposed by applicable state law, declare the entire unpaid outstanding balance due, but any failure of Lender to exercise such option will not constitute a waiver of Lender’s option in the event of any subsequent default; (c) recover from you all costs and disbursements in connection with any suit to collect a loan, including reasonable attorney fees from an attorney not a regularly salaried employee of the licensee incurred by Lender as a result of the suit or activity and to which the Lender becomes entitled by law, not to exceed 20% of the unpaid principal and interest; and/or (d) assign any and all of Lender's interest in and to this Agreement to a third party, thereby vesting in such a third party all rights, powers and privileges of Lender hereunder. Any delay by Lender in exercising any or all of these rights shall not constitute a waiver of such rights.", 0, 'L');
    } else if ($customer_state == 'MS') {
        $pdf->MultiCell(0, 4, "LENDER'S RIGHTS IN THE EVENT OF DEFAULT: Upon the occurrence of any event of default, Lender may at its option, do any one or more of the following: (a) exercise all rights, powers and remedies given by law; (b) subject to any limitations imposed by applicable state law, declare the entire unpaid outstanding balance due, but any failure of Lender to exercise such option will not constitute a waiver of Lender’s option in the event of any subsequent default; (c) recover from you all costs and disbursements in connection with any suit to collect a loan, including reasonable attorney fees incurred by Lender as a result of the suit or activity and to which the Lender becomes entitled by law; and/or (d) assign any and all of Lender's interest in and to this Agreement to a third party, thereby vesting in such a third party all rights, powers and privileges of Lender hereunder. Any delay by Lender in exercising any or all of these rights shall not constitute a waiver of such rights.", 0, 'L');
    } else if ($customer_state == 'UT') {
        $pdf->MultiCell(0, 4, "LENDER'S RIGHTS IN THE EVENT OF DEFAULT: Upon the occurrence of any event of default, Lender may at its option, do any one or more of the following: (a) exercise all rights, powers and remedies given by law; (b) subject to any limitations imposed by applicable state law, declare the entire unpaid outstanding balance due, but any failure of Lender to exercise such option will not constitute a waiver of Lender’s option in the event of any subsequent default; (c) recover from you all costs and disbursements in connection with any suit to collect a loan, including reasonable attorney fees incurred by Lender as a result of the suit or activity and to which the Lender becomes entitled by law; and/or (d) assign any and all of Lender's interest in and to this Agreement to a third party, thereby vesting in such a third party all rights, powers and privileges of Lender hereunder. Any delay by Lender in exercising any or all of these rights shall not constitute a waiver of such rights. If we hire an attorney or a third party collection agency to collect what you owe, you will also pay the lesser of (a) the actual amount we are required to pay the third party collection agency or attorney, regardless of whether that amount is a specific dollar amount or a percentage of the amount owed to us; or (b) 40% of the principal amount owed to us.", 0, 'L');
    } else {
        $pdf->MultiCell(0, 4, "LENDER'S RIGHTS IN THE EVENT OF DEFAULT: Upon the occurrence of any event of default, Lender may at its option and subject to your right to cure, do any one or more of the following: (a) exercise all rights, powers and remedies given by law; (b) subject to any limitations imposed by applicable state law, declare the entire unpaid outstanding balance due, but any failure of Lender to exercise such option will not constitute a waiver of Lender’s option in the event of any subsequent default; (c) assign any and all of Lender's interest in and to this Agreement to a third party, thereby vesting in such a third party all rights, powers and privileges of Lender hereunder. Any delay by Lender in exercising any or all of these rights shall not constitute a waiver of such rights.", 0, 'L');
    }
    $pdf->Ln(4);

    $tmp_state = mm_state_cd_to_display($customer_state);
    $pdf->MultiCell(0, 4, "GOVERNING LAW; SEVERABILITY; INTERSTATE COMMERCE:  This Agreement is governed by the laws of the State of $tmp_state, except that the Jury Trial Waiver and Arbitration Clause is governed by the Federal Arbitration Act (\"FAA\"), 9 U.S.C. §§ 1-9.  If any provision of this Agreement is held unenforceable, including any provision of the Jury Trial Waiver and Arbitration Clause, the remainder of this Agreement will remain in full force and effect.  You and we agree that the transaction represented by this Agreement involves interstate commerce for all purposes.", 0, 'L');
    $pdf->Ln(4);
    $pdf->setFont('Arial', 'B', 12);
    $pdf->MultiCell(0, 4, "JURY TRIAL WAIVER AND ARBITRATION CLAUSE.", 0, 'L');
    $pdf->Ln(4);
    $pdf->setFont('Arial', '', 12);
    $pdf->MultiCell(0, 4, "By signing this agreement, you agree to the Jury Trial Waiver and Arbitration Clause (\"Clause\"). You also understand that this Clause supersedes the arbitration provision included in your application.  In addition, this Clause applies to any modification of this Agreement.", 0, 'L');
    $pdf->Ln(4);

    $pdf->SetWidths(array(30, 30, 140));
    $pdf->Row(array("What is arbitration?", "An alternative to court.", "In arbitration, a third party (\"Arbiter\") resolves Disputes in a hearing (\"hearing\"). You, related third parties, and we, waive the right to go to court. Such \"parties\" waive jury trials."));
    $pdf->Row(array("Is it different from court and jury trials?", "Yes.", "The hearing is private and less formal than court. Arbiters may limit pre-hearing fact finding, called \"discovery.\" The decision of the Arbiter is final. Courts rarely overturn Arbiters' decisions."));
    $pdf->Row(array("Who does the Clause cover?", "You, Us, and Others.", "This Clause governs the parties, their heirs, successors, assigns, and third parties related to any Dispute."));
    $pdf->Row(array("Which Disputes are covered?", "All Disputes.", "In this Clause, the word \"Disputes\" has the broadest possible meaning. This Clause governs all \"Disputes\" involving the parties to this Agreement and/or our servicers and agents, including but not limited to consultants, banks, payment processors, software providers, data providers and credit bureaus. This includes all claims even indirectly related to your application and agreements with us. This includes claims related to information you previously gave us. It includes all past agreements. It includes extensions, renewals, refinancings, or payment plans. It includes claims related to collections, privacy, and customer information.  It includes claims related to setting aside this Clause. It includes claims about the Clause's validity and scope. It includes claims about whether to arbitrate."));
    $pdf->Row(array("Are you waiving rights?", "Yes.", "You waive your rights to:
 1.	Have juries resolve Disputes.
 2.	Have courts, other than small-claims courts, resolve Disputes.
 3.	Serve as a private attorney general or in a representative capacity.
 4.	Be in a class action.
 ", ));
    $pdf->Row(array("Are you waiving class action rights?", "Yes.", "COURTS AND ARBITERS WON'T ALLOW CLASS ACTIONS. You waive your rights to participate in a class action as a representative and a member. Only individual arbitration or small-claims courts will resolve Disputes. You waive your right to have representative claims. Unless reversed on appeal, if a court invalidates this waiver, the Clause will be void."));
    $pdf->Row(array("What law applies?", "The Federal Arbitration Act (\"FAA\").", "This transaction involves interstate commerce, so the FAA governs. If a court finds the FAA doesn't apply, and the finding can't be appealed, then your state's law governs. The Arbiter must apply substantive law consistent with the FAA. The Arbiter must follow statutes of limitation and privilege claims."));
    $pdf->Row(array("Can the parties try to resolve Disputes first?", "Yes.", "We can try to resolve Disputes if you call us at $lender_phone_nbr. If we are unable to resolve the Dispute by phone, mail us notice within 30 days of the date you first brought the Dispute to our attention.  In your notice, tell us the details and how you want to resolve it. We will try to resolve the Dispute. If we make a written offer (\"Settlement Offer\"), you can reject it and arbitrate. If we don't resolve the Dispute, either party may start arbitration. To start arbitration, contact an Arbiter or arbitration group listed below. No party may disclose settlement proposals to the Arbiter during arbitration."));
    $pdf->Row(array("How should you contact us?", "By mail.", "Send mail to: [State Entity Name AND State Correspondence ADDRESS]. You can call us or use certified mail to confirm receipt."));
    $pdf->Row(array("Can small-claims court resolve some Disputes?", "Yes.", "Each party has the right to arbitrate, or to go to small-claims court if the small-claims court has the power to hear the Dispute.  Arbitration will resolve all Disputes that the small-claims court does not have the power to hear.  If there is an appeal from small-claims court, or if a Dispute changes so that the small-claims court loses the power to hear it, then the Dispute will be heard only by an Arbiter."));
    $pdf->Row(array("Do other options exist?", "Yes.", "Both parties may seek remedies which don't claim money damages. This includes pre-judgment seizure, injunctions, or equitable relief."));
    $pdf->Row(array("Will this Clause continue to govern?", "Yes, unless otherwise agreed.", "The Clause will remain effective unless the parties agree otherwise in writing. The Clause governs if you rescind the transaction. It governs if you default, renew, prepay, or pay. It governs if your contract is discharged through bankruptcy. The Clause will remain effective despite a transaction's termination, amendment, expiration, or performance."));
    $pdf->setFont('Arial', 'B', 12);
    $pdf->Cell(0, 6, "Process.", 0, 1, 'C');
    $pdf->setFont('Arial', '', 12);
    $pdf->Row(array("How does arbitration start?", "Mailing a notice.", "Either party may mail the other a request to arbitrate, even if a lawsuit has been filed. The notice should describe the Dispute and relief sought. The receiving party must mail a response within 20 days. If you mail the demand, you may choose the arbitration group. Or your demand may state that you want the parties to choose a local Arbiter. If we or related third parties mail the demand, you must respond within 20 days. Your response must choose an arbitration group or propose a local Arbiter. If it doesn't, we may choose the group."));
    $pdf->Row(array("Who arbitrates?", "AAA, JAMS, or an agreed Arbiter.", "You may select the American Arbitration Association (\"AAA\") (1-800-778-7879) http://www.adr.org or JAMS (1-800-352-5267) http://www.jamsadr.com. The parties to a Dispute may also agree in writing to a local attorney, retired judge, or Arbiter in good standing with an arbitration group. The Arbiter must arbitrate under AAA or JAMS consumer rules. You may get a copy of these rules from such group. Any rules that conflict with any of our agreements with you don't apply. If these options aren't available and the parties can't agree on another, a court may choose the Arbiter. Such Arbiter must enforce your agreements with us as they are written."));
    $pdf->Row(array("Will the hearing be held nearby?", "Yes.", "The Arbiter will order the hearing within 30 miles of your home or where the transaction occurred unless otherwise required by law."));
    $pdf->Row(array("What about appeals?", "Appeals are limited.", "The Arbiter's decision will be final. A party may file the Arbiter's award with the proper court. Arbitration will resolve appeals of a small-claims court judgment. A party may appeal under the FAA. If the amount in controversy exceeds $10,000.00, a party may appeal the Arbiter's finding. Such appeal will be to a three-Arbiter panel from the same arbitration group. The appeal will be de novo, and resolved by majority vote. The appealing party bears appeal costs, despite the outcome."));
    $pdf->setFont('Arial', 'B', 12);
    $pdf->Cell(0, 6, "Arbitration Fees and Awards.", 0, 1, 'C');
    $pdf->setFont('Arial', '', 12);
    $pdf->Row(array("Will we advance Arbitration Fees?", "Yes, but you pay your costs.", "We will advance your arbitration fees if you ask us to in writing.  This includes filing, administrative, hearing, and Arbiter's fees. You are responsible for paying your attorney fees and other expenses."));
    $pdf->Row(array("Are damages and attorney fees possible?", "Yes, if allowed by law.", "The Arbiter may award the same damages as a court.  Arbiters may award reasonable attorney fees and expenses, if allowed by law."));
    $pdf->Row(array("Will you pay Arbitration Fees if you win?", "No.", "If the Arbiter awards you funds you don't reimburse us the arbitration fees."));
    $pdf->Row(array("Will you ever pay Arbitration Fees?", "Yes.", "If the Arbiter doesn't award you funds, then you must repay the arbitration fees. If you must pay arbitration fees, the amount won't exceed state court costs."));
    $pdf->Row(array("What happens if you win?", "You could receive an award from Arbiter. ", "If you are successful in arbitration you may receive an Arbiter's award. If so ordered by the Arbiter, we will pay your attorney the attorney fees conferred, as well as reasonable expert witness costs and other costs you incurred."));
    $pdf->Row(array("Can an award be explained?", "Yes.", "A party may request details from the Arbiter within 14 days of the ruling.  Upon such request, the Arbiter will explain the ruling in writing."));
    $pdf->setFont('Arial', 'B', 12);
    $pdf->Cell(0, 6, "Other Options.", 0, 1, 'C');
    $pdf->setFont('Arial', '', 12);
    $pdf->Row(array("If you don't want to arbitrate, can you still contract for our services?", "Yes. You can contract for our services and decide not to arbitrate.", "Consider these choices:
     1.	Informal Dispute Resolution. Contact us and attempt to settle any Disputes.
     2.	Small-claims Court. Seek to resolve Disputes in small-claims court, within state law limits.
     3.	Opt-Out of Arbitration. Sign the Agreement and then timely opt-out of Arbitration.
     ", ));
    $pdf->Row(array("Can you opt-out of the Clause?", "Yes. Within 60 days.", "Send us written notice of your intent to opt-out of the Arbitration Clause of this Agreement within sixty (60) calendar days of signing your Agreement. List your name, address, account number and date. List that you \"opt out.\" If you opt out, it will only apply to this Agreement."));
    $pdf->setFont('Arial', 'B', 12);
    $pdf->Cell(0, 6, "Additional Acknowledgements and Electronic Signature.", 0, 1, 'C');
    $pdf->setFont('Arial', '', 12);
    if ($customer_state == 'CA') {
        $pdf->MultiCell(0, 5, "YOU ACKNOWLEDGE AND REPRESENT THAT:
 (A)	YOU HAVE READ, UNDERSTAND, AND AGREE TO ALL OF THE TERMS AND CONDITIONS OF THIS AGREEMENT INCLUDING THE WAIVER OF JURY TRIAL AND ARBITRATION CLAUSE AND RIGHT TO PARTICIPATE IN A CLASS ACTION AS WELL AS THE PRIVACY NOTICE;
 (B)	THE INFORMATION GIVEN IN CONNECTION WITH THIS AGREEMENT IS TRUE AND CORRECT;
 (C)	YOU HAVE THE FINANCIAL ABILITY TO REPAY IN ACCORDANCE WITH THIS AGREEMENT;
 (D)	YOU AUTHORIZE LENDER TO VERIFY THE INFORMATION GIVEN IN CONNECTION WITH THIS AGREEMENT AND GIVE LENDER CONSENT TO OBTAIN INFORMATION ON YOU FROM CONSUMER REPORTING AGENCIES OR OTHER SERVICES;
 (E)	THIS AGREEMENT WAS FILLED IN BEFORE YOU SIGNED IT;
 (F)	YOU HAD AN OPPORTUNITY TO PRINT A COMPLETED COPY OF THIS AGREEMENT FOR MY RECORDS;
 (G)	THIS AGREEMENT IS SUBJECT TO APPROVAL BY LENDER;
 (H)	YOU ARE 18 YEARS OF AGE OR OLDER AND NOT AN EMPLOYEE OF MONEY MART; AND
 (I)	YOU ARE NOT A DEBTOR UNDER ANY PROCEEDING IN BANKRUPTCY AND HAVE NO INTENTION TO FILE A PETITION FOR RELIEF UNDER THE U.S. BANKRUPTCY CODE.
 (J)	NO PERSON HAS PERFORMED ANY ACT AS A BROKER IN CONNECTION WITH THE MAKING OF THIS LOAN.
 ", 0);
    } else {
        $pdf->MultiCell(0, 5, "YOU ACKNOWLEDGE AND REPRESENT THAT:
 (A)	YOU HAVE READ, UNDERSTAND, AND AGREE TO ALL OF THE TERMS AND CONDITIONS OF THIS AGREEMENT INCLUDING THE WAIVER OF JURY TRIAL AND ARBITRATION CLAUSE AND RIGHT TO PARTICIPATE IN A CLASS ACTION AS WELL AS THE PRIVACY NOTICE;
 (B)	THE INFORMATION GIVEN IN CONNECTION WITH THIS AGREEMENT IS TRUE AND CORRECT;
 (C)	YOU HAVE THE FINANCIAL ABILITY TO REPAY IN ACCORDANCE WITH THIS AGREEMENT;
 (D)	YOU AUTHORIZE LENDER TO VERIFY THE INFORMATION GIVEN IN CONNECTION WITH THIS AGREEMENT AND GIVE LENDER CONSENT TO OBTAIN INFORMATION ON YOU FROM CONSUMER REPORTING AGENCIES OR OTHER SERVICES;
 (E)	THIS AGREEMENT WAS FILLED IN BEFORE YOU SIGNED IT;
 (F)	YOU HAD AN OPPORTUNITY TO PRINT A COMPLETED COPY OF THIS AGREEMENT FOR MY RECORDS;
 (G)	THIS AGREEMENT IS SUBJECT TO APPROVAL BY LENDER;
 (H)	YOU ARE 18 YEARS OF AGE OR OLDER AND NOT AN EMPLOYEE OF MONEY MART; AND
 (I)	YOU ARE NOT A DEBTOR UNDER ANY PROCEEDING IN BANKRUPTCY AND HAVE NO INTENTION TO FILE A PETITION FOR RELIEF UNDER THE U.S. BANKRUPTCY CODE.
 ", 0);
    }
    $pdf->setFont('Arial', 'B', 12);
    $pdf->MultiCell(0, 4, "BY CLICKING THE ACKNOWLEDGMENT BUTTON BELOW, YOU UNDERSTAND THAT YOU ARE ELECTRONICALLY SIGNING THIS AGREEMENT. YOU AGREE THAT THIS ELECTRONIC SIGNATURE HAS THE FULL FORCE AND EFFECT OF MY PHYSICAL SIGNATURE AND THAT IT BINDS YOU TO THIS AGREEMENT IN THE SAME MANNER A PHYSICAL SIGNATURE WOULD.", 0, 'C');
    $pdf->setFont('Arial', '', 12);
    $pdf->Cell(0, 5, "$lender_name D/B/A $lender_dba_name", 0, 1, "L");
    $pdf->Ln(5);
    $pdf->Cell(0, 5, "By: <<Signatory>>, <<SigTitle>>", 0, 1, "L");
    $pdf->Ln(10);
    $pdf->setFont('Arial', 'B', 12);
    $pdf->Cell(0, 5, "Customer Electronic Signature:", 0, 1, "L");
    $pdf->setFont('Arial', '', 12);
    $pdf->Ln(5);
    if ($customer_state == 'AL') {
        $pdf->Cell(0, 5, "CAUTION -- IT IS IMPORTANT THAT YOU THOROUGHLY READ THE CONTRACT BEFORE YOU SIGN IT.", 0, 1, 'L');
        $pdf->Ln(5);
    }
    $pdf->Cell(70, 5, "/s/ $customer_name", 0, 0, "L");
    $pdf->Cell(70, 5, "$contract_dt", 0, 0, "L");

    //Save the PDF document
    $doc_path = mm_get_document_path();
    $doc_path = $doc_path . "$account_nbr/";
    if (!file_exists($doc_path)) {
        //The account directory doesn't exists so create it
        if (!mkdir($doc_path)) {
            //Unable to make the directory.
            mm_log_error('mm_generate_loan_agreement_pdf', "Unable to create the directory $doc_path");
        }
    }
    $doc_path = $doc_path . "$application_nbr/";
    if (!file_exists($doc_path)) {
        //The application directory doesn't exist so create it
        if (!mkdir($doc_path)) {
            mm_log_error('mm_generate_loan_agreement_pdf', "Unable to create the directory $doc_path");
        }
    }
    $doc_path = $doc_path . "loan_agreement.pdf";

    $pdf->Output('F', $doc_path);

    //Move the document to a secure location
    if ($signature_data == 1) {
        mm_send_ftp_document($doc_path, "loan_agreement_$application_nbr - signed.pdf", "customer_documents/$account_nbr/$application_nbr/", "B");
    } else {
        mm_send_ftp_document($doc_path, "loan_agreement_$application_nbr.pdf", "customer_documents/$account_nbr/$application_nbr/", "B");
    }
    //Delete the temporary document

    if (!unlink($doc_path)) {
        mm_log_error('mm_generate_loan_agreement_pdf', "Unable to delete the file $doc_path");
    }

    //Hard coded for now
    $return_array["document_path"] = "customer_documents/$account_nbr/$application_nbr/loan_agreement_$application_nbr.pdf";
    $return_array["return_value"] = 0;
    return $return_array;

}

/******************************************************************************************
 * Function mm_get_account_details_from_email_address($email_address)
 ******************************************************************************************/
function mm_get_account_details_from_email_address($email_address)
{
    error_log("flow: mm_get_account_details_from_email_address\n");
    //NEED TO ADD ERROR HANDLING TO THIS FUNCTION
    $return_array = array();
    //$account_nbr = 0;
    $sql_string = "select * from mm_account where email_address = ?";
    $conn = mm_get_db_connection();
    $stmt = $conn->prepare($sql_string);
    $stmt->bind_param('s', $email_address);
    $stmt->execute();
    $result = $stmt->get_result();
    $num_rows = $result->num_rows;
    if ($num_rows == 0) {
        //Need to add default value setting for accounts with mm account
        $return_array["account_nbr"] = 0;
    } else {
        $row = $result->fetch_assoc();
        foreach ($row as $key => $value) {
            $return_array["$key"] = $value;
        }
    }

    $return_array["return_value"] = 0;
    $return_array["return_message"] = "Account Lookup Successful";

    return $return_array;
}

/*******************************************************************************************
 * Function mm_get_most_recent_application_details($account_nbr);
 *******************************************************************************************/

function mm_get_most_recent_application_details($account_nbr)
{
    error_log("flow: mm_get_most_recent_application_details\n");
    // todo: NEED TO ADD ERROR HANDLING
    //Excludes voided applications (status = 11)
    $sql_string = "select * from mm_application where application_nbr = (select max(application_nbr) as application_nbr from mm_application where account_nbr = ?) and application_status <> 11";
    $conn = mm_get_db_connection();
    $stmt = $conn->prepare($sql_string);
    $stmt->bind_param('i', $account_nbr);
    $stmt->execute();
    $result = $stmt->get_result();
    $num_rows = $result->num_rows;
    $row = $result->fetch_assoc();
    $app_data = array();
    if ($num_rows == 0) {

    } else {
        foreach ($row as $key => $value) {
            $app_data["$key"] = $value;
        }
    }
    $return_array = array();
    $return_array["return_value"] = 0;
    $return_array["return_message"] = "Success";
    $return_array["app_data"] = $app_data;
    $return_array["num_apps"] = $num_rows;

    return $return_array;

}

/**
 * @param string $pay_freq
 * @param string $first_pay_dt
 * @return DateTime
 */
function mm_calculate_future_pay_dt_California(string $pay_freq, string $first_pay_dt)
{
    error_log("flow: mm_calculate_future_pay_dt_California\n");
    $dt_first_pay_date = new DateTime($first_pay_dt);
    $dt_next_pay_date = clone $dt_first_pay_date;
    switch (strtoupper($pay_freq)) {
        case 'W':
        case 'B':
            // loop to find the next pay date that is >= 15 days
            error_log("mark: the value of dt_first_pay_date: " . $dt_first_pay_date->format('Y-m-d'));
            error_log("mark: the value of dt_next_pay_date: " . $dt_next_pay_date->format('Y-m-d'));
            $tmp = $dt_next_pay_date->diff($dt_first_pay_date)->days;
            while ($tmp < 15) {
                $dt_next_pay_date->modify("+14 day");
                $tmp = $dt_next_pay_date->diff($dt_first_pay_date)->days;
            }
            break;
        case 'M':
            $start_dom = $dt_next_pay_date->format('j');
            $dt_next_pay_date->modify("+1 month");
            $end_dom = $dt_next_pay_date->format('j');
            if ($start_dom != $end_dom) {
                $dt_next_pay_date->modify('last day of last month');
            }
            break;
        case 'S1':
            $dom = $dt_next_pay_date->format('j');
            if ($dom === 1) {
                $dt_next_pay_date->modify("+14 day");
                if ($dt_next_pay_date->diff($dt_first_pay_date)->days < 15) {
                    $dt_next_pay_date->modify('first day of next month');
                }
            }
            break;
        case 'S2':
            $dom = $dt_next_pay_date->format('j');
            if ($dom === 15) {
                $dt_next_pay_date->modify('last day of this month');
            } else {
                $dt_next_pay_date->modify('+15 day');
            }
            break;
        default:
    }

    return $dt_next_pay_date->format("m/d/Y");
}
/*************************************************************
 * mm_calculate_future_pay_dt($pay_freq, $first_pay_dt, $payment_nbr)
 * pay_freq - 'W' - Weekly, 'B' - Bi-Weekly, 'M' - Monthly, 'S1' Semi-Monthly 1st and 15th, 'S2' - Semi-Montly 15th and last day of the month.
 *
 * Returns a date in the format MM/DD/YYYY which represents the next pay date from the date provided.  The current date is excluded if it is a paydate.
 *************************************************************/
function mm_calculate_future_pay_dt($pay_freq, $first_pay_dt, $payment_nbr)
{
    error_log("flow: mm_calculate_future_pay_dt\n");
    //error_log("mark: starting mm_calculate_future_pay_dt, pay_freq: ".print_r($pay_freq, true).", first_pay_date: ".print_r($first_pay_dt, true).", payment_nbr: ".print_r($payment_nbr, true));
    $date = new DateTime($first_pay_dt);

    // mark was here: adjusting for only bi-weekly payments
    /* if ($pay_freq == 'W') {
    $num_days_to_add = $payment_nbr * 7;
    $date->modify("+{$num_days_to_add} day");
    } else */

    if ($pay_freq == 'W' || $pay_freq == 'B') {
        $num_days_to_add = $payment_nbr * 14;
        $date->modify("+{$num_days_to_add} day");
    } else if ($pay_freq == 'M') {
        //$date = new DateTime($first_pay_dt);
        $start_day = $date->format('j');
        $date->modify("+{$payment_nbr} month");
        $end_day = $date->format('j');
        if ($start_day != $end_day) {
            $date->modify('last day of last month');
        }
    } else if ($pay_freq == 'S1') {
        $i = 0;
        while ($i < $payment_nbr) {
            $dom = $date->format('j');
            if ($dom == 1) {
                $date->modify("+14 day");
            } else {
                $date->modify('first day of next month');
            }
            $i = $i + 1;
        }
    } else if ($pay_freq == 'S2') {
        $i = 0;
        while ($i < $payment_nbr) {
            $dom = $date->format('j');
            if ($dom == 15) {
                $date->modify('last day of this month');
            } else {
                $date->modify('+ 15 day');
            }

            $i = $i + 1;
        }

    }

    $next_pay_dt = $date->format("m/d/Y");

    return $next_pay_dt;
}

/***************************************************************************************
 * Function mm_calculate_contract_date($curr_timestamp) - Takes a current time stamp and returns a date in the format mm/dd/YYYY which represents the proposed contract date.
 ***************************************************************************************/
function mm_calculate_contract_date($curr_timestamp, $deposit_type = "ACH")
{
    error_log("flow: mm_calculate_contract_date\n");
    $cutoff_time = 17;
    $date = new DateTime($curr_timestamp);
    //$is_contract_date = 0;
    //$i = 0;
    $curr_date = $date->format('m/d/Y');
    //$is_holiday = mm_is_bank_holiday($curr_date);
    $curr_hour = $date->format('H');
    $curr_dow = $date->format('D');

    if ($deposit_type == "Check") {
        $date->modify('+10 days');
        $date->setTime(0, 0, 0);
        $curr_date = $date->format('m/d/Y');
        //$curr_hour = $date->format('H');
        $curr_dow = $date->format('D');
    } else {
        /*if($is_holiday){
        //No processing on holiday so move to the next day to start the logic
        $date->modify('+1 day');
        $date->setTime(0,0,0);
        $curr_date = $date->format('m/d/Y');
        $curr_hour = $date->format('H');
        $curr_dow = $date->format('D');
        }*/

        if (($curr_dow == 'Fri' && $curr_hour >= $cutoff_time) || $curr_dow == 'Sat' || ($curr_dow == 'Sun' && $curr_hour < $cutoff_time)) {
            //The date is between Friday after cutoff and before Sunday's cutoff so make the effective date Monday
            while ($curr_dow != 'Mon') {
                $date->modify('+1 day');
                $curr_date = $date->format('m/d/Y');
                $curr_dow = $date->format('D');
            }
        } else if (($curr_dow == "Sun" && $curr_hour >= $cutoff_time) || ($curr_dow == 'Mon' && $curr_hour < $cutoff_time)) {
            //The date is between Sunday after cutoff and before Monday's cutoff so make the effective date Tuesday
            while ($curr_dow != 'Tue') {
                $date->modify('+1 day');
                $curr_date = $date->format('m/d/Y');
                $curr_dow = $date->format('D');
            }
        } else if (($curr_dow == 'Mon' && $curr_hour >= $cutoff_time) || ($curr_dow == 'Tue' && $curr_hour < $cutoff_time)) {
            //The date is between Monday after cutoff and Tuesday before cutoff so make the effective date Thursday
            while ($curr_dow != 'Wed') {
                $date->modify('+1 day');
                $curr_date = $date->format('m/d/Y');
                $curr_dow = $date->format('D');
            }
        } else if (($curr_dow == 'Tue' && $curr_hour >= $cutoff_time) || ($curr_dow == 'Wed' && $curr_hour < $cutoff_time)) {
            //The date is between Tuesday after cutoff and Wednesday before cutoff so make the effective date Friday
            while ($curr_dow != 'Thu') {
                $date->modify('+1 day');
                $curr_date = $date->format('m/d/Y');
                $curr_dow = $date->format('D');
            }
        } else if (($curr_dow == 'Wed' && $curr_hour >= $cutoff_time) || ($curr_dow == 'Thu' && $curr_hour < $cutoff_time)) {
            //The date is between Wednesday after cutoff and Thursday before cutoff so make the effective date Saturday
            while ($curr_dow != 'Fri') {
                $date->modify('+1 day');
                $curr_date = $date->format('m/d/Y');
                $curr_dow = $date->format('D');
            }
        } else if (($curr_dow == 'Thu' && $curr_hour >= $cutoff_time) || ($curr_dow == 'Fri' && $curr_hour < $cutoff_time)) {
            //The date is between Thursday after cutoff and Friday before cutoff so make the effective date Monday
            while ($curr_dow != 'Mon') {
                $date->modify('+1 day');
                $curr_date = $date->format('m/d/Y');
                $curr_dow = $date->format('D');
            }
        }
    }

    //Now that the BAU effective date is set.  Check to make sure that the date isn't a holiday.  If it is then keep moving the date until you reach the next business day.
    if (mm_is_bank_holiday($curr_date)) {
        if ($curr_dow == 'Sat') {
            $date->modify('+2 days');
            $curr_date = $date->format('m/d/Y');
        } else if ($curr_dow == 'Fri') {
            $date->modify('+3 days');
            $curr_date = $date->format('m/d/Y');
        } else {
            $date->modify('+1 days');
            $curr_date = $date->format('m/d/Y');
        }

    }

    return $curr_date;
}

/****************************************************************************************************************************************
 * Function mm_process_verifications()
 ****************************************************************************************************************************************/

function mm_process_verifications($preverif_lead_id_, $mm_application_nbr, $requested_amt, $requested_deposit_type)
{
    error_log("flow: mm_process_verifications\n");
    $return_array = array();

    /*mm_update_database_value('mm_application', 'requested_amt', $requested_amt, 'd', "application_nbr", $mm_application_nbr);
    mm_update_database_value('mm_application', 'requested_deposit_type', $requested_deposit_type, 's', "application_nbr", $mm_application_nbr);
    mm_update_database_value('mm_application', 'application_status', 3, 'i', "application_nbr", $mm_application_nbr);
    mm_update_database_value('mm_application', 'preverif_lead_id', $preverif_lead_id, 'i', "application_nbr", $mm_application_nbr);*/
    $preverif_results = mm_submit_preverifications($mm_application_nbr);
    $vf_decision = $preverif_results["vf_decision"];

    $return_array["vf_decision"] = $vf_decision;
    return $return_array;

}

/********************************************************************
 * Function process_application - Wrapper function called by the website to handle a customer submitting an application
 ********************************************************************/
function process_application($form_lead_id = 0)
{
    error_log("flow: process_application\n");
    //error_log("mark: start process_application, form_lead_id: $form_lead_id");
    $return_array = array();
    $app_data = array();

    // todo: is this still temporary?
    if ($form_lead_id > 3) { //Temporary For Testing
        $app_data = mm_get_lead_details($form_lead_id);
        $app_email_address = $app_data["email_address"];
    }

    // this is for subsquent mm_get_application_details
    //mm_set_cache_value('app_data', $app_data);
    $return_array["app_data"] = $app_data;

    if ($form_lead_id == 1) {
        $return_array["return_value"] = 0;
        $return_array["return_message"] = "the app was successfully processed";
        $return_array["mm_application_nbr"] = 24;
        $return_array["mm_uw_decision_nbr"] = 1;
        $return_array["uw_application_nbr"] = 115;
        $return_array["noaa_cd"] = 0;
        $return_array["noaa_location"] = "";
        $return_array["approved_amt"] = 1500;
        $return_array["uw_decision"] = "Approved";
    } else if ($form_lead_id == 2) {
        $return_array["return_value"] = 0;
        $return_array["return_message"] = "the app was successfully processed";
        $return_array["mm_application_nbr"] = 24;
        $return_array["mm_uw_decision_nbr"] = 1;
        $return_array["uw_application_nbr"] = 115;
        $return_array["noaa_cd"] = 55;
        $return_array["noaa_location"] = "dev.moneymartdirect.com/wp-content/themes/moneymartdirect/noaa.pdf";
        $return_array["approved_amt"] = 0;
        $return_array["uw_decision"] = "Declined";
    } else if ($form_lead_id == 3) {
        $return_array["return_value"] = 1;
        $return_array["return_message"] = "there was an issue processing the application";
        $return_array["mm_application_nbr"] = 24;
        $return_array["mm_uw_decision_nbr"] = 1;
        $return_array["uw_application_nbr"] = 115;
        $return_array["noaa_cd"] = 0;
        $return_array["noaa_location"] = "";
        $return_array["approved_amt"] = 0;
        $return_array["uw_decision"] = "Stuck";

    } else {
        $return_array = mm_submit_application($app_data);
        // this is for subsequent mm_get_application_details
        $tmp = $return_array["mm_account_nbr"];
        //error_log("after submit_application, mm_account_nbr: $tmp");
        mm_set_cache_value('app_data_' . $tmp, $app_data);
    }

    return $return_array;

}

/**********************************************************************************************
 * Function mm_get_lender_details - Function takes a state code and returns the information needed
 **********************************************************************************************/
function mm_get_lender_details($state_cd)
{
    error_log("flow: mm_get_lender_details\n");
    // NEED TO ADD ERROR HANDLING TO THIS FUNCTION
    // $return_array = array();

    $sql_string = "select * from mm_lender where state_cd = ?";
    $conn = mm_get_db_connection();
    $stmt = $conn->prepare($sql_string);
    $stmt->bind_param('s', $state_cd);
    $stmt->execute();
    $results = $stmt->get_result();
    $num_rows = $results->num_rows;
    if ($num_rows == 0) {
        //There are no rows returned so set the variables very conservatively.
        $row["lender_nbr"] = 0;
        $row["legal_name"] = SITE_NAME . " LLC";
        $row["dba_name"] = SITE_NAME . "LLC";
        $row["state_cd"] = "$state_cd";
        $row["display_state"] = mm_state_cd_to_display($state_cd);
        $row["mailing_street_address"] = SUPPORT_ADDRESS;
        $row["mailing_city"] = SUPPORT_CITY;
        $row["mailing_state_cd"] = SUPPORT_STATE;
        $row["mailing_zip_code"] = SUPPORT_ZIP;
        $row["physical_street_address"] = "1111 S. Main St Suite 215";
        $row["physical_city"] = "Grapevine";
        $row["physical_state_cd"] = "TX";
        $row["physical_zip_code"] = "76051";
        $row["phone_nbr"] = "(844) 678-5626";
        $row["fax_nbr"] = "(817) 523-8752";
        $row["email_address"] = SUPPORT_EMAIL;
        $row["late_fee_amt"] = 0;
        $row["late_fee_pct"] = 0;
        $row["late_fee_calc"] = "AMT";
        $row["late_fee_grace_period"] = 10;
        $row["returned_item_fee_amt"] = 0;

    } else {
        $row = $results->fetch_assoc();
    }
    $return_array = $row;
    $return_array["return_value"] = 0;

    return $return_array;
}

/******************************************************************************************************************
 * Function mm_get_noaa_text($noaa_cd) - Returns the text needed for the
 ******************************************************************************************************************/
function mm_get_noaa_text($noaa_cd)
{
    error_log("flow: mm_get_noaa_text\n");
    // Mark was here:  what is the purpose of this function?
    $return_string = '';
    switch ($noaa_cd) {
        case 10:
            $return_string = "Some NOAA Text";
            break;

    }
    //Temp Code
    $return_string = "This is some text that should go in the dynamic section of the NOAA.";

    return $return_string;
}

/*****************************************************************************************************************
 * Function mm_generate_application_pdf($application_nbr)
 *****************************************************************************************************************/
function mm_generate_application_pdf($application_nbr)
{
    error_log("flow: mm_generate_application_pdf\n");
    //Initialize Variables
    $return_array = array();
    //$app_data = array();
    //$return_value = 0;
    //$return_message = '';
    //$doc_path = '';
    //Get Application Data
    $app_array = mm_get_application_details($application_nbr);
    $app_data = $app_array["app_data"];
    $account_nbr = $app_data["account_nbr"];
    $ssn1 = $app_data["ssn"];
    $ssn_clean = "XXX-XX-" . substr($ssn1, 5, 4);

    //Generate Application PDF
    $pdf = new PDF('P', 'mm', 'Letter');
    $pdf->AddPage();
    //Add Heading For Personal Details Section
    $pdf->setFont('Arial', 'B', 12);
    $pdf->Cell(0, 10, 'Application For Credit', 0, 0, 'C');
    $pdf->Ln(10);
    $pdf->print_header('Section 1:Personal Information');
    //Add output for Personal Details Section
    $pdf->print_document_label('First Name: ', 20, 0, 0);
    $pdf->print_underline_text($app_data["first_name"], 50, 0, 0);
    $pdf->print_document_label('Last Name: ', 20, 0, 0);
    $pdf->print_underline_text($app_data["last_name"], 50, 0, 0);
    $pdf->print_document_label('DOB: ', 10, 0, 0);
    $pdf->print_underline_text($app_data["dob"], 25, 0, 1);
    $pdf->print_document_label('Street Address:  ', 25, 0, 0);
    $pdf->print_underline_text($app_data["street_address"], 100, 0, 0);
    $pdf->print_document_label('Apt/Suite:  ', 15, 0, 0);
    $pdf->print_underline_text($app_data["appt_suite"], 30, 0, 1);
    $pdf->print_document_label('City:  ', 15, 0, 0);
    $pdf->print_underline_text($app_data["city"], 50, 0, 0);
    $pdf->print_document_label('State:  ', 15, 0, 0);
    $pdf->print_underline_text($app_data["state"], 20, 0, 0);
    $pdf->print_document_label('Zip Code:  ', 15, 0, 0);
    $pdf->print_underline_text($app_data["zip_code"], 20, 0, 1);
    $pdf->print_document_label('Email Address:  ', 25, 0, 0);
    $pdf->print_underline_text($app_data["email_address"], 70, 0, 1);
    $pdf->print_document_label('Cell Phone:  ', 20, 0, 0);
    $pdf->print_underline_text($app_data["mobile_phone_nbr"], 30, 0, 0);
    $pdf->print_document_label('Home Phone:  ', 20, 0, 0);
    $pdf->print_underline_text($app_data["home_phone_nbr"], 30, 0, 1);
    $pdf->print_document_label('Social Security Number: ', 40, 0, 0);
    $pdf->print_underline_text($ssn_clean, 30, 0, 1);

    //Output information related to employment
    $pdf->print_header('Section 2: Income and Housing Expense');
    $pdf->print_document_label('Do you rent or own your residence?', 50, 0, 0);
    $pdf->print_underline_text($app_data["own_or_rent"], 15, 0, 1);
    $pdf->print_document_label('If you own, how much is your monthly mortgage?', 85, 0, 0);
    $pdf->print_underline_text($app_data["monthly_mtg_amt"], 10, 0, 1);
    $pdf->print_document_label('If you rent, how much is your monthly rent amount?', 100, 0, 0);
    $pdf->print_underline_text($app_data["monthly_rent_amt"], 10, 0, 1);
    $pdf->print_document_label('PRIMARY INCOME:', 25, 0, 1);
    $pdf->print_document_label('Income Source: ', 25, 0, 0);
    $pdf->print_underline_text($app_data["income_source_1"], 30, 0, 0);
    $pdf->print_document_label('Company Name: ', 25, 0, 0);
    $pdf->print_underline_text($app_data["company_name_1"], 70, 0, 1);
    $pdf->print_document_label('Are you paid hourly or salary? ', 43, 0, 0);
    $pdf->print_underline_text($app_data["wage_type_1"], 15, 0, 0);
    $pdf->print_document_label('If Salary, what is your annual salary? ', 55, 0, 0);
    $pdf->print_underline_text($app_data["annual_salary_1"], 15, 0, 1);
    $pdf->print_document_label('If Hourly, what is your hourly rate? ', 55, 0, 0);
    $pdf->print_underline_text($app_data["hourly_rate_1"], 15, 0, 0);
    $pdf->print_document_label('Average Hours Per Week? ', 40, 0, 0);
    $pdf->print_underline_text($app_data["hours_per_week_1"], 15, 0, 1);
    $pdf->print_document_label('Pay Frequency: ', 25, 0, 0);
    $pdf->print_underline_text($app_data["pay_frequency_1"], 15, 0, 1);
    $pdf->print_document_label('If Monthly, what day of the month are you paid? ', 75, 0, 0);
    $pdf->print_underline_text($app_data["day_paid_month_1"], 15, 0, 1);
    $pdf->print_document_label('If Weekly or Bi-Weekly, what day of the week are you paid? ', 90, 0, 0);
    $pdf->print_underline_text($app_data["day_paid_weekly_1"], 15, 0, 1);
    $pdf->print_document_label('If Twice Monthly, what days of the month are you paid? ', 75, 0, 0);
    $pdf->print_underline_text($app_data["day_paid_twice_monthly_1"], 15, 0, 1);
    $pdf->print_document_label('Do you have direct deposit? ', 55, 0, 0);
    $pdf->print_underline_text($app_data["direct_deposit_ind_1"], 15, 0, 0);
    $pdf->print_document_label('Next Pay Date:  ', 25, 0, 0);
    $pdf->print_underline_text($app_data["next_pay_dt_1"], 15, 0, 1);
    $pdf->print_document_label('Last Paycheck Amount:  ', 35, 0, 0);
    $pdf->print_underline_text($app_data["last_paycheck_amt_1"], 15, 0, 1);
    $pdf->print_document_label('SECONDARY INCOME:', 25, 0, 1);
    $pdf->print_document_label('Income Source: ', 25, 0, 0);
    $pdf->print_underline_text($app_data["income_source_2"], 30, 0, 0);
    $pdf->print_document_label('Company Name: ', 25, 0, 0);
    $pdf->print_underline_text($app_data["company_name_2"], 70, 0, 1);
    $pdf->print_document_label('Are you paid hourly or salary? ', 43, 0, 0);
    $pdf->print_underline_text($app_data["wage_type_2"], 15, 0, 0);
    $pdf->print_document_label('If Salary, what is your annual salary? ', 55, 0, 0);
    $pdf->print_underline_text($app_data["annual_salary_2"], 15, 0, 1);
    $pdf->print_document_label('If Hourly, what is your hourly rate? ', 55, 0, 0);
    $pdf->print_underline_text($app_data["hourly_rate_2"], 15, 0, 0);
    $pdf->print_document_label('Average Hours Per Week? ', 40, 0, 0);
    $pdf->print_underline_text($app_data["hours_per_week_2"], 15, 0, 1);
    $pdf->print_document_label('Pay Frequency: ', 25, 0, 0);
    $pdf->print_underline_text($app_data["pay_frequency_2"], 15, 0, 1);
    $pdf->print_document_label('If Monthly, what day of the month are you paid? ', 75, 0, 0);
    $pdf->print_underline_text($app_data["day_paid_month_1"], 15, 0, 1);
    $pdf->print_document_label('If Weekly or Bi-Weekly, what day of the week are you paid? ', 90, 0, 0);
    $pdf->print_underline_text($app_data["day_paid_weekly_2"], 15, 0, 1);
    $pdf->print_document_label('If Twice Monthly, what days of the month are you paid? ', 75, 0, 0);
    $pdf->print_underline_text($app_data["day_paid_twice_monthly_2"], 15, 0, 1);
    $pdf->print_document_label('Do you have direct deposit? ', 55, 0, 0);
    $pdf->print_underline_text($app_data["direct_deposit_ind_2"], 15, 0, 0);
    $pdf->print_document_label('Next Pay Date:  ', 25, 0, 0);
    $pdf->print_underline_text($app_data["next_pay_dt_2"], 15, 0, 1);
    $pdf->print_document_label('Last Paycheck Amount:  ', 35, 0, 0);
    $pdf->print_underline_text($app_data["last_paycheck_amt_2"], 15, 0, 1);

    //Output information related to Banking Information
    $pdf->print_header('Section 3: Banking Information');
    $pdf->print_document_label('Bank Routing Number:  ', 40, 0, 0);
    $pdf->print_underline_text($app_data["routing_nbr"], 20, 0, 0); // was 25
    $pdf->print_document_label('Bank Account Number:  ', 40, 0, 0);
    $pdf->print_underline_text($app_data["bank_acct_nbr"], 25, 0, 0);
    // Mark was here: todo:  Add the Account Type
    $pdf->print_document_label('Bank Account Type:  ', 40, 0, 0);
    $pdf->print_underline_text($app_data["bank_acct_type"], 25, 0, 1);
    $pdf->print_document_label('How long have you had this account?  ', 60, 0, 0); // was 55
    $pdf->print_document_label('Years: ', 15, 0, 0);
    $pdf->print_underline_text($app_data["bank_acct_age_years"], 5, 0, 0);
    $pdf->print_document_label('Months: ', 15, 0, 0);
    $pdf->print_underline_text($app_data["bank_acct_age_months"], 5, 0, 1);
    $pdf->Cell(5, 10, "  X  ", 0, 0, 'C');
    $pdf->setFont('Arial', '', 8);
    $pdf->Cell(5, 10, "", 0, 0, 'L');
    $pdf->Cell(0, 10, "Check here if this account is active and in good standing.*", 0, 0, 'L');
    $pdf->Ln(5);

    $pdf->AddPage();
    //Output information related to Legal Disclosures and Application Signature
    $pdf->print_header('Section 4: Legal Disclosures and Application Signatures');

    // BEGIN Agree to Electronic Disclosures
    $pdf->Ln(2);
    $pdf->setFont('Arial', 'I', 6);
    //$pdf->MultiCell(0, 4, "By checking this box and submitting your application, you agree that ".SITE_NAME." and its authorized representatives may contact you about our services and promotions by calling or texting you at any number provided on your account, including your cell phone. You acknowledge that you may be charged by your wireless provider in order to receive text messages. Agreeing to receive communications such as calls and/or texts is not required in order to continue the application process or to obtain credit.", 0, 'L', 0);
    //
    $pdf->MultiCell(0, 4, "CONSENT FOR ELECTRONIC RECORDS
    Please read this information carefully and, for future reference, either print a copy of this document or retain this information electronically:
    1. INTRODUCTION: You are submitting a credit application to Lender. We can give you the benefits of our on-line service only if you consent to use and accept electronic signatures, electronic records, and electronic disclosures in connection with the transaction (your “Consent”). By completing and submitting an on-line credit application (your “Application”), you acknowledge that you have received this document and have consented to the use of electronic signatures, electronic records, and electronic disclosures in connection with this transaction (Collectively, “Records”).
    2. ELECTRONIC COMMUNICATIONS: You may request a paper copy of any Record by emailing Lender at: support@aspenfinancialdirect.com. You may request a paper copy even if you withdraw your Consent. Lender will retain the Records as required by law and will provide you with a paper copy of any Record at no charge.
    3. CONSENTING TO DO BUSINESS ELECTRONICALLY: Before giving your Consent, you should consider whether you have the required hardware and software capabilities described below.
    4. SCOPE OF CONSENT: Your consent and our agreement to conduct this transaction electronically only apply to this transaction. If we receive your Consent, then we will conduct this transaction with you electronically.
    5. HARDWARE AND SOFTWARE REQUIREMENTS: To access and retain the Records electronically, you will need to use the following computer software and hardware: A computer with Internet access and an Internet Browser that meets the following minimum requirements: 1.3GHz or faster processor, Microsoft® Windows® XP (32 bit and 64 bit), Windows 7 (32 bit and 64 bit),Windows 8 or 8.1 (32 bit and 64 bit),256MB of RAM,320MB of available hard-disk space, Internet Explorer 7, 8, 9, 10, or 11; Firefox® Extended Support Release; Chrome®. Also the Internet Browser must support at least 128-bit encrypted browsers encryption. To read some documents, you will need a PDF file reader like Adobe Acrobat Reader®. If at any time during this transaction these requirements change in a way that creates a material risk that you may not be able to receive Records electronically, Lender will notify you of these changes.
    6. WITHDRAWING CONSENT: You may withdraw this Consent at any time and at no charge to you. If you withdraw your Consent prior to receiving the credit transaction, then your withdrawal will prevent you from obtain an online credit transaction (i.e. a credit transaction obtained over the Internet). To withdraw your Consent, you must e-mail us at support@aspenfinancialdirect.com. Note, however, that the withdrawal of your Consent will not affect the legal effectiveness, validity, or enforceability of the credit transaction or of any Records that you received electronically prior to such withdrawal (including but limited to the arbitration agreement).
    7. CHANGE TO YOUR CONTACT INFORMATION: You must keep us informed of any change in your e-mail address, your ordinary mail address, or other contact information. To update your address information, access your online account created or call us at (877) 293-2987 or e-mail us at support@aspenfinancialdirect.com.
    AUTHORIZATION, AGREEMENT, AND REPRESENTATIONS, BY ENTERING THE LAST FOUR DIGITS OF YOUR SOCIAL SECURITY NUMBER BELOW, YOU CONSENT TO USE AND ACCEPT ELECTRONIC SIGNATURES, ELECTRONIC RECORDS, AND ELECTRONIC DISCLOSURES IN CONNECTION WITH THIS TRANSACTION. YOU ACKNOWLEDGE THAT YOU CAN ACCESS THE RECORDS IN THE DESIGNATED FORMAT DESCRIBED ABOVE, AND YOU UNDERSTAND THAT YOU MAY REQUEST A PAPER COPY OF THE RECORDS AT ANY TIME AND AT NO CHARGE. IF YOU DO NOT GIVE YOUR CONSENT OR IF YOU DO NOT AGREE TO THE TERMS AND CONDITIONS DESCRIBED IN THIS DOCUMENT, THEN CLICK THE “CANCEL” BUTTON BELOW.

    IMPORTANT INFORMATION ABOUT PROCEDURES
    To help the government fight the funding of terrorism and money laundering activities, Federal law requires us to obtain, verify, and record information that identifies each borrower. When you apply for a loan, we will ask for your name, address, date of birth, and other information that will allow the lender to identify you. We may also ask to see your driver's license or other identifying document.
    NOTICE: WE ARE REQUIRED BY LAW TO ADOPT PROCEDURES TO REQUEST AND RETAIN IN OUR RECORDS INFORMATION NECESSARY TO VERIFY YOUR IDENTITY.
    TERMS AND CONDITIONS OF CREDIT APPLICATION:
    By entering the last four digits of your social security number below, you submit to Aspen Financial Solutions, Inc. d/b/a " . SITE_NAME . " (\"Lender\") this application for the purpose of inducing Lender to extend or maintain credit to you. You certify that this application presents a true, complete and correct statement of the matters shown as of the date of your application and does not omit any pertinent information. You understand that misrepresenting information on this application may be a criminal offense. You will notify Lender promptly in writing of any material unfavorable change in this information. In the absence of such notice, Lender may consider this a continuing application and substantially correct. If you apply for further credit, this application shall have the same force and effect as if delivered as an original application at the time you request such further credit.  Each of the parties voluntarily agrees to have all claims or controversies that arise from or relate in any way to our past, present or future business with each other, and with its or his respective affiliates, agents or employees, including the validity of any related agreements and the scope of this arbitration clause, resolved by BINDING ARBITRATION by a single arbitrator in accordance with the Commercial Rules of the American Arbitration Association. This arbitration agreement is made pursuant to a transaction involving interstate commerce and shall be governed by the Federal Arbitration Act, 9 U.S.C §§ 1-16. The parties understand that they have a right or opportunity to litigate disputes through a court, but that they prefer to resolve their disputes through arbitration. IF ARBITRATION IS CHOSEN, I WILL NOT HAVE THE RIGHT TO GO TO COURT, TO HAVE A JURY TRIAL, TO PARTICIPATE AS A REPRESENTATIVE OR MEMBER OF ANY CLASS OF CLAIMANTS, OR TO HAVE MY CLAIMS CONSOLIDATED OR JOINED WITH THOSE OF ANY OTHER CLAIMANT. I WILL HAVE VERY LIMITED RIGHTS TO PRETRIAL DISCOVERY AND APPEAL. I understand that Lender will only consider my application if I agree to the terms of this paragraph.
    By entering the last four digits of your social security number below, you also agree to the arbitration provision set forth above. Finally, you represent and warrant that all the information contained in the Credit Application is true and correct, that you are not currently a debtor in any bankruptcy proceeding, and that you do not intend to file a bankruptcy petition under any chapter of the U.S. Bankruptcy Code either during the term of a transaction or within the 90-day period following your repayment of a transaction.
    By submitting your application below, you agree to the terms of the privacy policy detailed here Privacy Policy.
    ", 0, 'L', 0);
    $pdf->setFont('Arial', 'U', 8);
    $pdf->Cell(5, 10, "  X  ", 0, 0, 'C');
    $pdf->setFont('Arial', '', 8);
    //$pdf->MultiCell(0, 10, $app_data["contact_agreement_agreement"], 0, 'L', 0);
    $tmp = $app_data["electronic_disclosure_agreement"];
    $pdf->MultiCell(0, 10, $app_data["electronic_disclosure_agreement"], 0, 'L', 0);
    $pdf->Ln(5);

    // BEGIN Agree to Communications (Optional)
    $pdf->setFont('Arial', 'I', 6);
    // Agree to Communications.
    $pdf->MultiCell(0, 4, "By checking the box and submitting my application, I expressly authorize Lender to monitor and record all telephone calls with me for quality assurance and for other purposes authorized by this Loan Agreement and allowed by law. I expressly authorize Lender to place account-related calls and SMS/CMS (“text”) messages to any mobile phone number I provide using an automatic telephone dialing system or an artificial or prerecorded voice message.  Lender’s messaging service will not charge me to receive or reply to text messages that I receive from Lender. However, I understand that my mobile carrier may charge their standard text message rates.  I may revoke this authorization by notifying Lender by any reasonable method including by phone at 1-877-293-2987 and in writing by U.S. mail to " . SITE_NAME . ", Attention: General Counsel, P.O. Box 802533, Dallas, TX 75380.  My mobile number(s) will not be shared or used for any other purposes unless specifically authorized.", 0, 'L', 0);
    //$pdf->MultiCell(0,4,"CONSENT TO ELECTRONIC DISCLOSURES AND COMMUNICATIONS In order for us (and, for Texas and Ohio residents, any third-party lender who may make a loan to you) to provide disclosures and other communications related to this loan application and any credit services agreement or loan agreement that we (or any third party lender) may enter into with you (now or in the future) electronically, we must obtain your consent in accordance with the provisions set forth in this Consent to Electronic Disclosures and Communications (this \"Consent\"). By consenting to the electronic delivery of disclosures and communications, you agree that we may, (and any third-party lender who makes a loan to you may), but are not obligated to, provide electronically any and all communications relating to your loan application and any loan or credit services that may be extended to you (now or in the future). Disclosures and communications that may be provided to you electronically include but are not limited to the Loan Application, the Loan Agreement, the Credit Services Disclosure/Information, the Credit Services Contract, this Consent, the Truth-in-Lending disclosures set forth in your Loan Agreement, change-in-term notices, fee and transaction information, statements, delayed disbursement letters, notices of adverse action, state mandated brochures and disclosures, and transaction information, as applicable. To electronically receive, view, and save the disclosures and communications from us, (or from a third-party lender who makes a loan to you), you must have: An up-to-date internet browser that supports 128–bit strong encryption, such as Microsoft® Internet, Explorer or Mozilla Firefox; Local, electronic storage capacity to retain our Communications and/or a printer to print them; A valid e-mail account and software to access it; An up-to-date device or devices (e.g., computer, smartphone, tablet, etc.) suitable for connecting to the Internet; Software that enables you to view files in the Portable Document Format (\"PDF\").Internet access Prior to submitting your application, you may withdraw your consent to electronic disclosures and communications by exiting the ".SITE_NAME." website. You will not be able to submit your loan application if you withdraw your consent to receive electronic disclosures and communications, because all loan applications require an electronic signature. If you electronically submit a loan application, you will be deemed to have agreed to the terms of this Consent (unless you withdraw such Consent, as discussed below). If you withdraw your Consent after electronically submitting your loan application but before a final credit determination, this action will be treated as the affirmative withdrawal of your loan application and the review of your loan request will not be continued. To withdraw this Consent after you have submitted your loan application or during the term of any loan you may obtain from us, (or a third-party lender), please contact us by calling (844) 678-5626, or write to us at: ".SITE_NAME.", Attn: Disclosure Request, ".SUPPORT_ADDRESS.", ".SUPPORT_CITY.", ".SUPPORT_STATE." ".SUPPORT_ZIP.". Note that it may take up to three business days from receipt to process your withdrawal request. You have the option to receive any information provided to you electronically in paper form at no cost to you. To request a paper copy of any information provided to you electronically, please contact us by calling (866) 678-5626, or write to us at: ".SITE_NAME.", Attn: Disclosure Request, ".SUPPORT_ADDRESS." ".SUPPORT_CITY.", ".SUPPORT_STATE." ".SUPPORT_ZIP.". You must notify us of your email address changes. To notify us of your new email address, please log on to www.aspenfinancialdirect.com and update your email address on the Account Settings page. Please print out a copy of this Consent to Electronic Disclosure and Communications and keep it for your records. By entering the last four digits of your social security number and submitting your application, you agree to the Consent to Electronic Disclosures as set forth above, and acknowledge that you have read and understand the Loan Cost and Terms, and agree to our Terms and Conditions for your state of residence.", 0, 'L', 0);
    $tmp = $app_data["contact_agreement_agreement"];
    $pdf->setFont('Arial', 'U', 8);
    if (!empty($tmp)) {
        $pdf->Cell(5, 10, "  X  ", 0, 0, 'C');
    } else {
        $pdf->Cell(5, 10, "     ", 0, 0, 'C');
        $tmp = "I have read and agree to receive communications via phone & text";
    }
    $pdf->setFont('Arial', '', 8);
    $pdf->MultiCell(0, 10, $tmp, 0, 'L', 0);
    $pdf->Ln(5);

    // Agree tp Credit Bureau Inquiry
    $pdf->setFont('Arial', 'I', 6);
    $pdf->MultiCell(0, 4, "By checking the box and submitting your application, you authorize " . SITE_NAME . " to verify the accuracy of the information contained in this Credit Application by, among other actions, checking your credit report before entering into any transaction with us and to further check your credit report for any legitimate business need in connection with a transaction between you and " . SITE_NAME . ", submitting your application to a consumer reporting agency, calling your employer to confirm employment, calling your residence or cell phone to confirm a working phone number, and obtaining your bank account information. You further authorize " . SITE_NAME . " to utilize and retain any information about you in " . SITE_NAME . "’s records previously provided by you for use by any other lender. I understand that this application and any other information furnished to " . SITE_NAME . " shall be the Lender’s property. " . SITE_NAME . " is authorized to answer any questions about Lender’s experience with me.", 0, 'L', 0);
    $pdf->setFont('Arial', 'U', 8);
    $pdf->Cell(5, 10, "  X  ", 0, 0, 'C');
    $pdf->setFont('Arial', '', 8);
    $pdf->MultiCell(0, 10, $app_data["personal_loan_agreement"], 0, 'L', 0);
    $pdf->Ln(5);
    //

    $pdf->setFont('Arial', 'U', 8);
    $pdf->Cell(5, 10, " " . $app_data["last_4_ssn"] . " ", 0, 0, 'C');
    $pdf->setFont('Arial', '', 8);
    $pdf->Cell(0, 10, "    Sign Here with Last 4 of Your SSN (Time Submitted: " . $app_data["create_dt"] . " CST)", 0, 0, 'L');
    $pdf->Ln(10);
    $pdf->setFont('Arial', 'U', 8);
    $pdf->Cell(5, 10, "  X  ", 0, 0, 'C');
    $pdf->setFont('Arial', '', 8);
    $pdf->Cell(3, 10, "", 0, 0, 'L');
    $pdf->MultiCell(0, 4, "By checking this box and submitting your application, you agree that this loan will be used for personal, family or household purposes. You also authorize " . SITE_NAME . " to access your credit bureau information to continue with this application.", 0, 'L', 0);

    //$pdf->Output();

    //Store Application PDF
    $doc_path = mm_get_document_path();
    $doc_path = $doc_path . "$account_nbr/";
    if (!file_exists($doc_path)) {
        //The account directory doesn't exists so create it
        if (!mkdir($doc_path)) {
            //Unable to make the directory.
            mm_log_error('mm_generate_application_pdf', "Unable to create the directory $doc_path");

        }
    }
    $doc_path = $doc_path . "$application_nbr/";
    if (!file_exists($doc_path)) {
        //The application directory doesn't exist so create it
        if (!mkdir($doc_path)) {
            mm_log_error('mm_generate_application_pdf', "Unable to create the directory $doc_path");
        }
    }
    $doc_path = $doc_path . "application_$application_nbr.pdf";

    $tmp = $pdf->Output('F', $doc_path);

    //Move the document to a secure location
    $temp = mm_send_ftp_document($doc_path, "application_$application_nbr.pdf", "customer_documents/$account_nbr/$application_nbr/", "B");
    //Delete the temporary document

    if (!unlink($doc_path)) {
        mm_log_error('mm_generate_application_pdf', "Unable to delete the file $doc_path");
    }

    //Hard coded for now
    $return_array["document_path"] = "customer_documents/$account_nbr/$application_nbr/loan_agreement_$application_nbr.pdf";
    $return_array["return_value"] = 0;
    return $return_array;

    //Return the function information
    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    $return_array["document_path"] = $doc_path;

    return $return_array;
}

/*****************************************************************************************************************************
 * Function mm_generate_noaa_pdf($application_nbr, $noaa_cd)
 *****************************************************************************************************************************/
function mm_generate_noaa_pdf($application_nbr, $noaa_cd)
{
    error_log("flow: mm_generate_noaa_pdf\n");
    //Initialize Variables
    $return_array = array();
    //$customer_info = array();
    //$noaa_text = array();
    //$header = array();
    //$lender_info = array();
    $return_value = 0;
    $return_message = "";
    //Get Customer data from mm database
    $customer_array = mm_get_application_details($application_nbr);
    $customer_info = $customer_array["app_data"];
    $account_nbr = $customer_info["account_nbr"];
    $customer_name = $customer_info["first_name"] . " " . $customer_info["last_name"];
    $customer_street = $customer_info["street_address"] . " " . $customer_info["appt_suite"];
    $customer_city_state_zip = $customer_info["city"] . ", " . $customer_info["state"] . " " . $customer_info["zip_code"];
    $time = strtotime($customer_info["create_dt"]);
    $application_dt = date('m/d/Y', $time);

    //Get Noaa text
    $noaa_text = mm_get_noaa_text($noaa_cd);
    if ($noaa_text == '') {
        //Insert error handling of Missing NOAA Text
    }

    //Get Lender Information
    $lender_info = mm_get_lender_details($customer_info["state"]);
    $lender_legal_name = $lender_info["legal_name"];
    $lender_dba_name = $lender_info["dba_name"];
    $lender_address_line_1 = $lender_info["mailing_street_address"];
    $lender_address_line_2 = $lender_info["mailing_city"] . ", " . $lender_info["mailing_state_cd"] . " " . $lender_info["mailing_zip_code"];
    $lender_phone_nbr = $lender_info["phone_nbr"];
    $lender_fax_nbr = $lender_info["fax_nbr"];

    //Build the NOAA
    $pdf = new PDF('P', 'mm', 'Letter');
    $pdf->AliasNbPages();
    $pdf->AddPage();
    // Logo
    // Arial bold 15
    $pdf->SetFont('Arial', 'B', 10);
    // Title
    $pdf->Cell(0, 10, $lender_legal_name, 0, 0, 'C');
    $pdf->Ln(4);
    $pdf->Cell(0, 10, $lender_address_line_1, 0, 0, 'C');
    $pdf->Ln(4);
    $pdf->Cell(0, 10, $lender_address_line_2, 0, 0, 'C');
    $pdf->Ln(4);
    $pdf->Cell(0, 10, "Phone: $lender_phone_nbr   Fax:$lender_fax_nbr ", 0, 0, 'C');
    $pdf->Ln(4);
    $pdf->Cell(0, 10, "Attn: Legal Department", 0, 0, 'C');
    $pdf->Ln(10);
    $pdf->Cell(0, 10, "Notice of Adverse Action", 0, 0, 'C');
    $pdf->Ln(10);
    $pdf->setFont('Arial', '', 10);
    $pdf->Cell(30, 10, "Application Date: ", 0, 0, 'L');
    $pdf->Cell(0, 10, "$application_dt", 0, 0, 'L');
    $pdf->Ln(10);
    $pdf->Cell(30, 10, "Applicant's Name:", 0, 0, 'L');
    $pdf->Cell(0, 10, "$customer_name", 0, 0, 'L');
    $pdf->Ln(5);
    $pdf->Cell(30, 10, "Address:", 0, 0, 'L');
    $pdf->Cell(0, 10, "$customer_street", 0, 0, 'L');
    $pdf->Ln(5);
    $pdf->Cell(30, 10, "", 0, 0, 'L');
    $pdf->Cell(0, 10, "$customer_city_state_zip", 0, 0, 'L');
    $pdf->Ln(10);
    $pdf->Cell(0, 10, "Dear Applicant,", 0, 0, 'L');
    $pdf->Ln(10);
    $pdf->Cell(0, 10, "Thank you for applying to $lender_dba_name for a loan.", 0, 0, 'L');
    $pdf->Ln(10);
    $pdf->MultiCell(0, 4, "After carefully reviewing your application, we are sorry to advise you that we cannot grant a loan to you at this time.  If you would like a statement of specific reasons why your application was denied, please contact us at the address below within SIXTY (60) days of the date of this letter.  We will provide you with the statement of reasons within THIRTY (30) days after receiving your request.", 0, 'L');
    $pdf->Ln(4);
    $pdf->Cell(0, 10, $lender_legal_name, 0, 0, 'C');
    $pdf->Ln(4);
    $pdf->Cell(0, 10, $lender_address_line_1, 0, 0, 'C');
    $pdf->Ln(4);
    $pdf->Cell(0, 10, $lender_address_line_2, 0, 0, 'C');
    $pdf->Ln(4);
    $pdf->Cell(0, 10, "Phone: $lender_phone_nbr   Fax:$lender_fax_nbr ", 0, 0, 'C');
    $pdf->Ln(4);
    $pdf->Cell(0, 10, "Attn: Legal Department", 0, 0, 'C');
    $pdf->Ln(12);

    $pdf->MultiCell(0, 4, "If we obtained information from a consumer reporting agency as part of our consideration of your application, its name, address, and toll-free telephone number is shown below.  The reporting agency played no part in our decision and is unable to supply specific reasons why we have denied credit to you.  You have a right under the Fair Credit Reporting Act to know the information contained in your credit file at the consumer reporting agency.  You have a right to a free copy of your report from the reporting agency, if you request it no later than SIXTY (60) days after you receive this notice.  In addition, if you find that any information contained in the report you received is inaccurate or incomplete, you have the right to dispute the matter with the reporting agency.  You can find out about the information contained in your file (if one was used) by contacting:", 0, 'L');

    $pdf->Ln(5);
    $pdf->Cell(0, 10, "Clarity Services, Inc.", 0, 0, 'C');
    $pdf->Ln(4);
    $pdf->Cell(0, 10, "PO BOX 5717", 0, 0, 'C');
    $pdf->Ln(4);
    $pdf->Cell(0, 10, "Clearwater, FL 33758", 0, 0, 'C');
    $pdf->Ln(4);
    $pdf->Cell(0, 10, "Phone: (866) 390-3118", 0, 0, 'C');
    $pdf->Ln(10);
    $pdf->Cell(0, 5, "Sincerely,", 0, 0, 'L');
    $pdf->Ln(10);
    $pdf->Cell(0, 5, "$lender_legal_name", 0, 0, 'L');
    $pdf->Ln(15);
    $pdf->setFont('Arial', 'I', 10);
    $pdf->MultiCell(0, 4, "Notice: The federal Equal Credit Opportunity Act prohibits creditors from discriminating against credit applicants on the basis of race, color, religion, national origin, sex, marital status, age (provided the applicant has the capacity to enter into a binding contract); because all or part of the applicant's income derives from any public assistance program; or because the applicant has in good faith exercised any right under the Consumer Credit Protection Act.  The federal agency that administers compliance with this law concerning this creditor is Federal Trade Commission, Equal Credit Opportunity, Washington, D.C. 20580", 0, 'L');

    //Store the NOAA
    $doc_path = mm_get_document_path();
    $doc_path = $doc_path . "$account_nbr/";
    if (!file_exists($doc_path)) {
        //Doc Path doesn't exist so create it
        mkdir($doc_path);
    }
    $doc_path = $doc_path . "$application_nbr/";
    if (!file_exists($doc_path)) {
        //Doc Path doesn't exist so create it
        mkdir($doc_path);
    }
    $doc_path = $doc_path . "noaa_$application_nbr.pdf";
    $pdf->Output('F', $doc_path);

    //Send the document to our secure location
    $temp = mm_send_ftp_document($doc_path, "noaa_$application_nbr.pdf", "customer_documents/$account_nbr/$application_nbr/", "B");

    //Delete the temporary document - log an error if it doesn't work

    if (!unlink($doc_path)) {
        mm_log_error('mm_generate_noaa_pdf', "Unable to delete the file $doc_path");
    } else {
        //The file was deleted successfully no need for action
    }

    //set the doc path to the storage location not the temp location
    $doc_path = mm_get_document_storage_path();
    $doc_path = $doc_path . "$account_nbr/$application_nbr/noaa_$application_nbr.pdf";

    //Build the return values
    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    $return_array["document_path"] = $doc_path;

    //Return NOAA information
    return $return_array;

}

/**********************************************************************************
 * Function mm_schedule_email_message()
 **********************************************************************************/
function mm_schedule_email_message($json_message)
{
    error_log("flow: mm_schedule_email_message\n");
    $return_array = array();
    $return_value = 0;
    $return_message = '';
    //$parm_array = array();
    $create_dt = new DateTime();
    $create_dt_string = $create_dt->format('Y-m-d H:i:s');
    $parm_array = json_decode($json_message, true);
    $scheduled_dt = isset($parm_array["scheduled_dt"]) ? $parm_array["scheduled_dt"] : $create_dt_string;
    $to = isset($parm_array["to"]) ? $parm_array["to"] : '';
    $from = isset($parm_array["from"]) ? $parm_array["from"] : '';
    $to_name = isset($parm_array["to_name"]) ? $parm_array["to_name"] : '';
    $from_name = isset($parm_array["from_name"]) ? $parm_array["from_name"] : '';
    $subject = isset($parm_array["subject"]) ? $parm_array["subject"] : '';
    $txt_body = isset($parm_array["txt_body"]) ? $parm_array["txt_body"] : '';
    $attachment = isset($parm_array["attachment"]) ? $parm_array["attachment"] : '';
    $account_results = mm_get_account_details_from_email_address($to);
    $account_nbr = $account_results["account_nbr"];
    $html_body = isset($parm_array["html_body"]) ? $parm_array["html_body"] : "<html><body><p>$txt_body</body></html>";
    $status = "READY TO SEND";

    //Build Insert Query
    $sql_string = "insert into mm_email_message_queue(email_to, email_to_name, email_from, email_from_name, subject, txt_body, html_body, attachment,  scheduled_dt, status, create_dt, account_nbr) values (?,?,?,?,?,?,?,?,?,?,?,?)";

    try {
        $conn = mm_get_pdo_connection();
        $stmt = $conn->prepare($sql_string);
        $parms = [$to, $to_name, $from, $from_name, $subject, $txt_body, $html_body, $attachment, $scheduled_dt, $status, $create_dt_string, $account_nbr];
        $stmt->execute($parms);
        $email_message_nbr = $conn->lastInsertId();

    } catch (PDOException $e) {
        $return_array["return_value"] = 1;
        $return_array["return_message"] = "$e";
        $return_array["sql_string"] = $sql_string;
        return $return_array;
    }

    if (is_resource($conn)) {
        $conn = null;
    }

    //Setup Return Array
    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    $return_array["email_message_nbr"] = isset($email_message_nbr) ? $email_message_nbr : 0;

    return $return_array;

}

/**********************************************************************************************************************************************************
 * Function mm_log_error() - Generic function to handle the processing of an error.  The function will try to add the error to the database, add it to a log file on the disk drive, and send an notification that there was an error.
 **********************************************************************************************************************************************************/
function mm_log_error($source, $message, $error_cd = 0)
{
    error_log("flow: mm_log_error\n");
    $log_path = mm_get_log_path();
    $create_dt = date("Y-m-d H:i:s");
    $create_dt_file_name = date("Y-m-dH:i:s");
    $microtime = microtime(true);
    $log_filename = "mm_error_log" . $create_dt_file_name . "." . $microtime . ".csv";
    $log_filepath = $log_path . "errors/";
    $log_file = $log_filepath . $log_filename;

    $return_value = 0;

    //Add a record to the log file
    $logfile_message = $create_dt . "," . $source . "," . $error_cd . "," . $message . "\n";
    $myfile = fopen($log_file, "w");
    fwrite($myfile, $logfile_message);
    fclose($myfile);

    // todo: handle $result or get rid of it
    $result = mm_send_ftp_document($log_file, $log_filename, "errors/", "A");
    unlink($log_file);

    //Add a record to the mm database documenting the error

    //Send an email to technical support notifying them of the error
    $to = 'jason.bumgarner@moneymart.com,mark.bench@moneymart.com';
    $from = 'MM Direct Monitoring <' . SUPPORT_EMAIL . '>';
    $subject = '******MM ERROR*********';
    $body = "There was an error with the site: $logfile_message";
    $html_body = "<html><head></head><body><p>$logfile_message</p></body></html>";
    mm_send_email($to, $from, $subject, $body, $html_body);

    return $return_value;

}

/*****************************************************************************************************************************************
 * Function mm_log_uw_request($message)
 *****************************************************************************************************************************************/
function mm_log_uw_request($json_message, $application_nbr)
{
    error_log("flow: mm_log_uw_request\n");
    
    $log_path = mm_get_log_path();
    //$create_dt = date("Y-m-d H:i:s");
    $create_dt_file_name = date("Y-m-dH:i:s");
    $microtime = microtime(true);
    $log_filename = "mm_uw_request_$application_nbr - " . $create_dt_file_name . "." . $microtime . ".json";
    $log_filepath = $log_path . "uw_requests/";
    $log_file = $log_filepath . $log_filename;
    $return_value = 0;

    //Add a record to the log file
    $logfile_message = $json_message;
    $myfile = fopen($log_file, "w");
    fwrite($myfile, $logfile_message);
    fclose($myfile);

    // todo: handle $result or get rid of it
    $result = mm_send_ftp_document($log_file, $log_filename, "uw_requests/", "A");
    if (!unlink($log_file)) {
        mm_log_error('mm_log_uw_request', "Error Deleting UW Request for Application $application_nbr");
    }

    //Add a record to the mm database documenting the error

    //Send an email to technical support notifying them of the error

    return $return_value;

}

/*****************************************************************************************************************************************
 * Function mm_log_uw_response($message, $app_id)
 *****************************************************************************************************************************************/
function mm_log_uw_response($json_message, $application_nbr)
{
    error_log("flow: mm_log_uw_response\n");
    $log_path = mm_get_log_path();
    //$create_dt = date("Y-m-d H:i:s");
    $create_dt_file_name = date("Y-m-dH:i:s");
    $microtime = microtime(true);
    $log_filename = "mm_uw_response_$application_nbr - " . $create_dt_file_name . "." . $microtime . ".json";
    //$log_filepath = $log_path . "uw_responses/";
    $log_file = $log_path . $log_filename;
    $return_value = 0;

    //Add a record to the log file
    $logfile_message = $json_message;
    $myfile = fopen($log_file, "w");
    fwrite($myfile, $logfile_message);
    fclose($myfile);

    // todo: handle $result or get rid of it
    $result = mm_send_ftp_document($log_file, $log_filename, "uw_responses/", "A");
    if (!unlink($log_file)) {
        mm_log_error('mm_log_uw_responses', "Error Deleting UW Response for Application $application_nbr");
    }
    //Add a record to the mm database documenting the error

    //Send an email to technical support notifying them of the error

    return $return_value;

}

/*****************************************************************************************************************************************
 * Function mm_log_preverif_response($message, $application_nbr)
 *****************************************************************************************************************************************/
function mm_log_preverif_response($json_message, $application_nbr)
{
    error_log("flow: mm_log_preverif_response\n");
    $log_path = mm_get_log_path();
    //$create_dt = date("Y-m-d H:i:s");
    $create_dt_file_name = date("Y-m-dH:i:s");
    $microtime = microtime(true);
    $log_filename = "mm_preverif_response_$application_nbr - " . $create_dt_file_name . "." . $microtime . ".json";
    //$log_filepath = $log_path . "preverif_responses/";
    $log_file = $log_path . $log_filename;
    $return_value = 0;

    //Add a record to the log file
    $logfile_message = $json_message;
    $myfile = fopen($log_file, "w");
    fwrite($myfile, $logfile_message);
    fclose($myfile);

    // todo: handle $result or get rid of it
    $result = mm_send_ftp_document($log_file, $log_filename, "preverif_responses/", "A");
    if (!unlink($log_file)) {
        mm_log_error('mm_log_uw_responses', "Error Deleting Preverification Response for Application $application_nbr");
    }
    //Add a record to the mm database documenting the error

    //Send an email to technical support notifying them of the error

    return $return_value;

}

/*****************************************************************************************************************************************
 * Function mm_log_preverif_request($message, $application_nbr)
 *****************************************************************************************************************************************/
function mm_log_preverif_request($json_message, $application_nbr)
{
    error_log("flow: mm_log_preverif_request\n");
    $log_path = mm_get_log_path();
    //$create_dt = date("Y-m-d H:i:s");
    $create_dt_file_name = date("Y-m-dH:i:s");
    $microtime = microtime(true);
    $log_filename = "mm_preverif_request_$application_nbr - " . $create_dt_file_name . "." . $microtime . ".json";
    //$log_filepath = $log_path . "preverif_requests/";
    $log_file = $log_path . $log_filename;
    $return_value = 0;

    //Add a record to the log file
    $logfile_message = $json_message;
    $myfile = fopen($log_file, "w");
    fwrite($myfile, $logfile_message);
    fclose($myfile);

    // todo: handle $result or get rid of it
    $result = mm_send_ftp_document($log_file, $log_filename, "preverif_requests/", "A");
    if (!unlink($log_file)) {
        mm_log_error('mm_log_uw_responses', "Error Deleting Preverification Requests for Application $application_nbr");
    }

    //Add a record to the mm database documenting the error

    //Send an email to technical support notifying them of the error

    return $return_value;

}

/*****************************************************************************************
 * Function mm_build_preverif_request
 ****************************************************************************************
 * @param $app_data
 * @return array
 */
function mm_build_preverif_request(array $app_data): array
{
    error_log("flow: mm_build_preverif_request\n");
    $return_array = array();
    $temp_array = array();
    $header_data = get_origination_system_connection_details();

    //Reformat Paydate from YYYY-MM-DD to MM
    $time = strtotime($app_data["next_pay_dt_1"]);
    $payment_date = date('mdY', $time);
    $state = strtoupper($app_data["state"]);
    $street_address = $app_data["street_address"];
    $income_source_1 = $app_data["income_source_1"];

    $temp_array["UserName"] = $header_data["UserName"];
    $temp_array["Password"] = $header_data["Password"];
    $temp_array["ApplicationSource"] = $header_data["ApplicationSource"];
    $temp_array["AuthToken"] = $header_data["AuthToken"];
    $temp_array["PortfolioID"] = $header_data["PortfolioID"];
    $temp_array["AcceptedLoanAmount"] = intval($app_data["requested_amt"]);
    $temp_array["ApplicationID"] = $app_data["orig_appid"];
    $temp_array["IPAddress"] = '1.1.1.1';
    $temp_array["PaymentDate"] = $payment_date;
    $temp_array["PaymentMode"] = $app_data["requested_deposit_type"];
    $temp_array["ReferenceID"] = $app_data["application_nbr"];

    $tmp_dt = new DateTime($app_data["create_dt"]);

    // Central Time Zone states are excluded from tests below as their time does not need to be modified.
    if ($state === 'HI') {
        $tmp_dt = $tmp_dt->modify("-5 Hours");
    } else if ($state === 'AK') {
        $tmp_dt = $tmp_dt->modify("-3 Hours");
    } else if (in_array($state, ["WA", "OR", "CA", "NV"])) {
        $tmp_dt = $tmp_dt->modify("-2 Hours");
    } else if (in_array($state, ["MT", "ID", "WY", "UT", "CO", "AZ", "NM"])) {
        $tmp_dt = $tmp_dt->modify("-1 Hours");
    } else if (in_array($state, ["CT", "DE", "MD", "FL", "GA", "IN", "KY", "ME", "MD", "MA", "MI", "NH", "NJ", "NY", "NC", "OH", "PA", "RI", "SC", "TN", "VT", "VA", "WV"])) {
        $tmp_dt = $tmp_dt->modify("+1 Hours");
    }

    /*if ($state == "CA") {
    $tmp_dt = $tmp_dt->modify("-2 Hours");
    } else if ($state == "NM" || $state == "UT") {
    $tmp_dt = $tmp_dt->modify("-1 Hours");
    } else if ($state == "TX" || $state == "MO" || $state == "WI" || $state == "IL" || $state == "AL" || $state == "MS") {
    //The customer is in central time so no modification is needed to the time stamp
    } else if ($state == "SC" || $state == "GA" || $state == "OH") {
    $tmp_dt = $tmp_dt->modify("+1 Hours");
    } else {
    mm_log_error('mm_build_uw_request', "Warning: Unable to check for after hours application due to unexpected State: $state");
    }*/

    $application_hour = $tmp_dt->format('H');
    if ($street_address == "TIME FAIL") {
        $temp_array["RequireFullVerification"] = "true";
        $note_data = array();
        $note_data["application_nbr"] = $app_data["application_nbr"];
        $note_data["category"] = "Automated Decisioning";
        $note_data["sub_category"] = "Pre-Verifications";
        $note_data["txt_body"] = "Application outsorted due to application time";
        $note_data["create_dt"] = date("Y-m-d H:i:s");
        $note_data["username"] = "System";
        $note_json = json_encode($note_data);
        $note_results = mm_add_application_note($note_json);
        $note_message = $note_results["return_message"];
        if ($note_results["return_value"] != 0) {
            //Failed to add note so log an error
            mm_log_error('mm_build_preverif_request', "Failed Adding an application note: $note_message");
        }
    } else if ($application_hour >= 1 && $application_hour < 5) {
        $temp_array["RequireFullVerification"] = "true";
        $note_data = array();
        $note_data["application_nbr"] = $app_data["application_nbr"];
        $note_data["category"] = "Automated Decisioning";
        $note_data["sub_category"] = "Pre-Verifications";
        $note_data["txt_body"] = "Application outsorted due to application time";
        $note_data["create_dt"] = date("Y-m-d H:i:s");
        $note_data["username"] = "System";
        $note_json = json_encode($note_data);
        $note_results = mm_add_application_note($note_json);
        $note_message = $note_results["return_message"];
        if ($note_results["return_value"] != 0) {
            //Failed to add note so log an error
            mm_log_error('mm_build_preverif_request', "Failed Adding an application note: $note_message");
        }
    } else if ($income_source_1 == 'Other / not listed') {
        $temp_array["RequireFullVerification"] = "true";
        $note_data = array();
        $note_data["application_nbr"] = $app_data["application_nbr"];
        $note_data["category"] = "Automated Decisioning";
        $note_data["sub_category"] = "Pre-Verifications";
        $note_data["txt_body"] = "Application set to require customer documents due to income type of Other / not listed";
        $note_data["create_dt"] = date("Y-m-d H:i:s");
        $note_data["username"] = "System";
        $note_json = json_encode($note_data);
        $note_results = mm_add_application_note($note_json);
        $note_message = $note_results["return_message"];
        if ($note_results["return_value"] != 0) {
            //Failed to add note so log an error
            mm_log_error('mm_build_preverif_request', "Failed Adding an application note: $note_message");
        }

    } else {
        $temp_array["RequireFullVerification"] = "false";
    }

    //Check to see if there are any other applications that are approved/funded from the same household
    $address_data = array();
    $address_data["street_address"] = $app_data["street_address"];
    $address_data["appt_suite"] = $app_data["appt_suite"];
    $address_data["first_name"] = $app_data["first_name"];
    $address_data["application_nbr"] = $app_data["application_nbr"];
    $address_json = json_encode($address_data);
    $household_data = mm_get_household_matches($address_json);
    $household_return_value = $household_data["return_value"];
    $household_return_message = $household_data["return_message"];
    $household_result_count = $household_data["row_count"];
    $household_results = $household_data["results"];
    if ($household_return_value != 0) {
        //Log an error but continue processing
        mm_log_error('mm_build_preverif_request', "Failed getting the household information with message $household_return_message");
    } else if ($household_result_count > 0) {
        //There were household matches to outsort for full verifications and add a note to the application
        $temp_array["RequireFullVerification"] = "true";
        //Add an application note
        $note_data = array();
        $note_data["application_nbr"] = $app_data["application_nbr"];
        $note_data["category"] = "Automated Decisioning";
        $note_data["sub_category"] = "Pre-Verifications";
        $note_data["txt_body"] = "Application outsorted for verifications because of an existing approval/loan in the household Matches: ";
        foreach ($household_results as $household) {
            $app_nbr = $household["application_nbr"];
            $note_data["txt_body"] .= "$app_nbr, ";
        }
        $note_data["create_dt"] = date("Y-m-d H:i:s");
        $note_data["username"] = "System";

        $note_json = json_encode($note_data);
        $note_results = mm_add_application_note($note_json);
        $note_message = $note_results["return_message"];
        if ($note_results["return_value"] != 0) {
            //Failed to add note so log an error
            mm_log_error('mm_build_preverif_request', "Failed Adding an application note: $note_message");
        }
    } else {
        // todo: what needs to happen here?
        //There were no matches so continue as normal
    }

    //For now default to success
    $return_value = 0;
    $return_message = '';
    $json_message = json_encode($temp_array);

    //Prepare return array
    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    $return_array["json_message"] = $json_message;

    //Send return_array
    return $return_array;
}

/*************************************************************************************
 * Function mm_send_preverif_request($app_data)
 ************************************************************************************
 * @param array $app_data
 * @return array
 */
function mm_send_preverif_request(array $app_data): array
{
    error_log("flow: mm_send_preverif_request\n");
    $return_array = array();
    $uw_response = array();
    $server_details = get_origination_system_connection_details();
    $application_nbr = $app_data["application_nbr"];
    $preverif_request_array = mm_build_preverif_request($app_data);
    $preverif_request_value = $preverif_request_array["return_value"];
    //$preverif_request_message = $preverif_request_array["return_message"];
    $preverif_request_json = $preverif_request_array["json_message"];

    $return_value = 0;
    $return_message = '';
    $url = $server_details["url"];
    $url = $url . "AcceptLoan";
    $curl = curl_init($url);

    if ($preverif_request_value != 0) {
        //Handle an error building the message
    } else {
        mm_log_preverif_request($preverif_request_json, $application_nbr);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $server_details["header_array"]);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $preverif_request_json);

        $json_response = curl_exec($curl);
        mm_log_preverif_response($json_response, $application_nbr);
        $response_array = json_decode($json_response, true);
        $response_array["application_nbr"] = $app_data["application_nbr"];

        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        if ($status != 201 && $status != 200) {
            //There was an error in the message sending  Add error handling
            $return_value = 1;
            $return_message = "Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl);
        } else {
            //The request worked.  Process the request, update the database application status
            $uw_response = json_decode($json_response, true);

            // todo: how to handle the return of the call?
            $insert_results = mm_create_preverif_decision($response_array);
        }
    }
    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    $return_array["preverif_response"] = $uw_response;

    curl_close($curl);

    return $return_array;
}

/********************************************************************************
 * Function mm_process_customer_acceptance($entry, $form) - Take action after the customer has accepted the form.  Update the database with the customers selections, submit the customer data to the originations system for preverificaitons determination.  Respond with the result of preverifications. For now, assumes that this is called directly from the gravity forms form.
 *******************************************************************************
 * @param $application_nbr
 * @return array
 */
function mm_submit_preverifications($application_nbr)
{
    error_log("flow: mm_submit_preverifications\n");
    //Initialize Variables
    $return_array = array();
    $request_array = array();
    $return_value = 0;
    $return_message = '';

    //Get the data you need from the entry form to continue processing
    //EDIT - Come back and get the details from the session variable if it exists....
    $app_array = mm_get_application_details($application_nbr);
    $app_data = $app_array["app_data"];
    $request_array["orig_appid"] = $app_data["orig_appid"];
    $request_array["application_nbr"] = $application_nbr;
    $request_array["next_pay_dt_1"] = $app_data["next_pay_dt_1"];
    $request_array["requested_amt"] = $app_data["requested_amt"];
    $request_array["requested_deposit_type"] = $app_data["requested_deposit_type"];
    $request_array["create_dt"] = $app_data["create_dt"];
    $request_array["state"] = $app_data["state"];
    $request_array["street_address"] = $app_data["street_address"];
    $request_array["first_name"] = $app_data["first_name"];
    $request_array["appt_suite"] = $app_data["appt_suite"];
    $request_array["income_source_1"] = $app_data["income_source_1"];

    //Update the application status

    //Submit the data using a preverifications message
    $preverif_result_array = mm_send_preverif_request($request_array);

    //Process the preverifications response
    $preverif_return_value = $preverif_result_array["return_value"];
    //$preverif_return_message = $preverif_result_array["return_message"];
    $preverif_response = $preverif_result_array["preverif_response"];
    $preverif_result = $preverif_response["AppStatusCode"];
    if ($preverif_return_value != 0) {
        //There was an issue set the app status to stuck and the appropriate return values for display to the customer
        $return_value = 1;
        $return_message = "There was an issue processing the preverifications request";
        $vf_decision = "Error";
        mm_update_database_value('mm_application', 'application_status', 9, 'i', 'application_nbr', $application_nbr);

    } else {
        //The request was fine determine if documents are needed or not.  Update the application status appropriately and set the return information
        if ($preverif_result == "Accepted") {
            mm_update_database_value('mm_application', 'application_status', 6, 'i', 'application_nbr', $application_nbr);
        } else if ($preverif_result == "Error") {
            //Need to handle the difference between Error and Need Documents
            mm_update_database_value('mm_application', 'application_status', 9, 'i', 'application_nbr', $application_nbr);
        } else if ($preverif_result == "ACCEPTED PENDING AGENT REVIEW") {
            mm_update_database_value('mm_application', 'application_status', 5, 'i', 'application_nbr', $application_nbr);
        } else if ($preverif_result == "ACCEPTED PENDING CUSTOMER DOCUMENTS") {
            mm_update_database_value('mm_application', 'application_status', 4, 'i', 'application_nbr', $application_nbr);
        } else {
            // todo: what to do here?
            //Need to handle the difference between Error and Need Documents

        }
        $vf_decision = $preverif_result;
    }

    //Build the function response array
    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    $return_array["vf_decision"] = $vf_decision;

    //Return the data from the function
    return $return_array;
}

/******************************************************************************************************************************************
 * Function mm_get_preverif_decision_details($decision_nbr)
 ******************************************************************************************************************************************/
function mm_get_preverif_decision_details($preverif_decision_nbr)
{
    error_log("flow: mm_get_preverif_decision_details\n");
    $return_array = array();
    //$decision_data = array();
    $preverif_data = array();

    $conn = mm_get_db_connection();
    $sql_string = "Select * from mm_preverif_decision where preverif_decision_nbr = ?";
    if (!$stmt = $conn->prepare($sql_string)) {
        mm_log_error('mm_get_preverif_decision_details', "$conn->error", $conn->errno);
    }
    $stmt->bind_param('i', $preverif_decision_nbr);
    if (!$stmt->execute()) {
        $return_value = $stmt->errno;
        $return_message = $stmt->error;
        //Need to log an error here as well
    } else {
        $rows = $stmt->get_result();
        $num_rows = $rows->num_rows;
        if ($num_rows == 0) {
            //The preverif decision record didn't exist
            $return_value = 1;
            $return_message = "There was no associated preverif record with a preverif number of $preverif_decision_nbr";
        } else {
            $preverif_data = $rows->fetch_assoc();
            $return_value = 0;
            $return_message = '';
        }
    }

    if (is_resource($conn)) {
        $conn->close();
    }

    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    $return_array["preverif_data"] = $preverif_data;
    return $return_array;

}

/******************************************************************************************************************************************
 * Function mm_get_uw_decision_details($decision_nbr)
 ******************************************************************************************************************************************/
function mm_get_uw_decision_details($decision_nbr)
{
    error_log("flow: mm_get_uw_decision_details\n");
    $return_array = array();
    $decision_data = array();

    $conn = mm_get_db_connection();
    $sql_string = "Select * from mm_uw_decision where decision_nbr = ?";
    if (!$stmt = $conn->prepare($sql_string)) {
        mm_log_error('mm_get_uw_decision_details', "$conn->error", $conn->errno);
    }
    $stmt->bind_param('i', $decision_nbr);
    if (!$stmt->execute()) {
        $return_value = $stmt->errno;
        $return_message = $stmt->error;
        //Need to log an error here as well
    } else {
        $rows = $stmt->get_result();
        $num_rows = $rows->num_rows;
        if ($num_rows == 0) {
            //The uw decision record didn't exist
            $return_value = 1;
            $return_message = "There was no associated decision with a decision number $decision_nbr";
        } else {
            $decision_data = $rows->fetch_assoc();
            $return_value = 0;
            $return_message = '';
        }
    }

    if (is_resource($conn)) {
        $conn->close();
    }

    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    $return_array["decision_data"] = $decision_data;
    return $return_array;

}

/***************************************************************************************************
 * Function mm_get_application_history($account_nbr)
 ***************************************************************************************************/
function mm_get_application_history($account_nbr)
{
    error_log("flow: mm_get_application_history\n");
    $return_array = array();
    $app_history = array(array());

    $conn = mm_get_db_connection();
    $sql_string = "Select * from mm_application where account_nbr = ?";
    if (!$stmt = $conn->prepare($sql_string)) {
        mm_log_error('mm_get_application_history', "$conn->error", $conn->errno);
    }
    $stmt->bind_param('i', $account_nbr);
    if (!$stmt->execute()) {
        //$return_value = $stmt->errno;
        //$return_message = $stmt->error;
        // todo: Need to log an error here as well
    } else {
        $rows = $stmt->get_result();
        $num_rows = $rows->num_rows;
        if ($num_rows == 0) {
            //The application id didn't exist so default all of the return values
            $return_value = 0;
            $return_message = "There were no applications associated with this account";
        } else {
            $i = 0;
            while ($row = $rows->fetch_assoc()) {
                foreach ($row as $key => $value) {
                    $app_history[$i]["$key"] = $value;
                }
                $i += 1;
            }
            $return_value = 0;
            $return_message = '';
        }

        if (is_resource($conn)) {
            $conn->close();
        }

        $return_array["return_value"] = $return_value;
        $return_array["return_message"] = $return_message;
        $return_array["app_history"] = $app_history;
        $return_array["num_apps"] = $num_rows;
        return $return_array;

    }

}

/******************************************************************************************************************************************
 * Function mm_get_application_details($application_nbr)
 ******************************************************************************************************************************************/
function mm_get_application_details($application_nbr)
{
    error_log("flow: mm_get_application_details\n");
    $return_array = array();
    $app_data = array();
    $return_value = 0;
    $return_message = '';

    // mark was here: try cache first
    $app_data = mm_get_cache_value('app_data_' . $application_nbr);

    if (!$app_data) {
        // cache did not work, so fall back
        error_log("mark: Cache did not work, falling back to db query");
        $conn = mm_get_db_connection();
        $sql_string = "Select * from mm_application where application_nbr = ?";
        if (!$stmt = $conn->prepare($sql_string)) {
            mm_log_error('mm_get_application_details', "$conn->error", $conn->errno);
        }
        $stmt->bind_param('i', $application_nbr);
        if (!$stmt->execute()) {
            $return_value = $stmt->errno;
            $return_message = $stmt->error;
            //Need to log an error here as well
        } else {
            $rows = $stmt->get_result();
            $num_rows = $rows->num_rows;
            if ($num_rows == 0) {
                //The application id didn't exist so default all of the return values
                $return_value = 1;
                $return_message = "There was no associated application with that application number.";
            } else {
                $app_data = $rows->fetch_assoc();

            }
        }

        if (is_resource($conn)) {
            $conn->close();
        }
    } else {
        error_log("cache value found for app_data");
    }

    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    $return_array["app_data"] = $app_data;
    return $return_array;

}

/************************************************************************************************
 * Function mm_get_preverif_lead_details($lead_id)
 ************************************************************************************************/
function mm_get_preverif_lead_details($lead_id)
{
    error_log("flow: mm_get_preverif_lead_details\n");
    $app_data = array();
    $return_array = array();
    $requested_amt = 0;
    $deposit_method = "";

    $conn = mm_get_db_connection();
    $sql_string = "select * from wp_rg_lead_detail where lead_id = ? and form_id = 17";
    $stmt = $conn->prepare($sql_string);
    $stmt->bind_param('i', $lead_id);
    $stmt->execute();
    $rows = $stmt->get_result();
    foreach ($rows as $row) {
        $field_number = $row["field_number"];
        $value = $row["value"];
        if ($field_number == 1) {
            $requested_amt = $value;
        } else if ($field_number == 4) {
            $deposit_method = $value;
        } else if ($field_number == 3) {
            $application_nbr = $value;

        } else {
            //Log Error Because Received Unexpected Field
        }

    }

    $app_data["requested_amt"] = $requested_amt;
    $app_data["application_nbr"] = $requested_amt;
    $app_data["deposit_method"] = $deposit_method;

    if (is_resource($conn)) {
        $conn->close();
    }
    //Set the return array value
    $return_array["app_data"] = $app_data;

    return $return_array;

}

function app_data_map()
{
    error_log("flow: app_data_map\n");
    return [
        '1' => 'income_source_1',
        '2' => 'secondary_income_ind',
        '3' => 'income_source_2',
        '4' => 'company_name_1',
        '6' => 'wage_type_1',
        '7' => 'hourly_rate_1',
        '8' => 'hours_per_week_1',
        '9' => 'pay_frequency_1',
        '11' => 'day_paid_month_1',
        '12' => 'last_paycheck_amt_1',
        '13' => 'direct_deposit_ind_1',
        '14' => 'next_pay_dt_1',
        '17' => 'day_paid_twice_monthly_1',
        '21' => 'day_paid_weekly_1',
        '23' => 'monthly_income_amt_1',
        '25' => 'company_name_1',
        '33' => 'annual_salary_1',
        '34' => 'monthly_income_amt_1',
        '51.1' => 'street_address',
        '51.2' => 'appt_suite',
        '51.3' => 'city',
        '51.4' => 'state',
        '51.5' => 'zip_code',
        '51.6' => 'country',
        '52' => 'state',
        '53.3' => 'first_name',
        '53.6' => 'last_name',
        '54' => 'email_address',
        '56' => 'own_or_rent',
        '57' => 'monthly_rent_amt',
        '58' => 'monthly_mtg_amt',
        '59' => 'mobile_phone_nbr',
        '60' => 'home_phone_nbr',
        '63' => 'routing_nbr',
        '64' => 'bank_acct_nbr',
        '68.1' => 'bank_acct_standing_ind',
        '70' => 'dob',
        '73.1' => 'contact_agreement_agreement',
        '75.1' => 'electronic_disclosure_agreement',
        '76' => 'last_4_ssn',
        '77.1' => 'personal_loan_agreement',
        '78' => 'ssn',
        '80' => 'wage_type_2',
        '81' => 'hourly_rate_2',
        '82' => 'hours_per_week_2',
        '83' => 'monthly_income_amt_2',
        '84' => 'annual_salary_2',
        '86' => 'company_name_2',
        '87' => 'monthly_income_amt_2',
        '91' => 'pay_frequency_2',
        '93' => 'day_paid_month_2',
        '95' => 'day_paid_twice_monthly_2',
        '97' => 'day_paid_weekly_2',
        '99' => 'last_paycheck_amt_2',
        '101' => 'direct_deposit_ind_2',
        '102' => 'next_pay_dt_2',
        '103' => 'company_name_1',
        '104' => 'company_name_2',
        '109' => "not used", // confirm routing number
        '110' => "not used", // confirm account number
        '113' => "bank_acct_type",
        '114' => "not used", // confirm SSN
        '115' => "bank_acct_age_years",
        '116' => "bank_acct_age_months",
        '119.1' => "delaware_schedule_agreement", // delaware agreement
        '122.1' => "california_schedule_acknowledgement",
        '129' =>'gross_income_1',
        '134' =>'gross_income_2',
        '136.1' => 'illinois_agreement'
    ];
}

/**************************************************************************************************************************************************************************
 * Function mm_get_lead_details($lead_id)
 * Description: The purpose of this plugin is to take a lead id, query the gravity forms database and convert the information from the form into an associative array of value from the application
 * Work Still Needing Completed: Handling of default values for missing/not applicable fields, error handling for database connectivity, handling when a lead id has no data in the database
 **************************************************************************************************************************************************************************/
function mm_get_lead_details($lead_id)
{
    error_log("flow: mm_get_lead_details\n");
    $app_data = array();
    $app_data["gf_lead_id"] = $lead_id;
    $conn = mm_get_db_connection();
    $sql_string = "select lead_id, form_id, field_number, value from wp_rg_lead_detail where lead_id = ? and form_id = 14";
    if (!$stmt = $conn->prepare($sql_string)) {
        mm_log_error('mm_get_lead_details', "$conn->error", $conn->errno);
    }
    $stmt->bind_param('i', $lead_id);
    if (!$stmt->execute()) {
        // todo: what to do here?
        //$return_value = $stmt->errno;
        //$return_message = $stmt->error;
    } else {
        $rows = $stmt->get_result();
        $tmp_map = app_data_map();
        foreach ($rows as $row) {
            $field_number = $row["field_number"];
            $tmp = $tmp_map[strval($field_number)];
            if (isset($tmp)) {
                $app_data[$tmp] = $row["value"];
            } else {
                mm_log_error('mm_get_lead_details', "received a field that I wasn't expecting: $field_number");
            }
        }
        
        //Set default values for all of the fields that may not be set
        $app_data["wage_type_2"] = isset($app_data["wage_type_2"]) ? $app_data["wage_type_2"] : '';
        $app_data["hourly_rate_2"] = isset($app_data["hourly_rate_2"]) ? $app_data["hourly_rate_2"] : 0;
        $app_data["hours_per_week_2"] = isset($app_data["hours_per_week_2"]) ? $app_data["hours_per_week_2"] : 0;
        $app_data["annual_salary_1"] = isset($app_data["annual_salary_1"]) ? $app_data["annual_salary_1"] : 0;
        $app_data["annual_salary_2"] = isset($app_data["annual_salary_2"]) ? $app_data["annual_salary_2"] : 0;
        $app_data["pay_frequency_2"] = isset($app_data["pay_frequency_2"]) ? $app_data["pay_frequency_2"] : '';
        $app_data["day_paid_month_2"] = isset($app_data["day_paid_month_2"]) ? $app_data["day_paid_month_2"] : '';
        $app_data["day_paid_weekly_2"] = isset($app_data["day_paid_weekly_2"]) ? $app_data["day_paid_weekly_2"] : '';
        $app_data["day_paid_biweekly_2"] = isset($app_data["day_paid_biweekly_2"]) ? $app_data["day_paid_biweekly_2"] : '';
        $app_data["day_paid_twice_monthly_2"] = isset($app_data["day_paid_twice_monthly_2"]) ? $app_data["day_paid_twice_monthly_2"] : '';
        $app_data["direct_deposit_ind_2"] = isset($app_data["direct_deposit_ind_2"]) ? $app_data["direct_deposit_ind_2"] : '';
        $app_data["next_pay_dt_2"] = isset($app_data["next_pay_dt_2"]) ? $app_data["next_pay_dt_2"] : '';
        $app_data["monthly_income_amt_2"] = isset($app_data["monthly_income_amt_2"]) ? $app_data["monthly_income_amt_2"] : 0;
        $app_data["company_name_2"] = isset($app_data["company_name_2"]) ? $app_data["company_name_2"] : '';
        $app_data["company_name_1"] = isset($app_data["company_name_1"]) ? $app_data["company_name_1"] : 'N/A';

        //perform some form validation and cleansing
        $app_data["home_phone_nbr"] = isset($app_data["home_phone_nbr"]) ? clean_phone_nbr($app_data["home_phone_nbr"]) : 0;
        $app_data["mobile_phone_nbr"] = clean_phone_nbr($app_data["mobile_phone_nbr"]);
        $app_data["ssn"] = clean_phone_nbr($app_data["ssn"]);
        $app_data["state"] = mm_state_to_cd($app_data["state"]);
        $app_data["direct_deposit_ind_1"] = ($app_data["direct_deposit_ind_1"] == "Yes") ? 'Y' : 'N';
        $app_data["direct_deposit_ind_2"] = ($app_data["direct_deposit_ind_2"] == "Yes") ? 'Y' : 'N';

    }

    //Close Database connection
    if (is_resource($conn)) {
        $conn->close();
    }

    //Send Return Data
    return $app_data;
}

/*************************************************************************************************************************************************
 * Function mm_account_exists($key, $value) - Returns 1 if the account exists, 0 if it doesn't and > 1 if there was an error.
 *************************************************************************************************************************************************/
function mm_account_exists($key, $value)
{
    error_log("flow: mm_account_exists: (\$key: $key, \$value = $value)\n");
    $return_array = array();
    $conn = mm_get_db_connection();
    $sql_string = "select account_nbr from mm_account where $key = ?";
    if (!$stmt = $conn->prepare($sql_string)) {
        mm_log_error("mm_account_exists($key, $value)", "$conn->error", "$conn->errorno");
        $return_value = $conn->errno;
        $return_message = $conn->error;
    } else {
        $stmt->bind_param('s', $value);
        if (!$stmt->execute()) {
            $return_value = $stmt->errno;
            $return_message = $stmt->error;
        } else {
            $rows = $stmt->get_result();
            $num_rows = $rows->num_rows;
            if ($num_rows == 0) {
                //There is no match in the database
                $return_value = 0;
                $return_message = "No Match";
                $account_nbr = 0;
            } else if ($num_rows == 1) {
                //There is an existing record in the database
                $row = $rows->fetch_assoc();
                $return_value = 1;
                $return_message = "Match";
                $account_nbr = $row["account_nbr"];
            } else {
                //There was more than 1 match.  This should never happen, but wanted to capture the scenario just in case
                $return_value = 2;
                $return_message = "Multiple Matches";
                $account_nbr = 0;
            }
        }
    }

    //Assign values to return array
    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    if (isset($account_nbr)) {
        $return_array["account_nbr"] = $account_nbr;
    }

    //close db connection if it exists
    if (is_resource($conn)) {
        $conn->close();
    }

    //return data
    return $return_array;
}

/************************************************************************************************************************************************
 * Function clean_phone_nbr($phone_nbr) - Removes all non-numbers from a phone number
 ************************************************************************************************************************************************/
function clean_phone_nbr($phone_nbr)
{
    error_log("flow: clean_phone_nbr\n");
    $clean_phone_nbr = preg_replace("/[^0-9,.]/", "", $phone_nbr);
    return $clean_phone_nbr;

}

/*******************************************************************************************************************************************
 * Function mm_create_account($account_data)
 *******************************************************************************************************************************************/
function mm_create_account($account_data)
{
    error_log("flow: mm_create_account\n");
    //Initialize Variables
    $return_array = array();
    $first_name = $account_data["first_name"];
    $last_name = $account_data["last_name"];
    $email_address = $account_data["email_address"];
    $ssn = $account_data["ssn"];
    $account_status = 'A';
    $mobile_phone_nbr = $account_data["mobile_phone_nbr"];
    $home_phone_nbr = $account_data["home_phone_nbr"];
    $create_dt = date("Y-m-d H:i:s");

    //Cleanse data before inserting

    //Build Insert Query
    $sql_string = "insert into mm_account(first_name, last_name, email_address, ssn, create_dt, account_status, mobile_phone_nbr, home_phone_nbr) values (?,?,?,?,?,?,?,?)";
    //Connect to the database
    $conn = mm_get_db_connection();

    if (!$stmt = $conn->prepare($sql_string)) {
        mm_log_error("mm_create_account", "$conn->error", "$conn->errno");
        $return_value = $conn->errno;
        $return_message = $conn->error;

    } else {
        $stmt->bind_param('ssssssss', $first_name, $last_name, $email_address, $ssn, $create_dt, $account_status, $mobile_phone_nbr, $home_phone_nbr);
        if (!$stmt->execute()) {
            $return_value = $conn->errno;
            $return_message = $conn->error;
        } else {
            //The account was inserted successfully
            $account_nbr = $stmt->insert_id;
            $return_value = 0;
            $return_message = "Account created successfully";
        }
    }

    //Close Connection if Exists
    if (is_resource($conn)) {
        $conn->close();
    }

    //Setup Return Array
    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    $return_array["account_nbr"] = isset($account_nbr) ? $account_nbr : 0;

    //Return values
    return $return_array;
}

/*************************************************************************************
 * Function mm_update_database_value($table_name, $column_name, $value, $type, $table_key_txt, $table_key_value)
 *************************************************************************************/
function mm_update_database_value($table_name, $column_name, $value, $type, $table_key_txt, $table_key_value, $user='SYSTEM')
{
    error_log("flow: mm_update_database_value\n");
    if ($type == 's' || $type == 'S') {
        $sql_ready_value = "'" . $value . "'";
    } else {
        $sql_ready_value = $value;
    }
    $type = "'" . $type . "'";

    $conn = mm_get_db_connection();

    //Get Current Value
    $sql_string = "select $column_name from $table_name where $table_key_txt = ?";
    if (!$stmt = $conn->prepare($sql_string)) {
        mm_log_error("mm_update_database_value", "$conn->error", "$conn->errno");
        $return_value = $conn->errno;
        $return_message = $conn->error;
    } else {
        if ($type == 'i') {
            $stmt->bind_param('i', $table_key_value);
        } else if ($type == 's') {
            $stmt->bind_param('s', $table_key_value);
        } else {
            $stmt->bind_param('d', $table_key_value);
        }
        if (!$stmt->execute()) {

        } else {
            $rows = $stmt->get_result();
            $num_rows = $rows->num_rows;
            if ($num_rows == 1) {
                $row = $rows->fetch_assoc();
                $old_value = $row["$column_name"];
            }
        }
    }
    //Update With the New Value
    $sql_string = "update $table_name set $column_name=$sql_ready_value where $table_key_txt = ?";
    if (!$stmt = $conn->prepare($sql_string)) {
        mm_log_error("mm_update_database_value", "Error with this query: $sql_string and this is the error $conn->error", "$conn->errno");
        $return_value = $conn->errno;
        $return_message = $conn->error;

    } else {
        if ($type == 'i') {
            $stmt->bind_param('i', $table_key_value);
        } else if ($type == 's') {
            $stmt->bind_param('s', $table_key_value);
        } else {
            $stmt->bind_param('d', $table_key_value);

        }
        if (!$stmt->execute()) {
            $return_value = $conn->errno;
            $return_message = $conn->error;
        } else {
            //The account was inserted successfully
            $account_nbr = $stmt->insert_id;
            $return_value = 0;
            $return_message = "Account created successfully";
        }
    }

    //Log the change
    $create_dt = date("Y-m-d H:i:s");
    $sql_string = "insert into mm_column_history(table_name, column_name, old_value, new_value, change_dt,key_value, user) values (?,?,?,?,?,?,?)";
    $stmt = $conn->prepare($sql_string);
    $stmt->bind_param('sssssis', $table_name, $column_name, $old_value, $value, $create_dt, $table_key_value, $user);
    $stmt->execute();

    if (is_resource($conn)) {
        $conn->close();
    }

    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    return $return_array;
}

/*********************************************************************************************************
 * Function mm_create_application($app_data)
 *********************************************************************************************************/
function mm_create_application($app_data)
{
    error_log("flow: mm_create_application\n");
    //Initialize Variables
    $return_array = array();
    $return_value = 0;
    $return_message = '';
    $first_name = $app_data["first_name"];
    $last_name = $app_data["last_name"];
    $state = $app_data["state"];
    $email_address = $app_data["email_address"];
    $street_address = $app_data["street_address"];
    $appt_suite = isset($app_data["appt_suite"]) ? $app_data["appt_suite"] : '';
    $city = $app_data["city"];
    $zip_code = $app_data["zip_code"];
    $own_or_rent = $app_data["own_or_rent"];
    $monthly_rent_amt = isset($app_data["monthly_rent_amt"]) ? $app_data["monthly_rent_amt"] : 0;
    $monthly_mtg_amt = isset($app_data["monthly_mtg_amt"]) ? $app_data["monthly_mtg_amt"] : 0;
    $mobile_phone_nbr = $app_data["mobile_phone_nbr"];
    $home_phone_nbr = isset($app_data["home_phone_nbr"]) ? $app_data["home_phone_nbr"] : '';
    $income_source_1 = $app_data["income_source_1"];
    $wage_type_1 = isset($app_data["wage_type_1"]) ? $app_data["wage_type_1"] : '';
    $hourly_rate_1 = isset($app_data["hourly_rate_1"]) ? $app_data["hourly_rate_1"] : 0;
    $hours_per_week_1 = isset($app_data["hours_per_week_1"]) ? $app_data["hours_per_week_1"] : 0;
    $annual_salary_1 = $app_data["annual_salary_1"];
    $pay_frequency_1 = $app_data["pay_frequency_1"];
    $day_paid_month_1 = isset($app_data["day_paid_month_1"]) ? $app_data["day_paid_month_1"] : '';
    $day_paid_twice_monthly_1 = isset($app_data["day_paid_twice_monthly_1"]) ? $app_data["day_paid_twice_monthly_1"] : '';
    $day_paid_weekly_1 = isset($app_data["day_paid_weekly_1"]) ? $app_data["day_paid_weekly_1"] : '';
    $day_paid_biweekly_1 = isset($app_data["day_paid_biweekly_1"]) ? $app_data["day_paid_biweekly_1"] : '';
    $direct_deposit_ind_1 = $app_data["direct_deposit_ind_1"];
    $next_pay_dt_1 = $app_data["next_pay_dt_1"];
    $monthly_income_amt_1 = isset($app_data["monthly_income_amt_1"]) ? $app_data["monthly_income_amt_1"] : 0;
    $last_paycheck_amt_1 = isset($app_data["last_paycheck_amt_1"]) ? $app_data["last_paycheck_amt_1"] : 0;
    $company_name_1 = isset($app_data["company_name_1"]) ? $app_data["company_name_1"] : '';
    $income_source_2 = isset($app_data["income_source_2"]) ? $app_data["income_source_2"] : '';
    $wage_type_2 = $app_data["wage_type_2"];
    $hourly_rate_2 = $app_data["hourly_rate_2"];
    $hours_per_week_2 = $app_data["hours_per_week_2"];
    $annual_salary_2 = $app_data["annual_salary_2"];
    $pay_frequency_2 = $app_data["pay_frequency_2"];
    $day_paid_month_2 = $app_data["day_paid_month_2"];
    $day_paid_twice_monthly_2 = $app_data["day_paid_twice_monthly_2"];
    $day_paid_weekly_2 = $app_data["day_paid_weekly_2"];
    $day_paid_biweekly_2 = $app_data["day_paid_biweekly_2"];
    $direct_deposit_ind_2 = $app_data["direct_deposit_ind_2"];
    $next_pay_dt_2 = $app_data["next_pay_dt_2"] == '' ? "1900-01-01" : $app_data["next_pay_dt_2"];
    $monthly_income_amt_2 = $app_data["monthly_income_amt_2"];
    $last_paycheck_amt_2 = isset($app_data["last_paycheck_amt_2"]) ? $app_data["last_paycheck_amt_2"] : 0;
    $company_name_2 = $app_data["company_name_2"];
    $routing_nbr = $app_data["routing_nbr"];
    $bank_acct_nbr = $app_data["bank_acct_nbr"];
    $bank_acct_age_years = $app_data["bank_acct_age_years"];
    $bank_acct_age_months = $app_data["bank_acct_age_months"];
    $ssn = $app_data["ssn"];
    $dob = $app_data["dob"];
    $contact_agreement_agreement = isset($app_data["contact_agreement_agreement"]) ? $app_data["contact_agreement_agreement"] : '';
    $electronic_disclosure_agreement = $app_data["electronic_disclosure_agreement"];
    $last_4_ssn = $app_data["last_4_ssn"];
    $personal_loan_agreement = $app_data["personal_loan_agreement"];
    $gf_lead_id = $app_data["gf_lead_id"];
    $account_nbr = $app_data["account_nbr"];
    $create_dt = date("Y-m-d H:i:s");
    $bank_acct_type = $app_data["bank_acct_type"];
    $application_status = 1; //Default the application status to draft

    //Build SQL Statement

    $sql_string = "insert into mm_application (first_name, last_name, state, email_address, street_address, appt_suite, city, zip_code, own_or_rent, monthly_rent_amt, monthly_mtg_amt, mobile_phone_nbr, home_phone_nbr, income_source_1, wage_type_1, hourly_rate_1, hours_per_week_1, annual_salary_1, pay_frequency_1, day_paid_month_1, day_paid_twice_monthly_1, day_paid_weekly_1, day_paid_biweekly_1, direct_deposit_ind_1, next_pay_dt_1, monthly_income_amt_1, company_name_1, income_source_2, wage_type_2, hourly_rate_2, hours_per_week_2, annual_salary_2, pay_frequency_2, day_paid_month_2, day_paid_twice_monthly_2, day_paid_weekly_2, day_paid_biweekly_2, direct_deposit_ind_2, next_pay_dt_2, monthly_income_amt_2, company_name_2, routing_nbr, bank_acct_nbr, bank_acct_age_years, bank_acct_age_months, ssn, dob, contact_agreement_agreement, electronic_disclosure_agreement, last_4_ssn, personal_loan_agreement, create_dt, application_status, gf_lead_id, account_nbr, last_paycheck_amt_1, last_paycheck_amt_2, bank_acct_type) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    //$sql_string = "insert into mm_application (first_name, last_name, state, email_address, street_address, appt_suite, city, zip_code, own_or_rent, monthly_rent_amt, monthly_mtg_amt, mobile_phone_nbr, home_phone_nbr, income_source_1, wage_type_1, hourly_rate_1, hours_per_week_1, annual_salary_1, pay_frequency_1, day_paid_month_1, day_paid_twice_monthly_1, day_paid_weekly_1, day_paid_biweekly_1, direct_deposit_ind_1, next_pay_dt_1, monthly_income_amt_1, company_name_1, income_source_2, wage_type_2, hourly_rate_2, hours_per_week_2, annual_salary_2, pay_frequency_2, day_paid_month_2, day_paid_twice_monthly_2, day_paid_weekly_2, day_paid_biweekly_2, direct_deposit_ind_2, next_pay_dt_2, monthly_income_amt_2, company_name_2, routing_nbr, bank_acct_nbr, bank_acct_age_years, bank_acct_age_months, ssn, dob, contact_agreement_agreement, electronic_disclosure_agreement, last_4_ssn, personal_loan_agreement, create_dt, application_status, gf_lead_id, account_nbr, last_paycheck_amt_1, last_paycheck_amt_2) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    //Get a database connection
    $conn = mm_get_db_connection();
    //Attempt to prepare the SQL statement
    if (!$stmt = $conn->prepare($sql_string)) {
        mm_log_error("mm_create_application", "$conn->error", $conn->errno);
        $return_value = $conn->errno;
        $return_message = $conn->error;

    } else {
        $stmt->bind_param('sssssssssddssssdddsisssssdsssdddsisssssdsssiisssssssiiidds', $first_name, $last_name, $state, $email_address, $street_address, $appt_suite, $city, $zip_code, $own_or_rent, $monthly_rent_amt, $monthly_mtg_amt, $mobile_phone_nbr, $home_phone_nbr, $income_source_1, $wage_type_1, $hourly_rate_1, $hours_per_week_1, $annual_salary_1, $pay_frequency_1, $day_paid_month_1, $day_paid_twice_monthly_1, $day_paid_weekly_1, $day_paid_biweekly_1, $direct_deposit_ind_1, $next_pay_dt_1, $monthly_income_amt_1, $company_name_1, $income_source_2, $wage_type_2, $hourly_rate_2, $hours_per_week_2, $annual_salary_2, $pay_frequency_2, $day_paid_month_2, $day_paid_twice_monthly_2, $day_paid_weekly_2, $day_paid_biweekly_2, $direct_deposit_ind_2, $next_pay_dt_2, $monthly_income_amt_2, $company_name_2, $routing_nbr, $bank_acct_nbr, $bank_acct_age_years, $bank_acct_age_months, $ssn, $dob, $contact_agreement_agreement, $electronic_disclosure_agreement, $last_4_ssn, $personal_loan_agreement, $create_dt, $application_status, $gf_lead_id, $account_nbr, $last_paycheck_amt_1, $last_paycheck_amt_2, $bank_acct_type);
        //$stmt->bind_param('sssssssssddssssdddsisssssdsssdddsisssssdsssiisssssssiiidd', $first_name, $last_name, $state, $email_address, $street_address, $appt_suite, $city, $zip_code, $own_or_rent, $monthly_rent_amt, $monthly_mtg_amt, $mobile_phone_nbr, $home_phone_nbr, $income_source_1, $wage_type_1, $hourly_rate_1, $hours_per_week_1, $annual_salary_1, $pay_frequency_1, $day_paid_month_1, $day_paid_twice_monthly_1, $day_paid_weekly_1, $day_paid_biweekly_1, $direct_deposit_ind_1, $next_pay_dt_1, $monthly_income_amt_1, $company_name_1, $income_source_2, $wage_type_2, $hourly_rate_2, $hours_per_week_2, $annual_salary_2, $pay_frequency_2, $day_paid_month_2, $day_paid_twice_monthly_2, $day_paid_weekly_2, $day_paid_biweekly_2, $direct_deposit_ind_2, $next_pay_dt_2, $monthly_income_amt_2, $company_name_2, $routing_nbr, $bank_acct_nbr, $bank_acct_age_years, $bank_acct_age_months, $ssn, $dob, $contact_agreement_agreement, $electronic_disclosure_agreement, $last_4_ssn, $personal_loan_agreement, $create_dt, $application_status, $gf_lead_id, $account_nbr, $last_paycheck_amt_1, $last_paycheck_amt_2);
        if (!$stmt->execute()) {
            $return_value = $conn->errno;
            $return_message = $conn->error;
        } else {
            $return_value = 0;
            $return_message = "Application Added Successfully";
            $application_nbr = $stmt->insert_id;
        }
    }

    //Close Connection if open
    if (is_resource($conn)) {
        $conn->close();
    }

    //Build Return Array
    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    $return_array["application_nbr"] = isset($application_nbr) ? $application_nbr : 0;

    //Return function results
    return $return_array;

}

/********************************************************************************************************************************************************
 * Function mm_get_lp_loans($ssn, $email_address) - This function will get the loan history needed for application data from the loan management system
 ********************************************************************************************************************************************************/
function mm_get_lp_loans($ssn, $email_address)
{
    error_log("flow: mm_get_lp_loans\n");
    $return_array = array();

    $return_array["return_value"] = 0;
    $return_array["return_message"] = "";
    $return_array["date_last_payoff"] = '';
    $return_array["active_loan_ind"] = 0;
    $return_array["charge_off_ind"] = 0;
    $return_array["former_ind"] = 0;

    return $return_array;
}

/***************************************************************************************************************************************************************
 * Function mm_is_bank_holiday($date) - Returns 1 if the date is a bank holiday 0 otherwise
 ***************************************************************************************************************************************************************/
function mm_is_bank_holiday($date)
{
    error_log("flow: mm_is_bank_holiday\n");
    $holiday_dates = array('01/02/2017', '01/16/2017', '02/20/2017', '05/29/2017', '07/04/2017', '09/04/2017', '10/09/2017', '11/23/2017', '12/25/2017',
        '01/01/2018', '01/15/2018', '02/19/2018', '05/28/2018', '07/04/2018', '09/03/2018', '10/08/2018', '11/11/2018', '11/23/2018', '12/25/2018',
        '01/01/2019', '01/21/2019', '02/18/2019', '05/27/2019', '07/04/2017', '09/02/2019', '10/14/2019', '11/11/2019', '11/28/2019', '12/25/2019',
        '01/01/2020', '01/20/2020', '02/17/2020', '05/25/2020', '07/03/2020', '09/07/2020', '10/12/2020', '11/11/2020', '11/26/2020', '12/25/2020',
        '01/01/2021', '01/18/2021', '01/18/2021', '02/15/2021', '05/31/2021', '07/05/2021', '09/06/2021', '10/11/2021', '11/11/2021', '12/24/2021', '12/31/2021',
        '01/17/2022', '02/21/2022', '05/30/2022', '07/04/2022', '09/05/2022', '10/10/2022', '11/11/2022', '11/24/2022', '12/26/2022', '01/02/2023', '02/20/2023',
        '05/29/2023', '07/04/2023', '09/04/2023', '10/09/2023', '11/11/2023', '11/23/2023', '12/25/2023');
    $return_value = 0;
    foreach ($holiday_dates as $holiday) {
        if ($holiday == $date) {
            $return_value = 1;
            break;
        }
    }

    return $return_value;
}

/********************************************************************************************************************************************************************
 * Function update_application_status($application_nbr, $status_id) - Update the application status of the provided application.  Log that the application status was changed also.
 ********************************************************************************************************************************************************************/
function mm_update_application_status($application_nbr, $application_status, $noaa_cd = 0)
{
    error_log("flow: mm_update_application_status\n");
    $return_array = array();
    $continue_processing = 1;
    $return_value = 0;
    $return_message = "";

    //Connect to the database
    $conn = mm_get_db_connection();
    $sql_string = "select application_status from mm_application where application_nbr = ?";
    //Prepare the SQL Statement
    if (!$stmt = $conn->prepare($sql_string)) {
        mm_log_error("mm_update_application_status", "$conn->error", $conn->errno);
        $return_value = $conn->errno;
        $return_message = $conn->error;
        $continue_processing = 0;

    } else {
        $stmt->bind_param('i', $application_nbr);
        if (!$stmt->execute()) {
            $return_value = $conn->errno;
            $return_message = $conn->error;
            mm_log_error("mm_update_application_status", "$conn->error", $conn->errno);
            $continue_processing = 0;
        } else {
            //The query was successful
            $rows = $stmt->get_result();
            $num_rows = $rows->num_rows;
            if ($num_rows == 1) {
                $row = $rows->fetch_assoc();
                $curr_application_status = $row["application_status"];
            } else {
                //Handle that the account number wasn't in the database
                $continue_processing = 0;
                $return_value = 1;
                $return_message = "The application provided doesn't exist.";
                $curr_application_status = 0;
            }
        }
    }
    if ($continue_processing == 1) {
        //Getting the current status was successful.  Update the new status
        $sql_string = "update mm_application set application_status = ? where application_nbr = ?";
        if (!$stmt = $conn->prepare($sql_string)) {
            mm_log_error("mm_update_application_status", "$conn->error", $conn->errno);
            $return_value = $conn->errno;
            $return_message = $conn->error;
            $continue_processing = 0;
        } else {
            $stmt->bind_param('ii', $application_status, $application_nbr);
            if (!$stmt->execute()) {
                $return_value = $conn->errno;
                $return_message = $conn->error;
                mm_log_error("mm_update_application_status", "$conn->error", $conn->errno);
                $continue_processing = 0;
            } else {
                //The query was successful - No action to take right now
            }
        }
    }

    if ($continue_processing == 1) {
        //The status was updated so add a record for debugging purposes to the mm_application_status_history
        $sql_string = "insert into mm_application_status_history (application_nbr, old_application_status, new_application_status, change_dt) values (?,?,?,?)";
        if (!$stmt = $conn->prepare($sql_string)) {
            $return_value = $conn->errno;
            $return_message = $conn->error;
            mm_log_error("mm_update_application_status", "$conn->error", $conn->errno);
            //$continue_processing = 0;
        } else {
            $create_dt = date("Y-m-d H:i:s");
            $stmt->bind_param('iiis', $application_nbr, $curr_application_status, $application_status, $create_dt);
            if (!$stmt->execute()) {
                $return_value = $conn->errno;
                $return_message = $conn->error;
                mm_log_error("mm_update_application_status", "$conn->error", $conn->errno);
                //$continue_processing = 0;
            } else {
                $return_value = 0;
                $return_message = "The application status was successfully updated.";
            }
        }

    }

    if (is_resource($conn)) {
        $conn->close();
    }
    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;

    return $return_array;

}

/*********************************************************************************************************************************************************************
 * Function mm_housing_expense_to_code($housing_amt) - Converts the housing amount to the arranges allowed by Klevrlend
 *********************************************************************************************************************************************************************/
function mm_housing_expense_to_code($housing_amt)
{
    error_log("flow: mm_housing_expense_to_code\n");
    if ($housing_amt < 400) {
        $return_string = 'Less than $400';
    } else if ($housing_amt < 600) {
        $return_string = '$400 - $599';
    } else if ($housing_amt < 800) {
        $return_string = '$600 - $799';
    } else if ($housing_amt < 1000) {
        $return_string = '$800 - $999';
    } else if ($housing_amt < 1200) {
        $return_string = '$1000 - $1199';
    } else if ($housing_amt < 1400) {
        $return_string = '$1200 - $1399';
    } else if ($housing_amt < 1600) {
        $return_string = '$1400 - $1599';
    } else if ($housing_amt < 1800) {
        $return_string = '$1600 - $1799';
    } else if ($housing_amt < 2000) {
        $return_string = '$1800 - $1999';
    } else {
        $return_string = "$2000+";
    }

    return $return_string;

}

/*********************************************************************************************************************************************************************
 * Function mm_calc_next_paydate($curr_date, $pay_frequency) - Calculates the next paydate from the current date given the provided frequencies
 *********************************************************************************************************************************************************************/
function mm_calc_next_paydate($curr_date, $pay_frequency)
{

    return "2017/01/01";
}

/**************************************************************************************************************************************************************************
 * Function mm_build_uw_request($app_data) - Builds a Json Message to be sent to the originations system
 **************************************************************************************************************************************************************************/
function mm_build_uw_request($app_data)
{
    error_log("flow: mm_build_uw_request\n");
    $return_array = array();
    $return_value = 0;
    $return_message = '';
    $temp_array = array();
    $custom_fields = array(array());
    //$content = array();
    $days_since_decline = 10000;
    $account_nbr = 0;
    $lp_customer_id = '';
    $account_status = '';

    //Get the application history to determine days since last hard decline
    $email_address = $app_data["email_address"];
    $account_details = mm_get_account_details_from_email_address($email_address);
    if ($account_details["return_value"]) {
        //Insert error handling if unable to get the account details
    } else {
        $account_nbr = $account_details["account_nbr"];
        $lp_customer_id = isset($account_details["lp_customer_id"]) ? $account_details["lp_customer_id"] : '';
    }

    $app_history_arr = mm_get_application_history($account_nbr);

    if ($app_history_arr["return_value"]) {
        //Need to insert error handling
    } else {
        $app_history = $app_history_arr["app_history"];
        $num_apps = $app_history_arr["num_apps"];
        if ($num_apps == 0) {
            //There are no historical apps so default the customer history.  Should never happen but think about it
            //$is_returning_customer = 'N';
            //$has_active_loan = 'N';
            //$collection_difficulty = 'N';
            //$has_charged_off_loan = 'N';
        } else {
            $today = new DateTime();
            $today->setTime(0, 0, 0);
            foreach ($app_history as $app) {
                $app_status = $app["application_status"];
                $app_date = $app["create_dt"];
                $app_dt = new DateTime($app_date);
                $app_dt->setTime(0, 0, 0);
                if ($app_status == 8) {
                    // Mark was here: commenting out the following line as I think its trying to compute an integer value
                    //$days_diff = $today->diff($app_dt)->format("%a") + 1;
                    $days_diff = $today->diff($app_dt)->days + 1;
                    if ($days_diff <= $days_since_decline) {
                        //This decline is more recent than any seen before
                        $days_since_decline = $days_diff;
                    }
                }
            }

        }
    }

    $account_status_arr = mm_get_account_status($app_data["email_address"]);
    if ($account_status_arr["return_value"]) {
        //There was an error getting the customer history - Need to add error handling
    } else {
        //The account status return value was zero
        $account_status = $account_status_arr["account_status"];
        //$application_status = $account_status_arr["application_status"];

    }
    $header_data = get_origination_system_connection_details();
    $temp_array["DaysSinceDecline"] = $days_since_decline;
    if ($lp_customer_id == '') {
        $temp_array["IsReturningCustomer"] = 'N';
    } else {
        $temp_array["IsReturningCustomer"] = "Y";
    }
    if ($account_status == 'ACTIVELOAN') {
        $temp_array["HasActiveLoan"] = 'Y';
    } else {
        $temp_array["HasActiveLoan"] = 'N';
    }
    if ($account_status == "CHARGEDOFF") {
        $temp_array["HasChargedOffLoan"] = 'Y';
        $temp_array["CollectionDifficulty"] = 'Y';
    } else {
        $temp_array["HasChargedOffLoan"] = 'N';
        $temp_array["CollectionDifficulty"] = 'N';
    }
    $temp_array["TotalMonthlyIncome"] = 0;

    $temp_array["UserName"] = $header_data["UserName"];
    $temp_array["Password"] = $header_data["Password"];
    $temp_array["ApplicationSource"] = $header_data["ApplicationSource"];
    $temp_array["AuthToken"] = $header_data["AuthToken"];
    $temp_array["PortfolioID"] = $header_data["PortfolioID"];
    $temp_array["LoanReason"] = 'Personal Loan';
    $temp_array["FirstName"] = $app_data["first_name"];
    $temp_array["MiddleName"] = '';
    $temp_array["LastName"] = $app_data["last_name"];
    $temp_array["CellPhone"] = $app_data["mobile_phone_nbr"];
    $temp_array["Email"] = $app_data["email_address"];
    $temp_array["DOB"] = substr($app_data["dob"], 5, 2) . substr($app_data["dob"], 8, 2) . substr($app_data["dob"], 0, 4);
    $temp_array["SSN"] = $app_data["ssn"];
    $temp_array["HomeAddress1"] = $app_data["street_address"];
    $temp_array["HomeAddress2"] = isset($app_data["appt_suite"]) ? $app_data["appt_suite"] : '';
    $temp_array["HomeCity"] = $app_data["city"];
    $temp_array["HomeState"] = $app_data["state"];
    $temp_array["HomeZip"] = $app_data["zip_code"];
    $temp_array["HomePhone"] = $app_data["home_phone_nbr"] == '' ? '0000000000' : $app_data["home_phone_nbr"];
    $temp_array["HomeType"] = $app_data["own_or_rent"];
    if ($app_data["income_source_1"] == "I'm self-employed") {
        $temp_array["EmploymentStatus"] = "Self Employed";
    } else if ($app_data["income_source_1"] == "I work for a company") {
        $temp_array["EmploymentStatus"] = "Employed";
    } else if ($app_data["income_source_1"] == "I receive Social Security / disability benefits") {
        $temp_array["EmploymentStatus"] = "Social Security";
    } else if ($app_data["income_source_1"] == "I receive pension / retirement benefits") {
        $temp_array["EmploymentStatus"] = "Other";
    } else if ($app_data["income_source_1"] == "Other / not listed") {
        $temp_array["EmploymentStatus"] = "Other";
    } else {
        $unexpected_emp_type = $app_data["income_source_1"];
        mm_log_error('mm_build_uw_request', "Unexpected Income Type: $unexpected_emp_type");
        $temp_array["EmploymentStatus"] = "Other";
    }
    $temp_array["EmployerName"] = isset($app_data["company_name_1"]) ? $app_data["company_name_1"] : "N/A";
    if ($app_data["pay_frequency_1"] == "Paid Monthly") {
        $temp_array["PayFrequency"] = "MNTH";
    } else if ($app_data["pay_frequency_1"] == "Paid Twice Monthly") {
        $temp_array["PayFrequency"] = "SMMT";
    } else if ($app_data["pay_frequency_1"] == "Bi-Weekly") {
        $temp_array["PayFrequency"] = "BIWK";
    } else if ($app_data["pay_frequency_1"] == "Paid Weekly") {
        $temp_array["PayFrequency"] = "WKLY";
    } else {
        $unexpected_pay_freq = $app_data["pay_frequency_1"];
        $temp_array["PayFrequency"] = "BIWK";
        mm_log_error("mm_build_uw_request", "Unexpected Pay Frequency 1: $unexpected_pay_freq");
    }
    //Need to handle logic to calculate monthly income when it's hourly or salary
   /* Replaced by Jason
    if ($app_data["income_source_1"] == "I'm self-employed") {
        $temp_array["MonthlyIncome"] = floor($app_data["monthly_income_amt_1"]);
    } else if ($app_data["income_source_1"] == "I work for a company") {
        if ($app_data["wage_type_1"] == "Hourly") {
            $temp_array["MonthlyIncome"] = floor($app_data["hourly_rate_1"] * min($app_data["hours_per_week_1"], 40) * 52 / 12);
        } else if ($app_data["wage_type_1"] == "Salary") {
            $temp_array["MonthlyIncome"] = floor($app_data["annual_salary_1"] / 12);
        } else {
            //Expecting a wage type and didn't get it so add some error handling and default the income to a failing amount
            fc_log_error('mm_build_uw_request', "Unexpected Wage Type For Company Employee so setting income to 0");
            $temp_array["MonthlyIncome"] = 0;
        }
    } else if ($app_data["income_source_1"] == "I receive Social Security / disability benefits" || $app_data["income_source_1"] == "I receive pension / retirement benefits" || $app_data["income_source_1"] == "Other / not listed") {
        $monthly_frequency = 0;
        if ($app_data["pay_frequency_1"] == "Paid Monthly") {
            $monthly_frequency = 1;
        } else if ($app_data["pay_frequency_1"] == "Paid Twice Monthly") {
            $monthly_frequency = 2;
        } else if ($app_data["pay_frequency_1"] == "Bi-Weekly") {
            $monthly_frequency = 2.16667;
        } else if ($app_data["pay_frequency_1"] == "Paid Weekly") {
            $monthly_frequency = 4.33333;
        } else {
            //Unexpected value for pay frequency so log an error message
            $unexpected_pay_frequency = $app_data["pay_frequency_1"];
            mm_log_error('mm_build_uw_request', "Unexpected Pay Frequency 1 Trying to calculate monthly income: $unexpected_pay_frequency");
            $temp_array["MonthlyIncome"] = 0;
        }
        $temp_array["MonthlyIncome"] = $monthly_frequency * $app_data["last_paycheck_amt_1"];
    } else {
        $unexpected_emp_type = $app_data["income_source_1"];
        mm_log_error('mm_build_uw_request', " Unable to calculate Income Due to Unexpected Income Type: $unexpected_emp_type");
        $temp_array["MonthlyIncome"] = 0;
    }

    //Need to handle logic to calculate other income when it's hourly or salary and customer lists 2 jobs
    if ($app_data["secondary_income_ind"] == "Yes") {
        if ($app_data["income_source_2"] == "I'm self-employed") {
            $temp_array["OtherIncome"] = floor($app_data["monthly_income_amt_2"]);
        } else if ($app_data["income_source_2"] == "I work for a company") {
            if ($app_data["wage_type_2"] == "Hourly") {
                $temp_array["OtherIncome"] = floor($app_data["hourly_rate_2"] * min($app_data["hours_per_week_2"], 40) * 52 / 12);
            } else if ($app_data["wage_type_2"] == "Salary") {
                $temp_array["OtherIncome"] = floor($app_data["annual_salary_2"] / 12);
            } else {
                //Expecting a wage type and didn't get it so add some error handling and default the income to a failing amount
                mm_log_error('mm_build_uw_request', "Unexpected Wage Type For Other Income and COmpany Employee so setting income to 0");
                $temp_array["OtherIncome"] = 0;
            }
        } else if ($app_data["income_source_2"] == "I receive social security / disability benefits" || $app_data["income_source_2"] == "I receive pension / retirement benefits" || $app_data["income_source_2"] == "Other / not listed") {
            $monthly_frequency = 0;
            if ($app_data["pay_frequency_2"] == "Paid Monthly") {
                $monthly_frequency = 1;
            } else if ($app_data["pay_frequency_2"] == "Paid Twice Monthly") {
                $monthly_frequency = 2;
            } else if ($app_data["pay_frequency_2"] == "Bi-Weekly") {
                $monthly_frequency = 2.16667;
            } else if ($app_data["pay_frequency_2"] == "Paid Weekly") {
                $monthly_frequency = 4.33333;
            } else {
                //Unexpected value for pay frequency so log an error message
                $unexpected_pay_frequency = $app_data["pay_frequency_2"];
                mm_log_error('mm_build_uw_request', "Unexpected Pay Frequency 1 Trying to calculate monthly income: $unexpected_pay_frequency");
                $temp_array["OtherIncome"] = 0;
            }
            $temp_array["OtherIncome"] = $monthly_frequency * $app_data["last_paycheck_amt_2"];
        } else {
            $unexpected_emp_type = $app_data["income_source_2"];
            mm_log_error('mm_build_uw_request', " Unable to calculate Income Due to Unexpected Income Type: $unexpected_emp_type");
            $temp_array["OtherIncome"] = 0;
        }
    } else if ($app_data["secondary_income_ind"] == "No") {
        $temp_array["OtherIncome"] = 0;
    } else {
        //Unexpected Value for Other Incomes.  Set value to zero and log an error message
        $secondary_income_ind = $app_data["secondary_income_ind"];
        $temp_array["OtherIncome"] = 0;
        mm_log_error("mm_build_uw_request", "Unexpected Secondary Income Indicator: $secondary_income_ind.  Other Income set to 0");
    }*/
    $temp_array["OtherIncomeSource"] = "OTHR"; //Hardcoded because not needed for now
    if ($app_data["own_or_rent"] == "Own") {
        $temp_array["MonthlyHousePayment"] = mm_housing_expense_to_code($app_data["monthly_mtg_amt"]);
    } else if ($app_data["own_or_rent"] == "Rent") {
        $temp_array["MonthlyHousePayment"] = mm_housing_expense_to_code($app_data["monthly_rent_amt"]);
    } else {
        //Unexpected value for own/rent flag so handle the error and set housing expense to zero
        $own_rent_flag = $app_data["own_or_rent"];
        $temp_array["MonthlyHousePayment"] = 0;
        mm_log_error("mm_build_uw_request", "Unexpected Own/Rent Flag: $own_rent_flag.  Housing Payment set to zero");
    }
    $temp_array["NextPayDate1"] = substr($app_data["next_pay_dt_1"], 5, 2) . substr($app_data["next_pay_dt_1"], 8, 2) . substr($app_data["next_pay_dt_1"], 0, 4);
    $temp_array["NextPayDate2"] = mm_calc_next_paydate($app_data["next_pay_dt_1"], 'M');
    $temp_array["NextPayDate2"] = substr($temp_array["NextPayDate2"], 5, 2) . substr($temp_array["NextPayDate2"], 8, 2) . substr($temp_array["NextPayDate2"], 0, 4);
    if ($app_data["direct_deposit_ind_1"] == "Y") {
        $temp_array["DirectDeposit"] = "true";
    } else {
        $temp_array["DirectDeposit"] = "false";
    }
    $temp_array["ReferenceID"] = $app_data["application_nbr"];
    $temp_array["PerformDataValidation"] = "true";
    // todo: mark, fix this with result from data input
    $temp_array["BankAccountType"] = "Checking";
    $temp_array["BankAccountNumber"] = $app_data["bank_acct_nbr"];
    $temp_array["BankABA"] = $app_data["routing_nbr"];
    $income_array = mm_calculate_monthly_income_amt($app_data["application_nbr"]);
	//Need Error Handling - Jason
	$temp_array["MonthlyIncome"] = $income_array["monthly_income"];
	$temp_array["OtherIncome"] = $income_array["other_income"];
    $temp_array["TotalMonthlyIncome"] = ceil($temp_array["MonthlyIncome"] + $temp_array["OtherIncome"]);

    //Get the affordable line amount
    /*if ($app_data["pay_frequency_1"] == "Paid Monthly") {
        $affordable_pay_freq = "M";
    } else if ($app_data["pay_frequency_1"] == "Paid Twice Monthly") {
        $affordable_pay_freq = "S";
    } else if ($app_data["pay_frequency_1"] == "Bi-Weekly") {
        $affordable_pay_freq = "B";
    } else if ($app_data["pay_frequency_1"] == "Paid Weekly") {
        $affordable_pay_freq = "B";
    } else {
        //Unexpected pay frequency however, error should already be logged above
        //$unexpected_pay_freq = $app_data["pay_frequency_1"];
        $affordable_pay_freq = "B";
    }
	*/
    //Create tracker of num custom fields added
    $num_custom = 0;


    // mark was here:  We are now just sending a very high amount t0 Klevrlend at this point
     $affordable_loan_amt = "10000";
     $temp1 = array();
    $temp1["Name"] = "AffordabilityLineAmount";
    $temp1["Value"] = $affordable_loan_amt;
    $temp1["Type"] = "int";
    $custom_fields[0] = $temp1;
    $num_custom = 1;
/*
    $affordable_loan_results = mm_get_affordable_loan_amt($app_data["state"], $affordable_pay_freq, $temp_array["TotalMonthlyIncome"]);
    $affordable_loan_return_value = $affordable_loan_results["return_value"];
    if ($affordable_loan_return_value == 0) {
        $affordable_loan_amt = $affordable_loan_results["affordable_loan_amt"];
        mm_update_database_value('mm_application', 'affordable_loan_amt', $affordable_loan_amt, 'd', 'application_nbr', $app_data["application_nbr"]);
        $temp1 = array();
        $temp1["Name"] = "AffordabilityLineAmount";
        $temp1["Value"] = $affordable_loan_amt;
        $temp1["Type"] = "int";
        $custom_fields[0] = $temp1;
        $num_custom = 1;
    } else {
        //There was an error and need to add error handling
        $return_value = 1;
        $return_message = $affordable_loan_results["return_message"];
    }
*/

    //Populate the last approved amount for former customers
    $account_nbr = $app_data["account_nbr"];
    $former_approved_amt_arr = mm_get_last_approved_amt($account_nbr);
    $former_approved_amt_return_value = $former_approved_amt_arr["return_value"];
    $former_approved_amt_return_message = $former_approved_amt_arr["return_message"];
    if ($former_approved_amt_return_value == 0) {
        $last_approved_amt = $former_approved_amt_arr["last_approved_amt"];
        mm_update_database_value('mm_application', 'last_approved_amt', $last_approved_amt, 'd', 'application_nbr', $app_data["application_nbr"]);
        $temp1["Name"] = "FormerAmount";
        $temp1["Value"] = $last_approved_amt;
        //Hardcoded for now will revisit at a later time when our former line assignment strategy becomes more advanced.
        if ($temp_array["IsReturningCustomer"] == 'N') {
            $temp1["Value"] = "0";
        } else {
            $temp1["Value"] = "2500";
        }
        $temp1["Type"] = "int";
        $custom_fields[$num_custom] = $temp1;
        //$num_custom += 1;
    } else {
        $return_value = 1;
        $return_message = $former_approved_amt_return_message;
    }
    //Set Customer Fields Element
    if (sizeof($custom_fields) > 0) {
        $temp_array["CustomFields"] = $custom_fields;
    }

    $json_message = json_encode($temp_array);
    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    $return_array["uw_request_json"] = $json_message;

    return $return_array;
}

/***************************************************
*
***************************************************/
function mm_calculate_monthly_income_amt($application_nbr){
	//Needs error handling on missing applications
	$return_array = array();
	$return_value = 0;
	$return_message = '';
	$MonthlyIncome = 0;
	$OtherIncome = 0;
	$TotalIncome = 0;
	$application_data = mm_get_application_details($application_nbr);
	if($application_data["return_value"] !=0){
		//Insert Error Handling
		$return_value =1;
		$return_message = "Unable to get the application details for application $application_nbr";
	}else{
		$app_data = $application_data["app_data"];
 		if ($app_data["income_source_1"] == "I'm self-employed") {
        		$MonthlyIncome = floor($app_data["monthly_income_amt_1"]);
    		} else if ($app_data["income_source_1"] == "I work for a company") {
        		if ($app_data["wage_type_1"] == "Hourly") {
            			$MonthlyIncome = floor($app_data["hourly_rate_1"] * min($app_data["hours_per_week_1"], 40) * 52 / 12);
        		} else if ($app_data["wage_type_1"] == "Salary") {
            			$MonthlyIncome = floor($app_data["annual_salary_1"] / 12);
        		} else {
            		//Expecting a wage type and didn't get it so add some error handling and default the income to a failing amount
            			mm_log_error('mm_calculate_monthly_income_amt', "Unexpected Wage Type For Company Employee so setting income to 0");
				$return_value = 1;
            			$MonthlyIncome = 0;
        		}
    		} else if ($app_data["income_source_1"] == "I receive Social Security / disability benefits" || $app_data["income_source_1"] == "I receive pension / retirement benefits" || $app_data["income_source_1"] == "Other / not listed") {
        		$monthly_frequency = 0;
        		if ($app_data["pay_frequency_1"] == "Paid Monthly") {
            		$monthly_frequency = 1;
        		} else if ($app_data["pay_frequency_1"] == "Paid Twice Monthly") {
            		$monthly_frequency = 2;
        		} else if ($app_data["pay_frequency_1"] == "Bi-Weekly") {
            		$monthly_frequency = 2.16667;
        		} else if ($app_data["pay_frequency_1"] == "Paid Weekly") {
            		$monthly_frequency = 4.33333;
        		} else {
            		//Unexpected value for pay frequency so log an error message
            		$unexpected_pay_frequency = $app_data["pay_frequency_1"];
            		mm_log_error('mm_calculate_monthly_income_amt', "Unexpected Pay Frequency 1 Trying to calculate monthly income: $unexpected_pay_frequency");
				$return_value = 1;
            		$MonthlyIncome = 0;
        		}
        		$MonthlyIncome = $monthly_frequency * $app_data["last_paycheck_amt_1"];
    		} else {
        		$unexpected_emp_type = $app_data["income_source_1"];
        		mm_log_error('mm_calculate_monthly_income_amt', " Unable to calculate Income Due to Unexpected Income Type: $unexpected_emp_type");
				$return_value = 1;
        		$MonthlyIncome = 0;
    		}
		
 		if ($app_data["secondary_income_ind"] == "Yes") {
       			if ($app_data["income_source_2"] == "I'm self-employed") {
       				$OtherIncome = floor($app_data["monthly_income_amt_2"]);
			} else if ($app_data["income_source_2"] == "I work for a company") {
       				if ($app_data["wage_type_2"] == "Hourly") {
       					$OtherIncome = floor($app_data["hourly_rate_2"] * min($app_data["hours_per_week_2"], 40) * 52 / 12);
            			} else if ($app_data["wage_type_2"] == "Salary") {
                			$OtherIncome = floor($app_data["annual_salary_2"] / 12);
            			} else {
                		//Expecting a wage type and didn't get it so add some error handling and default the income to a failing amount
                			mm_log_error('mm_calculate_monthly_income_amt', "Unexpected Wage Type For Other Income and COmpany Employee so setting income to 0");
				$return_value = 1;
                			$OtherIncome = 0;
            			}
        		} else if ($app_data["income_source_2"] == "I receive social security / disability benefits" || $app_data["income_source_2"] == "I receive pension / retirement benefits" || $app_data["income_source_2"] == "Other / not listed") {
            				$monthly_frequency = 0;
            			if ($app_data["pay_frequency_2"] == "Paid Monthly") {
                			$monthly_frequency = 1;
            			} else if ($app_data["pay_frequency_2"] == "Paid Twice Monthly") {
                			$monthly_frequency = 2;
            			} else if ($app_data["pay_frequency_2"] == "Bi-Weekly") {
                			$monthly_frequency = 2.16667;
            			} else if ($app_data["pay_frequency_2"] == "Paid Weekly") {
                			$monthly_frequency = 4.33333;
            			} else {
                		//Unexpected value for pay frequency so log an error message
                			$unexpected_pay_frequency = $app_data["pay_frequency_2"];
                			mm_log_error('mm_calculate_monthly_income_amt', "Unexpected Pay Frequency 1 Trying to calculate monthly income: $unexpected_pay_frequency");
				$return_value = 1;
                			$OtherIncome = 0;
            			}
            			$OtherIncome = $monthly_frequency * $app_data["last_paycheck_amt_2"];
        		} else {
            			$unexpected_emp_type = $app_data["income_source_2"];
            			mm_log_error('mm_calculate_monthly_income_amt', " Unable to calculate Income Due to Unexpected Income Type: $unexpected_emp_type");
            			$OtherIncome = 0;
				$return_value = 1;
        		}
    		} else if ($app_data["secondary_income_ind"] == "No") {
        		$OtherIncome = 0;
    		} else {
        		//Unexpected Value for Other Incomes.  Set value to zero and log an error message
        		$secondary_income_ind = $app_data["secondary_income_ind"];
        		$OtherIncome = 0;
        		mm_log_error("mm_calculate_monthly_income_amt", "Unexpected Secondary Income Indicator: $secondary_income_ind.  Other Income set to 0");
			$return_value = 1;
		}

	}

	$TotalIncome = $OtherIncome + $MonthlyIncome;

	$return_array["return_value"] = $return_value;
	$return_array["return_message"] = $return_message;
	$return_array["total_income"] = $TotalIncome;
	$return_array["other_income"] = $OtherIncome;
	$return_array["monthly_income"] = $MonthlyIncome;
}

/*****************************************************************************************************************************************************************
 * Function mm_send_uw_request($app_data) - The purpose of this function is to 1) build an UW Request, set the
 *****************************************************************************************************************************************************************/
function mm_send_uw_request($app_data)
{
    error_log("flow: mm_send_uw_request\n");
    $return_array = array();
    $uw_response = array();
    $uw_decision_nbr = 0;
    $application_nbr = $app_data["application_nbr"];
    $server_details = get_origination_system_connection_details();
    $uw_request_array = mm_build_uw_request($app_data);
    $uw_request_value = $uw_request_array["return_value"];
    $uw_request_json = $uw_request_array["uw_request_json"];
    $url = $server_details["url"];
    $url = $url . "UWAdd";
    $curl = curl_init($url);
    $return_value = 0;
    $return_message = '';

    if ($uw_request_value == 1) {
        //There was a problem generating the UW Request Add Error Handling
    } else {
        //Save the message being sent
        mm_log_uw_request($uw_request_json, $application_nbr);
        // mark was here:  commenting these two lines as they are not used
        //$url = $server_details["url"];
        //$url = $url . "UWAdd";

        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $server_details["header_array"]);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $uw_request_json);

        $json_response = curl_exec($curl);
        mm_log_uw_response($json_response, $application_nbr);

        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        if ($status != 201 && $status != 200) {
            //There was an error in the message sending  Add error handling
            $return_value = 1;
            $return_message = "Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl);
        } else {
            //The request worked.  Process the request, update the database application status  For now just echo the response
            $uw_response = json_decode($json_response, true);
            $return_value = 0;
            $return_message = '';
            $insert_uw_decision = mm_create_uw_decision($json_response);
            $uw_decision_nbr = $insert_uw_decision["decision_nbr"];
        }
    }
    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    $return_array["uw_decision_nbr"] = $uw_decision_nbr;
    $return_array["uw_response"] = $uw_response;

    curl_close($curl);

    return $return_array;

}

/*****************************************************************************************************************
 * Function mm_create_preverif_decision($json_response)
 *****************************************************************************************************************/
function mm_create_preverif_decision($response_data)
{
    error_log("flow: mm_create_preverif_decision\n");
    //NEED TO ADD ERROR HANDLING
    $return_array = array();
    //Default the variables
    $application_id = $response_data["ApplicationID"];
    $application_nbr = $response_data["application_nbr"];
    $app_status_cd = $response_data["AppStatusCode"];
    $app_status_desc = $response_data["AppStatusDesc"];
    $required_docs = $response_data["RequiredDocs"];
    $ofac_flag = array_key_exists("OFACFlag", $response_data) ? $response_data["OFACFlag"] : null;
    $facta_flag = array_key_exists("FACTAFlag", $response_data) ? $response_data["FACTAFlag"] : null;
    $bankruptcy_flag = array_key_exists("BankruptcyFlag", $response_data) ? $response_data["BankruptcyFlag"] : null;
    $manual_ach_flag = array_key_exists("ManualACHFlag", $response_data) ? $response_data["ManualACHFlag"] : null;
    $mm_outsort_flag = array_key_exists("MMOutsortFlag", $response_data) ? $response_data["MMOutsortFlag"] : null;
    $income_type_flag = array_key_exists("IncomeTypeFlag", $response_data) ? $response_data["IncomeTypeFlag"] : null;
    $create_dt = date("Y-m-d H:i:s");

    //Build the query string
    //$sql_string = "insert into mm_preverif_decision ( orig_appid, application_nbr, app_status_cd, app_status_desc, required_docs, create_dt) values (?,?,?,?,?,?)";
    $sql_string = "insert into mm_preverif_decision ( orig_appid, application_nbr, app_status_cd, app_status_desc,
                      required_docs, create_dt, ofac_flag, facta_flag, bankruptcy_flag, manual_ach_flag, mm_outsort_flag, income_type_flag)
                      values (?,?,?,?,?,?,?,?,?,?,?,?)";

    //Get connection, prepare statement, bind parameters, and execute query
    $conn = mm_get_db_connection();
    $stmt = $conn->prepare($sql_string);
    //$stmt->bind_param('iissss', $application_id, $application_nbr, $app_status_cd, $app_status_desc, $required_docs, $create_dt);
    $stmt->bind_param('iissssssssss', $application_id, $application_nbr, $app_status_cd, $app_status_desc, $required_docs, $create_dt,
        $ofac_flag, $facta_flag, $bankruptcy_flag, $manual_ach_flag, $mm_outsort_flag, $income_type_flag);
    $stmt->execute();
    $insert_nbr = $stmt->insert_id;
    mm_update_database_value('mm_application', 'preverif_decision_nbr', $insert_nbr, 'i', 'application_nbr', $application_nbr);

    //close database connections
    if (is_resource($conn)) {
        $conn->close();
    }

    //return values;
    return $return_array;

}

/*****************************************************************************************************************
 * Function mm_create_lead_detail($unique_id, $ip_address, $http_user_agent, $page_desc)
 *****************************************************************************************************************/
function mm_create_lead_detail($unique_id, $ip_address, $http_user_agent, $page_desc)
{
    error_log("flow: mm_create_lead_detail\n");
    //NEED TO ADD ERROR HANDLING
    $return_array = array();
    //Default the variables
    $create_dt = date("Y-m-d H:i:s");

    //Build the query string
    $sql_string = "insert into mm_lead_detail (unique_id, ip_address, http_user_agent, page_desc, create_dt) values (?,?,?,?,?)";

    //Get connection, prepare statement, bind parameters, and execute query
    $conn = mm_get_db_connection();
    $stmt = $conn->prepare($sql_string);
    $stmt->bind_param('sssss', $unique_id, $ip_address, $http_user_agent, $page_desc, $create_dt);
    $stmt->execute();

    //$return_value = 0;

    //close database connections
    if (is_resource($conn)) {
        $conn->close();
    }

    //return values;
    return $return_array;

}

/*****************************************************************************************************************
 * Function mm_create_field_validation_error($unique_id, $ip_address, $form_id, $field_id, $value, $message)
 *****************************************************************************************************************/
function mm_create_field_validation_error($unique_id, $ip_address, $form_id, $field_id, $value, $message)
{
    error_log("flow: mm_create_field_validation_error\n");
    //NEED TO ADD ERROR HANDLING
    $return_value = 0;
    $validation_error_id = 0;
    $return_message = '';
    $return_array = array();
    //Default the variables
    $create_dt = date("Y-m-d H:i:s");

    //Build the query string
    $sql_string = "insert into mm_field_validation_error (unique_id, ip_address, form_id, field_id, value, message, create_dt)  values (?,?,?,?,?,?,?)";
    try {
        $conn = mm_get_pdo_connection();
        $bind_params = [$unique_id, $ip_address, $form_id, $field_id, $value, $message, $create_dt];
        $stmt = $conn->prepare($sql_string);
        $stmt->execute($bind_params);
        $validation_error_id = $conn->lastInsertId();

    } catch (PDOException $e) {
        $return_array["return_value"] = 1;
        $return_array["return_message"] = "$e";
        return $return_array;
    }

    $conn = null;

    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    $return_array["validation_error_id"] = $validation_error_id;

    //return values;
    return $return_array;

}

/************************************************************************************************************************
 * Function mm_create_uw_decision($json_response)
 ************************************************************************************************************************/
function mm_create_uw_decision($json_response)
{
    error_log("flow: mm_create_uw_decision\n");
    //Initialize the variables
    $return_array = array();
    $decision_data = json_decode($json_response, true);
    //error_log_dump_associative_array("mark: dumping values passed into mm_create_uw_decision", $decision_data);
    $orig_appid = $decision_data["AppId"];
    $app_status_cd = $decision_data["AppStatusCode"];
    $app_status_desc = $decision_data["AppStatusDesc"];
    $approved_amt = $decision_data["ApprovedAmount"];
    $is_approved = $decision_data["IsApproved"];
    $product = $decision_data["Product"];
    $reference_id = $decision_data["ReferenceId"];
    $reject_reason_1 = $decision_data["RejectReason1"];
    $reject_reason_2 = $decision_data["RejectReason2"];
    $reject_reason_3 = $decision_data["RejectReason3"];
    $reject_reason_4 = $decision_data["RejectReason4"];
    $reject_reason_cd = $decision_data["RejectReasonCode"];
    $segment = $decision_data["Segment"];
    $ssn = $decision_data["SSN"];
    $strategy = $decision_data["Strategy"];

    // todo: fix these so that we do not apply in PROD
    // Klevrlend should pass these values at all times
    //error_log_vardump("is_approved: ", $is_approved);
    //error_log_vardump("is_approved test==1: ", $is_approved == 1);
    $approved_apr = array_key_exists('APR', $decision_data) ? $decision_data["APR"] : null;
    //error_log_vardump("approved_apr: ", $approved_apr);
    // if ($is_approved == 1 && $approved_apr == 0) {
    //     //$approved_apr = 132.73;
    // }

    $approved_term = array_key_exists('Term', $decision_data) ? $decision_data["Term"] : null;
    // error_log_vardump("approved_term: ", $approved_term);
    // if ($is_approved == 1 && $approved_term == 0) {
    //     //$approved_term = 24;
    // }

    $clr_action = array_key_exists("CLRAction", $decision_data) ? $decision_data["CLRAction"] : null;
    $clr_exp_auto_most_neg_last_12 = array_key_exists("CLREXPAutoMostNegLast12", $decision_data) ? $decision_data["CLREXPAutoMostNegLast12"] : null;
    $clr_exp_bankcard_total_bal_last_6 = array_key_exists("CLREXPBankcardTotalBalLast6", $decision_data) ? $decision_data["CLREXPBankcardTotalBalLast6"] : null;
    $clr_exp_bankcard_most_neg_last_6 = array_key_exists("CLREXPBankcardMostNegLast6", $decision_data) ? $decision_data["CLREXPBankcardMostNegLast6"] : null;
    $clr_exp_mtg_most_neg_last_6 = array_key_exists("CLREXPMtgMostNegLast6", $decision_data) ? $decision_data["CLREXPMtgMostNegLast6"] : null;
    $clr_ccb_num_ssns = array_key_exists("CLRCBBNumSSNs", $decision_data) ? $decision_data["CLRCBBNumSSNs"] : null;
    $clr_cca_days_since_last_inq = array_key_exists("CLRCCADaysSinceLastInq", $decision_data) ? $decision_data["CLRCCADaysSinceLastInq"] : null;
    $clr_cca_num_total_loans = array_key_exists("CLRCCANumTotalLoans", $decision_data) ? $decision_data["CLRCCANumTotalLoans"] : null;
    $clr_cca_days_since_last_loan_open = array_key_exists("CLRCCADaysSinceLastLoanOpen", $decision_data) ? $decision_data["CLRCCADaysSinceLastLoanOpen"] : null;
    //$clr_cca_days_since_last_loan_open = array_key_exists("CLRCCADaysSinceLastLoanOpen", $decision_data) ? $decision_data["CLRCCADaysSinceLastLoanOpen"] : null;
    $clr_cca_num_loans_current = array_key_exists("CLRCCANumLoansCurrent", $decision_data) ? $decision_data["CLRCCANumLoansCurrent"] : null;
    $clr_cca_amt_loans_current = array_key_exists("CLRCCAAmtLoansCurrent", $decision_data) ? $decision_data["CLRCCAAmtLoansCurrent"] : null;
    $clr_cca_amt_loans_past_due = array_key_exists("CLRCCAAmtLoansPastDue", $decision_data) ? $decision_data["CLRCCAAmtLoansPastDue"] : null;
    $clr_cca_num_loans_past_due = array_key_exists("CLRCCANumLoansPastDue", $decision_data) ? $decision_data["CLRCCANumLoansPastDue"] : null;
    $clr_cca_num_loans_paid_off = array_key_exists("CLRCCANumLoansPaidOff", $decision_data) ? $decision_data["CLRCCANumLoansPaidOff"] : null;
    $clr_cca_days_since_last_loan_paid = array_key_exists("CLRCCADaysSinceLastLoanPaid", $decision_data) ? $decision_data["CLRCCADaysSinceLastLoanPaid"] : null;
    $clr_cca_days_since_last_loan_co = array_key_exists("CLRCCADaysSinceLastLoanCO", $decision_data) ? $decision_data["CLRCCADaysSinceLastLoanCO"] : null;
    $clr_cca_worst_payment_rating = array_key_exists("CLRCCAWorstPaymentRating", $decision_data) ? $decision_data["CLRCCAWorstPaymentRating"] : null;
    $clr_cca_active_duty_status = array_key_exists("CLRCCAActiveDutyStatus", $decision_data) ? $decision_data["CLRCCAActiveDutyStatus"] : null;
    $line_assignment_segment = array_key_exists("LineAssignmentSegment", $decision_data) ? $decision_data["LineAssignmentSegment"] : null;
    $line_assignment_strategy = array_key_exists("LineAssignmentStrategy", $decision_data) ? $decision_data["LineAssignmentStrategy"] : null;

    $decision_nbr = 0;

    $transaction_dt = $decision_data["TransactionDate"];
    $transaction_dt = date_create($transaction_dt);
    $transaction_dt = date_format($transaction_dt, "Y-m-d H:i:s");

    //$transaction_dt = substr($decision_data["TransactionDate"],0,strlen($decision_data["TransactionDate"]) -3);
    //$transaction_id = $decision_data["TransactionId"];
    $create_dt = date("Y-m-d H:i:s");

    //Build Insert Query
    //$sql_string = "insert into mm_uw_decision(orig_appid, app_status_cd, app_status_desc, approved_amt, is_approved, product,
    //                application_nbr, reject_reason_1, reject_reason_2, reject_reason_3, reject_reason_4, reject_reason_cd,
    //                segment, ssn, strategy, transaction_dt, create_dt, transaction_id)
    //                values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    $sql_string = "insert into mm_uw_decision(orig_appid, app_status_cd, app_status_desc, approved_amt, is_approved, product,
                    application_nbr, reject_reason_1, reject_reason_2, reject_reason_3, reject_reason_4, reject_reason_cd,
                    segment, ssn, strategy, transaction_dt, create_dt, transaction_id, approved_apr, approved_term, clr_action,
                    clr_exp_auto_most_neg_last_12, clr_exp_bankcard_total_bal_last_6, clr_exp_bankcard_most_neg_last_6, clr_exp_mtg_most_neg_last_6,
                    clr_exp_cbb_num_ssns, clr_cca_days_since_last_inq, clr_cca_num_total_loans, clr_cca_days_since_last_loan_open,
                    clr_cca_num_loans_current, clr_cca_amt_loans_current, clr_cca_amt_loan_past_due, clr_cca_num_loans_past_due,
                    clr_cca_num_loans_paid_off, clr_cca_days_since_last_loan_paid, clr_cca_days_since_last_loan_co,
                    clr_cca_worst_payment_rating, clr_cca_active_duty_status,line_assignment_segment,line_assignment_strategy)
                    values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    //Connect to the database
    $conn = mm_get_db_connection();

    if (!$stmt = $conn->prepare($sql_string)) {
        mm_log_error("mm_create_uw_decision", "$conn->error", $conn->errno);
        $return_value = $conn->errno;
        $return_message = $conn->error;

    } else {
        //$stmt->bind_param('issdssissssisssssi', $orig_appid, $app_status_cd, $app_status_desc, $approved_amt, $is_approved, $product,
        //    $reference_id, $reject_reason_1, $reject_reason_2, $reject_reason_3, $reject_reason_4, $reject_reason_cd,
        //    $segment, $ssn, $strategy, $transaction_dt, $create_dt, $transaction_id);
        $stmt->bind_param('issdssissssisssssidisidiiiiiiiddiiiiss', $orig_appid, $app_status_cd, $app_status_desc, $approved_amt, $is_approved, $product,
            $reference_id, $reject_reason_1, $reject_reason_2, $reject_reason_3, $reject_reason_4, $reject_reason_cd,
            $segment, $ssn, $strategy, $transaction_dt, $create_dt, $transaction_id, $approved_apr, $approved_term,
            $clr_action, $clr_exp_auto_most_neg_last_12, $clr_exp_bankcard_total_bal_last_6, $clr_exp_bankcard_most_neg_last_6,
            $clr_exp_mtg_most_neg_last_6, $clr_ccb_num_ssns, $clr_cca_days_since_last_inq, $clr_cca_num_total_loans,
            $clr_cca_days_since_last_loan_open, $clr_cca_num_loans_current, $clr_cca_amt_loans_current, $clr_cca_amt_loans_past_due,
            $clr_cca_num_loans_past_due, $clr_cca_num_loans_paid_off, $clr_cca_days_since_last_loan_paid, $clr_cca_days_since_last_loan_co,
            $clr_cca_worst_payment_rating, $clr_cca_active_duty_status,$line_assignment_segment,$line_assignment_strategy);

        if (!$stmt->execute()) {
            $return_value = $conn->errno;
            $return_message = $conn->error;
            mm_log_error("mm_create_uw_decision", "$conn->error", $conn->errno);
        } else {
            //The account was inserted successfully
            if (empty($approved_apr) || empty($approved_term)) {
                $return_value = 1;
                $return_message = "APR or TERM is missing, zero or empty";
                mm_log_error("mm_create_uw_decision", $return_message);
            } else {
                $decision_nbr = $stmt->insert_id;
                $return_value = 0;
                $return_message = "Decision Added Successfully";
            }
        }
    }

    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    $return_array["decision_nbr"] = $decision_nbr;
    return $return_array;

}

/****************************************************************
 * Function mm_state_cd_to_state($state)
 ***************************************************************
 * @param string $state_cd
 * @return string
 */
function mm_state_cd_to_display(string $state_cd): string
{
    error_log("flow: mm_state_cd_to_display\n");
    switch (strtoupper($state_cd)) {
        case "AL":
            $state = "Alabama";
            break;
        case "AK":
            $state = "Alaska";
            break;
        case "AZ":
            $state = "Arizona";
            break;
        case "AR":
            $state = "Arkansas";
            break;
        case "CA":
            $state = "California";
            break;
        case "CO":
            $state = "Colorado";
            break;
        case "CT":
            $state = "Connecticut";
            break;
        case "DE":
            $state = "Delaware";
            break;
        case "FL":
            $state = "Florida";
            break;
        case "GA":
            $state = "Georgia";
            break;
        case "HI":
            $state = "Hawaii";
            break;
        case "ID":
            $state = "Idaho";
            break;
        case "IL":
            $state = "Illinois";
            break;
        case "IN":
            $state = "Indiana";
            break;
        case "IA":
            $state = "Iowa";
            break;
        case "KS":
            $state = "Kansas";
            break;
        case "KY":
            $state = "Kentucky";
            break;
        case "LA":
            $state = "Louisiana";
            break;
        case "ME":
            $state = "Maine";
            break;
        case "MD":
            $state = "Maryland";
            break;
        case "MA":
            $state = "Massachusetts";
            break;
        case "MI":
            $state = "Michigan";
            break;
        case "MN":
            $state = "Minnesota";
            break;
        case "MS":
            $state = "Mississippi";
            break;
        case "MO":
            $state = "Missouri";
            break;
        case "MT":
            $state = "Montana";
            break;
        case "NE":
            $state = "Nebraska";
            break;
        case "NV":
            $state = "Nevada";
            break;
        case "NH":
            $state = "New Hampshire";
            break;
        case "NJ":
            $state = "New Jersey";
            break;
        case "NM":
            $state = "New Mexico";
            break;
        case "NY":
            $state = "New York";
            break;
        case "NC":
            $state = "North Carolina";
            break;
        case "ND":
            $state = "North Dakota";
            break;
        case "OH":
            $state = "Ohio";
            break;
        case "OK":
            $state = "Oklahoma";
            break;
        case "OR":
            $state = "Oregon";
            break;
        case "PA":
            $state = "Pennsylvania";
            break;
        case "RI":
            $state = "Rhode Island";
            break;
        case "SC":
            $state = "South Carolina";
            break;
        case "SD":
            $state = "South Dakota";
            break;
        case "TN":
            $state = "Tennessee";
            break;
        case "TX":
            $state = "Texas";
            break;
        case "UT":
            $state = "Utah";
            break;
        case "VT":
            $state = "Vermont";
            break;
        case "VA":
            $state = "Virginia";
            break;
        case "WA":
            $state = "Washington";
            break;
        case "WV":
            $state = "West Virginia";
            break;
        case "WI":
            $state = "Wisconsin";
            break;
        case "WY":
            $state = "Wyoming";
            break;
        default:
            $state = '';
    }

    return $state;
}

/******************************************************************************************************************************************************************
 * Function mm_state_to_cd($state) - Converts the state from long name to 2 char code
 *****************************************************************************************************************************************************************
 * @param $state
 * @return string
 */
function mm_state_to_cd(string $state): string
{
    error_log("flow: mm_state_to_cd\n");
    switch (strtolower($state)) {
        case "alabama":
            $state_cd = "AL";
            break;
        case "alaska":
            $state_cd = "AK";
            break;
        case "arizona":
            $state_cd = "AZ";
            break;
        case "arkansas":
            $state_cd = "AR";
            break;
        case "california":
            $state_cd = "CA";
            break;
        case "colorado":
            $state_cd = "CO";
            break;
        case "connecticut":
            $state_cd = "CT";
            break;
        case "delaware":
            $state_cd = "DE";
            break;
        case "florida":
            $state_cd = "FL";
            break;
        case "georgia":
            $state_cd = "GA";
            break;
        case "hawaii":
            $state_cd = "HI";
            break;
        case "idaho":
            $state_cd = "ID";
            break;
        case "illinois":
            $state_cd = "IL";
            break;
        case "indiana":
            $state_cd = "IN";
            break;
        case "iowa":
            $state_cd = "IA";
            break;
        case "kansas":
            $state_cd = "KS";
            break;
        case "kentucky":
            $state_cd = "KY";
            break;
        case "louisiana":
            $state_cd = "LA";
            break;
        case "maine":
            $state_cd = "ME";
            break;
        case "maryland":
            $state_cd = "MD";
            break;
        case "massachusetts":
            $state_cd = "MA";
            break;
        case "michigan":
            $state_cd = "MI";
            break;
        case "minnesota":
            $state_cd = "MN";
            break;
        case "mississippi":
            $state_cd = "MS";
            break;
        case "missouri":
            $state_cd = "MO";
            break;
        case "montana":
            $state_cd = "MT";
            break;
        case "nebraska":
            $state_cd = "NE";
            break;
        case "nevada":
            $state_cd = "NV";
            break;
        case "new hampshire":
            $state_cd = "NH";
            break;
        case "new jersey":
            $state_cd = "NJ";
            break;
        case "new mexico":
            $state_cd = "NM";
            break;
        case "new york":
            $state_cd = "NY";
            break;
        case "north carolina":
            $state_cd = "NC";
            break;
        case "north dakota":
            $state_cd = "ND";
            break;
        case "ohio":
            $state_cd = "OH";
            break;
        case "oklahoma":
            $state_cd = "OK";
            break;
        case "oregon":
            $state_cd = "OR";
            break;
        case "pennsylvania":
            $state_cd = "PA";
            break;
        case "rhode island":
            $state_cd = "RI";
            break;
        case "south carolina":
            $state_cd = "SC";
            break;
        case "south dakota":
            $state_cd = "SD";
            break;
        case "tennessee":
            $state_cd = "TN";
            break;
        case "texas":
            $state_cd = "TX";
            break;
        case "utah":
            $state_cd = "UT";
            break;
        case "vermont":
            $state_cd = "VT";
            break;
        case "virginia":
            $state_cd = "VA";
            break;
        case "washington":
            $state_cd = "WA";
            break;
        case "west virginia":
            $state_cd = "WV";
            break;
        case "wisconsin":
            $state_cd = "WI";
            break;
        case "wyoming":
            $state_cd = "WY";
            break;
        default:
            $state_cd = 'XX';
    }

    return $state_cd;
}

/****************************************************************************************************************************************************
 * Function mm_submit_application() - Process the receipt of an application
 ****************************************************************************************************************************************************/
function mm_submit_application($app_data)
{
    error_log("flow: mm_submit_application\n");
    //Initialize variables
    $return_array = array();
    $return_message = '';
    $return_value = 0;
    $noaa_cd = 0;
    $uw_decision = '';
    $approved_amt = 0;
    $mm_application_nbr = 0;
    $mm_account_nbr = 0;
    $mm_uw_decision_nbr = 0;
    $uw_application_nbr = 0;
    //$gf_lead_id = $app_data["gf_lead_id"];

    $email_address = $app_data["email_address"];
    $ssn = $app_data["ssn"];
    $state_cd = $app_data["state"];
    $app_status_cd = '';
    $reject_reason_cd = '';

    //Check to see if the accounts exist
    $email_check_array = mm_account_exists('email_address', $email_address);
    $email_acct_nbr = $email_check_array["account_nbr"];
    $email_exists = $email_check_array["return_value"];
    $ssn_check_array = mm_account_exists('ssn', $ssn);
    $ssn_acct_nbr = $ssn_check_array["account_nbr"];
    $ssn_exists = $ssn_check_array["return_value"];
    if ($ssn_exists == 1 && $email_exists == 1) {
        //Both the SSN and Email are tied to an existing account
        //error_log("mark: Both the SSN and Email are tied to an existing account");
        if ($ssn_acct_nbr != $email_acct_nbr) {
            //The SSN and Email address both exist but are tied to different applications
            $continue_processing = 0;
            $return_value = 1;
            $return_message = "The username and SSN provided don't match our system records";
        } else {
            //Both the SSN and email exist and they are tied to the same record
            $mm_account_nbr = $email_acct_nbr;
            $app_data["account_nbr"] = $mm_account_nbr;
            $continue_processing = 1;
            $_SESSION["mm_account_nbr"] = $mm_account_nbr;
        }

    } else if ($ssn_exists + $email_exists == 1) {
        //error_log("mark: This is an error condition because either the SSN or username exists but doesn't match the other item");
        //This is an error condition because either the SSN or username exists but doesn't match the other item
        $return_value = 1;
        $return_message = "The username or SSN provided don't match our system records.";
        $continue_processing = 0;
    } else {
        //Neither the SSN or Email Exist so create a new account record
        //error_log("mark: calling mm_create_account");
        $return_array = mm_create_account($app_data);
        $return_value = $return_array["return_value"];
        $return_message = $return_array["return_message"];
        if ($return_value > 0) {
            //The account wasn't created successfully
            //error_log("mark: The account wasn't created successfully");
            $return_value = 1;
            //$return_message = "The account record couldn't be created.";
            //$return_message = $return_message;
            $continue_processing = 0;
        } else {
            //The account was created successfully
            $continue_processing = 1;
            $mm_account_nbr = $return_array["account_nbr"];
            $app_data["account_nbr"] = $mm_account_nbr;
            //error_log("mark: The account was created successfully: account_nbr: $mm_account_nbr");
            //UPDATE SESSION VARIABLE HERE
            $_SESSION["mm_account_nbr"] = $mm_account_nbr;
        }
    }

    //The account related information was ok, try to get the LMS data and make sure it's ok.
    if ($continue_processing == 1) {
        //The account related information is ok so move to the next step

        //Determine that the customer 1) doesn't have an open loan, 2) hasn't paid off in the last 3 days and 3) doesn't have unpaid debt from a previous loan
        // todo: I think that mm_get_lp_loans needs to take into consideration the above line.
        $lms_data = mm_get_lp_loans($ssn, $email_address);
        $return_value = $lms_data["return_value"];
        $charge_off_ind = $lms_data["charge_off_ind"];
        $active_loan_ind = $lms_data["active_loan_ind"];
        //$former_ind = $lms_data["former_ind"];
        if ($return_value != 0) {
            //There was an issue getting the customer history
            $return_value = 1;
            $return_message = "There was a technical issue.";
            $continue_processing = 0;
            //ADD ADVANCED ERROR HANDLING and Possible set the application status to stuck?

        } else {
            //The LMS system was able to return customer history
            //NEED TO APPEND THIS INFORMATION TO THE UW REQUEST AND HAVE KLEVRLEND MAKE THE DECISION FOR CONSISTENCY
            $days_since_last_payoff = ''; //NEED TO FIX THIS
            if ($days_since_last_payoff > 3 || $active_loan_ind == 1 || $charge_off_ind == 1) {
                //The customer is not eligible to complete an application.  Need to get the messaging rig
                $continue_processing = 0;
                $return_value = 1;
                $return_message = "The customer is not eligible to fill out an application based on prior loan information.";

            } else {
                //The customer is eligible based on customer history data.  Append to the application array and continue processing.
                $continue_processing = 1;
            }
        }
    }

    //Based on LMS info the app is ok so add the record to the database.
    if ($continue_processing == 1) {
        $add_app_data = mm_create_application($app_data);
        if ($add_app_data["return_value"] != 0) {
            //Add ERROR Handling
            $return_value = $add_app_data["return_value"];
            $return_message = $add_app_data["return_message"];
            mm_log_error("mm_submit_application", "Error Adding Application Record: $return_message");
            $continue_processing = 0;
        } else {
            //Record was successfully added so continue
            $app_data["application_nbr"] = $add_app_data["application_nbr"];
            $mm_application_nbr = $add_app_data["application_nbr"];
            $continue_processing = 1;
            mm_generate_application_pdf($mm_application_nbr);
            //UPDATE LATER - cycle through the app_data array and put all of the values in a session for recall later
            foreach ($app_data as $key => $value) {
                $session_key = "app_$key";
                $_SESSION["$session_key"] = $value;
            }
        }
    }

    if ($continue_processing == 1) {
        //At this point, the application data is ok, and we were able to get customer history.  Now Need to construct the Klevrlend Request
        $uw_results = mm_send_uw_request($app_data);
        $request_return_value = $uw_results["return_value"];
        $request_return_message = $uw_results["return_message"];
        if ($request_return_value != 0) {
            //There was an error sending the message so add error handling
            $return_value = 1;
            //$return_message = "There was an issue sending the uw request.";
            $return_message = $request_return_message;
            mm_log_error("mm_submit_application", "Issue Sending Underwriting Request: $request_return_message");
            mm_update_database_value('mm_application', 'application_status', 9, 'i', 'application_nbr', $mm_application_nbr);
            $continue_processing = 0;
        } else {
            $continue_processing = 1;
            $uw_response = $uw_results["uw_response"];
            //error_log_dump_associative_array("mark: uw_response",$uw_response);
            $app_status_cd = $uw_response["AppStatusCode"];
            //$app_status_desc = $uw_response["AppStatusDesc"];
            $approved_amt = $uw_response["ApprovedAmount"];
            //$is_approved = $uw_response["IsApproved"];
            $reject_reason_cd = $uw_response["RejectReasonCode"];
            $uw_application_nbr = $uw_response["AppId"];
            $uw_decision_nbr = $uw_results["uw_decision_nbr"];
            $approved_apr = $uw_response["APR"];
            $approved_term = $uw_response["Term"];
            mm_update_database_value('mm_application', 'orig_appid', $uw_application_nbr, 'i', 'application_nbr', $mm_application_nbr);
            mm_update_database_value('mm_application', 'uw_decision_nbr', $uw_decision_nbr, 'i', 'application_nbr', $mm_application_nbr);
            //error_log("mark: approved_apr: $approved_apr");
            mm_update_database_value('mm_application', 'approved_apr', $approved_apr, 'd', 'application_nbr', $mm_application_nbr);
            //error_log("mark: approved_term: $approved_term");
            mm_update_database_value('mm_application', 'approved_term', $approved_term, 'i', 'application_nbr', $mm_application_nbr);
        }

    }

    if ($continue_processing == 1) {
        //The UW request must have been successful so process the response.
        if ($app_status_cd == "Duplicate" || $app_status_cd == "Error" || $app_status_cd == "Pending") {
            $return_value = 1;
            $return_message = "There was an issue processing the application with the originations system. $app_status_cd";
            $uw_decision = "Stuck";
            //Need to update the application status to stuck
            mm_update_database_value('mm_application', 'application_status', 9, 'i', 'application_nbr', $mm_application_nbr);
            //Need to add an error message
            mm_log_error("mm_submit_application", "Bad Application status: $app_status_cd for app: $mm_application_nbr");
        } else if ($app_status_cd == "Declined") {
            $return_value = 0;
            $return_message = "The application has been declined.";
            $uw_decision = "Declined";
            $noaa_cd = $reject_reason_cd;
            //Need to update the status of the application to declined and add the NOAA
            mm_update_database_value('mm_application', 'application_status', 8, 'i', 'application_nbr', $mm_application_nbr);
            mm_update_database_value('mm_application', 'noaa_cd', $noaa_cd, 'i', 'application_nbr', $mm_application_nbr);
            mm_update_database_value('mm_application', 'approved_amt', 0, 'd', 'application_nbr', $mm_application_nbr);
            //Need to generate a PDF NOAA
            $noaa_details = mm_generate_noaa_pdf($mm_application_nbr, $noaa_cd);
            $noaa_location = $noaa_details["document_path"];

            //Schedule the NOAA email to be sent
            $email_data = array();
            $email_data["to"] = $email_address;
            $email_data["from"] = SUPPORT_EMAIL;
            $email_data["from_name"] = SITE_NAME . " Support Team";
            $email_data["subject"] = SITE_NAME . " Application Decision";
            $email_data["txt_body"] = "We are writing to inform you of our decision regarding your recent application with " . SITE_NAME . ".  At this time, we are unable to grant your credit request.  Attached you will find the notice of adverse action which contains additional information as well as your rights for additional information regarding this decision.\n\nThanks for considering " . SITE_NAME . ",\n\n" . SITE_NAME . " Support Team\nEmail:" . SUPPORT_EMAIL . "\nPhone:(844) 678-5626\n";
            $email_data["html_body"] = "<html><head></head><body><p>We are writing to inform you of our decision regarding your recent application with " . SITE_NAME . ".  At this time, we are unable to grant your credit request.  Attached you will find the notice of adverse action which contains additional information as well as your rights to request additional information regarding this decision.</p><br><p>Thanks for considering " . SITE_NAME . ",</p><br><p>" . SITE_NAME . " Support Team</p><p>Email:" . SUPPORT_EMAIL . "</p><p>Phone:(844) 678-5626</p></body></html>";
            $email_data["attachment"] = $noaa_location;
            $tmp_dt = new DateTime();
            $tmp_dt = $tmp_dt->modify("+1 Day");
            $scheduled_dt = $tmp_dt->format("Y-m-d H:i:s");
            $email_data["scheduled_dt"] = $scheduled_dt;
            $email_message = json_encode($email_data);
            $email_results = mm_schedule_email_message($email_message);
            if ($email_results["return_value"] != 0 || $email_results["email_message_nbr"] == 0) {
                //The email scheduling failed so log an error
                mm_log_error('mm_submit_application', "Failed to schedule the NOAA email to be sent to $email_address");
            }

        } else if ($app_status_cd == "Approved") {
            //Update the application record with the approved status and the approval amount
            //Set the return variables
            $uw_decision = "Approved";
            //$approved_amt = $approved_amt;
            //Need to get the details to get the minimum between affordable_loan_amt and the KL Decision amt
            $temp_app_data = mm_get_application_details($mm_application_nbr);
            $temp_arr = $temp_app_data["app_data"];
		if ($temp_arr["pay_frequency_1"] == "Paid Monthly") {
        		$affordable_pay_freq = "M";
    		} else if ($temp_arr["pay_frequency_1"] == "Paid Twice Monthly") {
        		$affordable_pay_freq = "S";
    		} else if ($temp_arr["pay_frequency_1"] == "Bi-Weekly") {
        		$affordable_pay_freq = "B";
    		} else if ($temp_arr["pay_frequency_1"] == "Paid Weekly") {
        		$affordable_pay_freq = "B";
    		} else {
        		//Unexpected pay frequency however, error should already be logged above
        		//$unexpected_pay_freq = $app_data["pay_frequency_1"];
        		$affordable_pay_freq = "B";
    		}

	    $cust_state = $temp_arr["state"];
	    $income_data = mm_calculate_monthly_income_amt($mm_application_nbr);
	    $monthly_income_amt = $income_data["total_income"];
	    $affordable_amt_array = mm_get_affordable_loan_amt($cust_state, $affordable_pay_freq, $monthly_income_amt, $approved_apr);
	    $affordable_loan_amt = $affordable_amt_array["affordable_loan_amt"];
            error_log_dump_associative_array("In mm_submit_application before affordable_loan_amt calc, dumping", $temp_arr);
            $affordable_loan_amt = $temp_arr["affordable_loan_amt"];
            $final_approved_amt = $affordable_loan_amt < $approved_amt ? $affordable_loan_amt : $approved_amt;
            mm_update_database_value('mm_application', 'application_status', 2, 'i', 'application_nbr', $mm_application_nbr);
            mm_update_database_value('mm_application', 'approved_amt', $final_approved_amt, 'i', 'application_nbr', $mm_application_nbr);
            mm_update_database_value('mm_application', 'credit_approved_amt', $approved_amt, 'i', 'application_nbr', $mm_application_nbr);
            mm_update_database_value('mm_application', 'affordable_loan_amt', $affordable_loan_amt, 'i', 'application_nbr', $mm_application_nbr);
            mm_update_database_value('mm_application', 'noaa_cd', 0, 'i', 'application_nbr', $mm_application_nbr);

        } else {
            //Received an application status that we weren't expecting to log an error.
            mm_log_error("mm_submit_application", "Were not expecting app_status_cd: $app_status_cd");
        }

    }

    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    $return_array["mm_account_nbr"] = $mm_account_nbr;
    $return_array["mm_application_nbr"] = $mm_application_nbr;
    $return_array["mm_uw_decision_nbr"] = $mm_uw_decision_nbr;
    $return_array["uw_application_nbr"] = $uw_application_nbr;
    $return_array["noaa_cd"] = isset($noaa_cd) ? $noaa_cd : 0;
    $return_array["noaa_location"] = isset($noaa_location) ? $noaa_location : '';
    $return_array["approved_amt"] = isset($final_approved_amt) ? $final_approved_amt : 0;
    $return_array["uw_decision"] = isset($uw_decision) ? $uw_decision : '';
    $return_array["state_cd"] = isset($state_cd) ? $state_cd : '';

    //Debugging

    //Return function results
    return $return_array;

}

/**************************************************************************************************************************
 * Function mm_expire_applications($num_days=10) - Function will pull all applications that are currently in an "active" state with a create date more than $num_days ago and will set the application status on the to Expired.
 **************************************************************************************************************************/

function mm_expire_applications($num_days = 10)
{
    error_log("flow: mm_expire_applications\n");
    $return_array = array();
    $timestamp = new DateTime();
    $temp = $timestamp->format("Y-m-d H:i:s");
    $expiration_dt = $timestamp->modify("- $num_days days")->setTime(0, 0, 0);
    $exp_dt = $expiration_dt->format('Y-m-d H:i:s');
    $num_successfully_expired = 0;
    $num_errors = 0;

    //1.  Select all application where the create date is more than 10 days ago
    try {
        $conn = mm_get_pdo_connection();
        $sql_string = "select application_nbr, email_address, loan_agreement_nbr from mm_application where create_dt <= ? and application_status in (1,2,3,4,5,6,9) and application_nbr not in (1089,1090,1093,1094,1095,1096,1097,900020,901041,901042,901043)";
        //$sql_string = "select application_nbr, email_address from mm_application where create_dt <= ? and application_status in (2) and application_nbr not in (1089,1090,1093,1094,1095,1096,1097,900020,901041,901042,901043)";
        $bind_params = [$exp_dt];
        $stmt = $conn->prepare($sql_string);
        $stmt->execute($bind_params);
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $row_count = $stmt->rowCount();
        if ($row_count > 0) {
            //There are applications that need to be expired.
            foreach ($results as $row) {
                //NEED TO HANDLE ERRORS
                $application_nbr = $row["application_nbr"];
                $email_address = $row["email_address"];
                $loan_agreement_nbr = $row["loan_agreement_nbr"];
                mm_update_database_value("mm_application", 'application_status', 10, 'i', 'application_nbr', $application_nbr);
                //Generate a NOAA
                $noaa_results = mm_generate_noaa_pdf($application_nbr, 99);
                $noaa_return_value = $noaa_results["return_value"];
                if ($noaa_return_value != 0) {
                    $noaa_return_message = $noaa_results["return_message"];
                    $num_errors += 1;
                    mm_log_error('mm_expire_applications', "Failed Generating NOAA for application $application_nbr");
                } else {
                    //Schedule a NOAA to be sent
                    $email_data = array();
                    $noaa_location = $noaa_results["document_path"];
                    $email_data["to"] = $email_address;
                    $email_data["from"] = SUPPORT_EMAIL;
                    $email_data["from_name"] = SITE_NAME . " Support Team";
                    $email_data["subject"] = SITE_NAME . " Application Decision";
                    $email_data["txt_body"] = "We are writing to inform you of our decision regarding your recent application with " . SITE_NAME . ".  At this time, we are unable to grant your credit request.  Attached you will find the notice of adverse action which contains additional information as well as your rights for additional information regarding this decision.\n\nThanks for considering " . SITE_NAME . ",\n\n" . SITE_NAME . " Support Team\nEmail:" . SUPPORT_EMAIL . "\nPhone:(844) 678-5626\n";
                    $email_data["html_body"] = "<html><head></head><body><p>We are writing to inform you of our decision regarding your recent application with " . SITE_NAME . ".  At this time, we are unable to grant your credit request.  Attached you will find the notice of adverse action which contains additional information as well as your rights to request additional information regarding this decision.</p><br><p>Thanks for considering ".SITE_NAME.",</p><br><p>" . SITE_NAME . " Support Team</p><p>Email:" . SUPPORT_EMAIL . "</p><p>Phone:(844) 678-5626</p></body></html>";
                    $email_data["attachment"] = $noaa_location;
                    $tmp_dt = new DateTime();
                    $tmp_dt = $tmp_dt->modify("+1 Day");
                    $scheduled_dt = $tmp_dt->format("Y-m-d H:i:s");
                    $email_data["scheduled_dt"] = $scheduled_dt;
                    $email_message = json_encode($email_data);
                    $email_results = mm_schedule_email_message($email_message);
                    if ($email_results["return_value"] != 0 || $email_results["email_message_nbr"] == 0) {
                        //The email scheduling failed so log an error
                        mm_log_error('mm_submit_application', "Failed to schedule the NOAA email to be sent to $email_address");
                        $num_errors += 1;
                    } else {
                        //Add a customer note
                        $note_array = array();
                        $note_array["application_nbr"] = $application_nbr;
                        $note_array["category"] = "Application Decisioning";
                        $note_array["sub_category"] = "Application Expiration";
                        $note_array["username"] = "System";
                        $note_array["create_dt"] = $temp;
                        $note_array["txt_body"] = "Application has been expired.";
                        $note_json = json_encode($note_array);
                        $note_results = mm_add_application_note($note_json);
                        $note_return_value = $note_results["return_value"];
                        if ($note_return_value != 0) {
                            $note_return_message = $note_results["return_message"];
                            mm_log_error('mm_expire_applications', "Failed to add a note to application $application_nbr with error message $note_return_message");
                            $num_errors += 1;
                        } else {
                            //Attempt to delete the loan if it's on loan pro
                            if ($loan_agreement_nbr > 0) {
                                //There is a loan agreement in place so delete the loan from loan pro
                                $la_details = mm_get_loan_agreement_details($loan_agreement_nbr);
                                $display_id = $la_details["loan_nbr"];
                                //Get the loan id from loan pro from a display id
                                $lp_loan_details = mm_get_lp_loan_details($display_id);
                                $loan_id = $lp_loan_details["loan_id"];
                                $delete_results = mm_lp_delete_loan($loan_id);
                                $delete_return_value = $delete_results["return_value"];
                                $delete_return_message = $delete_results["return_message"];
                                if ($delete_return_value == 0) {
                                    $num_successfully_expired += 1;
                                } else {
                                    mm_log_error('mm_expire_applications', "Failed to delete the loan $display_id from loan pro.  The return message is: $delete_return_message");
                                }

                            }

                        }
                    }
                }
            }
        }

    } catch (PDOException $e) {
        $return_array["return_value"] = 1;
        $return_array["return_message"] = "$e";
        return $return_array;
    }

    $conn = null;

    $return_array["return_value"] = 0;
    $return_array["return_message"] = '';
    //Incorrectly assumes that all records were updated needs to handle errors from above
    $return_array["num_records_expired"] = $row_count;

    return $return_array;

}

/******************************************************************************************
 * Function mm_get_lp_customer_loans($customer_id)
 ******************************************************************************************/
function mm_get_lp_customer_loans($customer_id)
{
    error_log("flow: mm_get_lp_customer_loans\n");
    $loans_arr = array(array());
    $return_array = array();

    //Get all loans tied to a customer - iterate through them and grab the necessary details to be returned.
    try {
        $sdk = LPSDK::GetInstance();
        $loans = $sdk->GetLoansForCustomer($customer_id, [LOAN::LOAN_SETUP, LOAN::LOAN_SETTINGS, LOAN::STATUS_ARCHIVE]);
        $num_loans = 0;
        foreach ($loans as $loan) {
            $loan_setup = $loan->get(LOAN::LOAN_SETUP);
            $loan_status_archive = $loan->get(LOAN::STATUS_ARCHIVE);
            $current_archive = $loan_status_archive[0];
            $days_past_due = $current_archive->get("daysPastDue");
            $loan_settings = $loan->get(LOAN::LOAN_SETTINGS);
            $loan_status_id = $loan_settings->get(LOAN_SETTINGS::LOAN_STATUS_ID);
            $admin_stats = $loan->GetAdminStats();
            $current_balance = $admin_stats["active"]['principalBalance'];

            $next_payment = $loan->GetNextScheduledPayment();

            //Get Next Payment Amount
            $temp = $next_payment["scheduledPayment"]["date"];
            $epoch = filter_var($temp, FILTER_SANITIZE_NUMBER_INT);
            if ($epoch != "") {
                $next_payment_dt = DateTime::CreateFromFormat('U', $epoch)->format('m/d/Y');
            } else {
                $next_payment_dt = "";
            }

            //Get Next Payment Amount
            $next_payment_amt = $next_payment["scheduledPayment"]["chargeAmount"];

            $display_id = $loan->get(LOAN::DISP_ID);
            $loan_sub_status_id = $loan_settings->get(LOAN_SETTINGS::LOAN_SUB_STATUS_ID);
            //if ACTIVE Need to get days past due, next due dates, amount due next payment remaining principal balance
            $apr = $loan_setup->get(LOAN_SETUP::APR);
            //$principal_balance = $loan_status_archive->get(LOAN_STATUS_ARCHIVE::PRINCIPAL_BALANCE);
            //$days_past_due = $loan_status_archive->get(LOAN_STATUS_ARCHIVE::DAYS_PAST_DUE);

            $maturity_dt = DateTime::createFromFormat('U', $loan_setup->get(LOAN_SETUP::ORIG_FINAL_PAY_DATE))->format('m/d/Y');
            $contract_dt = DateTime::createFromFormat('U', $loan_setup->get(LOAN_SETUP::CONTRACT_DATE))->format('m/d/Y');
            $loans_arr[$num_loans]["apr"] = $apr;
            $loans_arr[$num_loans]["loan_sub_status_id"] = $loan_sub_status_id;
            $loans_arr[$num_loans]["loan_status_id"] = $loan_status_id;
            $loans_arr[$num_loans]["maturity_dt"] = $maturity_dt;
            $loans_arr[$num_loans]["display_id"] = $display_id;
            $loans_arr[$num_loans]["next_payment_dt"] = $next_payment_dt;
            $loans_arr[$num_loans]["next_payment_amt"] = $next_payment_amt;
            $loans_arr[$num_loans]["contract_dt"] = $contract_dt;
            $loans_arr[$num_loans]["principal_balance"] = $current_balance;
            $loans_arr[$num_loans]["days_past_due"] = $days_past_due;

            $num_loans += 1;

        }

    } catch (Exception $e) {
        $return_array["return_value"] = 1;
        $return_array["return_message"] = "Error: $e";
        return $return_array;
    }

    $return_array["return_value"] = 0;
    $return_array["num_loans"] = $num_loans;
    $return_array["return_message"] = "Success";
    $return_array["loans_arr"] = $loans_arr;
    return $return_array;
}

/**********************************************************************************************************************************************
 * Function mm_get_account_status ($email_address)
 **********************************************************************************************************************************************/
function mm_get_account_status($email_address)
{
    error_log("flow: mm_get_account_status\n");
    //Initialize any variables needed
    $return_array = array();
    $return_value = 0;
    $return_message = '';
    //$account_status = '';
    $account_status = '';
    //$account_nbr = 0;
    $application_status = 0;
    $active_loan_ind = 0;
    $active_app_ind = 0;
    $charge_off_loan_ind = 0;
    $maturity_dt = '';
    $contract_dt = '';
    $loan_nbr = '';
    $first_name = '';
    $last_name = '';
    $next_payment_dt = '';
    $next_payment_amt = '';
    $loan_status_string = '';
    $loan_substatus_string = '';
    $days_past_due = 0;
    $principal_balance = 0;
    $lp_error = 0;

    $account_details = mm_get_account_details_from_email_address($email_address);
    $account_nbr = $account_details["account_nbr"];
    if ($account_nbr == 0) {
        //$lp_customer_id = 0;
        $charge_off_loan_ind = 0;
        $active_loan_ind = 0;
        $active_app_ind = 0;
    } else {
        $lp_customer_id = $account_details["lp_customer_id"];
        $account_nbr = $account_details["account_nbr"];
        $first_name = ucwords(strtolower($account_details["first_name"]));
        $last_name = ucwords(strtolower($account_details["last_name"]));
        if (!is_null($lp_customer_id)) {
            //Look up in Loan Pro and get the Loan History for the customer.  Loop through looking at the loan statuses looking for Active or Closed loans
            $results = mm_get_lp_customer_loans($lp_customer_id);
            if ($results["return_value"] != 0) {
                //LOG AN ERROR AND ADD ERROR HANDLING
                $lp_error = 1;
            } else {
                //The function worked so loop through the loans and check the status of each one.
                $loans = $results["loans_arr"];
                foreach ($loans as $loan) {
                    $loan_status = $loan["loan_status_id"];
                    $loan_sub_status = $loan["loan_sub_status_id"];
                    if (($loan_status == 1 && $loan_sub_status == 2) || $loan_status == 2) {
                        //The customer has a loan that is Pending-Ready to Fund or they have a loan with an Active Status
                        $active_loan_ind = 1;
                        $maturity_dt = $loan["maturity_dt"];
                        $contract_dt = $loan["contract_dt"];
                        $loan_nbr = $loan["display_id"];
                        $next_payment_dt = $loan["next_payment_dt"];
                        $next_payment_amt = $loan["next_payment_amt"];
                        $days_past_due = $loan["days_past_due"];
                        $principal_balance = $loan["principal_balance"];

                        if ($loan_status == 1 && $loan_sub_status == 2) {
                            $loan_status_string = "PENDING FUND";
                        } else {
                            //The loan is active so provide a description based on the status
                            if ($loan_status == 2) {
                                $loan_status_string = "ACTIVE";
                            }
                        }
                    } else if ($loan_status == 4) {
                        $charge_off_loan_ind = 1;
                    }
                    //Need to add other conditions for closed

                    //Adding loan sub status
                    if ($loan_sub_status == 1) {
                        $loan_substatus_string = "Signature";
                    } else if ($loan_sub_status == 2) {
                        $loan_substatus_string = "Ready To Fund";

                    } else if ($loan_sub_status == 9) {
                        $loan_substatus_string = "Active";

                    } else if ($loan_sub_status == 10) {
                        $loan_substatus_string = "Rescind Requested";

                    } else if ($loan_sub_status == 11) {
                        $loan_substatus_string = "Bankruptcy";

                    } else if ($loan_sub_status == 12) {
                        $loan_substatus_string = "Debt Management";

                    } else if ($loan_sub_status == 13) {
                        $loan_substatus_string = "Deceased";

                    } else if ($loan_sub_status == 14) {
                        $loan_substatus_string = "Paid Off";

                    } else if ($loan_sub_status == 15) {
                        $loan_substatus_string = "Settled";

                    } else if ($loan_sub_status == 16) {
                        $loan_substatus_string = "Rescinded";

                    } else if ($loan_sub_status == 17) {
                        $loan_substatus_string = "Funding Returned";

                    } else if ($loan_sub_status == 20) {
                        $loan_substatus_string = "Bankruptcy";

                    } else if ($loan_sub_status == 21) {
                        $loan_substatus_string = "Debt Management";

                    } else if ($loan_sub_status == 22) {
                        $loan_substatus_string = "Deceased";

                    } else if ($loan_sub_status == 23) {
                        $loan_substatus_string = "Charged Off";

                    } else if ($loan_sub_status == 24) {
                        $loan_substatus_string = "Sold";
                    } else if ($loan_sub_status == 31) {
                        $loan_substatus_string = "Bad Bank";
                    } else {
                        //Handle an unexpected substatus with an error
                    }

                }

            }
        }
        $application_sub_status = 0;
        //If active loan and active indicator are both still zero then the customer doesn't have an active or charged off loan.  Check to see if the customer has an active application
        if ($charge_off_loan_ind == 0 && $active_loan_ind == 0) {
            $result = mm_get_most_recent_application_details($account_nbr);
            $num_apps = $result["num_apps"];
            $app_data = $result["app_data"];
            if ($num_apps == 0) {
                //The customer has no applications so no need to do anything
            } else {
                $application_status = $app_data["application_status"];
                $application_sub_status = $app_data["application_sub_status"];
            }
            if ($application_status == 1 || $application_status == 2 || $application_status == 3 || $application_status == 4 || $application_status == 5 || $application_status == 6 || $application_status == 9) {
                $active_app_ind = 1;
            } else if ($application_status == 7 && $application_sub_status == 1) {
                //The customer has completed the application process and has a loan that is pending loan pro setup
                $active_loan_ind = 1;
                $loan_status_string = "PENDING FUND";
                $loan_substatus_string = "Ready To Fund";
            }
        }
    }
    //Set the account_status and application_status fields
    if ($lp_error == 1) {
        $account_status = "ERROR";
        $return_message = "There was an error with Getting Information from Loan Pro";
        $return_value = 1;
    } else if ($charge_off_loan_ind == 1) {
        $account_status = "CHARGEDOFF";
    } else if ($active_loan_ind == 1) {
        //NEED to populate more fields given the loan is active
        $account_status = "ACTIVELOAN";
    } else if ($active_app_ind == 1) {
        $account_status = "ACTIVEAPP";
    } else {
        $account_status = "IDLE";
    }

    $return_array["account_status"] = $account_status;
    $return_array["account_nbr"] = $account_nbr;
    $return_array["first_name"] = $first_name;
    $return_array["last_name"] = $last_name;
    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    $return_array["application_status"] = $application_status;
    $return_array["loan_status"] = $loan_status_string;
    $return_array["loan_substatus"] = $loan_substatus_string;
    $return_array["maturity_dt"] = $maturity_dt;
    $return_array["contract_dt"] = $contract_dt;
    $return_array["loan_nbr"] = $loan_nbr;
    $return_array["days_past_due"] = $days_past_due;
    $return_array["principal_balance"] = $principal_balance;
    $return_array["next_payment_dt"] = $next_payment_dt;
    $return_array["next_payment_amt"] = $next_payment_amt;
    return $return_array;

}

/**********************************************************************************************************
 * Function mm_send_ftp_document($source_file, $target_file, $target_path, $transmission_type)
 **********************************************************************************************************/
function mm_send_ftp_document($source_file, $target_file, $target_path = '', $transmission_type = 'A')
{
    error_log("flow: mm_send_ftp_document\n");
    //error_log("mark: starting mm_send_ftp_document:  $source_file,$target_file,$target_path\n");
    //Need to add error handling
    $return_array = array();
    $return_value = 0;
    $return_message = '';
    //$ftp_conn_details = array();
    $ftp_conn_details = mm_get_ftp_details();
    $ftp_server = $ftp_conn_details["server"];
    $ftp_username = $ftp_conn_details["username"];
    $ftp_password = $ftp_conn_details["password"];

    if ($transmission_type == "A") {
        $ftp_mode = FTP_ASCII;
    } else {
        $ftp_mode = FTP_BINARY;
    }

    // set up basic connection
    $conn_id = ftp_connect($ftp_server);

    // login with username and password
    $login_result = ftp_login($conn_id, $ftp_username, $ftp_password);

    //Try and cd to the directory.  If not successful then may need to create a the directory

    if ($target_path != '') {
        $directories = explode('/', $target_path);
        //$nav_path = '';
        $ftp_path = "ftp://$ftp_username:$ftp_password@$ftp_server/";
        foreach ($directories as $directory) {
            if ($directory != '') {
                $ftp_path = $ftp_path . "$directory/";
                $tmp = !file_exists($ftp_path);
                //error_log("mark: checking file_exists:  $ftp_path, not file exists: $tmp\n");
                if (!file_exists($ftp_path)) {
                    //Need to add error handling
                    //echo "Path Didn't Exist<br>";
                    //error_log("mark: before ftp_mkdir:  $conn_id,$directory\n");
                    $tmp = ftp_mkdir($conn_id, $directory);
                    //error_log("mark: after ftp_mkdir:  $tmp\n");
                } else {
                    //The directory already exists so no need to create it
                }
                ftp_chdir($conn_id, $directory);
            }
        }
    }

    // upload a file
    if (ftp_put($conn_id, $target_file, $source_file, $ftp_mode)) {
        $return_value = 0;
        $return_message = '';
    } else {
        $return_message = "There was a problem while uploading $source_file\n";
        $return_value = 1;
    }
    // close the connection
    ftp_close($conn_id);

    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;

    return $return_array;

}

/************************************************************************************************
 * Function mm_get_ftp_document($source_file, $target_file, $transmission_type)
 ******************************************************************************    ******************/
function mm_get_ftp_document($source_file, $target_file, $transmission_type)
{
    error_log("flow: mm_get_ftp_document\n");
    //Need to add error handling
    $return_array = array();
    $return_value = 0;
    $return_message = '';
    //$ftp_conn_details = array();
    $ftp_conn_details = mm_get_ftp_details();
    $ftp_server = $ftp_conn_details["server"];
    $ftp_username = $ftp_conn_details["username"];
    $ftp_password = $ftp_conn_details["password"];

    if ($transmission_type == "A") {
        $ftp_mode = FTP_ASCII;
    } else {
        $ftp_mode = FTP_BINARY;
    }

    // set up basic connection
    //error_log("mark: attempting connection to: $ftp_server\n");
    $conn_id = ftp_connect($ftp_server);
    if ($conn_id) {
        //error_log("mark: ftp connection successful.\n");
        // login with username and password
        if (ftp_login($conn_id, $ftp_username, $ftp_password)) {
            //error_log("mark: ftp login successful.\n");
            //error_log("mark: attempting ftp_get with localfile: $target_file, remotefile: $source_file.\n");
            if (ftp_get($conn_id, $target_file, $source_file, $ftp_mode)) {
                $return_value = 0;
                $return_message = '';
            } else {
                $return_message = "There was a problem while downloading $source_file\n";
                $return_value = 1;
            }
        } else {
            // todo: change t0 mm_log_error
            // error_log("mark: ftp login NOT successful.\n");
        }

        //Try and cd to the directory.  If not successful then may need to create a the directory

        //$ftp_path = "ftp://$ftp_username:$ftp_password@$ftp_server/$source_file";
        //if(!is_file($ftp_path)){
        //Need to add error handling
        //$return_value = 1;
        //$return_message = "The file $source_file doesn't exist";
        //}else{
        // if (ftp_get($conn_id, $target_file, $source_file, $ftp_mode)) {
        //     $return_value = 0;
        //     $return_message = '';
        // } else {
        //     $return_message = "There was a problem while downloading $source_file\n";
        //     $return_value = 1;
        // }
    } else {
        // todo: change t0 mm_log_error
        // error_log("mark: ftp connection failed.\n");
    }

    //}
    // close the connection
    ftp_close($conn_id);

    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;

    return $return_array;

}

/*******************************************************
 * Function RandomToken - Get a Random string for using as part of a file name
 *******************************************************/
function RandomToken($length = 32)
{
    error_log("flow: RandomToken\n");
    if (!isset($length) || intval($length) <= 8) {
        $length = 32;
    }
    if (function_exists('random_bytes')) {
        return bin2hex(random_bytes($length));
    }
    if (function_exists('mcrypt_create_iv')) {
        return bin2hex(mcrypt_create_iv($length, MCRYPT_DEV_URANDOM));
    }
    if (function_exists('openssl_random_pseudo_bytes')) {
        return bin2hex(openssl_random_pseudo_bytes($length));
    }
}

/*********************************************************************************
 * Function mm_send_email($to, $from, $subject, $txt_body, $html_body, $attachment_arr='')
 *********************************************************************************/
function mm_send_email($to, $from, $subject, $txt_body, $html_body = '', $attachment_arr = '', $log_email_ind = 0)
{
    error_log("flow: mm_send_email\n");
    $return_array = array();
    $return_value = 0;
    $return_message = '';

    ini_set('SMTP', SITE_URI);
    ini_set('smtp_port', 25);
    ini_set("include_path", '/home/mmdtcusr/php:' . ini_get("include_path"));
    require_once 'Mail.php';
    require_once 'Mail/mime.php';
    $message = new Mail_mime();
    $message->setTXTBody($txt_body);
    $message->setHTMLBody($html_body);
    if ($attachment_arr != '') {
        foreach ($attachment_arr as $attachment) {
            $message->addAttachment($attachment);
        }
    }

    $recipients = $to;
    $headers['From'] = $from;
    $headers['To'] = $to;
    $headers['Subject'] = $subject;

    $smtp = Mail::factory('smtp');

    $mail = $smtp->send($recipients, $message->headers($headers), $message->get());

    if (PEAR::isError($mail)) {
        $error_message = $mail->getMessage();
        mm_log_error('mm_send_email', "Failed Sending Email to $recipients with message $error_message");
        $return_value = 1;
        $return_message = "$error_message";
    } else {
        $return_value = 0;
        $return_message = "Email Sent Successfully";
    }

    //Potentially look at logging the email in the database at a later time.
    //$mail_log_arr = mm_log_email_message($to, $from, $subject,$txt_body, $html_body, $return_value);

    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;

    return $return_value;

}

/**************************************************************************************
 * FUNCTION mm_pci_authenticate() - Get the authorization and secret values from PCI wallet
 **************************************************************************************/

function mm_pci_authenticate()
{
    error_log("flow: mm_pci_authenticate\n");
    $return_array = array();
    $pci_credentials = mm_get_pci_credentials();
    $username = $pci_credentials["username"];
    $password = $pci_credentials["password"];

    //Prepare an array for building the json request
    $data_array = array();
    $data_array["username"] = $username;
    $data_array["password"] = $password;

    $json_message = json_encode($data_array);
    $url = "https://easypay.simnang.com/api/authenticate";
    $header_array = array("Content-Type: application/json", 'Accept: application/json');
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $header_array);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $json_message);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $json_response = curl_exec($curl);

    $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

    if ($status != 201 && $status != 200) {
        //There was an error in the message sending  Add error handling
        $return_value = 1;
        $return_message = "Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl);
        $token = '';
        $secret = '';
    } else {
        $response_array = json_decode($json_response, true);
        //$tokens = sizeof($response_array);

        $token = $response_array["token"];
        $secret = $response_array["secret"];
        $return_value = 0;
        $return_message = '';
    }

    //Set return array values
    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    $return_array["token"] = $token;
    $return_array["secret"] = $secret;
    return $return_array;
}

/**********************************************************************
 * Function mm_get_lp_pci_obo_token($secret, $authorization) - Get an OBO token for adding account information
 ************************************************************************/
function mm_get_lp_pci_obo_token($secret, $authorization)
{
    error_log("flow: mm_pci_authenticate\n");
    $return_array = array();
    $return_value = 0;
    $return_message = '';
    $obo_token = '';
    //$data_array = array();

    $url = "https://easypay.simnang.com/api/users/obo-token";
    $header_array = array("Content-Type: application/json", 'Accept: application/json', "authorization: $authorization", "secret: $secret");
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $header_array);
    curl_setopt($curl, CURLOPT_POST, true);
    //curl_setopt($curl, CURLOPT_POSTFIELDS, $json_message);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $json_response = curl_exec($curl);

    $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

    if ($status != 201 && $status != 200) {
        //There was an error in the message sending  Add error handling
        $return_value = 1;
        $return_message = "Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl);
        $obo_token = '';
    } else {
        $response_array = json_decode($json_response, true);
        $obo_token = $response_array["token"];
        $return_value = 0;
        $return_message = '';
    }

    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    $return_array["obo_token"] = $obo_token;
    return $return_array;
}

/*******************************************************************************************
 * Function mm_create_lp_pci_customer()
 *******************************************************************************************/
function mm_create_lp_pci_customer($customer_data, $authorization, $secret)
{
    error_log("flow: mm_create_lp_pci_customer\n");
    $return_array = array();
    $data_array = array();
    $customer_array = array();
    $primary_address_array = array();
    //$mail_address_array = array();

    $customer_array["birthdate"] = $customer_data["dob"];
    $customer_array["email"] = $customer_data["email_address"];
    $customer_array["first_name"] = $customer_data["first_name"];
    //$customer_array["generation_code"] = '';
    $customer_array["ssn"] = $customer_data["ssn"];
    //$customer_array["middle_name"] = '';
    //$customer_array["company_name"] = '';
    $customer_array["contact_name"] = $customer_data["first_name"] . " " . $customer_data["last_name"];
    $customer_array["last_name"] = $customer_data["last_name"];

    $primary_address_array["address1"] = $customer_data["street_address"];
    $primary_address_array["address2"] = $customer_data["appt_suite"];
    $primary_address_array["city"] = $customer_data["city"];
    $primary_address_array["state"] = $customer_data["state"];
    $primary_address_array["zipcode"] = $customer_data["zip_code"];
    $primary_address_array["country"] = "USA";

    $mailing_address_array["address1"] = $customer_data["street_address"];
    $mailing_address_array["address2"] = $customer_data["appt_suite"];
    $mailing_address_array["city"] = $customer_data["city"];
    $mailing_address_array["state"] = $customer_data["state"];
    $mailing_address_array["zipcode"] = $customer_data["zip_code"];
    $mailing_address_array["country"] = "USA";

    $customer_array["primary-address"] = $primary_address_array;
    $customer_array["mail-address"] = $mailing_address_array;

    $data_array["customer"] = $customer_array;

    $json_message = json_encode($data_array);

    $url = "https://easypay.simnang.com/api/customers";
    $header_array = array("Content-Type: application/json", 'Accept: application/json', "authorization: $authorization", "secret: $secret");
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $header_array);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $json_message);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $json_response = curl_exec($curl);

    $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

    if ($status != 201 && $status != 200) {
        //There was an error in the message sending  Add error handling
        $return_value = 1;
        $return_message = "Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl);
        $customer_id = '';
    } else {
        $response_array = json_decode($json_response, true);
        $customer_id = $response_array["id"];
        //$response_array = json_decode($json_response, true);
        $return_value = 0;
        $return_message = '';
    }

    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    $return_array["customer_id"] = $customer_id;

    return $return_array;

}

/***********************************************************************************************************
 * Function mm_add_pci_payment_profile($lp_customer_nbr, $pci_wallet_token)
 **********************************************************************************************************
 * function mm_add_pci_payment_profile($lp_customer_nbr, $pci_wallet_token){
 * $return_array;
 *
 * //$type = \Simnang\LoanPro\Constants\PAYMENT_ACCOUNT\PAYMENT_ACCOUNT_TYPE__C::CHECKING;
 * $type = "paymentAccount.type.checking";
 * $savings_acct = false;
 *
 * $data_array = array();
 * $payment_account_array = array();
 * $results = array((array()));
 * $checking_account = array();
 * $checking_account["token"] = $pci_wallet_token;
 * $results[0]["CheckingAccount"] = $checking_account;
 * $results[0]["isPrimary"] = 1;
 * $results[0]["isSecondary"] = 0;
 * $results[0]["title"] = "Checking Account";
 * $results[0]["type"] = $type;
 * $results[0]["active"] = 1;
 * $payment_account_array["results"] = $results;
 *
 * $data_array["__id"] = $lp_customer_nbr;
 * $data_array["__update"] = true;
 * $data_array["PaymentAccounts"] = $payment_account_array;
 *
 * $json_message = json_encode($data_array);
 *
 * $account_token = "Bearer f1c9d078d3828e7fbffcc8944cad2f00b93f109b";
 * $auto_pal_id = "5200206";
 *
 * $url = "https://loanpro.simnang.com/api/public/api/1/odata.svc/Customers(id=$lp_customer_nbr)";        $header_array = array("Content-Type: application/json", 'Accept: application/json', "Authorization: $account_token", "Autopal-Instance-ID: $auto_pal_id");
 * $curl = curl_init($url);
 * curl_setopt($curl, CURLOPT_HTTPHEADER,$header_array );
 * curl_setopt($curl, CURLOPT_POST, true);
 * curl_setopt($curl, CURLOPT_POSTFIELDS, $json_message);
 * curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
 *
 * $json_response = curl_exec($curl);
 *
 * $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
 *
 * if ( $status != 201 && $status != 200 ) {
 * //There was an error in the message sending  Add error handling
 * $return_value = 1;
 * $return_message = "Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl) . " $json_message";
 * $customer_id = '';
 * }else{
 * $response_array = json_decode($json_response, true);
 * $customer_id = $response_array["id"];
 * $return_value = 0;
 * $return_message = '';
 * }
 *
 * $return_array["return_value"] = $return_value;
 * $return_array["return_message"] = $return_message;
 * $return_array["customer_id"] = $customer_id;
 *
 * return $return_array;
 *
 * }*/

/**********************************************************************************************
 * Function mm_add_pci_payment_profile($lp_customer_nbr, $pci_wallet_token) - Add the payment profile to the loan pro customer
 **********************************************************************************************/
function mm_add_pci_payment_profile($lp_customer_nbr, $pci_wallet_token)
{
    error_log("flow: mm_add_pci_payment_profile\n");
    $return_array = array();

    //$type = \Simnang\LoanPro\Constants\PAYMENT_ACCOUNT\PAYMENT_ACCOUNT_TYPE__C::CHECKING;
    $type = "paymentAccount.type.checking";
    $savings_acct = false;
    $sdk = LPSDK::GetInstance();
    $pmt_profile = $sdk->CreateCustomerPaymentAccount("Checking", $type, $pci_wallet_token, $savings_acct);

    try {
        //grabs the customer and sets the payment account and then saves
        $customer = LPSDK::GetInstance()->GetCustomer($lp_customer_nbr)->Set(\Simnang\LoanPro\Constants\CUSTOMERS::PAYMENT_ACCOUNTS, $pmt_profile);
        $customer->Save();
        $return_value = 0;
        $return_message = '';
    } catch (\Simnang\LoanPro\Exceptions\ApiException $e) {
        $return_value = 1;
        $return_message = "There was an error adding the payment profile for loan pro customer $lp_customer_nbr.  The error message is $e.  The token trying to be added is $pci_wallet_token.";
        $return_array["return_value"] = $return_value;
        $return_array["return_message"] = $return_message;

        return $return_array;
    }

    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;

    return $return_array;

}

/***************************************************************************************
 * Function mm_create_lp_pci_customer_acct_link()
 ***************************************************************************************/
function mm_create_lp_pci_customer_acct_link($pci_customer_nbr, $acct_token, $authorization, $secret)
{
    error_log("flow: mm_create_lp_pci_customer_acct_link\n");
    $return_array = array();

    $data_array = array();
    $checking_account = array();
    $checking_account["token"] = $acct_token;
    $data_array["checking-account"] = $checking_account;
    $json_message = json_encode($data_array);

    $url = "https://easypay.simnang.com/api/customers/{$pci_customer_nbr}/add-checking-account";
    $header_array = array("Content-Type: application/json", 'Accept: application/json', "authorization: $authorization", "secret: $secret");
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $header_array);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_setopt($curl, CURLOPT_POSTFIELDS, $json_message);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $json_response = curl_exec($curl);

    $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

    if ($status != 201 && $status != 200) {
        //There was an error in the message sending  Add error handling
        $return_value = 1;
        $result = false;
        $return_message = "Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl);
        $customer_id = '';
    } else {
        $response_array = json_decode($json_response, true);
        $result = $response_array["result"];
        $response_array = json_decode($json_response, true);
        $return_value = 0;
        $return_message = '';
    }

    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    $return_array["result"] = $result;

    return $return_array;

}

/************************************************************************************
 * Function mm_create_lp_pci_acct()
 ************************************************************************************/
function mm_create_lp_pci_acct($app_data, $obo_token)
{
    error_log("flow: mm_create_lp_pci_acct\n");
    $return_array = array();
    $data_array = array();
    $bank_account = array();

    $bank_account["address"] = $app_data["street_address"];
    $bank_account["zipcode"] = $app_data["zip_code"];
    $bank_account["state"] = $app_data["state"];
    $bank_account["city"] = $app_data["city"];
    $bank_account["country"] = 'USA';
    $bank_account["bank_name"] = 'Default Bank';
    $bank_account["accountholder_name"] = $app_data["first_name"] . " " . $app_data["last_name"];
    $bank_account["routing_number"] = $app_data["routing_nbr"];
    $bank_account["account_number"] = $app_data["bank_acct_nbr"];
    // mark was here:  fixed for bank account type that was entered on the front-end
    //$bank_account["account_type"] = "checking";
    $bank_account["account_type"] = $app_data["bank_acct_type"];
    $data_array["checking-account"] = $bank_account;

    $json_message = json_encode($data_array);

    $url = "https://easypay.simnang.com/api/checking-account";
    $header_array = array("Content-Type: application/json", 'Accept: application/json', "authorization: $obo_token");
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $header_array);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $json_message);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $json_response = curl_exec($curl);

    $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

    if ($status != 201 && $status != 200) {
        //There was an error in the message sending  Add error handling
        $return_value = 1;
        $return_message = "Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl);
        $acct_token = '';
    } else {
        $response_array = json_decode($json_response, true);
        $acct_token = $response_array["token"];
        $response_array = json_decode($json_response, true);
        $return_value = 0;
        $return_message = '';
    }

    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    $return_array["acct_token"] = $acct_token;

    return $return_array;

}

/********************************************************************************************
 * Function mm_create_lp_payment_profile($lp_loan_number) - Function take a loan pro loan ID, pulls the necessary information from the MM database, creates(future updates) a payment profile and links it to the LP customer
 ********************************************************************************************/
function mm_create_lp_payment_profile($lp_loan_nbr)
{
    error_log("flow: mm_create_lp_payment_profile\n");
    $return_array = array();
    $return_message = "";
    $application_nbr = substr($lp_loan_nbr, 1) * 1;
    $app_array = mm_get_application_details($application_nbr);
    $return_value = $app_array["return_value"];
    if ($return_value != 0) {
        $return_value = 1;
        $return_message = "Unable to get the application details related to $lp_loan_nbr";
        $return_array["return_value"] = $return_value;
        $return_array["return_message"] = $return_message;
        return $return_array;
    }
    $app_data = $app_array["app_data"];
    $account_nbr = $app_data["account_nbr"];
    $account_array = mm_get_account_details($account_nbr);
    $return_value = $account_array["return_value"];
    if ($return_value != 0) {
        $return_value = 1;
        $return_message = "Unable to get the account details related to $lp_loan_nbr";
        $return_array["return_value"] = $return_value;
        $return_array["return_message"] = $return_message;
        return $return_array;
    }

    $lp_customer_id = $account_array["lp_customer_id"];

    //$temp_array = array();
    $temp_array = mm_pci_authenticate();
    $return_value = $temp_array["return_value"];
    if ($return_value != 0) {
        //Insert Error Handling
        $return_value = 1;
        $temp_message = $temp_array["return_message"];
        $return_message = "Failed getting OBO Token with error: $temp_message";
    } else {
        //Authenticate worked successfully so get an OBO token for account setup
        $secret = $temp_array["secret"];
        $token = $temp_array["token"];
        $temp_array = mm_get_lp_pci_obo_token($secret, $token);
        $return_value = $temp_array["return_value"];
        if ($return_value != 0) {
            //Failed Getting OBO Token so set return value and message
            $return_value = 1;
            $temp_message = $temp_array["return_message"];
            $return_message = "Failed getting OBO Token with error: $temp_message";
        } else {
            $obo_token = $temp_array["obo_token"];
            $temp_array = mm_create_lp_pci_acct($app_data, $obo_token);
            $return_value = $temp_array["return_value"];
            if ($return_value != 0) {
                //Failed creating the PCI account so return the message
                $return_value = 1;
                $temp_message = $temp_array["return_message"];
                $return_message = "Failed Creating PCI Account with error: $temp_message";
            } else {
                $acct_token = $temp_array["acct_token"];
                $temp_array = mm_create_lp_pci_customer($app_data, $token, $secret);
                $return_value = $temp_array["return_value"];
                if ($return_value != 0) {
                    //Failed Adding the customer to PCI so set the return message appropriately
                    $return_value = 1;
                    $temp_message = $temp_array["return_message"];
                    $return_message = "Failed Creating PCI Customer with error: $temp_message";
                } else {
                    $customer_id = $temp_array["customer_id"];
                    $temp_array = mm_create_lp_pci_customer_acct_link($customer_id, $acct_token, $token, $secret);
                    $return_value = $temp_array["return_value"];
                    if ($return_value != 0) {
                        //There was an error linking the acct and customer in PCI Wallet
                        $return_value = 1;
                        $temp_message = $temp_array["return_message"];
                        $return_message = "Failed Linking PCI Customer to PCI Account with error: $temp_message";
                    } else {
                        $result = $temp_array["result"];
                        if ($result != true) {
                            //The link didn't return an error but was false so log a message
                        } else {
                            $temp_array = mm_add_pci_payment_profile($lp_customer_id, $acct_token);
                            $return_value = $temp_array["return_value"];
                            if ($return_value != 0) {
                                //There was an error adding the payment profile so return an error
                                $return_value = 1;
                                $temp_message = $temp_array["return_message"];
                                $return_message = "Failed adding Payment Profile to Loan Pro Customer $lp_customer_id: $temp_message";
                            } else {
                                //The payment profile was added successfully
                                $return_value = 0;
                                $return_message = '';
                            }
                        }
                    }
                }
            }
        }
    }

    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;

    return $return_array;
}


/**
 ** Pulls Application Details From DB By Status
 **
 **    9999 Is A Place Holder For Organic Since There Is No Organic Status.
 **
 ** @param int $app_status
 ** @param string $state
 **
 ** @return array
**/
function mm_get_application_details_by_status($status = 0, $state = null)
{
    error_log('flow: ' . __FUNCTION__ . '  $status: ' . $status . '  $state: ' . $state . PHP_EOL);
    //    Default Return Data
    $return = array("return_value" => 0, "return_message" => "There were no applications associated with this account",);
    
    //    Sanitize Integer Value
    $status = (is_numeric($status) ? 1 * $status : 0);
    $data   = array();
    $dt     = date('Y-m-d', time() - (86400 * 30));    //    Last 30 Days
    $state_whr  = (null == $state ? '' :  '    AND app.state = ? ' . PHP_EOL);
    
    $sql = 'SELECT
    app.*,
    uwd.segment AS decision_segment
FROM
    mm_application AS app
LEFT OUTER JOIN
    mm_uw_decision AS uwd ON uwd.decision_nbr =
    ( SELECT
            uw.decision_nbr
        FROM
            mm_uw_decision AS uw
        WHERE
            ( uw.application_nbr = app.application_nbr )
        ORDER BY
            uw.application_nbr, uw.decision_nbr DESC
        LIMIT 1
    )
WHERE' . PHP_EOL;
    
    if (0 == $status)
    {
        $sql .= '        app.create_dt >= "' . $dt . '"' . PHP_EOL . $state_whr;
    }
    elseif (9999 == $status)
    {
        $sql .= '        app.application_status = 5
    AND app.create_dt >= "' . $dt .'" 
    AND UPPER(uwd.segment) LIKE "%ORGANIC%"' . PHP_EOL . $state_whr;
    }
    else
    {
        if (5 == $status)
        {
            $sql .= '        app.application_status = 5
    AND UPPER(uwd.segment) NOT LIKE "%ORGANIC%"
    AND app.create_dt >= "' . $dt .'"' . PHP_EOL . $state_whr;
        }
        else
        {
            $sql .= '        app.application_status = ' . $status . '
    AND app.create_dt >= "' . $dt .'"' . PHP_EOL . $state_whr;
        }
    }
    
    $sql .= 'ORDER BY
    app.create_dt DESC;';
    
    //echo '<!--  $status:  ' . $status . '  $sql: ' . PHP_EOL . $sql . PHP_EOL . '  -->' . PHP_EOL;

    $conn = mm_get_db_connection();
    $stmt = $conn->prepare($sql);

    if (($app_status != 0) && ($app_status != 9999))
    {
        if (null == $state)
        {
            $stmt->bind_param('i', $app_status);
        }
        else
        {
            $stmt->bind_param('is', $app_status, $state);
        }
    }

    $stmt->execute();

    //The application id didn't exist so default all of the return values
    $result         = $stmt->get_result();
    $num_rows       = $result->num_rows;

    if ($num_rows > 0)
    {
        while ($row = $result->fetch_assoc())
        {
            $app_data[] = $row;
        }

        $return["return_value"]   = 0;
        $return["return_message"] = '';
    }

    if (is_resource($conn))
    {
        $conn->close();
    }

    $return["app_data"] = $app_data;
    $return["num_apps"] = $num_rows;

    return $return;
}


/********************************************************************************************************************
 * Function mm_get_affordable_loan_amt($state_cd, $pay_freq, $monthly_income_amt)
 ********************************************************************************************************************/
function mm_get_affordable_loan_amt($state_cd, $pay_freq, $monthly_income_amt, float $apr = 132.7300)
{
    error_log("flow: mm_get_affordable_loan_amt\n");
    $return_array = array();
    $return_value = 0;
    $return_message = '';
    $affordable_loan_amt = '';
    //$sql_string = "select max(loan_amt) as affordable_loan_amt from mm_pricing_detail where state = '$state_cd' and payment_freq = '$pay_freq' and monthly_income_amt <= $monthly_income_amt";
    $sql_string = "select max(loan_amt) as affordable_loan_amt
                    from mm_pricing_detail
                    where state = :state_cd and payment_freq = :pay_freq and monthly_income_amt <= :monthly_income_amt and apr = :apr";
    try {
        $conn = mm_get_pdo_connection();
        //$results = $conn->query($sql_string);
        //$rows = $results->fetchAll();

        $stmt = $conn->prepare($sql_string);
        $stmt->execute([':state_cd' => $state_cd,
            ':pay_freq' => $pay_freq,
            ':monthly_income_amt' => $monthly_income_amt,
            ':apr' => $apr]);
        $rows = $stmt->fetchAll();

        $n = count($rows);
        $num_rows = count($rows);
        if ($num_rows == 0) {
            $affordable_loan_amt = 0;
            $return_value = 0;
            $return_message = "Succesful Query but insufficient income.";
        } else {
            //There should only be one result
            foreach ($rows as $row) {
                //If the return value is no then there isn't sufficient income for any product
                if ($row["affordable_loan_amt"] == '') {
                    $affordable_loan_amt = 0;
                } else {
                    $affordable_loan_amt = $row["affordable_loan_amt"];
                }
            }
            $return_value = 0;
            $return_message = "Success";
        }

    } catch (PDOException $e) {
        $return_array["return_value"] = 1;
        $return_array["return_message"] = "$e";
        $return_array["sql_string"] = $sql_string;
        mm_log_error('mm_affordable_loan_amt', "$e");
        return $return_array;
    }

    if (is_resource($conn)) {
        $conn = null;
    }

    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    $return_array["affordable_loan_amt"] = $affordable_loan_amt;

    return $return_array;

}

/******************************************************************************************************
 * Function mm_get_approval_amt_display_values($state_cd, $approved_amt)
 ******************************************************************************************************/
function mm_get_approval_amt_display_values($state_cd, $approved_amt)
{
    error_log("flow: mm_get_approval_amt_display_values\n");
    $return_array = array();
    $return_value = 0;
    $return_message = '';
    $display_values = array();
    $i = 0;
    $sql_string = "select display_amt from mm_approved_amt_display where state = '$state_cd' and approved_amt = $approved_amt order by display_order asc";
    try {
        $conn = mm_get_pdo_connection();
        $results = $conn->query($sql_string);
        $rows = $results->fetchAll();
        $n = count($rows);
        $num_rows = count($rows);
        if ($num_rows == 0) {
            $return_value = 1;
            $return_message = "No matching records for state $state_cd and approved amount $approved_amt.";
        } else {
            //There should only be one result
            foreach ($rows as $row) {
                //If the return value is no then there isn't sufficient income for any product
                if ($row["display_amt"] == '') {
                    $return_value = 1;
                    $return_message = "There are no matching records for state $state_cd and approved amount $approved_amt.";
                } else {
                    $display_value = $row["display_amt"];
                    $display_values[$i] = money_format('%0n', $display_value);
                    $i = $i + 1;
                }
            }
            $return_value = 0;
            $return_message = "Success";
        }

    } catch (PDOException $e) {
        $return_array["return_value"] = 1;
        $return_array["return_message"] = "$e";
        $return_array["sql_string"] = $sql_string;
        mm_log_error('mm_get_approval_amt_display_values', "$e");
        return $return_array;
    }

    if (is_resource($conn)) {
        $conn = null;
    }

    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    $return_array["display_values"] = $display_values;

    return $return_array;
}

/**************************************************************************************
 * Function mm_get_last_approved_amt($account_nbr)
 **************************************************************************************/
function mm_get_last_approved_amt($account_nbr)
{
    error_log("flow: mm_get_last_approved_amt\n");
    $return_value = 0;
    $return_message = '';
    $last_approved_amt = 0;

    $sql_string = "select credit_approved_amt from mm_application as a inner join mm_account as b on a.account_nbr = b.account_nbr where a.account_nbr = $account_nbr and a.application_status = 7 order by application_nbr desc";

    try {
        $conn = mm_get_pdo_connection();
        $results = $conn->query($sql_string);
        $row = $results->fetch(PDO::FETCH_ASSOC);
        //If the return value is no then there isn't sufficient income for any product
        if ($row["credit_approved_amt"] == '') {
            $return_message = "No matching records for account_nbr $account_nbr";
        } else {
            $last_approved_amt = $row["credit_approved_amt"];
        }
        $return_message = "Success";
    } catch (PDOException $e) {
        $return_array["return_value"] = 1;
        $return_array["return_message"] = "$e";
        $return_array["sql_string"] = $sql_string;
        mm_log_error('mm_get_last_approved_amt', "$e");
        return $return_array;
    }

    if (is_resource($conn)) {
        $conn = null;
    }

    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    $return_array["last_approved_amt"] = $last_approved_amt;

    return $return_array;
}

/*****************************************************************************
 * Function mm_ach_process_returns()
 *****************************************************************************/
function mm_ach_process_returns($run_dt = '')
{
    error_log("flow: mm_ach_process_returns\n");
    $return_array = array();
    $return_value = 0;
    $return_message = '';
    $ach_server_details = mm_get_ach_server_details();
    $ftp_server = $ach_server_details["server"];
    $ftp_username = $ach_server_details["username"];
    $ftp_password = $ach_server_details["password"];
    $ftp_port = $ach_server_details["port"];
    $log_path = mm_get_log_path();
    if ($run_dt == '') {
        $temp_dt = new DateTime();
        $run_dt = $temp_dt->format('Ymd');
    }
    //Using FTP download the return files to a temporary location
    if (!$sftp = new Net_SFTP($ftp_server, $ftp_port)) {
        $return_value = 1;
        $return_message = "Unable to connect to $ftp_server on port $ftp_port";
    } else if (!$sftp->login($ftp_username, $ftp_password)) {
        $return_value = 1;
        $return_message = "Failed logging in to server $ftp_server with username $ftp_username and password $ftp_password";
    } else {
        $sftp->get('Returns/RTNMMARTCR.txt', "$log_path" . "RTNMMARTCR.txt");
        $sftp->get('Returns/RTNMMARTDR.txt', "$log_path" . "RTNMMARTDR.txt");
        $sftp->get('NOC/NOCMMARTCR.txt', "$log_path" . "NOCMMARTCR.txt");
        $sftp->get('NOC/NOCMMARTDR.txt', "$log_path" . "NOCMMARTDR.txt");
        $rtn_pay_size = $sftp->size('Returns/RTNMMARTCR.txt');
        $rtn_dep_size = $sftp->size('Returns/RTNMMARTDR.txt');
        $noc_dep_size = $sftp->size('NOC/NOCMMARTCR.txt');
        $noc_pay_size = $sftp->size('NOC/NOCMMARTDR.txt');
        echo "Pay Size: $rtn_pay_size\n";
        echo "Pay Size: $rtn_dep_size\n";
        echo "Pay Size: $noc_dep_size\n";
        echo "Pay Size: $noc_pay_size\n";
    }

    //Loop through the files and see if there are any records
    $attachment_arr = array();
    if ($return_value != 0) {
        $subject = "ERROR: ACH Returns Process";
    } else if ($rtn_pay_size == 96 && $rtn_dep_size == 96 && $noc_dep_size ==
        96 && $noc_pay_size == 96) {
        //There are no returns
        $subject = "ACH Returns - No Returns Today";
    } else {
        $subject = "ATTN: ACH Returns - There are returns needing processed";
        //Construct the loanpro import file for the payment return file if there are payment returns to process
        if ($rtn_pay_size != 96) {
            $return_file = fopen("$log_path" . "RTNMMARTDR.txt", "r") or die("Unable to open input file!");
            $import_file = fopen("$log_path" . "lp_import_file.csv", "w+") or die("Unable to open input file!");
            $header_record =
                "action,loanid,id,amount,date,type,method,status,early,extraTowards,cashDrawer,info,chargeoff,reverseReason,nachaReturnCode,customFees,customPayoffFees,customInterest,customDiscount,customPrincipal,customEscrow1\n";
            //output the header record
            fwrite($import_file, $header_record);
            while ($line = fgets($return_file)) {
                //Loop through the kl file and build an EDW load record from each row.
                $line = str_replace("\r", "", $line);
                $line = str_replace("\n", "", $line);

                $record_type = substr($line, 0, 1);
                if ($record_type == 6) {
                    $tran_cd = substr($line, 1, 2);
                    $routing_nbr = substr($line, 3, 8);
                    $acct_nbr = substr($line, 12, 17);
                    $amt = substr($line, 29, 10);
                    $id = substr($line, 39, 15);
                    $loan_nbr = substr($id, 0, 9);
                    $payment_nbr = substr($id, 11);
                    $name = substr($line, 54, 22);

                } else if ($record_type == 7) {
                    $addenda_type = substr($line, 1, 2);
                    $return_cd = substr($line, 3, 3);

                    //Construct the return record
                    $output_line =
                        "update,$loan_nbr,$payment_nbr,,,,,reversed,,,,,,nachaErrorCode,$return_cd,,,,,,,\n";
                    fwrite($import_file, $output_line);

                } else {
                    //Not an expected record type so reset all of the variables
                    $tran_cd = '';
                    $routing_nbr = '';
                    $acct_nbr = '';
                    $amt = '';
                    $id = '';
                    $loan_nbr = '';
                    $payment_nbr = '';
                    $name = '';
                    $addenda_type = '';
                    $return_cd = '';
                    $output_line = '';
                }
            }
            fclose($return_file);
            fclose($import_file);
        }
    }
    //Construct an email with the necessary details and send it
    $to = 'jason.bumgarner@moneymart.com';
    $from = 'support@moneymartdirect.com';
    $txt_body = $return_message;
    $html_body = "<html><body><p>$return_message</p></body></html>";
    if ($return_value == 0) {
        $attachment_arr[0] = $log_path . "NOCMMARTDR.txt";
        $attachment_arr[1] = $log_path . "NOCMMARTCR.txt";
        $attachment_arr[2] = $log_path . "RTNMMARTCR.txt";
        $attachment_arr[3] = $log_path . "RTNMMARTDR.txt";
    }
    if ($rtn_pay_size != 96) {
        $attachment_arr[4] = $log_path . "lp_import_file.csv";
    }

    //Archive the documents
    $temp = mm_send_ftp_document($log_path . "RTNMMARTDR.txt",
        "RTNMMARTDR_${run_dt}.txt", "ach_documents/return_files/", "B");
    $temp = mm_send_ftp_document($log_path . "RTNMMARTCR.txt",
        "RTNMMARTCR_${run_dt}.txt", "ach_documents/return_files/", "B");
    $temp = mm_send_ftp_document($log_path . "NOCMMARTCR.txt",
        "NOCMMARTCR_{run_dt}.txt", "ach_documents/return_files/", "B");
    $temp = mm_send_ftp_document($log_path . "NOCMMARTDR.txt",
        "NOCMMARTDR_{run_dt}.txt", "ach_documents/return_files/", "B");
    if ($rtn_pay_size != 96) {
        $temp = mm_send_ftp_document($log_path . "lp_import_file.csv",
            "lp_import_file_${run_dt}.csv", "ach_documents/return_files/", "B");
    }

    //Send email - Need to add error handling
    mm_send_email($to, $from, $subject, $txt_body, $html_body, $attachment_arr, 1);

    //Delete the documents after sending them
    unlink($log_path . "RTNMMARTCR.txt");
    unlink($log_path . "RTNMMARTDR.txt");
    unlink($log_path . "NOCMMARTCR.txt");
    unlink($log_path . "NOCMMARTDR.txt");
    if ($rtn_pay_size != 96) {
        unlink($log_path . "lp_import_file.csv");
    }

    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;

    return $return_array;
}

/***********************************************************************************************************************
 * Function mm_log_page_view($data_array)
 ***********************************************************************************************************************/
function mm_log_page_view($data_array)
{
    error_log("flow: mm_log_page_view\n");
    $return_value = 0;
    $return_message = '';
    $return_array = array();
    $page_view_nbr = 0;
    $create_datetime = new DateTime();
    $create_dt = $create_datetime->format('Y-m-d H:i:s');

    //Format the variables provided
    $ip_address = isset($data_array["ip_address"]) ? $data_array["ip_address"] : '';
    $request_time = isset($data_array["request_time"]) ? $data_array["request_time"] : '';
    $http_cookie = isset($data_array["http_cookie"]) ? $data_array["http_cookie"] : '';
    $page_name = isset($data_array["page_name"]) ? $data_array["page_name"] : '';

    $sql_string = "insert into mm_page_views (ip_address, http_cookie, page_name, request_time, create_dt) values (?,?,?,?,?)";

    try {
        $conn = mm_get_pdo_connection();
        $stmt = $conn->prepare($sql_string);
        $parms = [$ip_address, $http_cookie, $page_name, $request_time, $create_dt];
        $stmt->execute($parms);
        $page_view_nbr = $conn->lastInsertId();

    } catch (PDOException $e) {
        $return_array["return_value"] = 1;
        $return_array["return_message"] = "$e";
        $return_array["sql_string"] = $sql_string;
        return $return_array;
    }

    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    $return_array["page_view_nbr"] = $page_view_nbr;

    return $return_array;

}

/*****************************************************************
 * Function mm_get_household_matches($json_data)-
 * Takes street_address, appt_sute, and first_name and determines if there are any applications
 * in an approved or funded status that are from the same address.  This is used to determine if verifications should be required.
 *****************************************************************/
function mm_get_household_matches($json_data)
{
    error_log("flow: mm_get_household_matches\n");
    $return_array = array();
    $return_value = 0;
    $return_message = '';
    $input_data = json_decode($json_data, true);
    $first_name = $input_data["first_name"];
    $street_address = $input_data["street_address"];
    $appt_suite = $input_data["appt_suite"];
    $application_nbr = $input_data["application_nbr"];
    $bind_params = [$street_address, $appt_suite, $first_name, $application_nbr];

    //select records where there is already another approved application in the household.
    $sql_string = "Select application_nbr from mm_application where street_address = ? and appt_suite = ? and first_name <> ? and application_status in (2,3,4,5,6,7) and application_nbr <> ?";
    try {
        $conn = mm_get_pdo_connection();
        $stmt = $conn->prepare($sql_string);
        $stmt->execute($bind_params);
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $row_count = $stmt->rowCount();
    } catch (PDOException $e) {
        $return_array["return_value"] = 1;
        //$row_count = 0;
        $return_array["return_message"] = "There was an issue executing the query $sql_string.  Error: $e";
        return $return_array;
    }

    $return_array["row_count"] = $row_count;
    $return_array["results"] = $results;
    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;

    return $return_array;
}

/*******************************************************************************************************
 * Function mm_add_application_note($note_json)
 *******************************************************************************************************/

function mm_add_application_note($note_json)
{
    error_log("flow: mm_add_application_note\n");
    $note_data = json_decode($note_json, true);
    $application_nbr = $note_data["application_nbr"];
    $category = $note_data["category"];
    $sub_category = $note_data["sub_category"];
    $txt_body = $note_data["txt_body"];
    $create_dt = $note_data["create_dt"];
    $username = $note_data["username"];
    $sql_string = "insert into mm_application_notes (application_nbr, category, sub_category, txt_body, create_dt, username) values (?,?,?,?,?,?)";
    try {
        $conn = mm_get_pdo_connection();
        $stmt = $conn->prepare($sql_string);
        $parms = [$application_nbr, $category, $sub_category, $txt_body, $create_dt, $username];
        $stmt->execute($parms);
        $note_nbr = $conn->lastInsertId();
        $return_array["return_value"] = 0;
        $return_array["return_message"] = '';
        $return_array["note_nbr"] = $note_nbr;

    } catch (PDOException $e) {
        $return_array["return_value"] = 1;
        $return_array["return_message"] = "$e";
        $return_array["sql_string"] = $sql_string;
        $return_array["note_nbr"] = 0;
    }

    if (is_resource($conn)) {
        $conn = null;
    }

    return $return_array;

}

/****************************************************************************************************************
 * Function mm_schedule_loan_funding($application_nbr)
 ****************************************************************************************************************/
function mm_schedule_loan_funding($application_nbr)
{
    error_log("flow: mm_schedule_loan_funding\n");
    $return_array = array();
    $return_value = 0;
    $return_message = '';
    $status = "READY TO BOOK";
    $create_dt = new DateTime();
    $create_dt_string = $create_dt->format('Y-m-d H:i:s');

    $sql_string = "insert into mm_loan_funding_queue (application_nbr, status, create_dt) values(?,?,?)";
    try {
        $conn = mm_get_pdo_connection();
        $stmt = $conn->prepare($sql_string);
        $parms = [$application_nbr, $status, $create_dt_string];
        $stmt->execute($parms);
        //$loan_funding_nbr = $conn->lastInsertId();

    } catch (PDOException $e) {
        $return_array["return_value"] = 1;
        $return_array["return_message"] = "$e";
        $return_array["sql_string"] = $sql_string;
        $loan_funding_nbr = 0;
        return $return_array;
    }

    if (is_resource($conn)) {
        $conn = null;
    }
    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    return $return_array;

}

/**************************************************************************
 * Function mm_lp_activate_loan($application_nbr)
 **************************************************************************/
function mm_lp_activate_loan($application_nbr)
{
    error_log("flow: mm_lp_activate_loan\n");
    $return_array = array();
    $return_value = 0;
    $return_message = '';
    //$display_id = '';
    $lp_loan_id = 0;

    $display_id = str_pad($application_nbr, 8, '0', STR_PAD_LEFT);
    $display_id = "L" . $display_id;
    try {
        $sdk = LPSDK::GetInstance();
        //Check to see if the loan exists and if it does then just update the data otherwise add a new loan
        $loans = $sdk->GetLoans_RAW([LOAN::LOAN_SETUP, LOAN::LOAN_SETTINGS, LOAN::TRANSACTIONS], $sdk->CreateNoPagingParam(), $sdk->CreateFilterParams_Logic(LOAN::DISP_ID . " = '$display_id'"));
        $num_loans = sizeof($loans);
        if ($num_loans == 0) {
            //There was an error because no loan exists with that id.
            $return_value = 1;
            $return_message = "There was no loan to activate for application number $application_nbr";
        } else {
            //There was a result so get the entity
            $loan = $loans[0];
            $lp_loan_id = $loan->Get(BASE_ENTITY::ID);
            $loan->Activate();
        }
    } catch (Exception $e) {
        $return_value = 1;
        $return_message = "Failed to activate the loan for application $application_nbr.  Exception is: $e";
    }

    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    $return_array["lp_loan_id"] = $lp_loan_id;
    $return_array["display_id"] = $display_id;

    return $return_array;
}

/**************************************************************
 *Function mm_lp_delete_loan($loan_id)
 **************************************************************/
function mm_lp_delete_loan($loan_id)
{
    error_log("flow: mm_lp_delete_loan\n");
    $return_value = 0;
    $return_message = '';
    $return_array = array();

    if ($loan_id == '') {
        $return_value = 1;
        $return_message = "Invalid Loan Id: $loan_id";
    } else {
        $lp_credentials = mm_get_lp_credentials();
        $account_token = $lp_credentials["token"];
        $auto_pal_id = $lp_credentials["tenant_id"];
        $header_array = array("Content-Type: application/json", 'Accept: application/json', "Authorization: $account_token", "Autopal-Instance-ID: $auto_pal_id");
        $url = "https://loanpro.simnang.com/api/public/api/1/odata.svc/Loans($loan_id)";
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header_array);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $json_response = curl_exec($curl);
        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        if ($status != 201 && $status != 200) {
            //There was an error in the message sending  Add error handling
            $return_value = 1;
            $return_message = "Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl);
        } else {
            //The delete command was successful
            $return_value = 0;
            $return_message = '';
        }

    }
    $return_array["return_value"] = $return_value;
    $return_array["return_message"] = $return_message;
    return $return_array;

}

/********************************************************
 * Function mm_get_uw_request_queue_details($app_nbr)
 * Added by Swapna for story no 333 on July 31st 2018
 ********************************************************/
function mm_get_uw_request_queue_details($app_nbr)
{
    error_log("flow: mm_get_uw_request_queue_details\n");
    $app_data = array();
    $sql_string = "select * from mm_uw_reqeust_queue where application_nbr=$app_nbr";
    $conn = mm_get_pdo_connection();
    $stmt = $conn->prepare($sql_string);
    //$parms = [$app_nbr];
   $stmt->execute();
    $app_data = $stmt->fetchAll(PDO::FETCH_ASSOC);

    if (is_resource($conn)) {
        $conn->close();
    }

    return $app_data;

}

