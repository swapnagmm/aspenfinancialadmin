<?php
//require_once 'mm_system_config.php';
require_once 'mm_middleware.php';

function mm_start_fundingreturn_process($display_id)
{
    error_log("Begin funding return process for: $display_id");
    //Get loans loanpro id from loan_agreement table
    $lp_loan_id = mm_fetch_lp_loan_id($display_id);

    //log payment to loanpro
    $logPaymentResult = mm_lp_log_payment($lp_loan_id, $display_id);
    error_log("End logging payment in LP for: $display_id");
    if ( $logPaymentResult != 201 && $logPaymentResult != 200 )
    {
        return "Error logging payment for ID: $display_id";
    }

    //cancel autopay for loan
    $cancelAutoPayResponse = mm_lp_cancel_autopay($lp_loan_id);
    error_log("End cancel autopay LP for: $lp_loan_id");
    if ( $cancelAutoPayResponse != 201 && $cancelAutoPayResponse != 200 )
    {
        return "Error canceling autopay for ID: $display_id";
    }

    //Change loan settings to set loan status to paid off and sub status to funding return
    $loanStatusChangesResponse = mm_lp_change_loanstatuses($lp_loan_id);
    error_log("End change loan statuses in LP for: $lp_loan_id");
    if ( $loanStatusChangesResponse != 201 && $loanStatusChangesResponse != 200 )
    {
        return "Error changing loan statuses for ID: $display_id";
    }

    //Set payments profile to inactive
    $paymentProfileResponse = mm_lp_setpaymentprofile_Inactive($lp_loan_id);
    error_log("End change payment profile in LP for: $lp_loan_id");
    if ( $paymentProfileResponse != 201 && $paymentProfileResponse != 200 )
    {
        return "Error setting payment profile to inactive for ID: $display_id";
    }

    //Suspend interest for account from funding date
    $suspendInterestResponse = mm_lp_suspend_interest($lp_loan_id,$display_id);
    error_log("End suspend interest in LP for: $lp_loan_id");
    if ( $suspendInterestResponse != 201 && $suspendInterestResponse != 200 )
    {
        return "Error suspending interest for ID: $display_id";
    }

    $expireAppResponse = mm_update_expire_application($display_id);
    if($expireAppResponse !== true)
    {
        return $expireAppResponse;
    }

    error_log("End funding return process for: $display_id");
    return true;

}

function mm_lp_log_payment($lp_loan_id, $display_id)
{
    error_log("Begin logging payment in LP for: $display_id");
    //fetch origination amount from mm_loan_agreement table
    $paymentAmount = mm_fetch_principal_amt($display_id);
    //prepare API body
    $apiBody = prepare_fundingreturn_payment_apibody($paymentAmount);
    //Make API call to put Funding Return payment in Loan Pro

    return mm_lp_put_loan_call($lp_loan_id, $apiBody);
}

function mm_fetch_lp_loan_id($display_id)
{
    $conn = mm_get_pdo_connection();
    $stmt = $conn->prepare('SELECT lp_loan_id FROM mm_loan_agreement WHERE loan_nbr = ? ORDER BY create_dt DESC');
    $stmt->execute([$display_id]);
    $lp_loan_id = $stmt->fetchAll();

    return $lp_loan_id[0]['lp_loan_id'];
}

function mm_update_expire_application($display_id)
{
    try{
        error_log("Begin expire application for: $display_id");
        //fetch latest application number of loan
        $conn = mm_get_pdo_connection();
        $stmt = $conn->prepare('SELECT application_nbr FROM mm_loan_agreement WHERE loan_nbr = ? ORDER BY create_dt DESC');
        $stmt->execute([$display_id]);
        $appnbr = $stmt->fetchAll();
        $appnbr = $appnbr[0]['application_nbr'];

        //Update application status to 11 for "EXPIRED"
        $stmt = $conn->prepare('UPDATE mm_application SET application_status = 10 WHERE application_nbr = ?');
        $stmt->execute([$appnbr]);

        error_log("End expire application for: $display_id");
    } catch (PDOException $e){
        return "Error expiring application for ID: $display_id";
    }
    return true;

}

function mm_fetch_principal_amt($display_id)
{
    $conn = mm_get_pdo_connection();
    $stmt = $conn->prepare('SELECT principal_amt FROM mm_loan_agreement WHERE loan_nbr = ? ORDER BY create_dt DESC');
    $stmt->execute([$display_id]);
    $paymentAmount = $stmt->fetchAll();

    return $paymentAmount[0]['principal_amt'];
}

function mm_fetch_funding_date($display_id)
{
    $conn = mm_get_pdo_connection();
    $stmt = $conn->prepare('SELECT effective_dt FROM mm_loan_agreement WHERE loan_nbr = ? ORDER BY create_dt DESC');
    $stmt->execute([$display_id]);
    $fundingDate = $stmt->fetchAll();

    return $fundingDate[0]['effective_dt'];
}

function prepare_fundingreturn_payment_apibody($paymentAmount)
{
    $date = date('Y-m-d');
    $payments = array('Payments' => array(
        'results' => array(0 => array(
            'selectedProcessor' => '0',
            'paymentMethodId' => 13,
            'amount' => $paymentAmount,
            'date' => $date,
            'info' => "$date Funding Return",
            'paymentTypeId' => 2,
            'active' => 1,
            'resetPastDue' => 0,
            'payOffPayment' => true,
            'extra' => 'payment.extra.tx.principal',
            '__logOnly' => true,
            'payoffFlag' => 1
        ))
    ));
    return json_encode($payments);
}

function mm_lp_put_loan_call($lp_loan_id, $apiBody)
{
    $lp_credentials = mm_get_lp_credentials();
    $account_token = $lp_credentials["token"];
    $auto_pal_id = $lp_credentials["tenant_id"];
    $header_array = array("Content-Type: application/json", 'Accept: application/json', "Authorization: $account_token", "Autopal-Instance-ID: $auto_pal_id");
    $url = "https://loanpro.simnang.com/api/public/api/1/odata.svc/Loans($lp_loan_id)";
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_HTTPHEADER,$header_array );
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $apiBody);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $json_response = curl_exec($curl);
    $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    return $status;
}

function mm_lp_put_customer_call($lp_customer_id, $apiBody)
{
    $lp_credentials = mm_get_lp_credentials();
    $account_token = $lp_credentials["token"];
    $auto_pal_id = $lp_credentials["tenant_id"];
    $header_array = array("Content-Type: application/json", 'Accept: application/json', "Authorization: $account_token", "Autopal-Instance-ID: $auto_pal_id");
    $url = "https://loanpro.simnang.com/api/public/api/1/odata.svc/Customers($lp_customer_id)";
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_HTTPHEADER,$header_array );
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $apiBody);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    curl_exec($curl);

    return $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
}

function mm_lp_cancel_autopay($lp_loan_id)
{
    error_log("Begin cancel autopay LP for: $lp_loan_id");
    $autoPayId = lp_get_autopayid($lp_loan_id);
    $apiBody = prepare_cancel_autopay_body($autoPayId);
    return mm_lp_put_loan_call($lp_loan_id, $apiBody);
}

function lp_get_autopayid($lp_loan_id)
{
    $lp_credentials = mm_get_lp_credentials();
    $account_token = $lp_credentials["token"];
    $auto_pal_id = $lp_credentials["tenant_id"];
    $header_array = array("Content-Type: application/json", 'Accept: application/json', "Authorization: $account_token", "Autopal-Instance-ID: $auto_pal_id");
    $url = "https://loanpro.simnang.com/api/public/api/1/odata.svc/Loans($lp_loan_id)?\$expand=Autopays";
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $header_array);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HTTPGET, true);
    $json_response = curl_exec($curl);
    $lp_loan = json_decode($json_response, 1);
    $d = $lp_loan["d"];
    $autoPayId = $d['Autopays']['results'][0]['id'];
    return $autoPayId;
}

function prepare_cancel_autopay_body($autoPayId)
{
    $autoPay = array('Autopays' => array(
        'results' => array(
            0 => array(
                'id' => $autoPayId,
                'status' => 'autopay.status.cancelled',
                '__id' => $autoPayId,
                '__update' => true
            )
        )
    ));

    return json_encode($autoPay);
}

function mm_lp_change_loanstatuses($lp_loan_id)
{
    error_log("Begin change loan statuses in LP for: $lp_loan_id");
    $lp_loanSetting_id = lp_get_loansetting_id($lp_loan_id);
    $apiBody = prepare_fundingreturn_changeloansettings_body($lp_loanSetting_id);
    return mm_lp_put_loan_call($lp_loan_id, $apiBody);
}

function lp_get_loansetting_id($lp_loan_id)
{
    $lp_credentials = mm_get_lp_credentials();
    $account_token = $lp_credentials["token"];
    $auto_pal_id = $lp_credentials["tenant_id"];
    $header_array = array("Content-Type: application/json", 'Accept: application/json', "Authorization: $account_token", "Autopal-Instance-ID: $auto_pal_id");
    $url = "https://loanpro.simnang.com/api/public/api/1/odata.svc/Loans($lp_loan_id)?\$expand=LoanSettings";
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $header_array);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HTTPGET, true);
    $json_response = curl_exec($curl);
    $lp_loan = json_decode($json_response, 1);
    $d = $lp_loan["d"];
    $loanSettingId = $d['LoanSettings']['id'];
    return $loanSettingId;
}

function prepare_fundingreturn_changeloansettings_body($lp_loanSetting_id)
{
    $loanSettingsBody = array('LoanSettings' => array(

                'loanStatusId' => '3',
                'loanSubStatusId' => '17',
                '__id' => $lp_loanSetting_id,
                '__update' => true
    ));

    return json_encode($loanSettingsBody);
}

function mm_lp_setpaymentprofile_Inactive($lp_loan_id)
{
    $customerDetails = lp_get_customerdetails_by_loanid($lp_loan_id);
    $apiBody = prepare_paymentaccountchange_body($customerDetails);
    return mm_lp_put_customer_call($customerDetails['customerId'], $apiBody);
}

function lp_get_customerdetails_by_loanid($lp_loan_id)
{
    $lp_credentials = mm_get_lp_credentials();
    $account_token = $lp_credentials["token"];
    $auto_pal_id = $lp_credentials["tenant_id"];
    $header_array = array("Content-Type: application/json", 'Accept: application/json', "Authorization: $account_token", "Autopal-Instance-ID: $auto_pal_id");
    $url = "https://loanpro.simnang.com/api/public/api/1/odata.svc/Loans($lp_loan_id)?\$expand=Customers,Customers/PaymentAccounts";
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $header_array);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HTTPGET, true);
    $json_response = curl_exec($curl);
    $lp_loan = json_decode($json_response, 1);
    $d = $lp_loan["d"];
    $returnArr = array();
    $returnArr['paymentAccountId'] = $d['Customers']['results'][0]['PaymentAccounts']['results'][0]['id'];
    $returnArr['customerId'] = $d['Customers']['results'][0]['id'];

    return $returnArr;
}

function prepare_paymentaccountchange_body($customerDetails)
{
    $paymentAccountsBody = array(
                'PaymentAccounts' => array(
                    'results' => array(
                        0 => array(
                            'active' => 0,
                            '__id' => $customerDetails['paymentAccountId'],
                            '__update' => true
                        )
                    )
                )
            );

    return json_encode($paymentAccountsBody);
}

function mm_lp_suspend_interest($lp_loan_id, $display_id)
{
    $funding_date = mm_fetch_funding_date($display_id);
    $apiBody = prepare_suspend_interest_apibody($funding_date);
    return mm_lp_put_loan_call($lp_loan_id, $apiBody);
}

function prepare_suspend_interest_apibody($funding_date)
{
    $data = array(
        "StopInterestDates" => array(
            "results" => array(
                0 => array(
                "type" => 'loan.stopInterestType.suspend',
                "date" => $funding_date
                )
            )
        )
    );

    return json_encode($data);
}

?>