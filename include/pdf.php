<?php

require "fpdf181/fpdf.php";


  class PDF extends FPDF{

                private $ROW_SPACING = 5;

                // Page header
                function Header(){
                        // Logo
                        // Arial bold 15
                /*        $this->SetFont('Arial','B',12);
                        // Title
                        $this->Cell(0,10,'Application For Credit',0,0,'C');
                        $this->Cell(0,10,'Line 2 Credit',0,0,'C');
                        // Line break
                        $this->Ln(10);*/
                }

// Page footer
                function Footer(){
                        /*// Position at 1.5 cm from bottom
                        $this->SetY(-15);
                        // Arial italic 8
                        $this->SetFont('Arial','I',8);
                        // Page number
                        $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');*/
                }
		function print_header($text){
                        $this->ln(3);
                        $this->setFont('Arial','B', 10);
                        $this->setFillColor(200,200,200);
                        $this->setTextColor(0,0,0);
                        $this->Cell(0,5,$text, 0,1,'L',true);
                        $this->ln(3);
                }
                function print_document_label($text, $width, $border, $new_line_ind){
                        $this->setFont('Arial','B',8);
                        $this->setFillColor(255,255,255);
                        $this->Cell($width,$this->ROW_SPACING,$text, $border,$new_line_ind,'L');
                }
                function print_underline_text($text, $min_length, $border, $new_line_ind){
                        $this->setFont('Arial','U',8);
                        $this->setFillColor(255,255,255);
                        $output_string = $this->pad_text($text, $min_length);
                        $this->Cell($min_length,$this->ROW_SPACING, $output_string, $border, $new_line_ind);
                }
		function pad_text($text, $min_length){
                        $output_string = "  " . $text;
                        $str_length = strlen($text);
                        $output_string = $text;
                        while($str_length< $min_length){
                                $output_string =  $output_string . " ";
                                $str_length+= 1;
                        }
                        return $output_string;
                }
        }

