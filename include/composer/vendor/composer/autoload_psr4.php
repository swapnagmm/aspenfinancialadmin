<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Simnang\\LoanPro\\' => array($vendorDir . '/simnang/loanpro-sdk/src'),
    'ODataQuery\\' => array($vendorDir . '/curiosity26/odataquery/src'),
    'Monolog\\' => array($vendorDir . '/monolog/monolog/src/Monolog'),
);
