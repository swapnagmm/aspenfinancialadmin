<?php
require_once("mm_middleware.php");
ini_set('display_errors', 0);

/**
 **    Pull Applliction Details From DB Matching Search Criteria
 **    
 **    @param    string  $email
 **    @param    string  $first_name
 **    @param    string  $last_name
 **    @param    string  $phone_nbr
 **    @param    int     $app_nbr
 **    @param    string  $state
 **    
 **    return    array   $return_array
**/
function mm_search_applications($email, $first_name, $last_name, $phone_nbr, $app_nbr, $state)
{
    $data   = array();
    $return = array("return_value" => 0, "return_message" => '');
    $dt     = date('Y-m-d', time() - (86400 * 30));    //    Last 30 Days
    
    $sql = "
SELECT 
    mm_application.*,
    mm_uw_decision.segment AS decision_segment
LEFT OUTER JOIN
    mm_uw_decision ON mm_uw_decision.decision_nbr =
    ( SELECT
          mm_uw_decision.decision_nbr
      FROM
          mm_uw_decision
      WHERE
          ( mm_uw_decision.application_nbr = mm_application.application_nbr )
      ORDER BY
          mm_uw_decision.application_nbr, mm_uw_decision.decision_nbr DESC
      LIMIT 1
    )
WHERE 
";
    
    $where_other = '';
    $where_app   = '';
    $or          = '' ;
    
    if($email != '')
    {
        $where_other .= $or . "    email_address like '%$email%'" . PHP_EOL;
        $or = '    OR ' ;
    }
    
    if($first_name != '')
    {
        $where_other .= $or . "    first_name like '%$first_name%'" . PHP_EOL;
        $or = '    OR ' ;
    }
    
    if($last_name != '')
    {
        $where_other .= $or . "    last_name like '%$last_name%'" . PHP_EOL;
        $or = '    OR ' ;
    }
    
    if($phone_nbr != '')
    {
        $where_other .= $or . "    (mobile_phone_nbr like '%$phone_nbr%' || home_phone_nbr like '%$phone_nbr%')" . PHP_EOL;
        $or = '    OR ' ;
    }
    
    if($state != '')
    {
        $where_other .= $or . "    state = '$state'" . PHP_EOL;
        $or = '    OR ' ;
    }
    
    if($app_nbr != '')
    {
        $where_app .= $or . '    application_nbr = "' . $app_nbr . '"' . PHP_EOL;
    }
    
    if (('' == $where_other) && ('' == $where_app))
    {
        $where_other = '    mm_application.create_dt >= "' . $dt . '"' . PHP_EOL;
    }
    else
    {
        if ('' != $where_other)
        {
            $where_other = '    (mm_application.create_dt >= "' . $dt . '" AND (' . $where_other . '))' . PHP_EOL;
        }
    }
    
    $sql .= $where_other . $where_app . "ORDER BY application_nbr DESC";
    
    echo '<!-- $sql_string: ' . $sql . '  -->' . PHP_EOL;
    
    $conn = mm_get_db_connection();
    
    if(!$stmt = $conn->prepare($sql))
    {
        // mm_log_error('mm_get_application_details',  "$conn->error", $conn->errno);
        echo $conn->error;
    }
    
    if(!$stmt->execute())
    {
        $return_value   = $stmt->errno;
        $return_message = $stmt->error;
        //Need to log an error here as well
        error_log(__FUNCTION__ . '  SQL: ' . $sql . '  Execute Error: ' . $stmt->error);
    }
    else
    {
        $results = $stmt->get_result();
        $num_rows = $results->num_rows;
        
        if($num_rows == 0)
        {
            //The application id didn't exist so default all of the return values
            $return["return_value"]   = 1;
            $return["return_message"] = "There was no associated application with that application number.";
        }
        else
        {
            while($row = $results->fetch_assoc())
            {
                $data[] = $row;
            }
        }
    }
    
    if(is_resource($conn))
    {
        $conn->close();
    }
    
    $return["app_data"] = $data;
    $return["num_apps"] = $num_rows;
    
    return $return;
}


/**************************************
 * Function mm_get_dm_customers
 * ************************************/
 
 function mm_get_dm_customers($parms){
                $return_array = array();
                $return_data = array(array());
                $last4 = $parms["last4"];
                $last_name = $parms["last_name"];
                $first_name = $parms["first_name"];
                $ssn = "xxxxx$last4";
                
                $flag = 0;
                
                $conn = mm_get_db_connection();
                $sql_string = "Select * from mm_dm_prospects ";
                if($last4 != ''){
                    if($flag == 0){
                        $sql_string .= " where ssn_matchkey like '%$last4'";
                    }else{
                        $sql_string .= " and ssn_matchkey like '%$last4' ";
                    }
                        $flag = 1;
                }
                if($last_name != ''){
                    if($flag == 0){
                        $sql_string .= " where last_name like '$last_name%'";
                    }else{
                        $sql_string .= " and last_name like '$last_name%' ";
                    }
                        $flag = 1;
                }
                
                if($first_name != ''){
                    if($flag == 0){
                        $sql_string .= " where first_name like '$first_name%'";
                    }else{
                        $sql_string .= " and first_name like '$first_name%' ";
                    }
                        $flag = 1;
                }
                if(!$stmt = $conn->prepare($sql_string)){
                       // mm_log_error('mm_get_application_details',  "$conn->error", $conn->errno);
                       echo $conn->error;
                }
                
                if(!$stmt->execute()){
                        $return_value = $stmt->errno;
                        $return_message = $stmt->error;
                        //Need to log an error here as well
                        echo $stmt->error;
                }else{
                        $rows = $stmt->get_result();
                        $num_rows = $rows->num_rows;
                        if($num_rows == 0){
                                //The application id didn't exist so default all of the return values
                                $return_value = 1;
                                $return_message = "There was no associated application with that application number.";
                        }else{
                            $i = 0;
                               while($lead_data = $rows->fetch_assoc()){
                                   foreach($lead_data as $key=>$value){
                                       $return_data[$i][$key] = $value;
                                   }
                                   $i = $i + 1;
                               }
                                $return_value = 0;
                                $return_message = '';
                        }
                }

                if(is_resource($conn)){
                        $conn->close();
                }

                $return_array["return_value"] = $return_value;
                $return_array["return_message"] = $return_message;
                $return_array["lead_data"] = $return_data;
                $return_array["num_leads"] = $num_rows;
                
                return $return_array;

}

/**********************************************************************
 * mm_add_dm_opt_out($ssn, $first_name, $last_name)
 * *********************************************************************/
 function mm_add_dm_opt_out($ssn, $first_name, $last_name){
     $return_value = 0;
    $return_message = '';
    $dm_opt_out_nbr = 0;
    $return_array = array();
     
     $conn = mm_get_pdo_connection();
    $sql_string = "insert into mm_dm_opt_out (ssn, first_name, last_name) values (?,?,?) ";
       try{
                $conn = mm_get_pdo_connection();
                $stmt = $conn->prepare($sql_string);
                $parms =[ $ssn, $first_name, $last_name];
                
                $stmt->execute($parms);
                $return_array["dm_opt_out_nbr"] = $conn->lastInsertId();
                $return_array["return_value"] = 0;
                $return_array["return_message"] = '';

        }catch(PDOException $e){
                $return_array["return_value"] = 1;
                $return_array["return_message"] = "$e";
                $return_array["dm_opt_out_nbr"] = 0;
                $return_array["sql_string"] = $sql_string;
                if(is_resource($conn)){
                $conn = null;
            }
                return $return_array;
        }

        if(is_resource($conn)){
                $conn = null;
        }

     
     return $return_array;
 }
   
function mm_upload_application_document($document_json){
        $return_array = array();
        $return_value = 0;
        $return_message = '';
        $document_data = json_decode($document_json, true);
        $application_nbr = $document_data["application_nbr"];
        $document_path = $document_data["document_path"];
        $username = $document_data["username"];
        $create_dt = $document_data["create_dt"];

         $sql_string = "insert into mm_application_documents (document_path, username, create_dt, application_nbr) values (?,?,?,?)";

        $application_data = mm_get_application_details($application_nbr);
        $app_data = $application_data["app_data"];
        $account_nbr = $app_data["account_nbr"];

        //Construct the document path
        try{
                $conn = mm_get_pdo_connection();
                $stmt = $conn->prepare($sql_string);
                $parms =[$document_path, $username, $create_dt, $application_nbr];
                $stmt->execute($parms);
                $document_nbr =   $conn->lastInsertId();
        }catch(PDOException $e){
                $return_array["return_value"] = 1;
                $return_array["return_message"] = "$e";
                $return_array["sql_string"] = $sql_string;
                $return_array["document_nbr"] = 0;
                return $return_array;
        }

        if(is_resource($conn)){
                $conn = null;
        }

        $return_array["return_value"] = $return_value;
        $return_array["return_message"] = $return_message;
        return $return_array;


}

function mm_get_application_document_details($document_nbr){
     $return_array = array();
        $return_value = 0;
        $return_message = '';

         $sql_string = "select * from mm_application_documents where document_nbr = ?";

        try{
                $conn = mm_get_pdo_connection();
                $stmt = $conn->prepare($sql_string);
                $parms = [$document_nbr];
                $stmt->execute($parms);
                $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
                $num_documents = $stmt->rowCount();
        }catch(PDOException $e){
                $return_array["return_value"] = 1;
                $return_array["return_message"] = "There was an issue executing the query $sql_string.  Error: $e";
                $return_array["num_documents"] = 0;
                $return_array["results"] = '';
                return $return_array;
        }

        if(is_resource($conn)){
                $conn = null;
        }

        $return_array["results"] = $results;
        $return_array["num_documents"] = $num_documents;
        $return_array["return_value"] = $return_value;
        $return_array["return_message"] = $return_message;
        return $return_array;
}


function mm_get_application_documents($application_nbr){
        $return_array = array();
        $return_value = 0;
        $return_message = '';

         $sql_string = "select * from mm_application_documents where application_nbr = ? order by document_nbr desc";

        try{
                $conn = mm_get_pdo_connection();
                $stmt = $conn->prepare($sql_string);
                $parms = [$application_nbr];
                $stmt->execute($parms);
                $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
                $num_documents = $stmt->rowCount();
        }catch(PDOException $e){
                $return_array["return_value"] = 1;
                $return_array["return_message"] = "There was an issue executing the query $sql_string.  Error: $e";
                $return_array["num_documents"] = 0;
                $return_array["results"] = '';
                return $return_array;
        }

        if(is_resource($conn)){
                $conn = null;
        }

        $return_array["results"] = $results;
        $return_array["num_documents"] = $num_documents;
        $return_array["return_value"] = $return_value;
        $return_array["return_message"] = $return_message;
        return $return_array;
}

/*******************************************
 * Function mm_get_email_messages($account_nbr)
 * ******************************************/
function mm_get_email_messages($account_nbr){
    $return_array = array();
    $return_value = 0;
    $return_message = '';
    $results = array();
    $num_emails = 0;
 
    $sql_string = "select * from mm_email_message_queue where account_nbr = ? order by email_message_nbr desc";   
    
     try{
                $conn = mm_get_pdo_connection();
                $stmt = $conn->prepare($sql_string);
                $parms = [$account_nbr];
                $stmt->execute($parms);
                $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
                $num_emails = $stmt->rowCount();
        }catch(PDOException $e){
                $return_array["return_value"] = 1;
                $return_array["return_message"] = "There was an issue executing the query $sql_string.  Error: $e";
                $return_array["num_emails"] = 0;
                $return_array["results"] = '';
                return $return_array;
        }

        if(is_resource($conn)){
                $conn = null;
        }

        $return_array["results"] = $results;
        $return_array["num_emails"] = $num_emails;
        $return_array["return_value"] = $return_value;
        $return_array["return_message"] = $return_message;
        return $return_array;
    
}


/***********************************************************
 * Function mm_cancel_email_message($email_message_nbr)
 * *********************************************************/

function mm_cancel_email_message($email_message_nbr){
	$return_array = array();
	$return_message = '';
	$return_value = 0;
	$logged_in_user = isset($_SESSION["username"]) ? $_SESSION["username"] : "SYSTEM";

	//Check to make sure the status of the email isn't alredy sent
	$sql_string = "select status from mm_email_message_queue where email_message_nbr = ?";
	try{
                $conn = mm_get_pdo_connection();
                $stmt = $conn->prepare($sql_string);
                $parms = [$email_message_nbr];
                $stmt->execute($parms);
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$rows = sizeof($results);
		if($rows == 0){
			//There was no matching email message so log an error
			$return_value = 1;
			$return_message = "There was no email message found for the number $email_message_nbr";
		}else{
			foreach($results as $result){
				if($result["status"] == "SENT SUCCESSFULLY"){
					$return_value = 1;
					$return_message = "The message couldn't be cancelled because it's already been sent.";
				}
			}
		}
        }catch(PDOException $e){
		$return_value = 1;
                $return_message = "There was an issue executing the query $sql_string.  Error: $e";
	}

	//If the return_value is still zero then the status check was successful
	if($return_value == 0){
		$update_results = mm_update_database_value('mm_email_message_queue', 'status',"CANCELLED", 's', 'email_message_nbr', $email_message_nbr, $logged_in_user);
		if($update_results["return_value"] != 0){
			$return_value = 1;
			$update_results_message = $update_results["return_message"];
			$return_message = "Unable to cancel the email.  Error: $update_results_message";	
		}
	}

        if(is_resource($conn)){
                $conn = null;
        }

	$return_array["return_value"] = $return_value;
	$return_array["return_message"] = $return_message;

	return $return_array;

}

/***********************************************************
 * Function mm_resend_email_message($email_message_nbr)
 * *********************************************************/

function mm_resend_email_message($email_message_nbr){
        $return_array = array();
        $return_message = '';
        $return_value = 0;
    $logged_in_user = isset($_SESSION["username"]) ? $_SESSION["username"] : "SYSTEM";
	$update_results = mm_update_database_value('mm_email_message_queue', 'status',"READY TO SEND", 's', 'email_message_nbr', $email_message_nbr, $logged_in_user);
        if($update_results["return_value"] != 0){
                $return_value = 1;
                $update_results_message = $update_results["return_message"];
                $return_message = "Unable to cancel the email.  Error: $update_results_message";
        }

        $return_array["return_value"] = $return_value;
        $return_array["return_message"] = $return_message;

        return $return_array;
}


/******************************************************************************
Function mm_change_application_status($input_array)
******************************************************************************/
function mm_change_application_status($input_array){
    error_log("mm change application status");
	$return_value = 0;
	$return_message = '';
	$return_array = array();

	$new_application_status = $input_array["new_app_status"];
	$logged_in_user = $input_array["logged_in_user"];
	$application_nbr = $input_array["application_nbr"];
	$application_details = mm_get_application_details($application_nbr);
	$app_data = $application_details["app_data"];
	$email_address = $app_data["email_address"];
	$first_name = ucfirst(strtolower($app_data["first_name"]));
	$loan_agreement_nbr = $app_data["loan_agreement_nbr"];
	$orig_appid = isset($input_array["orig_appid"]) ? $input_array["orig_appid"] : 0;
	$app_state =$app_data["state"];
	if($new_application_status == 1){
		//Draft
	}else if($new_application_status == 2){
		//Approved Pending Customer Acceptance
		//Create a new UW Response Record, link the record to the application record, update the approved amount record (Make sure that the requested amount isn't already set, possibly send an email if it's been requested
		//Need to add error handling
		//Update the application record to the new approved amount
		$approved_amt = $input_array["approved_amt"];
		$approved_apr = $input_array["approved_apr"];
		mm_update_database_value('mm_application', 'approved_amt', $approved_amt, 'i', 'application_nbr', $application_nbr, $logged_in_user);
		mm_update_database_value('mm_application', 'approved_apr', $approved_apr, 'd', 'application_nbr', $application_nbr, $logged_in_user);
		//Create a new underwriting record
		$temp_array = array();
		$temp_array["AppId"] = $orig_appid;
		$temp_array["AppStatusCode"] = "Approved";
		$temp_array["AppStatusDesc"] = "Manually Approved";
		$temp_array["ApprovedAmount"] = $approved_amt;
		$temp_array["IsApproved"] = 1;
		$temp_array["Product"] = "";	
		$temp_array["ReferenceId"] = $application_nbr;
		$temp_array["RejectReason1"] = '';
		$temp_array["RejectReason2"] = '';
		$temp_array["RejectReason3"] = '';
		$temp_array["RejectReason4"] = '';
		$temp_array["RejectReasonCode"] = '';
		$temp_array["Segment"] = 'Manual';
		$temp_array["SSN"] = '';
		$temp_array["Strategy"] = 'Manual Strategy';	
		$temp_date = new DateTime();
		$temp_date_string = $temp_date->format('Y-m-d H:i:s');
		$temp_array["TransactionDate"] = $temp_date_string;
		$json_response = json_encode($temp_array);

		
		$insert_uw_decision=mm_create_uw_decision($json_response);
                $uw_decision_nbr = $insert_uw_decision["decision_nbr"];
		//Link the new underwriting record to the application record
		mm_update_database_value('mm_application', 'uw_decision_nbr', $uw_decision_nbr, 'i', 'application_nbr', $application_nbr, $logged_in_user);
		//Update the application_status
		mm_update_database_value('mm_application', 'application_status', $new_application_status, 'i', 'application_nbr', $application_nbr, $logged_in_user);
		mm_update_database_value('mm_application', 'application_sub_status', 0, 'i', 'application_nbr', $application_nbr, $logged_in_user);
		mm_update_database_value('mm_application', 'noaa_cd', 0, 'i', 'application_nbr', $application_nbr, $logged_in_user);
		mm_update_database_value('mm_application', 'manual_approval', 1, 'i', 'application_nbr', $application_nbr, $logged_in_user);
		
	}else if($new_application_status == 4){
		//Approved Pending Customer Documents
		mm_update_database_value('mm_application', 'application_status', $new_application_status, 'i', 'application_nbr', $application_nbr, $logged_in_user);
		//Check to see if the email customer box has been checked
		if(isset($input_array["email_checkbox"])){
			$email_data = array();
			if(	$app_state == 'IL'){
			    $supportemail = VERIFICATION_IL_SUPPORT_EMAIL;
			}else{
			    $supportemail = VERIFICATION_SUPPORT_EMAIL;
			}
                        $email_data["to"] = $email_address;
                         $email_data["from"] = $supportemail;
                         $email_data["from_name"] = SITE_NAME." Support Team";
                         $email_data["subject"] = "Additional Information Needed For Your Application";
                         $email_data["txt_body"] = "Dear $first_name,\n\nThank you for your recent loan application with ".SITE_NAME.".  We are in need of some verification documents in order to fund your loan.\n\nPlease send a copy of your ID and your two most recent Bank Statements to ".$supportemail.".\n\nFor any questions you may respond directly to this email or call our customer support team at ".SUPPORT_PHONE;  
			             $email_data["html_body"] = "<html><head></head><body>Dear $first_name,<br><br>Thank you for your recent loan application with ".SITE_NAME.".  We are in need of some verification documents in order to fund your loan.<br><br>Please send a copy of your ID and your two most recent Bank Statements to ".$supportemail.".<br><br>For any questions you may respond directly to this email or call our customer support team at ".SUPPORT_PHONE.".</body></html>";
                         $tmp_dt = new DateTime();
                         $scheduled_dt = $tmp_dt->format("Y-m-d H:i:s");
                         $email_data["scheduled_dt"] = $scheduled_dt;
                         $email_message = json_encode($email_data);
                         $email_results = mm_schedule_email_message($email_message);
                         if($email_results["return_value"] != 0 || $email_results["email_message_nbr"] == 0){
                                 //The email scheduling failed so log an error
                                 mm_log_error('mm_update_application_status', "Failed to schedule the  email to be sent to $email_address");
                                                $return_value = 1;
                                                $return_message = "Failed to send the email for $email_address";
                                        }
		}
		
	}else if($new_application_status == 5){
		//Approved Pending Agent Review
                mm_update_database_value('mm_application', 'application_status', $new_application_status, 'i', 'application_nbr', $application_nbr, $logged_in_user);
	}else if($new_application_status == 6){
		//Approved Pending Customer Signature
		//Create a new verifications record and assign it to the application
		$verif_data = array();
		$verif_data["ApplicationID"] = 0;
		$verif_data["application_nbr"] = $application_nbr;
		$verif_data["AppStatusCode"] = "Accepted";
		$verif_data["AppStatusDesc"] = "Accepted - Manually in Admin";
		$verif_data["RequiredDocs"] = "None";
		$verif_record_results = mm_create_preverif_decision($verif_data);
		if($verif_record_results["return_value"] != 0){
			//Need to add error handling
		}else{
			//Check to see if the email flag is set
			if(isset($input_array["email_checkbox"])){
				$email_data = array();
                        	$email_data["to"] = $email_address;
                         	$email_data["from"] = SUPPORT_EMAIL;
                         	$email_data["from_name"] = SITE_NAME." Support Team";
                         	$email_data["subject"] = "Application Verification Complete";
                         	$email_data["txt_body"] = "Congratulations, your loan has been approved.  Please log into your account at ".SITE_URI." to sign your loan documents.&#10&#10For any questions you may respond directly to this email or call our customer support team at ".SUPPORT_PHONE.".&#10&#10".SITE_NAME." Support Team&#10Phone: ".SUPPORT_PHONE."&#10Email: ".SUPPORT_EMAIL."&#10&#10Privileged and Confidential.  This e-mail, and any attachments thereto, is intended only for use by the addressee(s) named herein and may contain privileged and/or confidential information.  If you have received this e-mail in error, please notify me immediately by return e-mail and delete this e-mail.  You are hereby notified that any dissemination, distribution or copying of this e-mail and/or any attachments thereto, is strictly prohibited.";
                        	$email_data["html_body"] = "<html><head></head><body>Congratulations, your loan has been approved.  Please log into your account at ".SITE_URI." to sign your loan documents.<br><br>For any questions you may respond directly to this email or call our customer support team at ".SUPPORT_PHONE.".<br><br>".SITE_NAME." Support Team<br>Phone: ".SUPPORT_PHONE."<br>Email: ".SUPPORT_EMAIL."<br><br>Privileged and Confidential.  This e-mail, and any attachments thereto, is intended only for use by the addressee(s) named herein and may contain privileged and/or confidential information.  If you have received this e-mail in error, please notify me immediately by return e-mail and delete this e-mail.  You are hereby notified that any dissemination, distribution or copying of this e-mail and/or any attachments thereto, is strictly prohibited.</body><html>";
                         	$tmp_dt = new DateTime();
                         	$scheduled_dt = $tmp_dt->format("Y-m-d H:i:s");
                         	$email_data["scheduled_dt"] = $scheduled_dt;
                         	$email_message = json_encode($email_data);
                         	$email_results = mm_schedule_email_message($email_message);
                         	if($email_results["return_value"] != 0 || $email_results["email_message_nbr"] == 0){
                                 	//The email scheduling failed so log an error
                                 	mm_log_error('mm_update_application_status', "Failed to schedule the  email to be sent to $email_address");
                                        $return_value = 1;
                                        $return_message = "Failed to send the email for $email_address";
				}
			}
		}
		//Update the application status
		mm_update_database_value('mm_application', 'application_status', $new_application_status, 'i', 'application_nbr', $application_nbr, $logged_in_user);
	}else if($new_application_status == 7){
		//Approved and Booked
		mm_update_database_value('mm_application', 'application_status', $new_application_status, 'i', 'application_nbr', $application_nbr, $logged_in_user);
	}else if($new_application_status == 8){
		//Declined	
		//Check to make sure there isn't an active loan
		if($loan_agreement_nbr >0){
			$la_details = mm_get_loan_agreement_details($loan_agreement_nbr);
			$loan_nbr = $la_details["loan_nbr"];
			$lp_details = mm_get_lp_loan_details($loan_nbr);
			$loan_status = $lp_details["loan_status"];
			if($loan_status !=1 && $loan_status != 0 && $loan_status != ''){
				//There is a loan on file that is active so don't delete the loan.
				$return_value = 1;
				$return_message = "There is an active loan on file so this loan can't be declined";
			}
		}
		if($return_value == 0){
			//Create a new underwriting record and assign it to the application
			$decline_reason = $input_array["decline_reason"];
			$temp_array = array();
                	$temp_array["AppId"] = $orig_appid;
                		$temp_array["AppStatusCode"] = "Declined";
                	$temp_array["AppStatusDesc"] = "Manually Declined";
                	$temp_array["ApprovedAmount"] = 0;
                	$temp_array["IsApproved"] = 0;
                		$temp_array["Product"] = "";
                	$temp_array["ReferenceId"] = $application_nbr;
                	$temp_array["RejectReason1"] = '';
                	$temp_array["RejectReason2"] = '';
                	$temp_array["RejectReason3"] = '';
                	$temp_array["RejectReason4"] = '';
                	$temp_array["RejectReasonCode"] = '';
                	$temp_array["Segment"] = 'Manual';
                	$temp_array["SSN"] = '';
                	$temp_array["Strategy"] = 'Manual Strategy';
                	$temp_date = new DateTime();
                	$temp_date_string = $temp_date->format('Y-m-d H:i:s');
                	$temp_array["TransactionDate"] = $temp_date_string;
                	$json_response = json_encode($temp_array);
                	$insert_uw_decision=mm_create_uw_decision($json_response);
			$uw_return_value = $insert_uw_decision["return_value"];
			if($uw_return_value !=0){
				//There was an error inserting the Uw Record
				mm_log_error('mm_update_application_status', "Failed adding a new U/W record to the database for application $application_nbr");
			}else{
                		$uw_decision_nbr = $insert_uw_decision["decision_nbr"];
                		//Link the new underwriting record to the application record
                		mm_update_database_value('mm_application', 'uw_decision_nbr', $uw_decision_nbr, 'i', 'application_nbr', $application_nbr, $logged_in_user);
			}
                	//Update the application_status
                	mm_update_database_value('mm_application', 'application_status', $new_application_status, 'i', 'application_nbr', $application_nbr, $logged_in_user);
                	mm_update_database_value('mm_application', 'application_sub_status', 0, 'i', 'application_nbr', $application_nbr, $logged_in_user);
                	mm_update_database_value('mm_application', 'noaa_cd', $decline_reason, 'i', 'application_nbr', $application_nbr, $logged_in_user);
                	mm_update_database_value('mm_application', 'approved_amt', 0, 'i', 'application_nbr', $application_nbr, $logged_in_user);
			//Generate a NOAA
			$noaa_results = mm_generate_noaa_pdf($application_nbr, $decline_reason);
			if($noaa_results["return_value"] != 0){
				//Failed to generate the NOAA PDF
				$noaa_message = $noaa_results["return_message"];
				mm_log_message('mm_update_application_status', "Failed creating a NOAA PDF for application $application_nbr");
				$return_value = 1;
				$return_message = "There was an issue $noaa_message";
			}else{
			//Schedule an email to be sent
                                	$noaa_location = $noaa_results["document_path"];
	
                                	//Schedule the NOAA email to be sent
                                	$email_data = array();
                                	$email_data["to"] = $email_address;
                                	$email_data["from"] = 'support@aspenfinancialdirect.com';
                                	$email_data["from_name"] = "Aspen Financial Direct Support Team";
                                	$email_data["subject"] = "Aspen Financial Direct Application Decision";
                                	$email_data["txt_body"] = "We are writing to inform you of our decision regarding your recent application with Aspen Financial Direct.  At this time, we are unable to grant your credit request.  Attached you will find the notice of adverse action which contains additional information as well as your rights for additonal information regarding this decision.\n\nThanks for considering Aspen Financial Direct,\n\nAspen Financial Direct Support Team\nEmail:support@aspenfinancialdirect.com\nPhone:(877) 293-2987\n";
                                	$email_data["html_body"] = "<html><head></head><body>We are writing to inform you of our decision regarding your recent application with Aspen Financial Direct.  At this time, we are unable to grant your credit request.  Attached you will find the notice of adverse action which contains additional information as well as your rights to request additonal information regarding this decision.<br><br>Thanks for considering Aspen Financial Direct,<br><br>Aspen Financial Direct Support Team<br>Email:support@aspenfinancialdirect.com<br>Phone:(877) 293-2987</body></html>";
                                	$email_data["attachment"] = $noaa_location;
                                	$tmp_dt = new DateTime();
                                	//$tmp_dt = $tmp_dt->modify("+1");
                                	$scheduled_dt = $tmp_dt->format("Y-m-d H:i:s");
                                	$email_data["scheduled_dt"] = $scheduled_dt;
                                	$email_message = json_encode($email_data);
                                	$email_results = mm_schedule_email_message($email_message);
                                	if($email_results["return_value"] != 0 || $email_results["email_message_nbr"] == 0){
                                        	//The email scheduling failed so log an error
                                        	mm_log_error('mm_update_application_status', "Failed to schedule the NOAA email to be sent to $email_address");
						$return_value = 1;
						$return_message = "Failed to send the NOAA email for $email_address";
					}
	
			}
			//Delete the loan off of Loan Pro
			if($loan_agreement_nbr >0){
				$loan_agreement_data = mm_get_loan_agreement_details($loan_agreement_nbr);
				$display_id = $loan_agreement_data["loan_nbr"];
				$lp_loan_details = mm_get_lp_loan_details($display_id);
				$loan_id = $lp_loan_details["loan_id"];
				if($loan_id >0){
					$delete_loan_results = mm_lp_delete_loan($loan_id);
					if($delete_loan_results["return_value"] != 0){
						$delete_return_message = $delete_loan_results["return_message"];
						mm_log_error('mm_update_application_status', "Failed deleting the loan from Loan Pro for $display_id. Error Message is: $delete_return_message");			
						$return_value = 1;
						$return_message = "Failed deleting the loan, $display_id, from Loan Pro.  Error was: $delete_return_message";
					}
					
				}

			}
		}	
	}else if($new_application_status == 9){
		//Error Status
		//Update the application status
                mm_update_database_value('mm_application', 'application_status', $new_application_status, 'i', 'application_nbr', $application_nbr, $logged_in_user);
	}else if($new_application_status == 10){
		//Expired
		mm_update_database_value('mm_application', 'application_status', $new_application_status, 'i', 'application_nbr', $application_nbr, $logged_in_user);
	}else if($new_application_status == 11){

		//Voided
		//Set the app status to voided
		//Check to see if there is a loan agreement
		//If there is a loan on Loan Pro then delete it.  Ultimately need to make sure the loan isn't already active on Loan Pro
                mm_update_database_value('mm_application', 'application_status', $new_application_status, 'i', 'application_nbr', $application_nbr, $logged_in_user);
		$application_details = mm_get_application_details($application_nbr);
		$app_data = $application_details["app_data"];
		$loan_agreement_nbr = $app_data["loan_agreement_nbr"];
		if($loan_agreement_nbr > 0){
			//There is a loan on loan pro so delete it	
			$loan_agreement_data = mm_get_loan_agreement_details($loan_agreement_nbr);
			$display_id = $loan_agreement_data["loan_nbr"];
			$lp_loan_details = mm_get_lp_loan_details($display_id);
			$loan_id = $lp_loan_details["loan_id"];
			if($loan_id >0){
				$delete_loan_results = mm_lp_delete_loan($loan_id);
				if($delete_loan_results["return_value"] != 0){
					$delete_return_message = $delete_loan_results["return_message"];
					mm_log_error('mm_update_application_status', "Failed deleting the loan from Loan Pro for $display_id. Error Message is: $delete_return_message");			
					$return_value = 1;
					$return_message = "Failed deleting the loan, $display_id, from Loan Pro.  Error was: $delete_return_message";
				}
			}
		}	
	}else{
		//Unexpected
	}


	$return_array["return_value"] = $return_value;
	$return_array["return_message"] = $return_message;
	return $return_array;
}

/******************************************************************************
Function mm_verify_gross_income($input_array)
******************************************************************************/
function mm_verify_gross_income($input_array){
	$return_array = array();
	$return_message = '';
	$return_value = 0;

	$sql_string = "select * from mm_state_specific_data where application_nbr = ? ";
	try{
                $conn = mm_get_pdo_connection();
                $stmt = $conn->prepare($sql_string);
                $parms = [$input_array["application_nbr"]];
                $stmt->execute($parms);
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$rows = sizeof($results);
		if($rows == 0){
			//There was no matching email message so log an error
			$return_value = 1;
			$return_message = "There was no state specific data found for the number ".$input_array['application_nbr'];
		}else{
			foreach($results as $result){
				if($result["blob"] != ""){
				    $blob_value=json_decode($result["blob"]);
				    $blob_value->illinois_agreement = $blob_value->illinois_agreement;
				    $blob_value->gross_income_verified = $input_array["verified_gross_income"];
				    $blob_updated = json_encode($blob_value);
				    // print_r($blob_value);
				    // echo $blob_updated;
				    // die;
				    //echo "update mm_state_specific_data set blob='".$blob_updated."' where application_nbr = '".$input_array["application_nbr"]."'";
				    //die;
				    $sql_string_update = "update mm_state_specific_data set `blob`=:blob where application_nbr = :application_nbr";
				    $stmt_update = $conn->prepare($sql_string_update);
                    //$parms_update = [$blob_updated, $input_array["application_nbr"]];
                    $stmt_update->bindValue(":blob", $blob_updated);
                    $stmt_update->bindValue(":application_nbr", $input_array["application_nbr"]);
                    $stmt_update->execute();
        			$return_value = 1;
        			$return_message = "There verified gross income is updated successfully.. ".$input_array['application_nbr'];
            
				}
			}
		}
        }catch(PDOException $e){
		$return_value = 1;
                $return_message = "There was an issue executing the query $sql_string.  Error: $e";
	}


        if(is_resource($conn)){
                $conn = null;
        }

	$return_array["return_value"] = $return_value;
	$return_array["return_message"] = $return_message;

	return $return_array;

}

/**************************************************************************
Function generate_random_token($length = 32)
**************************************************************************/
function generate_random_token($length = 32){
    if(!isset($length) || intval($length) <= 8 ){
      $length = 32;
    }
    if (function_exists('random_bytes')) {
        return bin2hex(random_bytes($length));
    }
    if (function_exists('mcrypt_create_iv')) {
        return bin2hex(mcrypt_create_iv($length, MCRYPT_DEV_URANDOM));
    }
    if (function_exists('openssl_random_pseudo_bytes')) {
        return bin2hex(openssl_random_pseudo_bytes($length));
    }
}

/*************************************************************************
Function mm_encrypt_password
*************************************************************************/
function mm_encrypt_password($password, $salt){
        $password_string = $password . $salt;
        $salt = '$2a$07$' . $salt;

        $encrypted_pass = crypt($password, $salt);
        return $encrypted_pass;
}

/**************************************************************************
Function mm_generate_salt($length=32)
**************************************************************************/
function mm_generate_salt($length=32){
    return substr(strtr(base64_encode(hex2bin(RandomToken(32))), '+', '.'), 0, $length);
}

/************************************************************************
Function mm_validate_admin_password
************************************************************************/
function mm_validate_admin_password($password, $password2){
	$return_array = array();
	$return_value = 0;
	$return_message = '';
	
	if(strlen($password) < 8){
                $return_message = "The password must contain at least 8 characters.";
                $return_value = 1;
        }
        else if($password != $password2){
                $return_message = "The two values entered for password don't match.";
                $return_value = 1;
        }else{
        	$uppercase = preg_match('@[A-Z]@', $password);
        	$lowercase = preg_match('@[a-z]@', $password);
        	$number    = preg_match('@[0-9]@', $password);
        	$special_char = preg_match('@[!$\@#$%^&*]@', $password);
        	if($uppercase == 0 || $lowercase == 0 || $number == 0 || $special_char == 0){
                	$return_message = "The password must contain at least 8 characters and at least 1 capital letter, 1 lower case letter, 1 number, and 1 of the following characters: !@#$%^&*";
                	$return_value = 1;
        	}
	}
		$return_array["return_value"] = $return_value;
		$return_array["return_message"] = $return_message;	
	return $return_array;
}

/*************************************************************************
Function mm_validation_admin_username
*************************************************************************/
function mm_validate_admin_username($username){
	$return_array = array();
	$return_message = '';
	$return_value = 0;

	if(preg_match('/\s/',$username)){
		//There is white space in the username which isn't allowed
		$return_value = 1;
		$return_message = "Spaces are not allowed in the username.  Please select a different username.";
	}else{
		$admin_user = mm_get_admin_user($username);	
		if($admin_user["user_id"] != 0){
			//There is already an admin user with this name so throw an error
			$return_value = 1;
			$return_message = "Sorry, this username is already taken.";
		}
	}
	
	$return_array["return_value"] = $return_value;
	$return_array["return_message"] = $return_message;

	return $return_array;


}

/***************************************************************************
Function mm_add_admin_user(array $user_data)
***************************************************************************/
function mm_add_admin_user(array $user_data){
        //Accept an array of information and attempt to insert it into the database.
        $return_array = array();
	$return_value = 0;
	$return_message = '';
	$username = $user_data["username"];
        $password = $user_data["password"];
        $password2 = $user_data["password2"];
	$display_name = $user_data["display_name"];
	//Need to create variables for the various permissions
        $create_dt = date("Y-m-d H:i:s");
	
	//Check to make sure the username doesn't already exist
	$user_results = mm_validate_admin_username($username);
	$username_return_value = $user_results["return_value"];
	$username_return_message = $user_results["return_message"];

        //Check to make sure that the password is valid
        $password_validation = mm_validate_admin_password($password, $password2);
	$password_return_value = $password_validation["return_value"];
	$password_message = $password_validation["return_message"];

 	if($password_return_value == 0 && $username_return_value == 0){
                //All of the fields validate so attempt to insert the data into database and set the return values appropriately.
                $salt_string = mm_generate_salt();
                $encrypted_password = mm_encrypt_password($password, $salt_string);
		$user_data["salt_string"] = $salt_string;
		$user_data["encrypted_password"] = $encrypted_password;
		$insert_user_results = mm_create_admin_user($user_data);
		$insert_return_value = $insert_user_results["return_value"];
		if($insert_return_value == 0){
			$return_value = 0;
			$return_message = "User Created Successfully";
		}else{
			$return_message = $insert_user_results["return_message"];
			$return_value = 1;
		}
        }else{
		$return_value = 1;
		if($username_return_value != 0){
			$return_message = $username_return_message;
		}else{
			$return_message = $password_message;
		}
	}

	//Build return array
	$return_array["return_value"] = $return_value;
	$return_array["return_message"] = $return_message;
        return $return_array;
	
}

/************************************************
Function mm_get_admin_user($username)
************************************************/
function mm_get_admin_user($username){
	//Need to add error handling
        $return_array = array();
	$user_id = 0;
        $sql_string = "select * from mm_admin_user where username = ?";
        $conn=mm_get_db_connection();
        $stmt = $conn->prepare($sql_string);
        $stmt->bind_param('s', $username);
        $stmt->execute();
        $results = $stmt->get_result();
        $row = $results->fetch_assoc();
	if($row != ''){
        	foreach($row as $key=>$value){
                	$return_array["$key"] = $value;
        	}
	}else{
		//There are no matching users
		$return_array["user_id"] = 0;
	}

                $return_array["return_value"] = 0;
                return $return_array;
}

/*******************************************************************************
Function mm_create_admin_user($user_data)
*******************************************************************************/
function mm_create_admin_user($user_data){
	$return_array = array();
	$return_value = 0;
	$return_message = '';
	$username = $user_data["username"];
	$display_name = $user_data["display_name"];
	$encrypted_password = $user_data["encrypted_password"];
	$salt_string = $user_data["salt_string"];
	$permissions_user_mgmt = isset($user_data["permissions_user_mgmt"]) ? "Y" : "N";
	$permissions_ach_mgmt = isset($user_data["permissions_ach_mgmt"]) ? "Y" : "N";
	$permissions_manual_decisioning = isset($user_data["permissions_manual_decisioning"]) ? "Y" : "N";
	$password = $user_data["encrypted_password"];
	$create_dt_tmp = new DateTime();
        $create_dt = $create_dt_tmp->format('Y-m-d H:i:s');
	
	$sql_string = "insert into mm_admin_user (username, display_name, password, salt_string, password_reset_ind, permissions_user_mgmt, permissions_ach_mgmt, permissions_manual_decisioning, active_ind, create_dt) values (?,?,?,?,0,?,?,?,1,?)";

	 try{
                $conn = mm_get_pdo_connection();
                $stmt = $conn->prepare($sql_string);
                $parms =[ $username, $display_name, $password, $salt_string, $permissions_user_mgmt, $permissions_ach_mgmt, $permissions_manual_decisioning, $create_dt];
                $stmt->execute($parms);
                $user_id =   $conn->lastInsertId();

        }catch(PDOException $e){
                $return_array["return_value"] = 1;
                $return_array["return_message"] = "$e";
                $return_array["sql_string"] = $sql_string;
		$return_array["user_id"] = 0;
                return $return_array;
        }

        if(is_resource($conn)){
                $conn = null;
        }
        //Need to add defaulting and handle errors
        $return_array["user_id"] = $user_id;
        $return_array["return_value"] = 0;
	$return_array["return_message"] = "User Created Successfully";

        return $return_array;

}

/******************************************************************************************
Function mm_admin_login_user($username, $password)
******************************************************************************************/
function mm_admin_login_user($username, $password){
	$return_array = array();
	$return_value = 0;
	$return_message = '';

	$user_data = mm_get_admin_user($username);
	$user_id = $user_data["user_id"];
	if($user_id ==0){
		//There is no user match
		$return_value = 1;
		$return_message = "The username doesn't exist.";
		mm_log_error('admin_login', "There was an inccorect login using the id $username");
	}else{
		$active_ind = $user_data["active_ind"];
		$salt_string = $user_data["salt_string"];
		$locked_flag = $user_data["locked_flag"];
		$stored_password = $user_data["password"];
		$num_unsuccessful_logins = $user_data["num_unsuccessful_logins"];
		$user_id = $user_data["user_id"];
		$encrypted_password = mm_encrypt_password($password, $salt_string);
		if($locked_flag == 'Y' || $active_ind == 0){
		    $return_value = 1;
		    $return_message = "The account is either inactive or locked. Please contact an administrator.";
		}else if($encrypted_password != $stored_password){
			//The passwords don't match
	    	$num_unsuccessful_logins = $user_data["num_unsuccessful_logins"] +1;
			mm_update_database_value('mm_admin_user', 'num_unsuccessful_logins',$num_unsuccessful_logins, 'i', 'user_id', $user_id);
			if($num_unsuccessful_logins >= 5 && $locked_flag == 'N'){
			    //The account now has more than 5 unsuccessful attempts so lock the account
			    mm_update_database_value('mm_admin_user', 'locked_flag', 'Y', 's', 'user_id', $user_id);
			}
			$return_value = 1;
			$return_message = "The password is incorrect";	
		}else{
			//The password is correct so build the return_array;
			if($num_unsuccessful_logins != 0){
			    //Reset the unsuccessful login counter
			    mm_update_database_value('mm_admin_user', 'num_unsuccessful_logins', 0, 'i', 'user_id', $user_id);
			}
			foreach($user_data as $key=>$value){
				$return_array["$key"] = $value;
			}	
		}
	}

	$return_array["return_value"] = $return_value;
	$return_array["return_message"] = $return_message;
	return $return_array;
}

/*********************************************************************************
Function mm_get_admin_users()
*********************************************************************************/
function mm_get_admin_users(){
	$return_array = array();
	$return_value = 0;
	$return_message = '';
	$sql_string = "select * from mm_admin_user order by active_ind asc, username desc";
	try{
                $conn = mm_get_pdo_connection();
                $stmt = $conn->prepare($sql_string);
                $stmt->execute();
                $return_array["results"] = $stmt->fetchAll(PDO::FETCH_ASSOC);
                $return_array["num_users"] = $stmt->rowCount();
        }catch(PDOException $e){
                $return_array["return_value"] = 1;
                $return_array["return_message"] = "There was an issue executing the query $sql_string.  Error: $e";
                $return_array["num_users"] = 0;
                $return_array["results"] = '';
		//Close the connection
		if(is_resource($conn)){
                $conn = null;
        	}


                return $return_array;
        }

        if(is_resource($conn)){
                $conn = null;
        }

	
	return $return_array;

}

/*********************************************************************************
Function mm_get_customer_eligible_amounts() for approval
*********************************************************************************/
function mm_get_customer_eligible_amounts($state, $tot_monthly_income_amt,$approved_apr,$pay_freq_1){
	$return_array = array();
	$return_value = 0;
	$return_message = '';
	$pay_cond = '';
	
	if($pay_freq_1 == "Paid Monthly" ){
	    $pay_cond='M';
	}else if($pay_freq_1 == "Bi-Weekly" ){
	    $pay_cond='B';
	}else if($pay_freq_1 == "Paid Twice Monthly" ){
	    $pay_cond='S';
	}else if($pay_freq_1 == "Weekly" || $pay_freq_1 == "Paid Weekly" ){
	    $pay_cond='B';
	}

	//echo "select loan_amt from mm_pricing_detail where apr='".$approved_apr."' and state='".$state."' and payment_freq='".$pay_cond."' and monthly_income_amt <='".$tot_monthly_income_amt."' order by loan_amt desc";
    
    echo "select distinct(loan_amt) from mm_pricing_detail where state='".$state."' order by loan_amt desc";
    
    //$sql_string = "select loan_amt from mm_pricing_detail where apr='".$approved_apr."' and state='".$state."' and payment_freq='".$pay_cond."' and monthly_income_amt <='".$tot_monthly_income_amt."' order by loan_amt desc";
    
    $sql_string = "select distinct(loan_amt) from mm_pricing_detail where state='".$state."' order by loan_amt desc";
    try{
                $conn = mm_get_pdo_connection();
                $stmt = $conn->prepare($sql_string);
                $stmt->execute();
                $return_array["results"] = $stmt->fetchAll(PDO::FETCH_ASSOC);
        }catch(PDOException $e){
                $return_array["return_value"] = 1;
                $return_array["return_message"] = "There was an issue executing the query $sql_string.  Error: $e";
                $return_array["results"] = '';
		//Close the connection
		if(is_resource($conn)){
                $conn = null;
        	}


                return $return_array;
        }

        if(is_resource($conn)){
                $conn = null;
        }

	
	return $return_array;

}
/*********************************************************************************
Function mm_get_state_aprs() for approval
*********************************************************************************/
function mm_get_state_aprs($state){
	$return_array = array();
	$return_value = 0;
	$return_message = '';

    $sql_string = "select distinct(apr) from mm_pricing_detail where state='".$state."' order by apr asc";
	try{
                $conn = mm_get_pdo_connection();
                $stmt = $conn->prepare($sql_string);
                $stmt->execute();
                $return_array["results"] = $stmt->fetchAll(PDO::FETCH_ASSOC);
        }catch(PDOException $e){
                $return_array["return_value"] = 1;
                $return_array["return_message"] = "There was an issue executing the query $sql_string.  Error: $e";
                $return_array["results"] = '';
		//Close the connection
		if(is_resource($conn)){
                $conn = null;
        	}


                return $return_array;
        }

        if(is_resource($conn)){
                $conn = null;
        }

	
	return $return_array;

}


?>
