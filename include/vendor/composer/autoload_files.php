<?php

// autoload_files.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'a0edc8309cc5e1d60e3047b5df6b7052' => $vendorDir . '/guzzlehttp/psr7/src/functions_include.php',
    'ddc0a4d7e61c0286f0f8593b1903e894' => $vendorDir . '/clue/stream-filter/src/functions.php',
    '8cff32064859f4559445b89279f3199c' => $vendorDir . '/php-http/message/src/filters.php',
    'f9e61f18f0291090ecf3f67a829046de' => $vendorDir . '/athari/yalinqo/YaLinqo/Linq.php',
    '9a31621970d4295d5f00acf4e277e6f9' => $vendorDir . '/stechstudio/backoff/src/helpers.php',
);
