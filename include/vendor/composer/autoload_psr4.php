<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'YaLinqo\\' => array($vendorDir . '/athari/yalinqo/YaLinqo'),
    'Violet\\StreamingJsonEncoder\\' => array($vendorDir . '/violet/streaming-json-encoder/src'),
    'Symfony\\Component\\OptionsResolver\\' => array($vendorDir . '/symfony/options-resolver'),
    'Simnang\\LoanPro\\' => array($vendorDir . '/simnang/loanpro-sdk/src'),
    'STS\\Backoff\\' => array($vendorDir . '/stechstudio/backoff/src'),
    'Psr\\Log\\' => array($vendorDir . '/psr/log/Psr/Log'),
    'Psr\\Http\\Message\\' => array($vendorDir . '/psr/http-message/src'),
    'Monolog\\' => array($vendorDir . '/monolog/monolog/src/Monolog'),
    'Http\\Promise\\' => array($vendorDir . '/php-http/promise/src'),
    'Http\\Message\\' => array($vendorDir . '/php-http/message-factory/src', $vendorDir . '/php-http/message/src'),
    'Http\\Discovery\\' => array($vendorDir . '/php-http/discovery/src'),
    'Http\\Client\\Socket\\' => array($vendorDir . '/php-http/socket-client/src'),
    'Http\\Client\\Curl\\' => array($vendorDir . '/php-http/curl-client/src'),
    'Http\\Client\\' => array($vendorDir . '/php-http/httplug/src'),
    'GuzzleHttp\\Psr7\\' => array($vendorDir . '/guzzlehttp/psr7/src'),
    'Clue\\StreamFilter\\' => array($vendorDir . '/clue/stream-filter/src'),
);
