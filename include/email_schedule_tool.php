<?php
	require "ffc_middleware.php";
	ini_set('display_errors', 1);
	$return_value = 0;
	$return_message = 1;
	$num_errors = 0;
	$error_array = array();
	$num_successful = 0;
	$num_emails_to_send = 0;
	$run_dt = new DateTime();
	$run_dt_string = $run_dt->format("Y-m-d H:i:s");

	//Get emails that are ready to send
	$sql_string = "select * from ffc_email_message_queue where status = 'READY TO SEND' and scheduled_dt <= '$run_dt_string'";
	echo "Query String is $sql_string\n";
 	try{
                $conn = ffc_get_pdo_connection();
                $results = $conn->query($sql_string);
                $email_to_send = $results->fetchAll();
		$n = count($email_to_send);
                $num_emails_to_send = count($email_to_send);
		echo "There are $num_emails_to_send emails to send\n";
                if($num_emails_to_send ==0){
			echo "There are no emails to send at this time\n";
                }else{
			//Loop through each email and attempt to send it
				$error_ind = 0;
			foreach($email_to_send as $email){			
				$attachment_array = array();
				$to = $email["email_to"];
				$from = $email["email_from"];
				$email_message_nbr = $email["email_message_nbr"];
				$to_name = $email["email_to_name"];
				$from_name = $email["email_from_name"];
				$attachment = $email["attachment"];
				$subject = $email["subject"];
				$txt_body = $email["txt_body"];
				$html_body = $email["html_body"];

				if($to_name != ''){
					$to = $to_name . " <$to>";
				}
				if($from_name != ''){
					$from = $from_name . " <$from>";
				}
			
				//Perform some validation
				if($attachment != ''){
					//There is an attachment with this email so make sure that the file exists
					if(!file_exists($attachment)){
						//The file doesn't exists so react accordingly
						echo "ERROR: The attachment file '$attachment' doesn't exist\n";
						ffc_update_database_value('ffc_email_message_queue', 'status', 'ERROR SENDING', 's', 'email_message_nbr', $email_message_nbr);
						ffc_update_database_value('ffc_email_message_queue', 'status_msg', 'ATTACHMENT NOT FOUND', 's', 'email_message_nbr', $email_message_nbr);
						$error_array[$num_errors] = "Didn't send email $email_message_nbr because attachment not found";
						$num_errors +=1;		
						$error_ind = 1;
					}else{
						$attachment_array[0] = $attachment;
					}
				}
				if($error_ind ==0){
					//There were no errors found so attempt to send the email
					$email_results = ffc_send_email($to, $from, $subject, $txt_body, $html_body, $attachment_array, 1);	
					if($email_results["return_value"] != 0){
						//There was an error sending the email so log this appropriately
						$error_array[$num_errors] = $email_results["return_message"];
						$num_errors +=1;
					}else{
						//There were no errors send the email so update the email record and data accordlingly
						$num_successful +=1;
						ffc_update_database_value('ffc_email_message_queue', 'status', 'SENT SUCCESSFULLY', 's', 'email_message_nbr', $email_message_nbr);
						ffc_update_database_value('ffc_email_message_queue', 'status_change_dt', "$run_dt_string", 's', 'email_message_nbr', $email_message_nbr);
					}
				}
				$error_ind = 0; //Reset the error indicator
			}

                }

        }catch(PDOException $e){
                $return_array["return_value"] = 1;
                $return_array["return_message"] = "$e";
                $return_array["sql_string"] = $sql_string;
                ffc_log_error('ffc_affordable_loan_amt', "$e");
                return $return_array;
        }

        if(is_resource($conn)){
                $conn = null;
        }


echo "Num Emails To Send: $num_emails_to_send\n";
echo "Num Errors: $num_errors\n";
echo "Num Successful: $num_successful\n";


?>
