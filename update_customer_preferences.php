<html>
<head><link rel="stylesheet" href="include/admin.css"></head>
<body>
<?php
include "include/header.php";
ini_set('display_errors',1);
$display_message = '';

if(isset($_SESSION["username"])){
	$username = $_SESSION["username"];
     if(isset($_GET["account_nbr"])){
		//The user id is set so get the user
		$account_nbr = $_GET["account_nbr"];
		$account_results = mm_get_account_details($_GET["account_nbr"]);
		$stored_pref_mail_acct_ind = $account_results["pref_mail_acct_ind"];
		$stored_pref_phone_mktg_ind = $account_results["pref_phone_mktg_ind"];
		$stored_pref_email_mktg_ind = $account_results["pref_email_mktg_ind"];
		$stored_pref_text_ind = $account_results["pref_text_ind"];	
		$stored_pref_aff_sharing_ind = $account_results["pref_aff_sharing_ind"];
		$stored_pref_arbitration_opt_out_ind = $account_results["pref_arbitration_opt_out_ind"];
		$application_nbr = $_GET["application_nbr"];

		
	}else if($_SERVER["REQUEST_METHOD"] == "POST"){
		//There has been a request to update the user info
		$account_nbr = $_POST["account_nbr"];
		$application_nbr = $_POST["application_nbr"];
		$account_results = mm_get_account_details($account_nbr);
		$stored_pref_mail_acct_ind = $account_results["pref_mail_acct_ind"];
		$stored_pref_phone_mktg_ind = $account_results["pref_phone_mktg_ind"];
		$stored_pref_email_mktg_ind = $account_results["pref_email_mktg_ind"];
		$stored_pref_text_ind = $account_results["pref_text_ind"];	
		$stored_pref_aff_sharing_ind = $account_results["pref_aff_sharing_ind"];
		$stored_pref_arbitration_opt_out_ind = $account_results["pref_arbitration_opt_out_ind"];
		
		$pref_mail_acct_ind = $_POST["pref_mail_acct_ind"];
	    $pref_phone_mktg_ind = $_POST["pref_phone_mktg_ind"];
		$pref_email_mktg_ind = $_POST["pref_email_mktg_ind"];
		$pref_text_ind = $_POST["pref_text_ind"];	
		$pref_aff_sharing_ind = $_POST["pref_aff_sharing_ind"];
		$pref_arbitration_opt_out_ind = $_POST["pref_arbitration_opt_out_ind"];
	
		
		//Check to see what fields changed
		if($stored_pref_mail_acct_ind != $pref_mail_acct_ind){
		    echo "Updating Mail Acct Indicator to $pref_mail_acct_ind<br>";
			mm_update_database_value("mm_account", 'pref_mail_acct_ind',$pref_mail_acct_ind , 's', 'account_nbr', $account_nbr, $username);
			$stored_pref_mail_acct_ind = $pref_mail_acct_ind;
				$display_message = "The customer preferences have been updated.";
		}
	
		if($stored_pref_phone_mktg_ind != $pref_phone_mktg_ind){
			mm_update_database_value("mm_account", 'pref_phone_mktg_ind',$pref_phone_mktg_ind , 's', 'account_nbr', $account_nbr, $username);	
			$stored_pref_phone_mktg_ind = $pref_phone_mktg_ind;
				$display_message = "The customer preferences have been updated.";
		}
		
			if($stored_pref_arbitration_opt_out_ind != $pref_arbitration_opt_out_ind){
			mm_update_database_value("mm_account", 'pref_arbitration_opt_out_ind',$pref_arbitration_opt_out_ind , 's', 'account_nbr', $account_nbr, $username);	
			$stored_pref_arbitration_opt_out_ind = $pref_arbitration_opt_out_ind;
				$display_message = "The customer preferences have been updated.";
		}

		if($stored_pref_email_mktg_ind != $pref_email_mktg_ind){
			mm_update_database_value("mm_account", 'pref_email_mktg_ind',$pref_email_mktg_ind , 's', 'account_nbr', $account_nbr, $username);
			$stored_pref_email_mktg_ind = $pref_email_mktg_ind;
				$display_message = "The customer preferences have been updated.";
		}
		
		if($stored_pref_aff_sharing_ind != $pref_aff_sharing_ind){
	    	mm_update_database_value("mm_account", 'pref_aff_sharing_ind',$pref_aff_sharing_ind , 's', 'account_nbr', $account_nbr, $username);
	    	$stored_pref_aff_sharing_ind = $pref_aff_sharing_ind;
	    		$display_message = "The customer preferences have been updated.";
		}
		
		if($stored_pref_text_ind != $pref_text_ind){
			mm_update_database_value("mm_account", 'pref_text_ind',$pref_text_ind , 's', 'account_nbr', $account_nbr, $username);	
			$stored_pref_text_ind = $pref_text_ind;
			$display_message = "The customer preferences have been updated.";
		}

	}else{
		//The user has permission but hasn't specified a user and doesn't 
		$display_message = "You must specify an account to edit.  Click <a href=\"manage_admin_users.php\">here</a> to find an account.";
	}
?>

<h2>Update Customer Preferences</H2>
<?php if($display_message != ''){echo "<p style=\"color:red\">$display_message</p>";}
		//User has permission to view this page so get the list of users and display a table
			//No user ID is set so set the display message
			echo "<form method = \"POST\" action=\"update_customer_preferences.php\">";	
			//output the stored variables for comparison
			
			$pref_mail_acct_ind_y_value = $stored_pref_mail_acct_ind == 'Y' ? "Selected" : "";
			$pref_mail_acct_ind_n_value = $stored_pref_mail_acct_ind == 'N' ? "Selected" : "";
			$pref_phone_mktg_ind_y_value = $stored_pref_phone_mktg_ind == 'Y' ? "Selected" : "";
			$pref_phone_mktg_ind_n_value = $stored_pref_phone_mktg_ind == 'N' ? "Selected" : "";
			$pref_email_mktg_ind_y_value = $stored_pref_email_mktg_ind == 'Y' ? "Selected" : "";
			$pref_email_mktg_ind_n_value = $stored_pref_email_mktg_ind == 'N' ? "Selected" : "";
			$pref_text_ind_y_value = $stored_pref_text_ind == 'Y' ? "Selected" : "";
			$pref_text_ind_n_value = $stored_pref_text_ind == 'N' ? "Selected" : "";
			$pref_aff_sharing_ind_y_value = $stored_pref_aff_sharing_ind == 'Y' ? "Selected" : "";
			$pref_aff_sharing_ind_n_value = $stored_pref_aff_sharing_ind == 'N' ? "Selected" : "";
			$pref_arbitration_opt_out_ind_y_value = $stored_pref_arbitration_opt_out_ind == 'Y' ? "Selected" : "";
			$pref_arbitration_opt_out_ind_n_value = $stored_pref_arbitration_opt_out_ind == 'N' ? "Selected" : "";
			
		//	$pref_phone_mktg_ind_y_value = $stored_pref_phone_mktg_ind
			
			echo "<input type=\"hidden\" name=\"account_nbr\" value=\"$account_nbr\"/>";
			echo "<input type=\"hidden\" name=\"application_nbr\" value = \"$application_nbr\"";
			echo "<label>Mail Preference:    </label> <select name=\"pref_mail_acct_ind\" ><option value=\"Y\" $pref_mail_acct_ind_y_value>Y</option><option value=\"N\" $pref_mail_acct_ind_n_value>N</option></select><br>";
			echo "<label>Phone Marketing Preference:    </label> <select name=\"pref_phone_mktg_ind\"><option value=\"Y\" $pref_phone_mktg_ind_y_value>Y</option><option value=\"N\" $pref_phone_mktg_ind_n_value >N</option></select><br>";
			echo "<label>Email Marketing Preference:    </label> <select name=\"pref_email_mktg_ind\" ><option value=\"Y\" $pref_email_mktg_ind_y_value>Y</option><option value=\"N\" $pref_email_mktg_ind_n_value>N</option></select><br>";
			echo "<label>Phone Texting Preference:    </label> <select name=\"pref_text_ind\" ><option value=\"Y\" $pref_text_ind_y_value>Y</option><option value=\"N\" $pref_text_ind_n_value>N</option></select><br>";
			echo "<label>Affiliate Sharing Preference:    </label> <select name=\"pref_aff_sharing_ind\" ><option value=\"Y\" $pref_aff_sharing_ind_y_value>Y</option><option value=\"N\" $pref_aff_sharing_ind_n_value>N</option></select><br>";
			echo "<label>Arbitration Opt Out Preference:    </label> <select name=\"pref_arbitration_opt_out_ind\" ><option value=\"Y\" $pref_arbitration_opt_out_ind_y_value>Y</option><option value=\"N\" $pref_arbitration_opt_out_ind_n_value>N</option></select><br>";
			echo "<br> <input type=\"submit\" value=\"Update Customer Preferences\"><br><br><a href=\"application_details.php?application_nbr=$application_nbr\">   Return To Application Summary</a><br>";
			
?>


<?php
}else{
include "include/login.php";
}
?>


</body>
</html>