<?php
include("include/start.php");


/**
 ** Creates HTML For Each List To Be Displayed
 **
 ** @param string $state    - State
 ** @param int    $app_stat - Status
 ** @param string $id       - Anchor ID
 ** @param string $list     - Anchor Text
 ** @param null   $prev     - Previous Anchor ID For Navigation
 ** @param null   $next     - Next Anchor ID For Navigation
 **
 ** @return string $html
**/
function make_list($state, $app_stat, $id = "List_ID", $list = "List Name", $prev = null, $next = null)
{
    $show_reason = (8 == $app_stat || 9 == $app_stat);
    $apps = mm_get_application_details_by_status($app_stat, $state);
    $prev = (null == $prev ? '' : '<a style="margin-left: 20px;" href="#' . $prev . '" >previous</a>');
    $next = '<a style="margin-left: 20px;" href="#' . $next . '" >next</a>' ;
    $html = '        <a id="' . $id . '"><H3 style="display: inline-block;">' . $list . '</H3></a>' . $prev . $next . '<br />' . PHP_EOL;

    if(0 == $apps["num_apps"])
    {
        $html .= "        There are no applications in this status<p />" . PHP_EOL;
    }
    else
    {
        $i = 0;    //    Model Reason DIVs
        $html .= '        There are ' . $apps["num_apps"] . ' in this status.
        <table style="width: 100%;" >
            <tr>
                <th style="width: 180px; white-space: nowrap;" >Application Number</th>
                <th style="width: 170px; white-space: nowrap; text-align: center;" >Create Date</th>
                <th style="width: 70px; white-space: nowrap; text-align: center;" >State</th>
                <th' . ($show_reason ? ' style="width: 260px; white-space: nowrap;" ' : '') . '>Customer Name</th>
' . ($show_reason ? '                <th>Reason</th>' : '' ) . PHP_EOL . '            </tr>' . PHP_EOL;

        foreach($apps["app_data"] as $app)
        {
            $reason = '';

            if ($show_reason)
            {
                $reason_data_substr = '';
                $reason_data        = '';
                //Code start - modified by Swapna for story no 333 on July 31st 2018
                //$uw_decision_q_data = mm_get_uw_request_queue_details($application_nbr);
                $uw_decision_data = mm_get_uw_decision_details($app["uw_decision_nbr"]);
                $decision_data = $uw_decision_data["decision_data"];
                
                if(isset($decision_data["app_status_desc"]))
                {
                    $reason = trim($decision_data["app_status_desc"]);
                }
                
                if (strlen($reason) > 60)
                {
                   $reason = PHP_EOL . '                <td>
                    <a id="errorBtn_' . $i . '" class="errorBtnCls" onclick="openModal(' . $i . ')" >' . substr($reason,0,60) . '.....</a>
                    <div id="errorModal_' . $i . '" class="modal" style="display:none;">
                        <div class="modal-content">
                            <span class="close" onclick="closeModal(' . $i++ . ')" >&times;</span>
                            <p id="reason_paragraph">' . $reason . '</p>
                        </div>
                    </div>
                </td>' . PHP_EOL ;
                }
                else
                {
                    $reason = PHP_EOL . '                <td>
                        ' . $reason .'
                 </td>' . PHP_EOL ;
                 }
             }
            
            $html .= '            <tr>
                <td><a href="application_details.php?application_nbr=' . $app["application_nbr"] . '">' . $app["application_nbr"] . '</td>
                <td style="text-align: center; white-space: nowrap;">' . $app["create_dt"] . '</td>
                <td style="text-align: center;">' . $app["state"] . '</td>
                <td style="white-space: nowrap;">' . $app["first_name"] . ' ' . $app["last_name"] . '</td>' . PHP_EOL;
            $html .= $reason;
            $html .= '            </tr>' . PHP_EOL;
        }
        
        $html .= '    </table>' . PHP_EOL;
    }
    
    return $html;
}

if(isset($_SESSION["username"]))
{
    $state = (isset($_POST["post_state"]) ? $_POST["post_state"] : '');

    $html = '';
    $html .= make_list($state,    9, "errors",    "Error Applications", null, "organic");
    $html .= make_list($state, 9999, "organic",   "Organic Loan Applications", "errors", "loan_amt");
    $html .= make_list($state,    2, "loan_amt",  "Pending Loan Amount Applications", "organic","accepted");
    $html .= make_list($state,    3, "accepted",  "Status 3 Apps", "loan_amt", "documents");
    $html .= make_list($state,    4, "documents", "Pending Customer Documents", "accepted", "agent");
    $html .= make_list($state,    5, "agent",     "Pending Agent Review", "documents", "signature");
    $html .= make_list($state,    6, "signature", "Pending Customer Signature", "agent", "approved");
    $html .= make_list($state,    7, "approved",  "Approved", "signature", "declined");
    $html .= make_list($state,    8, "declined",  "Declined Applications", "approved", "expired");
    $html .= make_list($state,   10, "expired",   "Expired Applications", "declined", "draft");
    $html .= make_list($state,    1, "draft",     "Draft Applications", "expired", "voided");
    $html .= make_list($state,   11, "voided",    "Voided Applications", "draft");
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="stylesheet" href="include/admin.css" />
        <script type="text/javascript">
            function openModal(id)
            {
                //alert('openModal');
                
                var modal = document.getElementById('errorModal_' + id);
                modal.style.display = "block";
            }
            
            function closeModal(id)
            {
                //alert('closeModal');
                
                var modal = document.getElementById('errorModal_' + id);
                modal.style.display = "none";
            }
        </script>
    </head>
    <body>
<?php
    include("include/title.php");
?>
        <h2>Applications by Status</h2>
        <form method="POST" action="applications_by_status.php" >
            <h3 style="display: inline-block;" >Filter by State:</h3>&nbsp;
            <select name="post_state">
                <option <?php if (''   == $state) { echo 'selected="selected"'; } ?> value="">All States</option>
                <option <?php if ('AL' == $state) { echo 'selected="selected"'; } ?> value="AL">AL - Alabama</option>
                <option <?php if ('CA' == $state) { echo 'selected="selected"'; } ?> value="CA">CA - California</option>
                <option <?php if ('DE' == $state) { echo 'selected="selected"'; } ?> value="DE">DE - Delaware</option>
                <option <?php if ('ID' == $state) { echo 'selected="selected"'; } ?> value="ID">ID - Idaho</option>
                <option <?php if ('IL' == $state) { echo 'selected="selected"'; } ?> value="IL">IL - Illinois</option>
                <option <?php if ('Ms' == $state) { echo 'selected="selected"'; } ?> value="MS">MS - Mississippi</option>
                <option <?php if ('MO' == $state) { echo 'selected="selected"'; } ?> value="MO">MO - Missouri</option>
                <option <?php if ('NM' == $state) { echo 'selected="selected"'; } ?> value="NM">NM - New Mexico</option>
                <option <?php if ('SC' == $state) { echo 'selected="selected"'; } ?> value="SC">SC - South Carolina</option>
                <option <?php if ('UT' == $state) { echo 'selected="selected"'; } ?> value="UT">UT - Utah</option>
                <option <?php if ('WI' == $state) { echo 'selected="selected"'; } ?> value="WI">WI - Wisconsin</option>
            </select>
            &nbsp;&nbsp;<button>Filter</button>
        </form>
        <br />
        <table style="width: 75%;">
            <tr>
                <td><a href="#draft">Draft Applications</a></td>
                <td><a href="#errors">Error Applications</a></td>
                <td><a href="#documents">Pending Documents</a></td>
                <td><a href="#loan_amt">Pending Loan Amount</a></td>
            </tr>
            <tr>
                <td><a href="#accepted">Status 3 Apps</a></td>
                <td><a href="#agent">Pending Agent Review</a></td>
                <td><a href="#signature">Pending Signature</a></td>
                <td><a href="#approved">Approved Applications</a></td>
            </tr>
             <tr>
                <td><a href="#declined">Declined Applications</a></td>
                <td><a href="#voided">Voided Applications</a></td>
                <td><a href="#expired">Expired Applications</a></td>
                <td><a href="#organic">Organic</a></td>
            </tr>
        </table>
<?php
    echo $html;
}
else
{
    include("include/login.php");
}
?>
    </body>
</html>
