
<html>
<head><title>Add A Document</title></head>
<body>
<?php

include "include/header.php";
ini_set('display_errors',1);
ini_set('upload_max_filesize', '7M');
$display_message = '';

//Initialize Variables
$txt_body = '';
if(isset($_SESSION["username"])){
        $username = $_SESSION["username"];
        if(isset($_GET["application_nbr"])){
                $application_nbr = $_GET["application_nbr"];
        }else{
                $application_nbr = '';
        }
            
        if(isset($_POST["submit"])){
                //Comment here to validate the form data
                //The customer submitted the form so add the note
                echo "Files<br>";
                $application_nbr = $_POST["application_nbr"];
                $file_size = ini_get('upload_max_filesize');
                echo "File Size: $file_size<br>";
                foreach($_FILES["uploaded_document"] as $key=>$values){
                    echo "$key: $values<br>";
                }
                
                $application_data = mm_get_application_details($application_nbr);
                $app_data = $application_data["app_data"];
                $account_nbr = $app_data["account_nbr"];
                $file_name=$_FILES["uploaded_document"]["name"];
                $file_string_array = explode(".", $file_name);
                $file_string_size = sizeof($file_string_array);
                $file_extn = strtolower($file_string_array[$file_string_size -1]);
                $target_file = mm_get_document_storage_path();
                $target_file .= "$account_nbr/$application_nbr/";
                $target_file .= $file_name;
                    $temp_jason = $_FILES["uploaded_document"]["tmp_name"];
                    echo "File Name: $temp_jason<br>";
                    echo "Target File: $target_file<br>";
                if(file_exists($target_file)) {
                    $display_message = "Sorry, file already exists.";
                }else if($file_extn != 'txt' && $file_extn != 'gif' && $file_extn != 'jpg' && $file_extn != 'jpeg' && $file_extn != 'pdf' && $file_extn != 'htm' && $file_extn != 'html' && $file_extn != 'png'){
                    $display_message = "The document was not uploaded.  The allowable file types are pdf, htm, html, jpg, jpeg, png, gif, or txt";
                }else{
                   
                    $temp_result = move_uploaded_file($_FILES["uploaded_document"]["tmp_name"], $target_file);
                    if($temp_result == false){
                        $display_message = "There was a problem uploading the file";
                    }else{
                        $temp_array = array();
                        $tmp_dt = new DateTime();
                        $create_dt = $tmp_dt->format("Y-m-d H:i:s");

                        $temp_array["username"] = $username;
                        $temp_array["create_dt"] = $create_dt;
                        $temp_array["document_path"] = $target_file;
                        $temp_array["application_nbr"] = $application_nbr;

                       $document_json = json_encode($temp_array);

                        $document_results = mm_upload_application_document($document_json);
                    if($document_results["return_value"] != 0 ){
                            //The document addition failed so log an error
                            $return_message = $document_results["return_message"];
                            $return_value = $document_results["return_value"];
                            $display_message = "There was a problem adding the document at this time.  Please try again. $return_message $return_value";
                            unlink($target_file);
                    }else{
                        $display_message = "The document was successfully added.";
                }
                       
                    }
                }
                
                
        }
?>

<h2>Upload Application Document</H2>

<?php
        if($display_message != ''){
            echo "<font color=\"red\"><b>$display_message</b></font><br><br>"   ;
        }
        if($application_nbr != ''){

        ?>
            <form method="Post" action="" enctype="multipart/form-data">


            <label>Application Number: <?php echo "$application_nbr";?><br></label><input name="application_nbr" type="hidden" value=<?php echo "$application_nbr";?>></input>
            <label>Select A Document</label><input type="file" name="uploaded_document"></input><br>
            <input type="Submit" name="submit" value="Add Document"><br><br>
            <a href="application_details.php?application_nbr=<?php echo "$application_nbr";?>">Return To Application_Summary</a>
            </form>
        <?php
                }else{
                        echo "In order to upload a document, you must first select an application.  Click <a href=\"application_details.php\">here</a> to find an application.<br>";
                }
?>

<?php
        }else{
                include "include/login.php";
        }
?>



