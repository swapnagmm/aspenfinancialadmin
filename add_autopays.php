<html><body>
<?php
	include "include/header.php";
	require "include/ach_middleware.php";
	ini_set('display_errors', 1);
	
	if(isset($_SESSION["username"])){
		 $ach_permissions = $_SESSION["permissions_ach_mgmt"] == "Y" ? "Y" :"N";
		if($ach_permissions == "Y"){
	    		if($_SERVER["REQUEST_METHOD"] == "POST"){
	    			if(isset($_POST["display_id"]) && isset($_POST["loan_id"])){
		    			//Insert code here to run the ach deposits process
		    			$display_id = $_POST["display_id"];
		    			$loan_id = $_POST["loan_id"];
		    			echo($display_id . $loan_id);
			    		$autopay_results = mm_lp_create_autopays($display_id, $loan_id);
    					$autopay_return_value = $autopay_results["return_value"];
	    				$autopay_return_message = $autopay_results["return_message"];
		    			$autopay_num_errors = $autopay_results["num_errors"];
    					$autopay_error_array = $autopay_results["error_array"];
    				
    		    		$json_message = json_encode($autopay_results);
    		    		echo "$json_message\n";
	    			}
	    	
	    		}

?>
<form method ="POST" action="">
	<H2>Set up Loanpro Autopay</h2>
	<input type="text" name="display_id" checked><label for="display_id">LoanPro Display ID</label><br><br>
	<input type="text" name="loan_id" checked><label for="loan_id">LoanPro Loan Id</label><br><br>
	
	<input type="submit" name="ach_submit" value="Create Autopays">
</form>
<?php
	}else{
		echo "<p style=\"color:red\">You do not have permission to view this page.  Please contact your system administrator.</p>";
	}
    }else{
        include "include/login.php";
    }

?>
</body>
<html>
