<?php
//include "include/header.php";
session_start();
include "include/admin_functions.php";
$num_apps = 0;
$app_history = array(array());
$create_dt = '';

if(isset($_SESSION["username"])){
    $username = $_SESSION["username"];
    //Check to see if the GET statement is populated as expected
    if(isset($_GET["document_nbr"]) || (isset($_GET["application_nbr"]) && isset($_GET["document_type"]))){
	    //Either a document number or a application number and document type has been provided.
	    if(isset($_GET["document_nbr"])){
	        //The URL contains a document number so try to look this up
	        $document_nbr = $_GET["document_nbr"];
	        $document_details = mm_get_application_document_details($document_nbr);
	        if($document_details["return_value"] !=0){
	            $display_message = "Unable to find a document with a document number of $document_nbr. Please contact a system administrator.";
	        }else{
	            $document_data = $document_details["results"];
	            $document_path = $document_data[0]["document_path"];
	            //This code will work as long as the primary folder is titled customer documents
	            $document_path_arr = explode('customer_documents', $document_path);
	            $document_path = "/customer_documents". $document_path_arr[1];
	            $document_array = explode(".", $document_path);
	            $size_arr = sizeof($document_array);
	            $stored_document_type = strtolower($document_array[$size_arr -1]);
	        }
	        
	    }else{
	        //No document number was provided so it must be an application number and document type
	        $stored_document_type = "pdf";
	        $application_nbr = $_GET["application_nbr"];
	        $document_type = $_GET["document_type"];
	        if($document_type != 'LA' && $document_type != 'APP' && $document_type != 'LA_SIGNED' && $document_type != 'NOAA'){
	            //The document type is not exepcted
	            $display_message = "The document type of $document_type was not expected.  Please contact your system administrator to report this error.";
	        }else{
	            //The document type was expected
	            $app_details = mm_get_application_details($application_nbr);
	            if($app_details["return_value"] ==1){
	                $display_message = "There was a problem getting the application details.";
	            }else{
	                 $app_data = $app_details["app_data"];
	                 $account_nbr = $app_data["account_nbr"];
	                 $document_path = "/customer_documents/$account_nbr/$application_nbr/";
	                 
	                 switch ($document_type) {
                        case "LA":
                        $document_name = "loan_agreement_". $application_nbr . ".pdf";
                        break;
                        case "LA_SIGNED":
                        $document_name = "loan_agreement_". $application_nbr . " - signed.pdf";
                        break;
                        case "NOAA":
                        $document_name = "noaa_". $application_nbr . ".pdf";
                        break;
                        case "APP":
                        $document_name = "application_". $application_nbr . ".pdf";
                        break;
                    }
                    
                    $document_path .= $document_name;
    
                }
	        }
	        
	    }
	  }else{
	      //No document number of application and document type were provided
	      $display_message = "In order to view a document, you must provide a document number or application number and document type.";
	  }
	  
	  if(isset($display_message)){
	      echo "<p>$display_message</p>";
	  }else{
	      $conn_details = array();
	      $conn_details = mm_get_ftp_details();
	      $ftp_server = $conn_details["server"];
	      $ftp_username = $conn_details["username"];
	      $ftp_password = $conn_details["password"];
	      $conn_id = ftp_connect($ftp_server);
	      $login_results = ftp_login($conn_id, $ftp_username, $ftp_password);
	      //echo "Document Path is $document_path<br>";
            $size = ftp_size($conn_id, $document_path);
             header("Content-Disposition: inline");
            header("Content-Length: $size"); 
            
            if($stored_document_type == 'pdf'){
                  header("Content-Type: application/pdf");
            }else if($stored_document_type == 'gif'){
                  header("Content-Type: image/gif");
            }else if($stored_document_type == 'png'){
                header("Content-Type: image/png");
            }else if($stored_document_type == 'jpg' || $stored_document_type == 'jpeg'){
                header("Content-Type: image/jpeg");
            }else if($stored_document_type == 'html' || $stored_document_type == 'htm'){
                header("Content-Type: text/html");
            }else if($stored_document_type == 'txt'){
                header("Content-Type: text/plain");
            }else{
                //The document is a type that was unexpected so throw an error
                
            }
                  
                  
            //echo "Doc Path is: $document_path\n";
            //echo "Doc Extention is $stored_document_type";

            ftp_get($conn_id, "php://output", $document_path, FTP_BINARY);
            
            ftp_close($conn_id);
            
	  }
	  

}else{
    include "include/login.php";
}
?>
