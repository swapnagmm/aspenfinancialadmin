<html>
<head><link rel="stylesheet" href="include/admin.css"></head>
<body>
<?php
include "include/header.php";
require "include/mm_funding_returns.php";
ini_set('display_errors',0);

?>

<h2>Void Applications</h2>
<form action="" method="post" enctype="multipart/form-data">
<h3>Select .csv to upload:</h3>
<input type='file' name='filename'/>
<input type='submit' name='submit' value='Upload File'/>
</form>
<?php
if (isset($_POST['submit'])) 
 {
    $file = fopen($_FILES['filename']['tmp_name'], "r");
    $IdArray = array();
    $results = array();
    while(!feof($file)){
        $row = fgetcsv($file, 1000);
        if($row[0] !== null) {
            array_push($IdArray, $row[0]);
        }
    }

    $fundingReturns = new mm_funding_returns;
    foreach($IdArray as $Id){
        $result = $fundingReturns->mm_start_fundingreturn_process($Id);
        if($result !== true) {
            array_push($results, $result);
        }
    }
    fclose($handle);
     if(count($results) > 0)
     {
         echo "<h3 style=\"color:red\">ID's not processed</h3>";
         foreach($results as $err)
         {
             echo "<h4 style=\"color:red\">$err</h4>\n";
         }
     }
     else
     {
         echo "<p style=\"color:green\">All records updated</p>";
     }
 }

?>


</body>
</html>
