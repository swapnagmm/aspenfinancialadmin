<html><body>
<?php
	include "include/header.php";
	require "include/ach_middleware.php";
	ini_set('display_errors', 1);
	
	if(isset($_SESSION["username"])){
	    $ach_permissions = $_SESSION["permissions_ach_mgmt"] == "Y" ? "Y" :"N";
		if($ach_permissions == "Y"){
	        if($_SERVER["REQUEST_METHOD"] == "POST"){
	    	    if(isset($_POST["ach_deposits"])){
		    	//Insert code here to run the ach deposits process
			    $ach_funding_arr = mm_process_ach_funding();
    			$ach_funding_return_value = $ach_funding_arr["return_value"];
	    		$ach_funding_return_message = $ach_funding_arr["return_message"];
		    	$ach_funding_num_errors = $ach_funding_arr["num_errors"];
    			$ach_funding_num_warnings = $ach_funding_arr["num_warnings"];
	    		$ach_funding_num_detail_records = $ach_funding_arr["num_funding_records"];
		    	$ach_funding_ach_file_nbr = $ach_funding_arr["ach_file_nbr"];
    			$ach_funding_ach_file_name = $ach_funding_arr["ach_file_name"];
	    		$ach_funding_error_array = $ach_funding_arr["error_array"];
		    	$ach_funding_warning_array = $ach_funding_arr["warning_array"];
    			echo "<br><br>********** Gathering the Funidng Records ****************<br><br>";
	    		echo "Funding Record Return Value: $ach_funding_return_value<br>";
		    	if($ach_funding_return_value !=0){
        			echo "Funding Record Return Message: $ach_funding_return_message<br>";
			    }
    			echo "ACH File Name: $ach_funding_ach_file_name<br>";
	    		echo "ACH File Number: $ach_funding_ach_file_nbr<br>";
		    	echo "Num Succesful ACH Records: $ach_funding_num_detail_records<br>";
    			echo "Num Errors: $ach_funding_num_errors<br>";
	    		if($ach_funding_num_errors != 0){
        			$error_array = $ach_funding_error_array;
        			foreach($error_array as $key=>$value){
                			echo "ERROR:    $value<br>";
        			}
		    	}
			    echo "Num Warnings: $ach_funding_num_warnings<br>";
			    if($ach_funding_num_warnings != 0){
        			foreach($ach_funding_warning_array as $key=>$value){
                			echo "WARNING:          $value<br>";
        			}
			    }
			
		    }
		    if(isset($_POST["ach_payments"])){
    			//Insert code here to run the ach deposits process
	    		echo "Running ACH Payments Process<br>";
		    	//Get the payment transactions
			    $ach_payment_arr = mm_process_ach_payments();
    			$ach_payment_return_value = $ach_payment_arr["return_value"];
	    		$ach_payment_return_message = $ach_payment_arr["return_message"];
		    	$ach_payment_num_payments = $ach_payment_arr["num_payments"];
    			$ach_payment_num_errors = $ach_payment_arr["num_errors"];
	    		$ach_payment_num_warnings = $ach_payment_arr["num_warnings"];
		    	$ach_payment_error_array = $ach_payment_arr["error_array"];
		    	$ach_payment_warning_array = $ach_payment_arr["warning_array"];

			//Output the results of the ACH Payments Process
    			echo "<br><br>**********Gathering ACH PAYMENTS***********<br><br>";
	    		echo "Payment Return Value: $ach_payment_return_value<br>";
		    	if($ach_payment_return_value != 0){
        			echo "Payment Return Message: $ach_payment_return_message<br>";
			    }
			    echo "Num Successful Payments Selected: $ach_payment_num_payments<br>";
		    	echo "Num Errors: $ach_payment_num_errors<br>";
		    	if($ach_payment_num_errors != 0){
        	    		foreach($ach_payment_error_array as $key=>$value){
                			echo "          $value<br>";
        		    	}
			    }
			    echo "Num Warnings: $ach_payment_num_warnings<br>";
			    if($ach_payment_num_warnings != 0){
        			foreach($ach_payment_warning_array as $key=>$value){
                			echo "          $value<br>";
        			}
			    }

		    }
		    if(isset($_POST["build_nacha_records"])){
			    //Insert code here to run the ach deposits process
			    echo "<br><br>*********Building Nacha File Records **************** <br><BR>";
    			$ach_nacha_build_results = mm_ach_build_nacha_recs_wrapper();
	    		if($ach_nacha_build_results["return_value"] != 0){
        			//There was a problem building the nacha records
        			$ach_nacha_build_error_message = $ach_nacha_build_results["return_err_message"];
        			$ach_nacha_build_error_status = $ach_nacha_build_results["return_err_status"];
        			echo "There was a problem building the nacha file records.  The error status is $ach_nacha_build_error_status and the message is $ach_nacha_build_error_message";
		    	}
		    }
		    if(isset($_POST["build_nacha_files"])){
    			//Insert code here to run the ach deposits process
	    		$ach_nacha_file_build_results = mm_ach_create_nacha_file_wrapper();
                	if($ach_nacha_file_build_results["return_value"] != 0){
                        	//Insert error handling in case the file(s) couldn't be built
                        	$return_value = 1;
                        	$return_err_message = $ach_nacha_build_results["return_err_message"];
                        	$return_err_status = $ach_nacha_build_results["return_err_status"];
                        	$return_message = "Failed generating the NACHA FILES. Erorr Message: $return_err_message and Status: $return_err_status";
			        }
		    }
		    if(isset($_POST["ftp_nacha_files"])){
			    //Insert code here to run the ACH ftp process
    			$ftp_results = mm_ach_nacha_ftp_wrapper();
    			$ftp_return_value = $ftp_results["return_value"];
    			$ftp_return_message = $ftp_results["return_message"];
    			$ftp_num_errors = $ftp_results["num_errors"];
    			$ftp_num_files = $ftp_results["num_files"];
    			$ftp_num_successful = $ftp_results["num_successful"];
    			$ftp_error_array = $ftp_results["error_array"];
    			$ftp_successful_files_array = $ftp_results["successful_files_array"];
    			
    			echo "<br><br>***********FTP'ing ACH FILES ***************************<br><br>";
    			echo "Return Value: $ftp_return_value<br>";
    			echo "Return Message: $ftp_return_message<br>";
    			echo "Num Files To FTP: $ftp_num_files<br>";
    			echo "Num Successful: $ftp_num_successful<br>";
    			if($ftp_num_successful >0){
    			    foreach($ftp_successful_files_array as $filename){
    			        echo "File: $filename<br>";
    			    }
    			        echo "<br>";
    			}
    			echo "Num Errors: $ftp_num_errors<br>";
    			if($ftp_num_errors >0){
    			    //Loop throguh error array and output the error message
    			    foreach($ftp_error_arry as $error){
    			        echo "Error: $error<br>";
    			    }
    			        echo "<br>";
    			}
    			
    			
	    	}
	    }

?>
<form method ="POST" action="">
	<H2>ACH Processing Interface</h2>
	<input type="checkbox" name="ach_deposits" checked><label for="ach_deposits">Run ACH Deposits</label><br><br>
	<input type="checkbox" name="ach_payments" checked><label for="ach_payments">Run ACH Payments</label><br><br>
	<input type="checkbox" name="build_nacha_records" checked><label for="build_nacha_records">Build Nacha File Records</label><br><br>
	<input type="checkbox" name="build_nacha_files" checked><label for="build_nacha_files">Build Nacha Files</label><br><br>
	<input type="checkbox" name="ftp_nacha_files" checked><label for="ftp_nacha_files">FTP Nacha Files</label><br><br>
	<input type="submit" name="ach_submit" value="RUN ACH PROCESS">
</form>
<?php
		}else{
		    echo "<p style=\"color:red\">You do not have permission to view this page.  Please contact your system administrator.";
		}
    }else{
        include "include/login.php";
    }

?>
</body>
<html>
