<?php
include("include/start.php");

$num_apps    = 0;
$app_history = array(array());
$create_dt   = '';

if (isset($_SESSION["username"]))
{
    $username = $_SESSION["username"];
    $state = '';
    
    if (isset($_POST["email"])
      || isset($_POST["first_name"])
      || isset($_POST["last_name"])
      || isset($_POST["phone_nbr"])
      || isset($_POST["app_nbr"])
      || isset($_POST["state"])
       )
    {
        $email       = isset($_POST["email"])      ? $_POST["email"]      : '';
        $first_name  = isset($_POST["first_name"]) ? $_POST["first_name"] : '';
        $last_name   = isset($_POST["last_name"])  ? $_POST["last_name"]  : '';
        $phone_nbr   = isset($_POST["phone_nbr"])  ? $_POST["phone_nbr"]  : '';
        $app_nbr     = isset($_POST["app_nbr"])    ? $_POST["app_nbr"]    : '';
        $state       = isset($_POST["state"])      ? $_POST["state"]      : '';
        
        $app_list    = mm_search_applications($email, $first_name, $last_name, $phone_nbr, $app_nbr, $state);
        
        $num_apps    = $app_list["num_apps"];
        $app_history = $app_list["app_data"];
    }
    elseif (isset($_POST["new_app_status"]) && isset($_POST['change_app_status_btn']))
    {
        $new_app_status = $_POST["new_app_status"];
        $application_nbr = $_POST["application_nbr"];
        //ERROR Handling - Add handling of the POST VALUES rather than just passing the array
        $status_change_results = mm_change_application_status($_POST);
        
        if ($status_change_results["return_value"] == 0)
        {
            $display_message = "The status was updated successfully";
            $_GET["application_nbr"] = $application_nbr;
        }
        else
        {
            $error_message = $status_change_results["return_message"];
            $display_message = "There was a problem updating the application_status.  $error_message";
        }
        
        header('Location:'.$_SERVER['PHP_SELF']."?application_nbr=".$_GET["application_nbr"]);
    }
    elseif (isset($_POST["verified_gross_income"]))
    {
        $arr['verified_gross_income']=  $_POST["verified_gross_income"];
        $arr['application_nbr'] = $_POST["application_nbr"];;
        $gros_income_change_results = mm_verify_gross_income($arr);;
        
        if($gros_income_change_results["return_value"] == 0)
        {
            $display_message = "The verified gross  was updated successfully";
            $_GET["application_nbr"] = $application_nbr;
        }
        else
        {
            $error_message = $status_change_results["return_message"];
            //$display_message = "There was a problem updating the verified gross income.  $error_message";
            $display_message = "";
        }
        
        header('Location:'.$_SERVER['PHP_SELF']."?application_nbr=".$_GET["application_nbr"]);
    }
    
    if (isset($_GET["application_nbr"]))
    {
        //There is a URL string with an application number so look the account up based on this
        $application_nbr = $_GET["application_nbr"];
        $application_details = mm_get_application_details($_GET["application_nbr"]);
        
        $return_value = $application_details["return_value"];
        
        if ($return_value == 0)
        {
            $app_data = $application_details["app_data"];
            
            foreach ($app_data as $key => $value)
            {
                $app_history[0][$key] = $value;
            }
            
            $num_apps = 1;
        }
        else
        {
            $num_apps = 0;
        }
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="stylesheet" href="include/admin.css" />
    </head>
    <body>
<?php
    include("include/title.php");
?>
        <h2>Application Search</H2>
        <form method="Post" action="application_details.php" accept-charset="utf-8">
            <label>Email Address:</label><input type="text" name="email" />
            <label>First Name:</label><input type="text" name="first_name" />
            <label>Last Name:</label><input type="text" name="last_name" />
            <label>Phone Number:</label><input type="text" name="phone_nbr" />
            <label>App Number:</label><input type="text" name="app_nbr" />
            <label>State:</label>
            <select name="state">
                <option <?php if (''   == $state) { echo 'selected="selected"'; } ?> value="">All States</option>
                <option <?php if ('AL' == $state) { echo 'selected="selected"'; } ?> value="AL">AL - Alabama</option>
                <option <?php if ('CA' == $state) { echo 'selected="selected"'; } ?> value="CA">CA - California</option>
                <option <?php if ('DE' == $state) { echo 'selected="selected"'; } ?> value="DE">DE - Delaware</option>
                <option <?php if ('ID' == $state) { echo 'selected="selected"'; } ?> value="ID">ID - Idaho</option>
                <option <?php if ('IL' == $state) { echo 'selected="selected"'; } ?> value="IL">IL - Illinois</option>
                <option <?php if ('Ms' == $state) { echo 'selected="selected"'; } ?> value="MS">MS - Mississippi</option>
                <option <?php if ('MO' == $state) { echo 'selected="selected"'; } ?> value="MO">MO - Missouri</option>
                <option <?php if ('NM' == $state) { echo 'selected="selected"'; } ?> value="NM">NM - New Mexico</option>
                <option <?php if ('SC' == $state) { echo 'selected="selected"'; } ?> value="SC">SC - South Carolina</option>
                <option <?php if ('UT' == $state) { echo 'selected="selected"'; } ?> value="UT">UT - Utah</option>
                <option <?php if ('WI' == $state) { echo 'selected="selected"'; } ?> value="WI">WI - Wisconsin</option>
            </select>
            <button>Search</button><br /><br />
        </form>
        <H2>Application Results</H2>
<?php
    if (isset($_GET["application_nbr"]) && isset($display_message))
    {
        echo '        <p style="color:red;"><b>' . $display_message . '</b></p>' . PHP_EOL;
    }
    
    if ($num_apps > 0)
    {
        echo '        <p>' . $num_apps . ' application(s)</p>' . PHP_EOL;
        
        $i = 0;
        
        foreach ($app_history as $app)
        {
            $application_nbr            = $app["application_nbr"];
            $app_status                 = $app["application_status"];
            $create_dt                  = $app["create_dt"];
            $first_name                 = $app["first_name"];
            $last_name                  = $app["last_name"];
            $email_address              = $app["email_address"];
            $mobile_phone_nbr           = $app["mobile_phone_nbr"];
            $home_phone_nbr             = $app["home_phone_nbr"];
            $street_address             = $app["street_address"];
            $appt_suite                 = $app["appt_suite"];
            $city                       = $app["city"];
            $state                      = $app["state"];
            $zip_code                   = $app["zip_code"];
            $application_status         = $app["application_status"];
            $uw_decision_nbr            = $app["uw_decision_nbr"];
            $preverif_decision_nbr      = $app["preverif_decision_nbr"];
            $loan_agreement_nbr         = $app["loan_agreement_nbr"];
            $gf_lead_id                 = $app["gf_lead_id"];
            $bank_acct_nbr              = $app["bank_acct_nbr"];
            $routing_nbr                = $app["routing_nbr"];
            $approved_amt               = $app["approved_amt"];
            $counter_offer_dm_offer_amt = $app['counter_offer_dm_offer_amt'];
            $requested_amt              = $app["requested_amt"];
            $create_dt                  = $app["create_dt"];
            $orig_appid                 = $app["orig_appid"];
            $formatted_dt               = new DateTime($create_dt);
            $display_application_dt     = $formatted_dt->format('m/d/Y');
            $is_organic                 = (false !== strpos($app["decision_segment"], 'Organic') );
            $app["decision_segment"]    = ($is_organic ? 'Organic' : 'Pre Selected' );
            
            $uw_decision_details = mm_get_uw_decision_details($app['uw_decision_nbr']);
            $is_counter_offer = strtoupper($uw_decision_details['decision_data']['counter_offer_flag']) == "Y" ? true : false;
            $counter_offer_amt = $uw_decision_details['decision_data']['counter_offer_line_assignment_amt'];
            
            //Code Start - added by swapna on July 17th 2018 For story 319 -As an agent , I want to select a loan amount on the Admin so that the customer is approved for a loan based on their monthly income.
            $tot_monthly_income_amt = $app["total_monthly_income_amt"];
            $approved_apr           = $app["approved_apr"];
            $pay_freq_1             = $app["pay_frequency_1"];
            
            $approval_eligible_amt_details_obj = mm_get_customer_eligible_amounts($state, $tot_monthly_income_amt,$approved_apr,$pay_freq_1);
            
            $approval_eligible_amt_details_arr = $approval_eligible_amt_details_obj['results'];
            
            foreach($approval_eligible_amt_details_arr as $val)
            {
                $approval_eligible_amt_details[]= $val['loan_amt'];
            }
            
            $state_aprs_details_obj = mm_get_state_aprs($state);
            $state_aprs_details_arr = $state_aprs_details_obj['results'];
            
            foreach($state_aprs_details_arr as $val)
            {
                $state_aprs_details[]= $val['apr'];
            }
            
            //print_r($approval_eligible_amt_details_arr);
            //print_r($state_aprs_details);
            //Code End - added by swapna on July 17th 2018 For story 319.
            
            $app_status_table = array( '',
                                    'Draft',
                                    'Pending Customer Loan Amount',
                                    'Pending Verifications Decision',
                                    'Pending Customer Documents',
                                    'Pending Agent Review',
                                    'Pending Loan Agreement Signature',
                                    'Approved',
                                    'Declined',
                                    'Error',
                                    'Expired',
                                    'Voided',);
            
            $ftp_details  = mm_get_ftp_details();
            $server       = $ftp_details["server"];
            $ftp_username = urlencode($ftp_details["username"]);
            $ftp_password = urlencode($ftp_details["password"]);
            $port         = $ftp_details["port"];
            $account_nbr  = $app["account_nbr"];
            $section_id   = "section_" . $i++ ;
            
            $loan_agreement_nbr   = $app["loan_agreement_nbr"];
            $address_query_string = "$street_address $appt_suite $city $state $zip_code";
            $address_query_string = str_replace(" ", "+", $address_query_string);
            $txt_app_status       = ( ( $application_status > 0 ) && ( $application_status < 12 ) ? $app_status_table[ $application_status ] : 'Unexpected Application Status' );
            $html = '';
            
            if ($is_organic)
            {
                $html = '        <div style="border: 10px solid #00cc00;" >
            <div style="background-color: #00cc00; color: #ffffff; font-weight: bold; padding: 0 0 10px; text-align: center;" >
                Organic
            </div>' . PHP_EOL;
            }
            
            // reformatted the $html content while adding the COUNTER OFFER AMOUNT line, please take to production as one hunk. RCameron 9/18/2018
            $html .= '<section id="' . $section_id . '" style="padding: 8px;">';
            $html .= '<h3>Application Summary</H3><hr />';
            $html .= '<div class="Sum Label">Channel:</div>' . $app["decision_segment"] . '<br />';
            $html .= '<div class="Sum Label">Application Number:</div>' . $application_nbr . ' (<a href="add_note.php?application_nbr=' . $application_nbr . '">Add Note</a>)<br />';
            $html .= '<div class="Sum Label">Customer Name:</div>' . $first_name . ' ' . $last_name . ' <a href="update_customer_preferences.php?account_nbr=' . $account_nbr . '&application_nbr=' . $application_nbr .'"> (Update Customer Preferences)</a><br />';
            $html .= '<div class="Sum Label">Email Address:</div>' . $email_address . ' (<a href="email_customer.php?email_address=' . $email_address . '&application_nbr=' . $application_nbr . '">Email Customer</a>)<br />';
            $html .= '<div class="Sum Label">Mobile Phone:</div>' . $mobile_phone_nbr. '<br />';
            $html .= '<div class="Sum Label">Home Phone:</div>' . $home_phone_nbr . '<br />';
            $html .= '<div class="Sum Label">Address:</div>' . $street_address . ' ' . $appt_suite . '&nbsp;&nbsp;&nbsp;&nbsp;<a href="https://www.google.com/maps/place/' . $address_query_string . '" target="blank">(Google Map)</a><br />';
            $html .= '<div class="Sum Label">City State ZIP:</div>' . $city . '&nbsp;&nbsp;' . $state . '&nbsp;&nbsp;' . $zip_code .'<br />';
            $html .= '<div class="Sum Label">Application Date:</div>' . $display_application_dt . '<br />';
            $html .= '<div class="Sum Label">Application Status:</div>' . $txt_app_status . '<br />';
            $html .= '<div class="Sum Label">Approved Amount:</div>' . $approved_amt . '<br />';
            $html .= '<div class="Sum Label">Requested Amount:</div>' . $requested_amt . '<br />';
            
            if ($is_counter_offer === true)
            {
                $html .= '<div class="Sum Label">Counter Offer Amount:</div>' . $counter_offer_amt . '<br />';
            }
            
            $html .= '<div class="Sum Label">Approved APR:</div>' . $approved_apr . '<br />' . PHP_EOL;
            
            echo $html;
?>
    <script>
        function myfunction() {
            var x = document.getElementById("a2").value;
            document.getElementById("app_status_change_div").innerHTML = "";
            var container = document.getElementById("app_status_change_div");
            if (x == 1) {

            } else if (x == 2) { //Approved Pending Customer Signature
                 //Code Start - modified by swapna on July 17th 2018 For story 319 -As an agent , I want to select a loan amount on the Admin so that the customer is approved for a loan based on their monthly income.
                var label = document.createElement("label");
                var state = '<?php echo "$state";?>';
                label.innerHTML = 'Approved APR:   ';
                container.appendChild(label);
                var approved_apr_val = '<?php echo $approved_apr;?>';
                console.log("a"+approved_apr_val);
                if(state == 'IL'){
                    var aprList = document.createElement("INPUT");
                    aprList.setAttribute("type", "text");
                    aprList.setAttribute("id", 'approved_apr_id');
                    aprList.setAttribute("name", 'approved_apr');
                    aprList.setAttribute("value", approved_apr_val);
                    aprList.setAttribute("disabled", 'true');
                    container.appendChild(aprList);
                }else{
                    //aprList.value = approved_apr_val;
                    var aprList = document.createElement("select");
                    aprList.id = 'approved_apr_id';
                    aprList.name = 'approved_apr';
                    container.appendChild(aprList);
                }
                container.appendChild(document.createElement("br"));
                var label = document.createElement('label');
                label.innerHTML = "Approved Amount:  ";
                container.appendChild(label);
                var selectList = document.createElement("select");
                selectList.id = "approved_amt_id";
                selectList.name = "approved_amt";
                container.appendChild(selectList);

                var approval_eligible_amt_details_arr=new Array;
<?php
                    for($i=0;$i<count($approval_eligible_amt_details); $i++)
                            {
                        echo "approval_eligible_amt_details_arr[$i]='".$approval_eligible_amt_details[$i]."';\n";
                    }
?>
                    
                for (var i = 0; i < approval_eligible_amt_details_arr.length; i++) {
                var option = document.createElement("option");
                     var value = approval_eligible_amt_details_arr[i];
                        option.value = value;
                        option.text = value;
                        selectList.appendChild(option);
                }
                
                var state_aprs_details_arr=new Array;
<?php
                    for($i=0;$i<count($state_aprs_details); $i++)
                            {
                        echo "state_aprs_details_arr[$i]='".$state_aprs_details[$i]."';\n";
                    }
?>
                    
                if(state != 'IL'){
                    for (var i = 0; i < state_aprs_details_arr.length; i++) {
                        var approved_apr_val = '<?php echo $approved_apr;?>';
                        var option = document.createElement("option");
                             var value = state_aprs_details_arr[i];
                                option.value = value;
                                option.text = value;
                                //option.id= value;
                                if(approved_apr_val == value){
                                    console.log("true"+value);
                                   // option.selected = true;
                                }
                                aprList.appendChild(option);
                        }
                }
                var aprListCheck = document.getElementById("approved_apr_id");


            } else if (x == 3) {
                //There is no 3 status
            } else if (x == 4) {
                //Approved Pending customer Documents
                var br = document.createElement("br");
                container.appendChild(br);

                var checkbox = document.createElement('input');
                checkbox.type = "checkbox";
                checkbox.name = "email_checkbox";
                checkbox.value = "Send Email";
                checkbox.id = "email_checkbox_id";
                checkbox.checked = true;
                container.appendChild(checkbox);

                var label = document.createElement('label');
                label.innerHTML = "Send Customer Email?";
                container.appendChild(label);
                container.appendChild(br);

                var label2 = document.createElement("p");
                label2.innerHTML = "Warning:  If the customer has already accepted the loan agreement then you will need to inactivate the loan and modify the loan status to 'Pending' and the Sub Status to 'Signature' in order to prevent the loan from funding";
                container.appendChild(label2);

            } else if (x == 5) {
                //Approved Pending Agent Review
                var br = document.createElement("br");
                container.appendChild(br);
                /*
                var checkbox = document.createElement('input');
                checkbox.type = 'checkbox';
                checkbox.name = "email_checkbox";
                checkbox.value = "Send Email";
                checkbox.checked = true;
                container.appendChild(checkbox);

                var label = document.createElement('label');
                label.innerHTML = "Send Customer Email?";
                container.appendChild(label);
                */

            } else if (x == 6) {
                //Approved Pending Customer Signature
                var br = document.createElement("br");
                container.appendChild(br);

                var checkbox = document.createElement('input');
                checkbox.type = 'checkbox';
                checkbox.name = "email_checkbox";
                checkbox.value = "Send Email";
                checkbox.checked = true;
                container.appendChild(checkbox);

                var label = document.createElement('label');
                label.innerHTML = "Send Customer Email?";
                container.appendChild(label);
            } else if (x == 7) {
                //Customer is Approved
                var br = document.createElement("br");
                container.appendChild(br);

                var warning = document.createElement("p");
                warning.innerHTML = "Warning:  Only use this status if the customer has signed the loan agreement.  Login to LoanPro and make sure the loan is in Ready To Fund Status, the customer has been added to the loan, the payment profile is added to the customer and the autopays are present.  If any of these is not correct then please contact a system administrator.";
                warning.style.color = "Red";
                container.appendChild(warning);
                /*var checkbox = document.createElement('input');
                checkbox.type = 'checkbox';
                checkbox.name = 'email_checkbox';
                checkbox.value = "Send Email";
                checkbox.checked = true;
                container.appendChild(checkbox);

                var label = document.createElement('label');
                label.innerHTML = "Send Customer Email?";
                container.appendChild(label);

                var br2 = document.createElement("br");
                container.appendChild(br2);

                var checkbox2 = document.createElement('input');
                    checkbox2.type = 'checkbox';
                    checkbox2.name = 'email_checkbox';
                    checkbox2.value = "Schedule Loan Funding";
                    checkbox2.checked = true;
                    container.appendChild(checkbox2);

                var label2 = document.createElement('label');
                label2.innerHTML = "Schedule Loan Funding?";
                container.appendChild(label2);
                */


            } else if (x == 8) {
                //Customer is declined
                var label = document.createElement("label");
                label.innerHTML = "Decline Reason";
                container.appendChild(label);
                var selectList = document.createElement("select");
                selectList.id = "decline_reason_id";
                selectList.name = "decline_reason";
                container.appendChild(selectList);

                //List a set of reasons
                option = document.createElement("option");
                option.value = 52;
                option.text = "Pre-Screen Violation";
                selectList.appendChild(option);

                option = document.createElement("option");
                option.value = 1;
                option.text = "ID Fails Inspection";
                selectList.appendChild(option);

                option = document.createElement("option");
                option.value = 2;
                option.text = "Bank Statement Fails Inspection";
                selectList.appendChild(option);

                option = document.createElement("option");
                option.value = 3;
                option.text = "Customer Fails Verification Call";
                selectList.appendChild(option);

                option = document.createElement("option");
                option.value = 4;
                option.text = "Affordability - Insufficient Income";
                selectList.appendChild(option);

                option = document.createElement("option");
                option.value = 5;
                option.text = "Affordability - Too Many Outstanding Loans";
                selectList.appendChild(option);

                option = document.createElement("option");
                option.value = 110;
                option.text = "Failed OFAC Verification";
                selectList.appendChild(option);

                option = document.createElement("option");
                option.value = 109;
                option.text = "Unable to Clear FACTA Alert";
                selectList.appendChild(option);

                option = document.createElement("option");
                option.value = 53;
                option.text = "Active Military";
                selectList.appendChild(option);

                option = document.createElement("option");
                option.value = 55;
                option.text = "Not Pre-Approved";
                selectList.appendChild(option);

                option = document.createElement("option");
                option.value = 56;
                option.text = "State Not Serviced";
                selectList.appendChild(option);

                option = document.createElement("option");
                option.value = 57;
                option.text = "Not of Legal Age";
                selectList.appendChild(option);

                option = document.createElement("option");
                option.value = 58;
                option.text = "Active Loan Outstanding";
                selectList.appendChild(option);

                option = document.createElement("option");
                option.value = 59;
                option.text = "Previously Charged-Off Loan";
                selectList.appendChild(option);


                var label2 = document.createElement("p");
                label2.innerHTML = "Warning:  A NOAA will be generated and sent immediately to the customer.  Also, if there is a loan on LoanPro associated with this application it will be deleted";
                container.appendChild(label2);


            } else if (x == 9) {
                //Customer is in error status

            } else if (x == 10) {
                //Expired
                var label = document.createElement('p');
                label.innerHTML = "This will set the status to expired and schedule an email to be sent in 24 hours.  Remember to delete the loan if it's on loan pro";
                container.appendChild(label);

            } else if (x == 11) {
                //Void
                var label = document.createElement('p');
                container.appendChild(label);
            } else {
                alert("There was an unexpected status selected.  Please Notify IT.");
            }

        }
    </script>
<?php
        if ($_SESSION["permissions_manual_decisioning"] == "Y")
        {
?>
            <br />
            <span style="font-weight: bold; text-decoration: underlined; ">Modify Application Status</span>
<?php
            $html = '<table border="2" style="width:400px; border: 1px solid black;">
    <caption>Status History</caption>
        <th style="width: 1%; white-space: nowrap; padding:0px">Old Value</th>
        <th style="width: 1%; white-space: nowrap; padding:0px">New Value</th>
        <th style="width: 1%; white-space: nowrap; padding:0px">User</th>
        <th style="width: auto; white-space: nowrap; padding:0px">Date Changed</th>' . PHP_EOL;

        $conn = mm_get_db_connection();
        $sql_string = "select * from mm_column_history mch where mch.key_value = ? AND mch.column_name = 'APPLICATION_STATUS'";
        $stmt = $conn->prepare($sql_string);
        $stmt->bind_param('s', $application_nbr);
        $stmt->execute();
        $rows = $stmt->get_result();
        $num_rows = $rows->num_rows;

        foreach ($rows as $row)
        {
            $html .=  '        <tr>
            <td style="width: 1%; white-space: nowrap; padding:0px; text-align: center;">' . $row['old_value'] . '</td>
            <td style="width: 1%; white-space: nowrap; padding:0px; text-align: center;">' . $row['new_value'] . '</td>
            <td style="width: 1%; white-space: nowrap; padding:0px; text-align: center;">' . $row['user'] . '</td>
            <td style="width: 1%; white-space: nowrap; padding:0px;">' . $row['change_dt'] . '</td></tr>' . PHP_EOL;
        }

        echo $html . "</table>" . PHP_EOL;
?>
    <form method="POST" name="change_app_status" action="" accept-charset="utf-8">
        <input type="hidden" name="application_nbr" value="<?php echo $application_nbr; ?>" />
        <input type="hidden" name='logged_in_user' value="<?php echo $username; ?>" />
        <input type="hidden" name="orig_appid" value="<?php echo $orig_appid; ?>" />
        <label>New Application Status:</label>
        <select id="a2" name="new_app_status" onchange="myfunction()">
            <option value="0"></option>
            <option value="1"> 1 - Draft</option>
            <option value="2"> 2 - Approved Pending Customer Acceptance</option>
            <option value="4"> 4 - Approved Pending Customer Documents</option>
            <option value="5"> 5 - Approved Pending Agent Review</option>
            <option value="6"> 6 - Approved Pending Customer Signature</option>
            <option value="7"> 7 - Approved</option>
            <option value="8"> 8 - Declined</option>
            <option value="9"> 9 - Stuck/Error</option>
            <option value="10"> 10 - Expired</option>
            <option value="11"> 11 - Void</option>
        </select>
        <div name="app_status_change_div_name" id="app_status_change_div"></div>
        <input type="submit" value="Change Status" name="change_app_status_btn" />
    </form>
    <br />
<?php
    //Code Added by Swapna on July13th 2018 for Illinois admin gross monthly income verification
    }

    if($state == 'IL')
    {
        $sql_state_specific_data = "select * from mm_state_specific_data where application_nbr = ? ";
        $conn = mm_get_pdo_connection();
        $stmt = $conn->prepare($sql_state_specific_data);
        $parms = [$application_nbr];
        $stmt->execute($parms);
        $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
        ///print_r($results);
        $rows = sizeof($results);
        $blob = json_decode($results[0]['blob']);

        if ($rows > 0)
        {
            echo '<div class="Gross Label">Main Source - Gross Monthly Income:</div>' . $blob->gross_income_1 . '<br />';
            echo '<div class="Gross Label">Secondary Source - Gross Monthly Income:</div>' . $blob->gross_income_2 . '<br />';

            if (isset($blob->gross_income_verified) && $blob->gross_income_verified > 0 )
            {
                echo '<div class="Gross Label">Verified Gross Monthly Income:</div>' . $blob->gross_income_verified . '<br />';
            }
?>
    <form method ="POST" name="verify_gross_income" action="" accept-charset="utf-8">
        <input type="hidden" name="application_nbr" value="<?php echo "$application_nbr";?>" />
        <input type = "hidden" name='logged_in_user' value = "<?php echo "$username";?>" />
        <input type="hidden" name="orig_appid" value="<?php echo "$orig_appid";?>" />
        <label>Verify Gross Income:</label>
        <input type="text" name="verified_gross_income" id="verified_gross_income" />
        <div name="verify_income_change_div_name" id="verify_income_change_div_name"></div>
        <input type="submit" value="Verified Gross Income " />
    </form>
    <br />
<?php
            }
        }

        echo "Application PDF: ";
        //$document_path = "customer_documents/$account_nbr/$application_nbr/application_$application_nbr.pdf";
        $app_link = "view_document.php?application_nbr=$application_nbr&document_type=APP";
        //""ftp://$ftp_username:$ftp_password@$server:$port/$document_path";
        echo "<a href=\"$app_link\" target=\"_blank\">View Application</a><br />";

        if ($application_status == 8 || $application_status == 10)
        {
            // $document_path = "customer_documents/$account_nbr/$application_nbr/noaa_$application_nbr.pdf";
            //$noaa_link = "ftp://$ftp_username:$ftp_password@$server:$port/$document_path";
            $noaa_link = "view_document.php?application_nbr=$application_nbr&document_type=NOAA";
            echo "NOAA: <a href=\"$noaa_link\" target=\"_blank\">View NOAA</a><br />";
        }

        if ($loan_agreement_nbr > 0)
        {
            // $document_path = "customer_documents/$account_nbr/$application_nbr/loan_agreement_$application_nbr.pdf";
            // $noaa_link = "ftp://$ftp_username:$ftp_password@$server:$port/$document_path";
            $la_link = "view_document.php?application_nbr=$application_nbr&document_type=LA";
            echo 'Loan Agreement: <a href="' . $la_link . '" target="_blank">View Loan Agreement</a><br />' . PHP_EOL;
        }

        echo "<br />" . PHP_EOL;
        echo '<h4>Application Notes (<a href="add_note.php?application_nbr=' . $application_nbr . '">Add Note</a>)</H4>' . PHP_EOL;

        $application_notes = mm_get_db_records('mm_application_notes', 'application_nbr', $application_nbr);
        $result_count = $application_notes["result_count"];

        if ($result_count == 0)
        {
            echo "<p>No Notes To Display</p>" . PHP_EOL;
        }
        else
        {
            $notes = $application_notes["results"];
            echo "<table><th><td>Date</td><td>Category</td><td>Sub Category</td><td>User</td><td>Note Text</td></th>" . PHP_EOL;

            foreach ($notes as $note)
            {
                //echo "$create_dt $category $sub_category $txt_body $username <br />";
                echo '<tr><td></td><td>' . $note["create_dt"] . '</td><td>' . $note["category"] . '</td><td>' . $note["sub_category"] . '</td><td>' . $note["username"] . '</td><td>' . $note["txt_body"] . '</td><tr>' . PHP_EOL;
            }

            echo "</table>" . PHP_EOL;
        }

        //Output the Documents Uploaded

        $results = mm_get_application_documents($application_nbr);
        echo '            <H4>Application Documents <a href="add_document.php?application_nbr=' . $application_nbr . '">(Add Document)</a></H4>' . PHP_EOL;
        $document_return_value = $results["return_value"];

        if ($document_return_value != 0)
        {
            //There was an error getting the documents so log an error and display amessage.
            //mm_log_error('application_details.php', "There was a problem getting the list of documents. Error message is: $document_return_message");
            echo "There was a problem getting the list of documents. The error message is: $document_return_message<br />";
        }
        else
        {
            $num_documents = $results["num_documents"];
            if ($num_documents == 0)
            {
                echo "There are no documents to display.<br />";
            }
            else
            {
                $documents = $results["results"];
                $html = "<table><tr><th>Document</th><th>Uploaded By</th><th>Create Date</th></tr>" . PHP_EOL;
                
                foreach ($documents as $document)
                {
                    $document_nbr = $document["document_nbr"];
                    $path = $document["document_path"];
                    $tmp_array = explode("customer_documents/", $path);
                    $path = "customer_documents/" . $tmp_array[1];
                    $document_link = "ftp://$ftp_username:$ftp_password@$server:$port/$path";
                    $document_link = "view_document.php?document_nbr=$document_nbr";
                    $temp_array = explode('/', $path);
                    $temp_size = sizeof($temp_array);
                    $document_name = $temp_array[$temp_size - 1];
                    $path = "<a href=\"$document_link\" target=\"blank\">$document_name</a>";
                    
                    $html .= '<tr><td>' . $path . '</td><td>' . $document["username"] . '</td><td>' . $document["create_dt"] . '</td></tr>' . PHP_EOL;
                }
                
                echo $html . "</table>";
            }
        }
        
        //Output the emails that have been sent to the customer
        $html = '<h4>Customer Communications (<a href="email_customer.php?email_address=' . $email_address . '&application_nbr=' . $application_nbr . '">Email Customer</a>)</H4>' . PHP_EOL;
        
        $email_results = mm_get_email_messages($account_nbr);
        $email_return_value = $email_results["return_value"];
        $email_return_message = $email_results["return_message"];
        
        if ($email_return_value != 0)
        {
            $error_message = $email_results["return_message"];
            $html .= "<p>There was a problem getting the email messages. $error_message</p>" . PHP_EOL;
        }
        else
        {
            $num_emails = $email_results["num_emails"];
            
            if ($num_emails == 0)
            {
                $html .= "<p>There are no email communications with the customer</p>";
            }
            else
            {
                //There have been communications so build a table and output it.
                $html .= "            <table>
                    <tr><th>Status</th><th>To</th><th>From</th><th>Subject</th><th>Text</th><th>Scheduled Date</th><th>Actions</th></tr>" . PHP_EOL;
                $emails = $email_results["results"];
                
                foreach ($emails as $email)
                {
                    if ($email["status"] == "ERROR" || $email["status"] == "CANCELLED")
                    {
                        $action = '<a href="reschedule_email.php?email_message_nbr=' . $email["email_message_nbr"] . '">Re-Schedule Email</a>' . PHP_EOL;
                    }
                    elseif ($email["status"] == "READY TO SEND")
                    {
                        $action = '<a href="cancel_email.php?email_message_nbr=' . $email["email_message_nbr"] . '">Cancel Email</a>' . PHP_EOL;
                    }
                    else
                    {
                        $action = '&nbsp;';
                    }
                    
                    $html .= '                <tr><td>' . $email["status"] . '</td><td>' . $email["email_to"] . '</td><td>' . $email["email_from"] . '</td><td>' . $email["subject"] . '</td><td>' . $email["txt_body"] . '</td><td>' . $email["scheduled_dt"] . '</td><td>' . $action . '</td></tr>' . PHP_EOL;
                }
                
                $html .= "            </table>";
            }
        }
        
        echo $html;
        
        if ($uw_decision_nbr != '' && $uw_decision_nbr != 0)
        {
            $uw_decision_details = mm_get_uw_decision_details($uw_decision_nbr);
            $decision_data = $uw_decision_details["decision_data"];
            
            echo '            <H4>Underwriting Information</H4>
            <div class="Under Label">Decision Status:</div>' . $decision_data["app_status_cd"] . '<br />
            <div class="Under Label">Approved Rate:</div>' . (isset($app_data['approved_apr']) ? $app_data['approved_apr'] : '') . '%<br />
            <div class="Under Label">Credit Approved Amount:</div>' . (isset($app_data['credit_approved_amt']) ? '$' . $app_data['credit_approved_amt'] : '') . '<br />
            <div class="Under Label">Decision Description:</div>' . $decision_data["app_status_desc"] . '<br />
            <div class="Under Label">Kleverlend App Id:</div>' . $decision_data["orig_appid"] . '<br />
            <div class="Under Label">Decline Reason:</div>' . $decision_data["reject_reason_1"] . '<br />' . PHP_EOL;
        }
        
        if (($preverif_decision_nbr != '') && ($preverif_decision_nbr != 0))
        {
            // override to get the min record for the app number
            $conn = mm_get_db_connection();
            $sql_string = "SELECT * FROM mm_preverif_decision mpd WHERE mpd.application_nbr = ? ORDER BY 1 LIMIT 1";
            $stmt = $conn->prepare($sql_string);
            $stmt->bind_param('s', $application_nbr);
            $stmt->execute();
            $row = $stmt->get_result();
            $preverif_data = $row->fetch_assoc();
            
            $html = "            <h4>*** &nbsp Verifications Information &nbsp ***</h4>" . PHP_EOL;
            
            //echo "Docs Required Reason: $docs_required_reason<br />";
            
            $alert_check = array( 'facta_flag' => array('words' => 'FACTA Alert', 'bg' => '' ),
                                  'ofac_flag' => array('words' => 'OFAC Alert', 'bg' => ' background-color: rgb(255, 125, 115);' ),
                                  'mm_outsort_flag' => array('words' => 'Outsorted', 'bg' => '' ),
                                  'manual_ach_flag' => array('words' => 'Manual Check Selection', 'bg' => '' ));
            
            $alert     = '';
            $bg        = '';
            $alert_cnt = 0;
            $organic   = ($is_organic ? 'Account outsorted because it\'s an organic application! Please call customer to confirm the customer applied for the loan.\n\n\n' : '');
            
            //echo '<!-- ' . var_export($preverif_data, true) . ' -->' . PHP_EOL;
            
            foreach ($preverif_data as $key => $value)
            {
                $key = trim( $key );
                
                //echo '<!-- $key:  ' . $key . '   $app_status:  ' . $app_status . '  -->' . PHP_EOL;
                
                if ( ($app_status == 4 || $app_status == 5 || $app_status == 6)
                  && isset($alert_check[$key])
                  && ('true' == $value)
                   )
                {
                    //echo '<!-- Adding To Alert -->' . PHP_EOL;
                    
                    $alert .= '\n\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' . $alert_check[$key]['words'];
                    $bg .= $alert_check[$key]['bg'];
                    $alert_cnt ++;
                }
                
                $value = ('NULL' == gettype($value) ? 'false' : $value);
                $html .= '            <div class="VI Label">' . $key . ':</div>' . $value . '<br />' . PHP_EOL;
            }
            
            if ($alert_cnt || $is_organic)
            {
                $html .= '<script>
    let tmp = document.getElementById("' . $section_id . '");
    tmp.setAttribute("style", "' . (0 == $alert_cnt ? '' : 'border: 5px solid red;') . ' padding: 8px;' . $bg . '");
    alert("' . $organic . (0 == $alert_cnt ? '' : 'This application has ' . ($alert_cnt > 1 ? 'issues' : 'an issue' ) . ' which needs to be addressed.\n' . $alert) . '");
</script>' . PHP_EOL;
            }
            
            echo $html;
        }
        
        if ($loan_agreement_nbr != '' && $loan_agreement_nbr != 0)
        {
            $loan_agreement_details = mm_get_loan_agreement_details($loan_agreement_nbr);
            $origination_dt = $loan_agreement_details["origination_dt"];
            $effective_dt = $loan_agreement_details["effective_dt"];
            $apr = $loan_agreement_details["apr"];
            $loan_nbr = $loan_agreement_details["loan_nbr"];
            $signature_timestamp = $loan_agreement_details["signature_timestamp"];
            
            $document_path = "customer_documents/$account_nbr/$application_nbr/loan_agreement_$application_nbr.pdf";
            $signed_document_path = "customer_documents/$account_nbr/$application_nbr/loan_agreement_$application_nbr - signed.pdf";
            //$loan_agreement_pdf_link = "ftp://$ftp_username:$ftp_password@$server:$port/$document_path";
            // $signed_loan_agreement_pdf_link = "ftp://$ftp_username:$ftp_password@$server:$port/$signed_document_path";
            $signed_loan_agreement_pdf_link = "view_document.php?application_nbr=$application_nbr&document_type=LA_SIGNED";
            $loan_agreement_pdf_link = "view_document.php?application_nbr=$application_nbr&document_type=LA";
            $link_agreement_signed = '';
            $link_agreement_signed_button = '';
            $link_agreement_button = '            <a href="' . $loan_agreement_pdf_link . '" target="_blank" style="margin: 8px 16px; cursor: pointer;" >
                <button type="button" style="padding: 6px 10px; color: #0052cc; font-weight: bold; width: 250px; cursor: pointer;" >
                    View Loan Agreement
                </button>
            </a>' . PHP_EOL;
            
            if ($signature_timestamp != '')
            {
                $link_agreement_signed = '            <a href="' . $signed_loan_agreement_pdf_link . '" target="_blank">
                View Signed Loan Agreement
            </a>
            <br />' . PHP_EOL;
                $link_agreement_signed_button = '            <a href="' . $signed_loan_agreement_pdf_link . '" target="_blank" style="margin: 8px 16px; cursor: pointer;" >
                <button type="button" style="padding: 6px 10px; color: #0052cc; font-weight: bold; width: 250px; cursor: pointer;" >
                    View Signed Loan Agreement
                </button>
            </a>' . PHP_EOL;
            }
            
            $html = '            <H4>Loan Agreement Details</H4>' . PHP_EOL;
            //$html .= $link_agreement_button;
            //$html .= $link_agreement_signed_button;
            $html = '            <p />
            <a href="' . $loan_agreement_pdf_link . '" target="_blank">
                View Loan Agreement
            </a>
            <br />
' . $link_agreement_signed . '            <div class="VI Label">Origination Date:</div>' . $origination_dt . '<br />
            <div class="VI Label">Contract Date:</div>' . $effective_dt . '<br />
            <div class="VI Label">Contract APR:</div>' . $apr . '<br />
            <div class="VI Label">Loan Number:</div>' . $loan_nbr . '<br />
            <div class="VI Label">Signature Date:</div>' . $signature_timestamp . '<br />' . PHP_EOL;
        }
        
        $html .= '        </section>' . PHP_EOL; // app_block
        
        if ($is_organic)
        {
            $html .= '    </div>' . PHP_EOL;
        }
        
        $html .= '    <div style="background-color: #0066ff; height: 10px; font-size: 1px; margin: 8px 0;" >
        &nbsp;
    </div>' . PHP_EOL; // app_block
        
        echo $html;
        }
    }
    else
    {
        echo "<p>No results to display</p>" . PHP_EOL;
    }
}
else
{
    include "include/login.php";
}
?>
        <div style="height:30px;" >
            &nbsp;<!--  Give The Page Some Padding At The Bottom  -->
        </div>
    </body>
</html>
<?php
//    Removed The Following Commented Lines From Line 334
                /*if (state == 'MO' || state == 'UT' || state == 'SC' || state == 'WI' || state == 'DE' || state == 'NM') {
                    //Create and append the options
                    for (var i = 0; i < 44; i++) {
                        var option = document.createElement("option");
                        var value = 5000 - i * 100;
                        option.value = value;
                        option.text = value;
                        selectList.appendChild(option);
                    }
                    var option = document.createElement('option');
                    var value = '116.13';
                    option.text = value;
                    aprList.appendChild(option);
                    var option = document.createElement('option');
                    var value = '132.73';
                    option.text = value;
                    aprList.appendChild(option);
                    if (state != 'NM') {
                        var option = document.createElement('option');
                        var value = '213.27';
                        option.text = value;
                        aprList.appendChild(option);
                        var option = document.createElement('option');
                        var value = '266.34';
                        option.text = value;
                        aprList.appendChild(option);
                    }


                } else if (state == 'CA') {
                    var option = document.createElement("option");
                    var value = 4999;
                    option.value = value;
                    option.text = value;
                    selectList.appendChild(option);
                    var option = document.createElement("option");
                    var value = 4500;
                    option.value = value;
                    option.text = value;
                    selectList.appendChild(option);
                    var option = document.createElement("option");
                    var value = 4000;
                    option.value = value;
                    option.text = value;
                    selectList.appendChild(option);
                    var option = document.createElement("option");
                    var value = 3500;
                    option.value = value;
                    option.text = value;
                    selectList.appendChild(option);
                    var option = document.createElement("option");
                    var value = 3000;
                    option.value = value;
                    option.text = value;
                    selectList.appendChild(option);
                    var option = document.createElement("option");
                    var value = 2500;
                    option.value = value;
                    option.text = value;
                    selectList.appendChild(option);

                    var option = document.createElement('option');
                    var value = '116.13';
                    option.text = value;
                    aprList.appendChild(option);
                    var option = document.createElement('option');
                    var value = '132.73';
                    option.text = value;
                    aprList.appendChild(option);

                } else if (state == 'AL') {
                    var option = document.createElement("option");
                    var value = 5000;
                    option.value = value;
                    option.text = value;
                    selectList.appendChild(option);
                    var option = document.createElement("option");
                    var value = 4500;
                    option.value = value;
                    option.text = value;
                    selectList.appendChild(option);
                    var option = document.createElement("option");
                    var value = 4000;
                    option.value = value;
                    option.text = value;
                    selectList.appendChild(option);
                    var option = document.createElement("option");
                    var value = 3500;
                    option.value = value;
                    option.text = value;
                    selectList.appendChild(option);
                    var option = document.createElement("option");
                    var value = 3000;
                    option.value = value;
                    option.text = value;
                    selectList.appendChild(option);
                    var option = document.createElement("option");
                    var value = 2500;
                    option.value = value;
                    option.text = value;
                    selectList.appendChild(option);

                    var option = document.createElement('option');
                    var value = '116.13';
                    option.text = value;
                    aprList.appendChild(option);
                    var option = document.createElement('option');
                    var value = '132.73';
                    option.text = value;
                    aprList.appendChild(option);

                } else if (state == 'MS') {
                    var option = document.createElement("option");
                    var value = 2500;
                    option.value = value;
                    option.text = value;
                    selectList.appendChild(option);
                    var option = document.createElement("option");
                    var value = 2300;
                    option.value = value;
                    option.text = value;
                    selectList.appendChild(option);
                    var option = document.createElement("option");
                    var value = 2000;
                    option.value = value;
                    option.text = value;
                    selectList.appendChild(option);
                    var option = document.createElement("option");
                    var value = 1500;
                    option.value = value;
                    option.text = value;
                    selectList.appendChild(option);
                    var option = document.createElement("option");
                    var value = 1000;
                    option.value = value;
                    option.text = value;
                    selectList.appendChild(option);
                    var option = document.createElement("option");
                    var value = 800;
                    option.value = value;
                    option.text = value;
                    selectList.appendChild(option);

                    var option = document.createElement('option');
                    var value = '132.73';
                    option.text = value;
                    aprList.appendChild(option);
                    var option = document.createElement('option');
                    var value = '213.27';
                    option.text = value;
                    aprList.appendChild(option);
                    var option = document.createElement('option');
                    var value = '266.34';
                    option.text = value;
                    aprList.appendChild(option);
                }
                else if (state == 'IL') {
                    var option = document.createElement("option");
                    var value = 2500;
                    option.value = value;
                    option.text = value;
                    selectList.appendChild(option);
                    var option = document.createElement("option");
                    var value = 2300;
                    option.value = value;
                    option.text = value;
                    selectList.appendChild(option);
                    var option = document.createElement("option");
                    var value = 2000;
                    option.value = value;
                    option.text = value;
                    selectList.appendChild(option);
                    var option = document.createElement("option");
                    var value = 1500;
                    option.value = value;
                    option.text = value;
                    selectList.appendChild(option);
                    var option = document.createElement("option");
                    var value = 1000;
                    option.value = value;
                    option.text = value;
                    selectList.appendChild(option);
                    var option = document.createElement("option");
                    var value = 800;
                    option.value = value;
                    option.text = value;
                    selectList.appendChild(option);

                    var option = document.createElement('option');
                    var value = '96.73';
                    option.text = value;
                    aprList.appendChild(option);
                    // var option = document.createElement('option');
                    // var value = '213.27';
                    // option.text = value;
                    // aprList.appendChild(option);
                    // var option = document.createElement('option');
                    // var value = '266.34';
                    option.text = value;
                    aprList.appendChild(option);
                }*/
