<html>
<head><link rel="stylesheet" href="include/admin.css"></head>
<body>
<?php
include "include/header.php";
$num_apps = 0;
$app_history = array(array());
$create_dt = '';

if(isset($_SESSION["username"])){
	$username = $_SESSION["username"];
	if($_SESSION["permissions_user_mgmt"] != "Y"){
		//The logged in user doesn't have permission to add users
		$display_message = "You do not have permission to view this page.  Please contact a system administrator if you believe this is incorrect.";
	}else if(isset($_POST["password2"])){
		//The add user form must have been submitted
		$new_user_data = array();
		$add_user_results = mm_add_admin_user($_POST);
		$display_message = $add_user_results["return_message"];
			
	}



?>

<h2>Add New User</H2>
<form method="Post" action="add_admin_user.php">
<?php if(isset($display_message)){echo "<p style=\"color:red\">$display_message</p>";}
	if($_SESSION["permissions_user_mgmt"] == "Y"){ ?>
<label>Username:</label><input type="text" name="username"></input><br>
<label>Display Name:</label><input type="text" name="display_name"</input><br>
<label>Password:</label><input type="password" name="password"></input><br>
<label>Re-Type Password:</label><input type="password" name="password2"></input><br>
<label>Allow Manual Decisioning?  </label><input type="checkbox" name="permissions_manual_decisioning" value="Y" style="zoom:1.5"><br>
<label>Allow User Management?  </label><input type="checkbox" name="permissions_user_mgmt" value="Y" style="zoom:1.5"></input><br>
<label>Allow ACH Management?</label><input type="checkbox" name="permissions_ach_mgmt" style="zoom:1.5"></input><br>
<input type="Submit" value="Add User"/><br><br>
</form>


<?php
}//display of the form
}else{
include "include/login.php";
}
?>


</body>
</html>
