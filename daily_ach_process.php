<html>
<head><link rel="stylesheet" href="include/admin.css"></head>
<body>
<?php

//Check to see if the customer is logged in

require "include/ach_middleware.php";
ini_set('display_errors', 1);

//Get the funding transactions
$ach_funding_arr = mm_process_ach_funding();
$ach_funding_return_value = $ach_funding_arr["return_value"];
$ach_funding_return_message = $ach_funding_arr["return_message"];
$ach_funding_num_errors = $ach_funding_arr["num_errors"];
$ach_funding_num_warnings = $ach_funding_arr["num_warnings"];
$ach_funding_num_detail_records = $ach_funding_arr["num_funding_records"];
$ach_funding_ach_file_nbr = $ach_funding_arr["ach_file_nbr"];
$ach_funding_ach_file_name = $ach_funding_arr["ach_file_name"];
$ach_funding_error_array = $ach_funding_arr["error_array"];
$ach_funding_warning_array = $ach_funding_arr["warning_array"];
echo "********** Gathering the Funidng Records ****************<br><br>";
echo "Funding Record Return Value: $ach_funding_return_value<br>";
if($ach_funding_return_value !=0){
	echo "Funding Record Return Message: $ach_funding_return_message<br>";
}
echo "ACH File Name: $ach_funding_ach_file_name<br>";
echo "ACH File Number: $ach_funding_ach_file_nbr<br>";
echo "Num Succesful ACH Records: $ach_funding_num_detail_records<br>";
echo "Num Errors: $ach_funding_num_errors<br>";
if($ach_funding_num_errors != 0){
	$error_array = $ach_funding_error_array;
	foreach($error_array as $key=>$value){
		echo "ERROR:	$value<br>";
	}
}
echo "Num Warnings: $ach_funding_num_warnings<br>";
if($ach_funding_num_warnings != 0){
	foreach($ach_funding_warning_array as $key=>$value){
		echo "WARNING:		$value<br>";
	}
}

//Get the payment transactions
$ach_payment_arr = mm_process_ach_payments();
$ach_payment_return_value = $ach_payment_arr["return_value"];
$ach_payment_return_message = $ach_payment_arr["return_message"];
$ach_payment_num_payments = $ach_payment_arr["num_payments"];
$ach_payment_num_errors = $ach_payment_arr["num_errors"];
$ach_payment_num_warnings = $ach_payment_arr["num_warnings"];
$ach_payment_error_array = $ach_payment_arr["error_array"];
$ach_payment_warning_array = $ach_payment_arr["warning_array"];


echo "<br><br>**********Gathering ACH PAYMENTS***********<br><br>";
echo "Payment Return Value: $ach_payment_return_value<br>";
if($ach_payment_return_value != 0){
	echo "Payment Return Message: $ach_payment_return_message<br>";
}
echo "Num Successful Payments Selected: $ach_payment_num_payments<br>";
echo "Num Errors: $ach_payment_num_errors<br>";
if($ach_payment_num_errors != 0){
	foreach($ach_payment_error_array as $key=>$value){
		echo "		$value<br>";
	}
}
echo "Num Warnings: $ach_payment_num_warnings<br>";
if($ach_payment_num_warnings != 0){
	foreach($ach_payment_warning_array as $key=>$value){
		echo "		$value<br>";
	}
}

//Build the Nacha files
echo "<br><br>*********Building Nacha File Records **************** <br><BR>";
$ach_nacha_build_results = mm_ach_build_nacha_recs_wrapper();
if($ach_nacha_build_results["return_value"] != 0){
	//There was a problem building the nacha records
	$ach_nacha_build_error_message = $ach_nacha_build_results["return_err_message"];
	$ach_nacha_build_error_status = $ach_nacha_build_results["return_err_status"];
	echo "There was a problem building the nacha file records.  The error status is $ach_nacha_build_error_status and the message is $ach_nacha_build_error_message";
}else{
	//LEFT OFF HERE - NEEED TO HANDLE THE BUILDING OF THE FILES
                //There wasn't a problem building the nacha records so build the nacha files(s)
                $ach_nacha_file_build_results = mm_ach_create_nacha_file_wrapper();
                if($ach_nacha_file_build_results["return_value"] != 0){
                        //Insert error handling in case the file(s) couldn't be built
                        $return_value = 1;
			$return_err_message = $ach_nacha_build_results["return_err_message"];
			$return_err_status = $ach_nacha_build_results["return_err_status"];
			$return_message = "Failed generating the NACHA FILES. Erorr Message: $return_err_message and Status: $return_err_status";
                        $return_err_status = $ach_nacha_build_results["return_err_status"];
                        $return_err_message = $ach_nacha_build_results["return_err_message"];
                }else{
                        //The file(s) were able to be build so place them in the archive directory, FTP them and then remove them from the temporary location?

                }
        }

//FTP the files to Greenbank

//Construct an email to send regarding the process

?>

</body>

</html>
