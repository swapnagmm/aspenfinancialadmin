<html>
<head><title>Add A Note</title><link rel="stylesheet" href="include/admin.css"></head>
<body>
<?php

include "include/header.php";
$display_message = '';

//Initialize Variables
$txt_body = '';

if(isset($_SESSION["username"])){
	$username = $_SESSION["username"];
	if(isset($_GET["application_nbr"])){
        	$application_nbr = $_GET["application_nbr"];
	}else{
        	$application_nbr = '';
	}

	if(isset($_POST["txt_body"])){
                //Comment here to validate the form data
                //The customer submitted the form so add the note
        $application_nbr = $_POST["application_nbr"];
		$note_data["category"] = $_POST["category"];
		$note_data["sub_category"] = $_POST["sub_category"];
		$note_data["txt_body"] = $_POST["txt_body"];
		$note_data["username"] = $_SESSION["username"];
		$note_data["application_nbr"] = $_POST["application_nbr"];
		$tmp_dt = new DateTime();
		$create_dt = $tmp_dt->format("Y-m-d H:i:s");
		$note_data["create_dt"] = $create_dt;
		$note_json = json_encode($note_data);
		$note_results = mm_add_application_note($note_json);
		if($note_results["return_value"] != 0 || $note_results["note_nbr"] == 0){
			//The note addition failed so log an error
			$return_message = $note_results["return_message"];
   			$display_message = "There was a problem adding the note at this time.  Please try again. $return_message";
		}else{
		        $display_message = "The note was successfully added.";
		}
	}
?>

<h2>Add Customer Note</H2>

<?php
        if($display_message != ''){
            echo "<font color=\"red\"><b>$display_message</b></font><br><br>"   ;
        }
        if($application_nbr != ''){
            
        ?>
            <form method="Post" action="">
            
            
            <label>Application Number: <?php echo "$application_nbr";?><br></label><input name="application_nbr" type="hidden" value=<?php echo "$application_nbr";?>></input>
            <label>Category</label><select name="category">
		<Option value="Customer Communication">Customer Communication</option>
		<Option value="Verifications Processing">Verifications Processing</option>
	    </select><br>
            <label>Sub-Category</label><select name="sub_category">
	    <option value="">TBD</option>
	    </select><br>
            <label>Note:</label><textarea name="txt_body" rows="15" cols="200"></textarea><br>
            <input type="Submit" value="Add Note"><br><br>
            <a href="application_details.php?application_nbr=<?php echo "$application_nbr";?>">Return To Application Summary</a>
            </form>
        <?php
        	}else{
                	echo "You must first select an application to add a note.  Click <a href=\"application_details.php\">here</a> to find an account.<br>";
        	}
?>

<?php
        }else{
                include "include/login.php";
        }
?>


</body>
</html>
