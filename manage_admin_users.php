<html>
<head><link rel="stylesheet" href="include/admin.css"></head>
<body>
<?php
include "include/header.php";
ini_set('display_errors',1);

if(isset($_SESSION["username"])){
	$username = $_SESSION["username"];
	if($_SESSION["permissions_user_mgmt"] != "Y"){
		//The logged in user doesn't have permission to add users
		$display_message = "You do not have permission to view this page.  Please contact a system administrator if you believe this is incorrect.";
	}

?>

<h2>Manage Admin Users</H2>
<?php if(isset($display_message)){echo "<p style=\"color:red\">$display_message</p>";}
	if($_SESSION["permissions_user_mgmt"] == "Y"){ 
		//User has permission to view this page so get the list of users and display a table
		$user_results = mm_get_admin_users();
		$user_array = $user_results["results"];
		$num_users = $user_results["num_users"];

		if($num_users ==0){
			//There are no users so set the display message
			$display_message = "There are no users to manage";
		}else{
			echo "<table><tr><th>Username</th><th>Display Name</th><th>Action</th></tr>\n";
			foreach($user_array as $user){
				$username = $user["username"];
				$display_name = $user["display_name"];
				$active_ind = $user["active_ind"];
				echo "<tr><td>$username</td><td>$display_name</td><td><a href=\"edit_admin_user.php?username=$username\">Edit User</a></td></tr>";
			}
			echo "</table>";

		}
	}
?>


<?php
}else{
include "include/login.php";
}
?>


</body>
</html>
